import React from "react";
import Userprofile from "../src/containers/userprofile/Userprofile";
import IssueTable from "../src/components/eventTable/EventTable";
import PositionedSnackbar from "../src/components/Snackbar";

describe('Testing Userprofile container', () => {
    it('renders Snackbar', () => {
        const wrapper = shallow(
            <Userprofile>
                <PositionedSnackbar />
            </Userprofile>
        );
        expect(wrapper.exists()).toBe(true);
    });

    it('takes a snapshot of what the component should look like', () => {
        const wrapper = shallow(
            <Userprofile />
        );
        expect(wrapper).toMatchSnapshot();
    });

    it('renders IssueTable', () => {
        const wrapper = shallow((
            <Userprofile>
                <IssueTable />
            </Userprofile>
        ));
        expect(wrapper.contains(<IssueTable />)).toEqual(true);
    });

    it('has a matching element and is false if it does not match', () => {
        const wrapper = shallow(
            <Userprofile>
                <div className='classes.outerWrapper'>
                    <div className='classes.content'/>
                </div>
            </Userprofile>
        );
        expect(wrapper.containsMatchingElement(<div className='classes.content'/>)).toEqual(true);
        expect(wrapper.containsMatchingElement(<div className='classes.grontent'/>)).toEqual(false);
    });
});
