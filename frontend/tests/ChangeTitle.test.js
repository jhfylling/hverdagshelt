import React from "react";
import ChangeTitle from "../src/containers/issue/components/ChangeTitle";

describe('Testing changeTitle component', () => {

    it('renders correctly', () => {
        const wrapper = shallow(
            <ChangeTitle />
        );
        expect(wrapper.exists()).toBe(true);
    });

    it('takes a snapshot of what the component should look like', () => {
        const wrapper = shallow(
            <ChangeTitle title="title" changeTitle={() =>{}}/>
        );
        expect(wrapper).toMatchSnapshot();
    });


    it('testing changetitle function', () => {
        function changetitle(){
            return 'newtitle';
        }
        const wrapper = shallow(
            <ChangeTitle title="title" changeTitle={changetitle}/>
        );
        const instance = wrapper.instance();
        expect(instance.props.changeTitle()).toEqual('newtitle');
    });

});