import React from "react";
import RegisterNewCompany from "../src/containers/admin/municipal/components/RegisterNewCompany";

describe('testing RegisterNewCompany', () => {

    it('renders correctly', () => {
        const wrapper = shallow(
            <RegisterNewCompany />
        );
        expect(wrapper.exists()).toBe(true);
    });

    it('takes a snapshot of what the component should look like', () => {
        const wrapper = render(
            <RegisterNewCompany />
        );
        expect(wrapper).toMatchSnapshot();
    });

    it('RegisterNewCompany, find text in h5 component', () => {
        const wrapper = mount(
            <RegisterNewCompany />
        );
        const amount = wrapper.find("h5").containsMatchingElement(3);
        const text2 = wrapper.find("Button").text();
        expect(amount).toEqual(false);
        expect(text2).toEqual("Opprett bedrift");
    });
});