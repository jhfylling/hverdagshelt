import React from 'react';
import CompanyRow from "../src/components/companyTable/CompanyRow";

describe('Testing CompanyRow component', () => {

    it('renders correctly', () => {
        const wrapper = shallow(<CompanyRow/>);
        expect(wrapper.exists()).toBe(true)
    });

    it('takes a snapshot of what the component should look like', () => {
        const wrapper = render(
            <CompanyRow company={{name: 'testname',description: 'testDescription', email: 'test@mail.com'}} />
        );
        expect(wrapper).toMatchSnapshot();

    });

    it('component contains elements', () => {
        const wrapper = shallow((
            <CompanyRow company={{name: 'testname',description: 'testDescription', email: 'test@mail.com'}}>
                <div className="rowWrapper"/>
                <div className="name"><b>testname</b></div>
                <div className="description"><b>testDescription</b></div>
                <div className="email"><b>test@mail.com</b></div>
            </CompanyRow>
        ));
        expect(wrapper.containsMatchingElement(<div className="rowWrapper"/>)).toEqual(true);
        expect(wrapper.containsMatchingElement(<div className="name"><b>testname</b></div>)).toEqual(true);
        expect(wrapper.containsMatchingElement(<div className="description"><b>testDescription</b></div>)).toEqual(true);
        expect(wrapper.containsMatchingElement(<div className="email"><b>test@mail.com</b></div>)).toEqual(true);
    });

    it('allows us to set props', () => {
        const company = {
            name: "Coca-Cola",
            description: "soda",
            email: "cc@mail.com"
        };
        const wrapper = mount(
            <CompanyRow company={{name: 'soda',description: 'poppin', email: 'soda@mail.com'}}/>
        );
        wrapper.setProps({company: company});
        expect(wrapper.props().company).toEqual(company);
    });

});