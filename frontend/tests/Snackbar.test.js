import React from 'react';
import Snackbar from "../src/components/Snackbar";
import PositionedSnackbar from "../src/components/Snackbar";


describe('Testing snackbar component', () => {
    it('snackbar renders correctly', () => {
        const wrapper = shallow(
            <Snackbar/>
        );
        expect(wrapper.exists()).toBe(true);
    });

    it('takes a snapshot of what the component should look like', () => {
        const wrapper = render(
            <Snackbar />
        );
        expect(wrapper).toMatchSnapshot();
    });
});


describe('Testing positionedSnackbar', () => {
    it('positionedSnackbar renders correctly', () => {
        const wrapper = shallow(
            <PositionedSnackbar />
        );
        expect(wrapper.exists()).toBe(true);
    });

    it('takes a snapshot of what the component should look like', () => {
        const wrapper = render(
            <PositionedSnackbar />
        );
        expect(wrapper).toMatchSnapshot();
    });
});


describe('Testing handleSnackbarOpen', () => {
    it('handleSnackbarOpen renders correctly', () => {
        const wrapper = shallow(
            <PositionedSnackbar />
        );
        expect(wrapper.exists()).toBe(true);
    });


    it('takes a snapshot of what the component should look like', () => {
        const wrapper = render(
            <handleSnackbarOpen message="fullført" variant="success"/>
        );
        expect(wrapper).toMatchSnapshot();
    });

    it('setProps handleSnackbarOpen', () => {
        const wrapper = mount(
            <handleSnackbarOpen message="fullført" variant="success"/>
        );
        wrapper.setProps({message:'error',variant: 'error'});
        expect(wrapper.props().message).toEqual('error');
        expect(wrapper.props().variant).toEqual('error')
    });
});