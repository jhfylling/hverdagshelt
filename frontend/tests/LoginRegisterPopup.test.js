import React from "react";
import LoginPopup from "../src/components/LoginPopup";
import RegisterPopup from "../src/components/RegisterPopup";

describe('Testing Login popup', () => {
    it('it should render without throwing an error', () => {
        const wrapper = shallow(
            <LoginPopup />
        );
        expect(wrapper.exists()).toBe(true);
    });

    it('takes a snapshot of what the component should look like', () => {
        const wrapper = render(
            <LoginPopup buttonText="Logg inn" divStyle="floatRight" buttonStyle="loginButton" />
        );
        expect(wrapper).toMatchSnapshot();
    });

    it('has props', () => {
        const wrapper = shallow(
            <LoginPopup buttonText="Logg inn" divStyle="floatRight" buttonStyle="loginButton" />
        );
        wrapper.setProps({buttonText: 'login',divStyle:'floatLeft',buttonStyle: 'logoutButton'});
        expect(wrapper.props().buttonText).toEqual('login');
        expect(wrapper.props().divStyle).toEqual('floatLeft');
        expect(wrapper.props().buttonStyle).toEqual('logoutButton');
    });
});

describe('Testing RegisterPopup', () => {
    it('should render handleSnackbarOpen', () => {
        const wrapper = shallow(
            <RegisterPopup>
                <handleSnackbarOpen message="test" variant="success"/>
            </RegisterPopup>
        );
        expect(wrapper.contains(<handleSnackbarOpen message="test" variant="success"/>)).toEqual(true);
    });

    it('has props', () => {
        const wrapper = shallow(
            <RegisterPopup>
                <handleSnackbarOpen message="test" variant="success"/>
            </RegisterPopup>
        );
        wrapper.setProps({message:'error',variant:'error'});
        expect(wrapper.props().message).toEqual('error');
        expect(wrapper.props().variant).toEqual('error');
    });
});