//React
import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import PropTypes from 'prop-types';

//React-cookie
import { withCookies } from 'react-cookie';

//MaterialUI
import { withStyles } from '@material-ui/core/styles';

//Own
import Home from './containers/home/Home';
import CreateIssue from './containers/createissue/CreateIssue';
import Navbar from './components/Navbar';
import Footer from './components/Footer';
import Issue from './containers/issue/Issue';
import GreetingPanel from './components/GreetingPanel';
import ClientValidator from './components/ClientValidator';
import MunicipalAdmin from './containers/admin/municipal/MunicipalAdmin';
import AdministerIssuesBusiness from './containers/admin/company/CompanyAdmin';
import Userprofile from "./containers/userprofile/Userprofile";
import {siteBackground} from './components/Colors'
import ResetPassword from './components/ResetPassword';
import Statistics from './containers/statistics/Statistics';


import './printStyle.css';

//Development. Remove this later
import ImageComponent from './containers/createissue/components/ImageComponent';


let createHistory = require('history').createBrowserHistory;
export const history = createHistory();

let styles = theme => ({
  mainWrapper: {
    display: "grid",
    gridTemplateColumns: "1fr 10fr 1fr",
    gridTemplateAreas:
      "'me me me'" +
      "'header header header '" +
      "'. content .'" +
      "'footer footer footer'",
    backgroundColor: siteBackground,
    [theme.breakpoints.down("sm")]: {
      gridTemplateColumns: "1fr 20fr 1fr",
      gridTemplateAreas:
        "'me me me'" +
        "'header header header '" +
        "'content content content'" +
        "'footer footer footer'"
	},
	'&:print': {
		width: '100vw',
		height: '1754px',
	}
  },
  header: {
    gridArea: "header",
	width: "100%",
	'&:print': {
		display: 'none',
	}
  },
  content: {
    gridArea: "content",
    backgroundColor: siteBackground
  },
  footer: {
	gridArea: "footer",
	'&:print': {
		display: 'none',
	}
  },
  me: {
	gridArea: "me",
	'&:print': {
		display: 'none',
	},
  }
});

class App extends Component {
	render() {
		const { classes } = this.props;
		return (
				<div className={classes.mainWrapper}>
					<div className={classes.me}>
						<GreetingPanel cookies={this.props.cookies}/>
					</div>
					<div className={classes.header}>
						<Navbar cookies={this.props.cookies}/>
					</div>
					<div className={classes.content}>
						<Switch>
							<Route exact path="/" render={() => <Home cookies={this.props.cookies} />} />
							<Route
								exact
								path="/nysak"
								render={() => (
									<ClientValidator
										cookies={this.props.cookies}
										invalidText="Du må være logget inn for å registrere saker"
										category_id={0}
									>
										<CreateIssue cookies={this.props.cookies} />
									</ClientValidator>
								)}
							/>
							<Route exact path="/saker/:id" render={() => <Issue cookies={this.props.cookies} />} />
							<Route
								exact
								path="/kommunaladmin/saker"
								render={() => (
									<ClientValidator
										cookies={this.props.cookies}
										invalidText="Du må være logget inn med kommunal ansatt bruker"
										category_id={3}
									>
										<MunicipalAdmin cookies={this.props.cookies} />
									</ClientValidator>
								)}
							/>
							<Route
								exact
								path="/kommunaladmin/saker/:id"
								render={() => (
									<ClientValidator
										cookies={this.props.cookies}
										invalidText="Du må være logget inn med kommunal ansatt bruker"
										category_id={3}
									>
										<Issue cookies={this.props.cookies} />
									</ClientValidator>
								)}
							/>
							<Route
								exact
								path="/bedrift/saker"
								render={() => (
									<ClientValidator
										cookies={this.props.cookies}
										invalidText="Du må være logget inn med bedriftbruker"
										category_id={4}
									>
										<AdministerIssuesBusiness cookies={this.props.cookies} />
									</ClientValidator>
								)}
							/>
							<Route
								exact
								path="/bedrift/saker/:id"
								render={() => (
									<ClientValidator
										cookies={this.props.cookies}
										invalidText="Du må være logget inn med bedriftbruker"
										category_id={4}
									>
										<Issue cookies={this.props.cookies} />
									</ClientValidator>
								)}
							/>
							<Route
								exact
								path="/brukerprofil"
								render={() => (
									<ClientValidator
										cookies={this.props.cookies}
										invalidText="Du må være logget inn for å se din brukerprofil"
										category_id={0}
									>
										<Userprofile cookies={this.props.cookies} />
									</ClientValidator>
								)}
							/>

							<Route
								exact
								path="/nyttpassord/:token"
								render={() => <ResetPassword cookies={this.props.cookies} />}
							/>

							<Route
								exact
								path="/administrator"
								render={() => (
									<ClientValidator
										cookies={this.props.cookies}
										invalidText="Du må være logget inn med administratorbruker"
										category_id={2}
									>
										<MunicipalAdmin cookies={this.props.cookies} />
									</ClientValidator>
								)} />

							<Route exact path="/statistics" render={() => (<Statistics cookies={this.props.cookies} />)} />

							{/* Development Remove routes below this */}
							<Route exact path="/test" component={ImageComponent} />
						</Switch>
					</div>
					<div className={classes.footer}>
						<Footer cookies={this.props.cookies}/>
					</div>
				</div>
			
		);
	}
}

App.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withCookies(withStyles(styles)(App));
