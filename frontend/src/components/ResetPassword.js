//React
import { Component } from 'react';
import React from 'react';
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';

//Own
import { reportButtonColor, white} from './Colors';
import {userService} from "../serviceObjects/userService";
//import withStyles from "@material-ui/core/es/styles/withStyles";
import { withStyles } from '@material-ui/core/styles';
import Button from "@material-ui/core/Button/Button";
import {withRouter} from "react-router-dom";
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import PositionedSnackbar, {handleSnackbarOpen} from "./Snackbar";




const styles = theme => ({
    pwField:{
        marginTop: '20px',
    },
    loginForm:{
        fontSize: '20px',
    },
    navLink: {
        backgroundColor: reportButtonColor,
        textAlign: 'center',
        color: 'white',
        height: '50px',
        padding: '10px',
        display: 'inline-block',
        '&:hover': {
            backgroundColor: 'white',
            color: reportButtonColor,
            textDecoration: 'none'
        }
    },
    loginButton: {
        color: white,
        outline: 'none',
        '&:hover': {
            backgroundColor: 'white',
            color: reportButtonColor,
            textDecoration: 'none'
        },
        '&:active': {
            outline: 'none',
        },
        '&:focus': {
            outline: 'none',
        },
    },
});



class ResetPassword extends Component {

    constructor(props){
        super(props);
        this.state = {
            open: false,
            pw: '',
            pwRepeat: ''
        }

    }

    componentDidMount(){
        this.setState({open: true});
        // custom rule will have name 'isPasswordMatch'
        ValidatorForm.addValidationRule('isPasswordMatch', (value) => {
            if (value !== this.state.pw) {
                return false;
            }
            return true;
        });
    }

    render() {
        const { classes } = this.props;
        return (
            <div className={this.props.divStyle}>
                    <Dialog
                        disableBackdropClick={true}
                        id="resetPasswordPopup"
                        ref="resetPasswordPopup"
                        open={this.state.open}
                        onClose={this.handleClose.bind(this)}
                        aria-labelledby="form-dialog-title"
                    >
                        <DialogTitle id="form-dialog-title">Endre passord</DialogTitle>
                        <DialogContent>
                            <DialogContentText>
                            </DialogContentText>
                            <ValidatorForm
                                id="resetPasswordForm"
                                ref="form"
                                onSubmit={this.resetPassword.bind(this)}
                                onError={errors => console.log(errors)}
                            >
                                <TextValidator
                                    autoFocus
                                    type="password"
                                    label="Passord"
                                    onChange={this.handleChange.bind(this)}
                                    name="pw"
                                    value={this.state.pw}
                                    validators={['required']}
                                    errorMessages={['Fyll inn passord']}
                                    fullWidth
                                />
                                <TextValidator
                                    className={classes.pwField}
                                    type="password"
                                    label="Passord"
                                    onChange={this.handleChange.bind(this)}
                                    value={this.state.pwRepeat}
                                    name="pwRepeat"
                                    validators={['isPasswordMatch', 'required']}
                                    errorMessages={['Passordene er ikke like', 'Gjenta passord']}
                                    fullWidth
                                />
                            </ValidatorForm>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClose.bind(this)} color="primary">
                                Avbryt
                            </Button>
                            <Button type="submit" form="resetPasswordForm" color="primary">
                                Endre passord
                            </Button>
                        </DialogActions>
                    </Dialog>
                <PositionedSnackbar/>
            </div>
        );
    }

    handleChange = (event) => {
        if(event.target.name === 'pw'){
            const pw = event.target.value;
            this.setState({pw: pw });
        }
        if(event.target.name === 'pwRepeat'){
            const pwRepeat = event.target.value;
            this.setState({pwRepeat: pwRepeat });
        }
    };

    resetPassword(){
        userService.resetPassword(this.props.match.params.token, this.state.pw)
            .then(response => {
                handleSnackbarOpen({message: 'Endret passord.', variant: 'success'});
                this.handleClose();

            })
            .catch(error => {
                handleSnackbarOpen({message: 'Klarte ikke endre passord. Dette skyldes trolig at du må ha ny link.', variant: 'error'});
            });
    }

    handleClose = () =>{
        this.setState({open: false});
    }
}


export default withRouter(withStyles(styles)(ResetPassword));