//#963747 - deilig rødfarge
//rgb(77, 105, 128) gammel grå 

//Confirmer
let backdropColor = "rgba(0, 0, 0, 0.5)";
let invisible = "rgba(0, 0, 0, 0)";
let whiteInvisible = "rgba(255, 255, 255, 0)";
let white = "rgba(255, 255, 255, 1)";

//Color profile

/**
 * TODO: change variable names
 */
let color1 = "#1c1d21";
let color2 = "#31353d";
let color3 = "#445878";
let color4 = "#92cdcf";
let color5 = "#eeeff7";

let contentColor = "#f9fbff";

let siteBackground = '#f7f7f7';
//Snackbar
let snackSuccess = "#349c51";
let snackError = "#d82c33";

let logoColor = "rgb(255,255,255)"
let reportButtonColor = "#31353d";

export {reportButtonColor, logoColor, backdropColor, contentColor, invisible, whiteInvisible, white, siteBackground, snackSuccess, snackError, color1, color2, color3, color4, color5};

