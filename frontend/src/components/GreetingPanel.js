//React
import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import Scroll from 'react-scroll';

//MaterialUI
import { withStyles } from '@material-ui/core/styles';

//Own
import { reportButtonColor } from './Colors';



const styles = theme => ({
	mainWrapper: {
		display: 'grid',
		backgroundImage:'url(https://i.imgur.com/LSiClU4.jpg)',
		backgroundSize: 'cover',
		height: '100vh',
		gridTemplateColumns: '1fr 1fr 1fr',
		gridTemplateAreas: 
		"'. logo .'" + 
		"'. register .'" + 
		"'. scroll . '"
	},
	logo: {
		gridArea: 'logo',
		marginTop: '10vh',
	},
	logoPicture:{
		maxHeight: '10vw',
		display: 'block',
		margin: 'auto',
		[theme.breakpoints.down('sm')]: {
			maxHeight: '15vw'
		},
		[theme.breakpoints.down('xs')]: {
			maxHeight: '20vw'
		}
	},
	register: {
		gridArea: 'register',
		textAlign: 'center'
	},
	registerButton: {
		paddingTop: '70px',
		paddingBottom: '70px',
		width: '400px',
		textAlign: 'center',
		backgroundColor: reportButtonColor,
		color: 'white',
		display: 'inline-block',
		borderRadius: '6px',
		border: '3px solid transparent',
		'&:hover': {
			backgroundColor: 'white',
			color: reportButtonColor,
			border: '3px solid ' + reportButtonColor
		},
		[theme.breakpoints.down('sm')]: {
            width: '80vw',
        },
		boxShadow: '11px 13px 23px 0px rgba(0,0,0,0.35)',
	},
	scroll: {
		gridArea: 'scroll',
		position: 'relative',
		align: 'center'
	},
	scrollButton: {
		position: 'absolute',
		marginLeft: '45%',
		borderRadius: '200px',
		bottom: '40px',
		backgroundColor: 'transparent',
		borderColor: 'white',
		color: 'white',
		padding: '8px',
		fontSize: '16px',
		cursor: 'pointer',
		'&:hover': {
			opacity: '0.5'
		},
		[theme.breakpoints.down('sm')]: {
            marginLeft: '40%'
        },
		boxShadow: '11px 13px 23px 0px rgba(0,0,0,0.35)',
	}

});

class MeetingPanel extends Component {
	scrollDown() {
		var scroll = Scroll.animateScroll;
		scroll.scrollTo(window.innerHeight);
	}

	render() {
		const { classes } = this.props;
        
		return (
			<div>
				<div className={classes.mainWrapper}>
					<div className={classes.logo}>
						<img className={classes.logoPicture} src={require('../img/logo_white.png')} alt="Logo"/>
						<h3 style={{color:'white', textAlign:'center', fontWeight: '100'}}><i>Rapporter en sak for din kommune i dag!</i></h3>
					</div>
					<div className={classes.register}>
						<NavLink className={classes.registerButton} to="../nysak" onClick={this.scrollDown}>
							<h1>Rapporter sak </h1>
						</NavLink>
					</div>
					<div className={classes.scroll}>
						<button onClick={this.scrollDown} className={classes.scrollButton}>
							<i className="fa fa-arrow-down fa-2x" />
						</button>
					</div>
				</div>
			</div>
		);
	}
}
export default withStyles(styles)(MeetingPanel);
