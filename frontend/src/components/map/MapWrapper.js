//React
import React, { Component } from 'react';

//Own
import {issueService} from "../../serviceObjects/IssueService";
import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';
import Geocode from "react-geocode";


class MapWrapper extends Component{
    constructor(props){
        super(props);
        this.state = {
            issues: [],
            initialCenter: {},
            mapStyle: {},
            infoWindowId: undefined,
            infoWindow: {
                position: {lat: 0, lng: 0},
                visible: false,
                title: '',
                itemId: -1,
                address: ''
            },
        };
        this.loadInfowindow = this.loadInfowindow.bind(this);
    }

    componentDidUpdate(prevProps){
        Geocode.setApiKey('AIzaSyATAz6K9aKSkbyQMLbivjIrp5KoPJ8_rX4');
        if(this.props.mapStyle !== prevProps.mapStyle){
            this.setState({mapStyle: this.props.mapStyle});
        }


        if(this.props.municipality_name !== prevProps.municipality_name && this.props.municipality_name !== undefined){
            Geocode.fromAddress(this.props.municipality_name).then(
                response => {
                    const {lat, lng} = response.results[0].geometry.location;
                    this.setState({initialCenter: {lat: lat, lng: lng}});
                },
                error => {
                    console.log(error);
                });
        }
        if(this.props.infoWindowId !== prevProps.infoWindowId){
            this.loadInfowindow(this.props.infoWindowId);
        }

        if(this.props.municipality_id !== prevProps.municipality_id){
            issueService.getIssues(this.props.municipality_id,[2,3])
                .then(issues => this.setState({issues: issues}))
                .catch(error => console.log('Error: ' + error));
        }

    }


    componentDidMount(){
        const {cookies} = this.props;
        Geocode.setApiKey('AIzaSyATAz6K9aKSkbyQMLbivjIrp5KoPJ8_rX4');
        if(cookies.get('userInfo') !== undefined && this.props.municipality_name === undefined){
            //A user is logged in
            if("geolocation" in navigator){
                //Browser has opportunity to send current position
                navigator.geolocation.getCurrentPosition(position => {
                    let initialCenter = {lat: JSON.stringify(position.coords.latitude), lng: JSON.stringify(position.coords.longitude)};
                    this.setState({initialCenter: initialCenter});
                }, error => {
                    //Could not get location and could be because user has blocked this
                    //Tries to get information from cookie
                    Geocode.fromAddress(cookies.get('userInfo').municipality_name).then(
                        response => {
                            const {lat, lng} = response.results[0].geometry.location;
                            this.setState({initialCenter: {lat: lat, lng: lng}});
                        },
                        error => {
                            //Could not find information from cookie. Sets Trondheim as default
                            console.log(error);
                            this.setState({initialCenter: {lat: 0.430606, lng: 0.391804}});
                        });
                });
            }
        }

        //checks if component is given a start address
        if(this.props.municipality_name !== undefined){
            Geocode.fromAddress(this.props.municipality_name).then(
                response => {
                    const {lat, lng} = response.results[0].geometry.location;
                    this.setState({initialCenter: {lat: lat, lng: lng}});
                },
                error => {
                    console.log(error);
                });
        }

        if(this.props.mapStyle){
            this.setState({mapStyle: this.props.mapStyle});
        }

        if(this.props.municipality_id){
            issueService.getIssues(this.props.municipality_id,[2,3])
                .then(issues => this.setState({issues: issues}))
                .catch(error => console.log('Error: ' + error));
        }

        if(this.props.municipality_id === undefined && cookies.get('userInfo').municipality_id !== undefined){
            issueService.getIssues(cookies.get('userInfo').municipality_id,[1,2,3])
                .then(issues => this.setState({issues: issues}))
                .catch(error => console.log('Error: ' + error));
        }


    }

    render(){
        return(
            <Map containerStyle={this.state.mapStyle} center={this.state.initialCenter}
                 google={this.props.google} resetBoundsOnResize={true} zoom={12} onClick={this.mapClicked.bind(this)}

            >
                {this.state.issues.map(item =>
                    <Marker key={item.id} onClick={this.markerClicked.bind(this)} id={item.id} name={item.title}
                            position={{lat: item.latitude, lng: item.longitude}} />
                )}
                <InfoWindow position={this.state.infoWindow.position} visible={this.state.infoWindow.visible}>
                    <div>
                        <h6>{this.state.infoWindow.title}</h6>
                        <p>{this.state.infoWindow.address}</p>
                        <button onClick={this.buttonClicked.bind(this)}><a href={"https://hverdagshelt.net/saker/" + this.state.infoWindow.itemId}>Gå til sak</a></button>
                    </div>
                </InfoWindow>
            </Map>






        );
    }
    /**
     * Function that loads a infowindow for a issue.
     * Finds the address with Geocode from google maps API.
     */
    loadInfowindow(issueId){
        let issue = this.state.issues.find(i => i.id === issueId);
        if(issue){
            let address = '';
            let data = {};
            Geocode.setApiKey("AIzaSyATAz6K9aKSkbyQMLbivjIrp5KoPJ8_rX4");
            Geocode.fromLatLng(issue.latitude, issue.longitude)
                .then(response => {
                        address = response.results[0].formatted_address;
                        data = {
                            position: {lat: issue.latitude, lng: issue.longitude},
                            visible: true,
                            title: issue.title,
                            itemId: issueId,
                            address: address
                        };
                        this.setState({infoWindow: data});
                    },
                    error => {
                        console.error(error);
                        data = {
                            position: {lat: issue.latitude, lng: issue.longitude},
                            visible: true,
                            title: issue.title,
                            itemId: issueId,
                            address: 'Fant ikke adressen'
                        };
                        this.setState({infoWindow: data});
                    }
                );
        }
    }

    /**
     * Function that sends the viewer to a issue-page.
     */
    buttonClicked(props) {
        console.log(this.state.infoWindow.itemId);
    }

    /**
     * Function that loads a infowindow for the marker that has been clicked.
     * Using loadInfowindow function.
     */
    markerClicked(props, marker, e) {
        this.loadInfowindow(marker.id);
    }


    /**
     * Saves the coordinates of where map is clicked and places a marker at that location.
     * If props contains getCoordinates function, the coordinates will be sent to parent.
     */
    mapClicked(mapProps, map, event){
        if(this.state.infoWindow.visible){
            this.setState({infoWindow: {
                    visible: false
                }});
        }
        let lat = event.latLng.lat();
        let lng = event.latLng.lng();

        if(this.props.getCoordinates){
            this.props.getCoordinates(lat, lng);
        }

        let newMarker = {id: -1, title: 'Ny sak', latitude: lat, longitude: lng};
        let newItems = this.state.issues;
        let marker = newItems.find(function(element) {
            return element.id === -1;
        });
        if(marker){
            newItems.pop();
            newItems.push(newMarker);
            this.setState({issues: newItems});
        }else{
            newItems.push(newMarker);
            this.setState({issues: newItems});
        }
    }
}

export default GoogleApiWrapper({
    apiKey: ('AIzaSyATAz6K9aKSkbyQMLbivjIrp5KoPJ8_rX4')
})(MapWrapper)
