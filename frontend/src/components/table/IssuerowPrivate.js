//React
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class IssuerowPrivate extends Component{
    //Får inn issue som props
    render(){
        return (
            <tr key={this.props.item.id}>
                <td className="pt-3-half"><Link to={'/saker/' + this.props.item.id}>{this.props.item.title === null ? 'Ny Sak' : this.props.item.title} id:{this.props.item.id}</Link></td>
                <td className={"pt-3-half"}>{this.props.item.latitude} / {this.props.item.latitude}</td>
                <td className="pt-3-half">{this.props.item.date}</td>
                <td className="pt-3-half">{this.props.item.status}</td>
            </tr>
        );
    }
}

export default IssuerowPrivate;