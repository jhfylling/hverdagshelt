//React
import React, { Component } from 'react';
import Table from "./Table";

class Testpage extends Component{
    render(){
        return (
            <div>
                <Table details={{type: 'Event', user: 'Private'}} municipality_id={1} />
                <br/>
                <br/>
                <Table details={{type: 'Issue', user: 'Private'}} municipality_id={1} />
                <br/>
                <br/>
                <Table details={{type: 'Issue', user: 'Municipal'}} municipality_id={2} />
            </div>
        );
    }
}
export default Testpage;