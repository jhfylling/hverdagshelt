//React
import React, { Component } from 'react';

//Own
import {issueService} from "../../serviceObjects/IssueService";
import {eventService} from "../../serviceObjects/eventService";
import Eventrow from "./Eventrow";
import IssuerowMunicipal from "./IssuerowMunicipal";
import IssuerowPrivate from './IssuerowPrivate';


class Table extends Component {
    constructor(props){
        super(props);
        this.state = {
            items: ['']
        }
    }

    componentDidMount(){
        if(this.props.details.type === 'Event'){
            eventService.getEvents(this.props.municipality_id)
                .then(events => {
                    if(events !== undefined){
                        this.setState({items: events});
                    }})
                .catch(error => console.log(error));

        }else if(this.props.details.type === 'Issue'){
            issueService.getIssues(this.props.municipality_id)
                .then(issues => {if(issues !== undefined){this.setState({items: issues})}})
                .catch(error => console.log(error));
        }else if(this.props.issues){
            this.setState({items: this.props.issues});
        }
    }
    componentDidUpdate(prevProps) {
        if(this.props.details.type !== prevProps.details.type){
            if(this.props.details.type === 'Event'){
                    eventService.getEvents(this.props.municipality_id)
                        .then(events => {
                            this.setState({items: events});})
                        .catch(error => console.log(error));

            }else if(this.props.details.type === 'Issue'){
                    issueService.getIssues(this.props.municipality_id)
                        .then(issues => {
                            this.setState({items: issues});})
                        .catch(error => console.log(error));

            }
        }else if(this.props.municipality_id !== prevProps.municipality_id){
            if(this.props.details.type === 'Event'){
                eventService.getEvents(this.props.municipality_id)
                    .then(events => {
                        this.setState({items: events});})
                    .catch(error => console.log(error));

            }else if(this.props.details.type === 'Issue'){
                issueService.getIssues(this.props.municipality_id)
                    .then(issues => {
                        this.setState({items: issues});})
                    .catch(error => console.log(error));

            }
        }else if(this.props.issues !== prevProps.issues){
            this.setState({items: this.props.issues});
        }

    }

    render(){


        if(this.props.details.type === 'Event'){
            return (
                <div className={"card "}>
                    <h3 className="card-header text-center font-weight-bold text-uppercase py-4">Arrangement i din kommune</h3>
                    <table id={"tableview"} className="table table-hover table-responsive-md table-sm text-center">
                        <thead>
                        <tr className={"table-primary"} >
                            <th>Tittel</th>
                            <th>Dato</th>
                            <th>Sted</th>
                            <th>Kategori</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.items.map(i =>
                            <Eventrow item={i} key={i.id}/>
                        )}
                        </tbody>
                    </table>
                </div>
            )

        }else if(this.props.details.type === 'Issue' && this.props.details.user === 'Private'){
            return (
                <div className={"card"}>
                    <h3 className="card-header text-center font-weight-bold text-uppercase py-4">Saker i din kommune</h3>
                    <table className="table table-responsive-md table-hover table-sm text-center">
                       <thead>
                            <tr className={"table-primary"}>
                                <th>Sak</th>
                                <th>Sted</th>
                                <th>Dato</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.items.map(i =>
                                <IssuerowPrivate item={i} key={i.id}/>
                            )}
                        </tbody>
                    </table>
                </div>
            );

        }else if(this.props.details.type === 'Issue' && this.props.details.user === 'Municipal') {
            return (
                <div className={"card"}>
                    <h3 className="card-header text-center font-weight-bold text-uppercase py-4">Saker i din kommune
                        (kommuneansatt)</h3>
                    <table className="table table-responsive-md table-hover table-sm text-center">
                        <thead>
                        <tr className={"table-primary"}>
                            <th>Sak</th>
                            <th>Sted</th>
                            <th>Dato</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.items.map(i =>
                            <IssuerowMunicipal item={i} key={i.id}/>
                        )}
                        </tbody>
                    </table>
                </div>
            );
        }else if(this.props.issues){
            return(
                <div className={"card"}>
                    <h3 className="card-header text-center font-weight-bold text-uppercase py-4">Saker i din kommune</h3>
                    <table className="table table-responsive-md table-hover table-sm text-center">
                        <thead>
                        <tr className={"table-primary"}>
                            <th>Sak</th>
                            <th>Sted</th>
                            <th>Dato</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.items.map(i =>
                            <IssuerowPrivate item={i} key={i.id}/>
                        )}
                        </tbody>
                    </table>
                </div>
            )

        }else{
            return (
                <div className={"card"}>
                    <h3 className="card-header text-center font-weight-bold text-uppercase py-4">Saker i din kommune</h3>
                    <table className="table table-responsive-md table-hover table-sm text-center">
                       <thead>
                            <tr className={"table-primary"}>
                                <th>Sak</th>
                                <th>Sted</th>
                                <th>Dato</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            );
        }
    }
}

export default Table;