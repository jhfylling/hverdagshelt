//React
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
class Eventrow extends Component{
    //Får inn event som props
    render(){
        return (
            <tr key={this.props.item.municipality_id}>
                <td className="pt-3-half" ><Link to={'/arrangementer/' + this.props.item.id}>{this.props.item.title}</Link></td>
                <td className="pt-3-half">{this.props.item.date}</td>
                <td className="pt-3-half">{this.props.item.latitude} / {this.props.item.longitude}</td>
                <td className="pt-3-half">{this.props.item.description}</td>
            </tr>
        );
    }
}

export default Eventrow;