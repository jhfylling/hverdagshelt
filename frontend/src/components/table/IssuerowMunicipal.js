//React
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class RowIssueMunicipal extends Component{
    render(){
        return (
            <tr>
                <td className="pt-3-half"><Link to={'/saker/' + this.props.item.id + '/administrere'}>{this.props.item.title}</Link></td>
                <td className="pt-3-half">{this.props.item.latitude} / {this.props.item.latitude}</td>
                <td className="pt-3-half">{this.props.item.date}</td>
                <td className="pt-3-half">{this.props.item.status}</td>
                <td className="pt-3-half">
                    <Link to={'/saker/administrere/' + this.props.item.id}>
                        <button type="button" className="btn btn-indigo btn-sm m-0">
                            <i className="fa fa-pencil-square-o" aria-hidden="true"></i>
                        </button>
                    </Link>
                </td>
            </tr>
        );
    }
}

export default RowIssueMunicipal;