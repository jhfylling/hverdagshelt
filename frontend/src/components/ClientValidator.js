//React
import React, { Component } from 'react';
import LoginPopup from './LoginPopup';

/**
 * ClientValidator is used for clientside user validation.
 * Can be used to confirm user type for user friendlyness only.
 * 
 * Set pros.category_id = 0 to accept all user categories
 * Otherwise set props.category_id to correct category.
 * 
 * TODO: Change category system to be able to accept only specified categories.
 * TODO: Stop login popup if user is allready logged in to incorrect category.
 */
class ClientValidator extends Component{
    state = {
        validated: false,
    }

    render(){
        const { validated } = this.state;
        if(validated){
            console.log("ClientValidator: Valid");
            return this.props.children;
        }
        else{
            console.log("ClientValidator: Not valid. Required category: " + this.props.category_id);
            return <div>
                <LoginPopup open={true} buttonText={this.props.invalidText} cookies={this.props.cookies} clientValidator={this.validate} />               
            </div>;
        }
    }

    validate = () => {
        const { cookies } = this.props;
        const { validated } = this.state;

        if(cookies.get('userInfo') === undefined){
            this.setState({validated: false});
        }
        else if(validated === false && (cookies.get('userInfo').category_id === this.props.category_id || this.props.category_id === 0)){
            this.setState({validated: true});
        }
    }

    componentDidMount(){
        this.validate();
    }

    componentDidUpdate(prevProps){
        if(this.props !== prevProps){
            this.validate();
        }
    }

}

export default ClientValidator;