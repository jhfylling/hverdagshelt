//React
import React, { Component } from 'react';

//Material-UI
import { withStyles } from '@material-ui/core/styles';


let styles = {
    rowWrapper: {
        display: 'grid',
        gridTemplateColumns: '1fr 1fr 1fr',
        gridTemplateAreas: "' name description email'",
        textAlign:'center',
    },
    name: {
        gridArea: 'name',
        height: '40px',
    },
    description: {
        gridArea: 'description'
    },
    email: {
        gridArea: 'email'
    },

};

class CompanyHeader extends Component {

    render() {
        const { classes } = this.props;

        return (
            <div className={classes.rowWrapper}>
                <div className={classes.name}><b><i className="fas fa-file fa-sm"/>&nbsp; Bedriftsnavn</b></div>
                <div className={classes.description}><b><i className="fas fa-align-justify fa-sm"/>&nbsp; Beskrivelse</b></div>
                <div className={classes.email}><b><i className="fas fa-map-marker-alt fa-sm"/>&nbsp; Emailadresse</b></div>
            </div>
        );
    }
}
export default withStyles(styles)(CompanyHeader);
