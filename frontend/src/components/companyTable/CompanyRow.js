//React
import React, { Component } from 'react';

//Material-UI
import { withStyles } from '@material-ui/core/styles';

let styles = {
    rowWrapper: {
        display: 'grid',
        gridTemplateColumns: '1fr 1fr 1fr',
        gridTemplateAreas: "' name description email'",
        textAlign:'center',
    },
    name: {
        gridArea: 'name',
        height: '60px',
    },
    description: {
        gridArea: 'description'
    },
    email: {
        gridArea: 'email'
    },
    
};

class CompanyRow extends Component {
    render(){
        const { classes } = this.props;
        return(
            <div className={classes.rowWrapper}>
                <div className={classes.name}><b>{this.props.company.name}</b></div>
                <div className={classes.description}><b>{this.props.company.description}</b></div>
                <div className={classes.email}><b>{this.props.company.email}</b></div>
            </div>
        );
    }
}
export default withStyles(styles)(CompanyRow);