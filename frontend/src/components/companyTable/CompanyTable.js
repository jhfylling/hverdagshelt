//Material-UI
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { withStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';

//Own
import CompanyRow from './CompanyRow';
import CompanyHeader from './CompanyHeader';

import React, { Component } from 'react';
import { businessService } from '../../serviceObjects/businessService';
import ListSubheader from "@material-ui/core/ListSubheader";
import {handleSnackbarOpen} from "../Snackbar";
import PositionedSnackbar from "../Snackbar";

let styles = {
	list: {
		height: '50vh',
		overflow: 'auto'
	}
};

class CompanyTable extends Component {
	constructor(props) {
		super(props);
		this.state = {
			company: [ '' ]
		};
	}

	componentDidMount() {
		this.getCompanies();
	}
	componentDidUpdate(prevProps) {
		if (prevProps !== this.props) {
			this.getCompanies();
		}
	}
	getCompanies = () => {
		businessService
			.getCompaniesJoinCompanyUsers(this.props.municipality_id)
			.then((company) => {
				this.setState({ company: company });
			})
			.catch((error) => console.log(error));
	};

	deleteCompany(companyid, userid){
		businessService
			.disableCompanyUserAccount(companyid, userid)
			.then(response => {
                handleSnackbarOpen({message: 'Bedrift slettet', variant: 'success'});
                this.getCompanies();
            })
			.catch((error) => {handleSnackbarOpen({message: 'Kunne ikke slette bedrift', variant: 'error'}); console.log(error)});

	}

	render() {
		const { classes } = this.props;
		return (
			<List className={classes.list}>
				<ListSubheader style={{ background: 'white', paddingTop: '-50px' }}>
					<ListItem>
						<ListItemText>
							<CompanyHeader />
							<Divider />
						</ListItemText>
					</ListItem>
				</ListSubheader>

				{this.state.company.map((company, i) => (
					<ListItem key={i} role={undefined} dense >
						<ListItemText>
							<CompanyRow company={company} />{' '}
						</ListItemText>
						<ListItemSecondaryAction>
							<Button key={i} onClick={() => this.deleteCompany(company.companyid, company.userid)}>
								<DeleteIcon className={classes.icon} />
							</Button>
						</ListItemSecondaryAction>
					</ListItem>
				))}
				<PositionedSnackbar/>
			</List>
		);
	}
}

export default withStyles(styles)(CompanyTable);
