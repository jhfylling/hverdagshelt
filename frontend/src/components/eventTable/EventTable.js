//React
import React, { Component } from 'react';

//Material-UI
import { withStyles } from '@material-ui/core/styles';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';

//Own
import { eventService } from '../../serviceObjects/eventService';
import EventInfo from './EventInfo';
import { handleSnackbarOpen } from '../Snackbar';
import PositionedSnackbar from '../Snackbar';

let styles = {
	list: {
		height: '50vh',
		overflow: 'auto'
	}
};

class IssueTable extends Component {
	constructor(props) {
		super(props);
		this.state = {
			events: [ '' ],
			expanded: null,
			municipality_id: null
		};
	}
	componentDidMount() {
		eventService
			.getEvents(this.props.municipality_id)
			.then((events) => {
				this.setState({ events: events });

			})
			.catch((error) => console.log(error));
	}
	componentDidUpdate(prevProps) {
		if (prevProps !== this.props) {
			eventService
				.getEvents(this.props.municipality_id)
				.then((events) => {
					this.setState({ events: events });

				})
				.catch((error) => console.log(error));
		}
	}
	handleChange = (panel) => (event, expanded) => {
		this.setState({
			expanded: expanded ? panel : false
		});
	};

	deleteEvent(id) {
		eventService
			.deleteEvent(id)
			.then((response) => handleSnackbarOpen({ message: 'Event ble slettet', variant: 'success' }))
			.then(() =>
				eventService
					.getEvents(this.props.municipality_id)
					.then((events) => {
						this.setState({ events: events });
					})
					.catch((error) => console.log(error))
			)
			.catch((error) => {
				console.log(error);
				handleSnackbarOpen({ message: 'Noe gikk galt', variant: 'error' });
			});
	}

	render() {
		const { cookies } = this.props;

		return (
			<div>
				{this.state.events.map((event, i) => (
					<ExpansionPanel
						key={i}
						expanded={this.state.expanded === i}
						defaultExpanded
						onChange={this.handleChange(i)}
					>
						<ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
							<div>
								<div>
									<i className="far fa-calendar-check" />
									<b>{event.title !== null ? ' ' + event.title : ' Kommer'}</b>
								</div>
							</div>
						</ExpansionPanelSummary>
						<ExpansionPanelDetails>
							<EventInfo cookies={cookies} event={event} deleteEvent={this.deleteEvent.bind(this)} />
						</ExpansionPanelDetails>
					</ExpansionPanel>
				))}
				<PositionedSnackbar />
			</div>
		);
	}
}

export default withStyles(styles)(IssueTable);
