//React
import React, { Component } from 'react';

//Material-UI
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';


let styles = {
	rowWrapper:{
		textAlign: 'left',
	}
};

class EventInfo extends Component {
	render() {
		const { classes } = this.props;
		const { cookies } = this.props;
		let showDeleteButton = false;
		if(cookies.get('userInfo') !== undefined){
			showDeleteButton = [2, 3].includes(cookies.get('userInfo').category_id);
		}

		return (
			<div className={classes.rowWrapper}>
				<div className={classes.description}>
					<b>Description: </b>
					<p>{this.props.event.description === null ? 'Ikke satt' : this.props.event.description}</p>
				</div>
				<div className={classes.price}>
					<b>Pris: </b>
					<p>{this.props.event.price === '' ? 'Ikke satt' : this.props.event.price},-</p>
				
				</div>
				<div className={classes.event_homepage}>
					<b>Les mer på eventets hjemmeside: </b>
					<p>
					<a target="_blank" rel="noopener noreferrer" href={this.props.event.event_homepage}> {this.props.event.event_homepage} </a> 
					</p>
					
				</div>
				{ showDeleteButton ? (
					<Button variant="outlined" onClick={() => this.props.deleteEvent(this.props.event.id)}> Delete </Button>
				) : null}
			</div>
		);
	}
}

export default withStyles(styles)(EventInfo);
