//React
import React from 'react';
import PropTypes from 'prop-types';

//Material UI
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import { withStyles } from '@material-ui/core/styles';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import DialogTitle from '@material-ui/core/DialogTitle';
import withMobileDialog from '@material-ui/core/withMobileDialog';

//Own
import { reportButtonColor, white} from './Colors';
import {userService} from "../serviceObjects/userService";
import RegisterPopup from './RegisterPopup';
import ForgotPasswordPopup from './ForgotPasswordPopup';
import { handleSnackbarOpen } from './Snackbar';
import PositionedSnackbar from './Snackbar';

const styles = (theme) => ({
	pwField: {
		marginTop: '20px'
	},
	loginForm: {
		fontSize: '20px'
	},
	navLink: {
		backgroundColor: reportButtonColor,
		textAlign: 'center',
		color: 'white',
		height: '50px',
		padding: '10px',
		display: 'grid',
		'&:hover': {
			backgroundColor: 'white',
			color: reportButtonColor,
			textDecoration: 'none'
		}
	},
	loginButton: {
		color: white,
		outline: 'none',
		'&:hover': {
			backgroundColor: 'white',
			color: reportButtonColor,
			textDecoration: 'none'
		},
		'&:active': {
			outline: 'none'
		},
		'&:focus': {
			outline: 'none'
		}
	}
});

/**
 * Material UI popup form dialog for login
 */
class LoginPopup extends React.Component {
	state = {
		open: false,
		email: '',
		pw: ''
	};

	componentDidUpdate(prevProps) {
		if (this.props !== prevProps) {
			if (this.props.open !== undefined) {
				this.setState({ open: this.props.open });
			}
		}
	}

	componentDidMount() {
		if (this.props.open !== undefined) {
			this.setState({ open: this.props.open });
		}
	}

	/**
	 * Validate login info and recieve access token
	 */
	login() {
		const { email, pw } = this.state;
		const { cookies } = this.props;
		userService
			.login(email, pw)
			.then((response) => {
				cookies.remove('x-access-token');
				cookies.remove('userInfo');
				cookies.set('x-access-token', response['x-access-token'], { path: '/' });
				cookies.set('userInfo', response.userInfo, { path: '/' });
				console.log("TEST");
				this.handleClose();
			})
			.catch((err) => {
				console.log('Error with login: ', err);
				handleSnackbarOpen({ message: 'Feil brukernavn eller passord', variant: 'error' });
			});
	}

	/**
	 * Called when typing
	 */
	handleChange = (event) => {
		if (event.target.name === 'email') {
			const email = event.target.value;
			this.setState({ email });
		}
		if (event.target.name === 'pw') {
			const pw = event.target.value;
			this.setState({ pw });
		}
	};

	/**
	 * Open popup
	 */
	handleClickOpen = () => {
		this.setState({ open: true });
	};

	/**
	 * Close popup
	 */
	handleClose = () => {
		this.setState({ open: false });
		//this.props.clientValidator();
		if(this.props.clientValidator !== undefined && this.props.clientValidator !== null){
			this.props.clientValidator();
		}
	};

	/**
	 * Called by submit button
	 */
	handleSubmit = () => {
		this.login();
	};

	render() {
		const { classes } = this.props;
		const { email, pw } = this.state;
		const { fullScreen } = this.props;
		return (
			<div className={this.props.divStyle}>
				<Button color="inherit" className={this.props.buttonStyle} onClick={this.handleClickOpen}>
					{this.props.buttonText}
				</Button>
				<Dialog
					fullScreen={fullScreen}
					id="loginPopup"
					ref="loginPopup"
					open={this.state.open}
					onClose={this.handleClose}
					aria-labelledby="form-dialog-title"
				>
					<DialogTitle id="form-dialog-title">Logg inn</DialogTitle>
					<DialogContent>
						<DialogContentText />
						<ValidatorForm
							id="loginform"
							ref="form"
							onSubmit={this.handleSubmit}
							onError={(errors) => console.log(errors)}
						>
							<TextValidator
								autoFocus
								label="E-mail adresse"
								onChange={this.handleChange}
								name="email"
								value={email}
								validators={[ 'required', 'isEmail' ]}
								errorMessages={[ 'Fyll inn E-mail', 'Mail adresse må være ekte' ]}
								fullWidth
							/>
							<TextValidator
								className={classes.pwField}
								type="password"
								label="Passord"
								onChange={this.handleChange}
								name="pw"
								value={pw}
								validators={[ 'required' ]}
								errorMessages={[ 'Fyll inn passord' ]}
								fullWidth
							/>
						</ValidatorForm>
						<div style={{paddingTop: '10px'}}>					
							<ForgotPasswordPopup
								closeLoginPopup={this.handleClose.bind(this)}
								cookies={this.props.cookies}
							/>
							<RegisterPopup closeLoginPopup={this.handleClose.bind(this)} cookies={this.props.cookies} />
						</div>
					</DialogContent>
					<DialogActions>
						<Button onClick={this.handleClose} variant='outlined'>
							Avbryt
						</Button>
						<Button type="submit" form="loginform" variant='outlined'>
							Logg inn
						</Button>
					</DialogActions>
				</Dialog>
				<PositionedSnackbar />

			</div>
		);
	}
}

LoginPopup.propTypes = {
	fullScreen: PropTypes.bool.isRequired
};

export default withMobileDialog({ breakpoint: 'sm' })(withStyles(styles)(LoginPopup));
