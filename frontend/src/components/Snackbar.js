//React
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

//Material-ui
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import { withStyles } from '@material-ui/core/styles';
import green from '@material-ui/core/colors/green';
import blue from '@material-ui/core/colors/blue';
import amber from '@material-ui/core/colors/amber';
import IconButton from '@material-ui/core/IconButton';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import CloseIcon from '@material-ui/icons/Close';
import WarningIcon from '@material-ui/icons/Warning';


/**
 * Snackbar used to give feedback on actions.
 * Implement <PositionedSnackbar/> in your render method, and
 * call ' handleSnackbarOpen({message: 'your message', variant: 'type of snackbar (error, warning, success, info)'});
 * whenever you want to show snackbar.
 */

const variantIcon = {
    success: CheckCircleIcon, //Circle with check mark
    warning: WarningIcon, //Triangle with !
    error: ErrorIcon, //Circle with !
    info: InfoIcon, //Circle with i
};

const styles = theme => ({
    success: {
        backgroundColor: green[600],
    },
    error: {
        backgroundColor: theme.palette.error.dark,
    },
    info: {
        backgroundColor: blue[600],
    },
    warning: {
        backgroundColor: amber[700],
    },
    message: {
        display: 'flex',
        alignItems: 'center',
    },
    icon: {
        fontSize: 20
    },
    iconVariant: {
        opacity: 0.9,
        marginRight: theme.spacing.unit,
    }
});

function MySnackbarContent(props) {
    const { classes, className, message, onClose, variant} = props;
    const Icon = variantIcon[variant];

    return (
        <SnackbarContent
            className={classNames(classes[variant], className)}
            aria-describedby="client-snackbar"
            message={
                <span id="client-snackbar" className={classes.message}>
                    <Icon className={classNames(classes.icon, classes.iconVariant)} />
                    {message}
                </span>
            }
            action={[
                <IconButton
                    key="close"
                    aria-label="Close"
                    color="inherit"
                    className={classes.close}
                    onClick={onClose}
                >
                    <CloseIcon className={classes.icon} />
                </IconButton>,
            ]}
        />
    );
}

let openSnackbar;

class PositionedSnackbar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            message: '',
            vertical: 'bottom',
            horizontal: 'left',
            classes: '',
            className: '',
            variant: ''
        }
    }

    componentDidMount() {
        openSnackbar = this.handleSnackbarOpen;
    }

    handleSnackbarOpen = ({ message, variant }) => {
        this.setState({
            open: true,
            message: message,
            variant: variant
        });
    };

    handleSnackbarClose = () => {
        this.setState({
            open: false,
            message: ''
        });
    };

    render() {
        const {vertical, horizontal, open, variant, message} = this.state;
        return(
            <Snackbar
                anchorOrigin={{ vertical, horizontal }}
                open={open}
                autoHideDuration={6000}
                onClose={this.handleSnackbarClose}
                >
                <MySnackbarContentWrapper
                    onClose={this.handleSnackbarClose}
                    variant={variant}
                    message={<span id="message-id">{message}</span>}
                />
            </Snackbar>
        );
    }
}

PositionedSnackbar.propTypes = {
    classes: PropTypes.object.isRequired,
    className: PropTypes.string,
    variant: PropTypes.oneOf(['success', 'warning', 'error', 'info']),
};

MySnackbarContent.propTypes = {
    classes: PropTypes.object.isRequired,
    className: PropTypes.string,
    message: PropTypes.node,
    onClose: PropTypes.func,
    variant: PropTypes.oneOf(['success', 'warning', 'error', 'info']).isRequired,
};


const MySnackbarContentWrapper = withStyles(styles)(MySnackbarContent);

export function handleSnackbarOpen({message, variant}) {
    openSnackbar({message, variant});
}
export default withStyles(styles)(PositionedSnackbar);
