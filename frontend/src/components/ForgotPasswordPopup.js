//React
import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

//Material UI
import { withStyles } from '@material-ui/core/styles';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';

//Own
import { userService } from '../serviceObjects/userService';

const styles = (theme) => ({
	emailField: {
		marginTop: '45px'
	},
	forgotPasswordForm: {
		fontSize: '20px'
	},
	button: {
		background: 'none',
		border: 'none',
		padding: '0',
		color: '#3f51b5',
		textDecoration: 'underline',
		cursor: 'pointer'
	}
});

/**
 * Material UI popup form dialog for forgot password
 */
class ForgotPasswordPopup extends React.Component {
	state = {
		open: false,
		email: ''
	};

	/**
     * Sends link on email where users can decide a new password.
     * Does not give any feedback on whether the mail was sent or not for security reasons.
     *
     */
	sendLink() {
		const { email } = this.state;
		userService.resetPasswordLink(email, false).then((response) => {}).catch((error) => {});
		this.setState({ open: false });
	}

	/**
     * Called when typing
     */
	handleChange = (event) => {
		if (event.target.name === 'email') {
			const email = event.target.value;
			this.setState({ email });
		}
	};

	/**
     * Open popup
     */
	handleClickOpen = () => {
		this.setState({ open: true });
	};

	/**
     * Close popup
     */
	handleClose = () => {
		this.setState({ open: false });
		this.props.closeLoginPopup();
	};

	/**
     * Called by submit button
     */
	handleSubmit = () => {
		this.sendLink();
	};

	render() {
		const { classes } = this.props;
		const { email } = this.state;
		return (
			<div>
				<button className={classes.button} onClick={this.handleClickOpen}>
					Glemt passord?
				</button>
				<Dialog
					id="forgotPasswordPopup"
					ref="forgotPasswordPopup"
					open={this.state.open}
					onClose={this.handleClose}
					aria-labelledby="form-dialog-title"
				>
					<DialogTitle id="form-dialog-title">Glemt passord?</DialogTitle>
					<DialogContent>
						<DialogContentText>
							Skriv din email og du vil motta en link på mail hvor du kan endre passord.
						</DialogContentText>
						<ValidatorForm
							id="forgotPasswordForm"
							ref="form"
							onSubmit={this.handleSubmit}
							onError={(errors) => console.log(errors)}
						>
							<TextValidator
								autoFocus
								className={classes.emailField}
								label="E-mail adresse"
								onChange={this.handleChange}
								name="email"
								value={email}
								validators={[ 'required', 'isEmail' ]}
								errorMessages={[ 'Fyll inn E-mail' ]}
								fullWidth
							/>
						</ValidatorForm>
					</DialogContent>
					<DialogActions>
						<Button onClick={this.handleClose} color="primary">
							Avbryt
						</Button>
						<Button type="submit" form="forgotPasswordForm" color="primary">
							Send link
						</Button>
					</DialogActions>
				</Dialog>
			</div>
		);
	}
}

export default withStyles(styles)(ForgotPasswordPopup);
