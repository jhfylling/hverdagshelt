//React
import React, { Component } from 'react';

class Footer extends Component {
	render() {
		const { cookies } = this.props;
		let municipality = cookies.get('userInfo') !== undefined ? cookies.get('userInfo').municipality_name : 'Oslo ';
		municipality = municipality.substring(0, 1) + municipality.substring(1, municipality.length).toLowerCase();

		return <div className="footer-copyright text-center py-3" style={{ marginTop: "10vh" }}>
        <p>
          Copyright&copy; {new Date().getFullYear()} HverdagsHelt | <a target="_blank" rel="noopener noreferrer" href={cookies.get("userInfo") !== undefined ? "https://" + cookies.get("userInfo").municipality_name + ".kommune.no" : "https://oslo.kommune.no"}>
            {municipality} kommunes hjemmeside
          </a>
        </p>
      </div>;
	}
}

export default Footer;
