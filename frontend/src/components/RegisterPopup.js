//React
import React from 'react';

//Material UI
import { withStyles } from '@material-ui/core/styles';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

//Own
import { reportButtonColor, white } from './Colors';
import { userService } from '../serviceObjects/userService';
import { handleSnackbarOpen } from './Snackbar';
import PositionedSnackbar from './Snackbar';

const styles = (theme) => ({
	pwField: {
		marginTop: '20px'
	},
	loginForm: {
		fontSize: '20px'
	},
	navLink: {
		backgroundColor: reportButtonColor,
		textAlign: 'center',
		color: 'white',
		height: '50px',
		padding: '10px',
		display: 'inline-block',
		'&:hover': {
			backgroundColor: 'white',
			color: reportButtonColor,
			textDecoration: 'none'
		}
	},
	loginButton: {
		color: white,
		outline: 'none',
		'&:hover': {
			backgroundColor: 'white',
			color: reportButtonColor,
			textDecoration: 'none'
		},
		'&:active': {
			outline: 'none'
		},
		'&:focus': {
			outline: 'none'
		}
	},
	dialogContent: {
		[theme.breakpoints.down('sm')]: {}
	},
	button: {
		background: 'none',
		border: 'none',
		padding: '0',
		color: '#3f51b5',
		textDecoration: 'underline',
		cursor: 'pointer'
	}
});

/**
 * Material UI popup form dialog for login
 */
class RegisterPopup extends React.Component {
	state = {
		open: false,
		email: '',
		pw: '',
		pwRepeat: ''
	};

	/**
	 * Registrer user and log in
	 * TODO: Add municipality selector in form.
	 */
	register() {
		const { email, pw } = this.state;
		const { cookies } = this.props;
		let municipality_id = 1; // document.querySelector("#municipality_id");
		let new_user_category_id = 1;
		userService
			.createUser(email, pw, municipality_id, new_user_category_id)
			.then((response) => {
				cookies.remove('x-access-token');
				cookies.remove('userInfo');
				cookies.set('x-access-token', response['x-access-token'], { path: '/' });
				cookies.set('userInfo', response.userInfo, { path: '/' });
				this.handleClose();
			})
			.catch((err) => {
				console.log(err + ' failed to register!');
				if (err.response.status === 409) {
					handleSnackbarOpen({ message: 'Epost er allerede registrert', variant: 'error' });
				}
			});
	}

	/**
	 * Called when typing
	 */
	handleChange = (event) => {
		if (event.target.name === 'email') {
			const email = event.target.value;
			this.setState({ email });
		}
		if (event.target.name === 'pw') {
			const pw = event.target.value;
			this.setState({ pw });
		}
		if (event.target.name === 'pwRepeat') {
			const pwRepeat = event.target.value;
			this.setState({ pwRepeat });
		}
	};

	/**
	 * Open popup
	 */
	handleClickOpen = () => {
		this.setState({ open: true });
	};

	/**
	 * Close popup
	 */
	handleClose = () => {
		this.setState({ open: false });
		this.props.closeLoginPopup();
	};

	/**
	 * Called by submit button
	 */
	handleSubmit = () => {
		this.register();
	};

	componentDidMount() {
		// custom rule will have name 'isPasswordMatch'
		ValidatorForm.addValidationRule('isPasswordMatch', (value) => {
			if (value !== this.state.pw) {
				return false;
			}
			return true;
		});
	}

	render() {
		const { classes } = this.props;
		const { email, pw, pwRepeat } = this.state;
		const { fullScreen } = this.props;
		return (
			<div>

				<button className={classes.button} onClick={this.handleClickOpen}>
					Registrer ny bruker her.
				</button>

				<Dialog
					fullScreen={fullScreen}
					id="registerPopup"
					ref="registerPopup"
					open={this.state.open}
					onClose={this.handleClose}
					aria-labelledby="form-dialog-title"
				>
					<DialogTitle id="form-dialog-title">Registrering</DialogTitle>

					<DialogContent className={classes.dialogContent}>
						<DialogContentText>Fyll inn informasjonen under for å registrere deg</DialogContentText>

						<ValidatorForm
							id="registerForm"
							ref="form"
							onSubmit={this.handleSubmit}
							onError={(errors) => console.log(errors)}
						>
							<TextValidator
								autoFocus
								label="E-mail adresse"
								onChange={this.handleChange}
								name="email"
								value={email}
								validators={[ 'required', 'isEmail' ]}
								errorMessages={[ 'Fyll inn E-mail', 'Mail adresse må være ekte' ]}

								fullWidth
							/>
							<TextValidator
								className={classes.pwField}
								type="password"
								label="Passord"
								onChange={this.handleChange}
								name="pw"
								value={pw}
								validators={[ 'required', 'minStringLength:3', 'maxStringLength:24' ]}
								errorMessages={[
									'Fyll inn passord',
									'Passordet må ha minst 8 tegn',
									'Passordet kan maks ha 24 tegn'
								]}

								fullWidth
							/>
							<TextValidator
								className={classes.pwField}
								type="password"
								label="Gjenta passord"
								onChange={this.handleChange}
								name="pwRepeat"
								value={pwRepeat}
								validators={[
									'isPasswordMatch',
									'required',
									'minStringLength:3',
									'maxStringLength:24'
								]}
								errorMessages={[
									'Passordene er ikke like',
									'Gjenta passord',
									'Passordet må ha minst 8 tegn',
									'Passordet kan maks ha 24 tegn'
								]}

								fullWidth
							/>
						</ValidatorForm>
					</DialogContent>
					<DialogActions>
						<Button onClick={this.handleClose} color="primary">
							Avbryt
						</Button>
						<Button type="submit" form="registerForm" color="primary">
							Registrer
						</Button>
					</DialogActions>
					<PositionedSnackbar />

				</Dialog>
			</div>
		);
	}
}

RegisterPopup.propTypes = {
	fullScreen: PropTypes.bool.isRequired
};

export default withMobileDialog({ breakpoint: 'sm' })(withStyles(styles)(RegisterPopup));
