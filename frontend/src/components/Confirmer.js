//react
import React from 'react';
import {Component} from 'react';

//MaterialUI
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';


//Own
import { backdropColor, invisible, whiteInvisible, white, logoColor } from './Colors';

const styles = theme => ({
	wrapper: {
        transition: 'background-color 0.2s ease-in-out',
        backgroundColor: invisible,
        position: 'fixed',
        top: 0,
        left: 0,
        width: '100vw',
        height: '100vh',
        visibility: 'hidden',
        zIndex: 5,
    },
    centerContent: {
        display: 'flex',
        position: 'relative',
        marginLeft: 'auto',
        marginRight: 'auto',
        [theme.breakpoints.down('sm')]: {
            width: '90vw',
        },
        [theme.breakpoints.up('md')]: {
            width: '50vw',
        },
        [theme.breakpoints.up('lg')]: {
            width: '40vw',
        },
        [theme.breakpoints.up('xl')]: {
            width: '30vw',
        },
        height: '100%',
        alignItems: 'center',
    },
    content: {
        transition: 'background-color 0.2s ease-in-out',
        backgroundColor: whiteInvisible,
        position: 'relative',
        marginLeft: 'auto',
        marginRight: 'auto',
        width: '100%',
        height: '300px',
        display: 'grid',
        padding: '5px',
        borderRadius: '6px',
        gridTemplateColumns: '1fr 10fr 1fr',
        gridTemplateAreas: 
            "'top top top'" +
            "'mid mid mid'" + 
            "'bot bot bot'"
    },
    top: {
        gridArea: 'top',
    },
    mid: {
        gridArea: 'mid',
    },
    bot: {
        gridArea: 'bot',
    },
    show: {
        backgroundColor: backdropColor,
    },
    buttonBar: {
        position: 'absolute',
        bottom: '10px',
        right: '10px',
    },
    yesBtn: {
        width: '90px',
        height: '50px',
        float: 'right',
        marginRight: '10px',
    },
    noBtn: {
        width: '90px',
        height: '50px',
        float: 'right',
    },
    exitbutton:{
        position: 'absolute',
        top: '5px',
        right: '5px',
        '&:hover': {
            color: backdropColor,
        },
    }

});

/**
 * TODO:
 * Use props for button value, heading, mid content and yes/no options
 */

/**
 * General purpose confirm dialog
 * Includes a button, which when clicked, will produce a centered confirm dialog with a grayed out backdrop
 */
class Confirmer extends Component{
    render() {
        const { classes } = this.props;
        return (
            <div>
                <Button variant='outlined' onClick={this.open.bind(this)}>{this.props.buttonText == undefined ? 'Send' : this.props.buttonText}</Button>
                <div id="confirmerWrapper" onClick={this.close.bind(this)} className={classes.wrapper}>
                    <div className={classes.centerContent}>
                        <div id="confirmerContent" className={classes.content}>
                            <div className={classes.top}>
                                <h1 className="display-4">Bekreft</h1>
                                <i id="exitbtn" className={"far fa-times-circle fa-2x " + classes.exitbutton}></i>
                            </div>
                            <div className={classes.mid}>{this.props.body}</div>
                            <div className={classes.bot}>
                                <div className={classes.buttonBar}>
                                    <button id="nobtn" className={"btn btn-secondary " + classes.noBtn}>Nei</button>
                                    <button id="yesbtn" className={"btn btn-primary " + classes.yesBtn} onClick={this.props.yesAction}>Ja</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    open(){
        if(this.props.validateBefore === true){
            if(this.props.validation() === true){
                //entire confirmer
                let confirmer = document.querySelector("#confirmerWrapper");
                //confirmer.style.display = "block";
                confirmer.style.visibility = "visible";
                confirmer.style.backgroundColor = backdropColor;
                //confirmer.classList.toggle(classes.show);

                //confirmercontent
                let confirmerContent = document.querySelector("#confirmerContent");
                confirmerContent.style.backgroundColor = white;
            }
        }
        else{
            //entire confirmer
            let confirmer = document.querySelector("#confirmerWrapper");
            //confirmer.style.display = "block";
            confirmer.style.visibility = "visible";
            confirmer.style.backgroundColor = backdropColor;
            //confirmer.classList.toggle(classes.show);

            //confirmercontent
            let confirmerContent = document.querySelector("#confirmerContent");
            confirmerContent.style.backgroundColor = white;
        }
    }
    
    close(event){
        if(event.target.id === "confirmerWrapper" || event.target.id === "exitbtn" || event.target.id === "yesbtn" || event.target.id === "nobtn"){
            //entire confirmer
            let confirmer = document.querySelector("#confirmerWrapper");
            //confirmer.style.display = "none";  
            confirmer.style.visibility = "hidden";
            confirmer.style.backgroundColor = invisible;
            //confirmer.classList.toggle(classes.show);

            //confirmercontent
            let confirmerContent = document.querySelector("#confirmerContent");
            confirmerContent.style.backgroundColor = whiteInvisible;
        } 
    }
}

export default withStyles(styles)(Confirmer);