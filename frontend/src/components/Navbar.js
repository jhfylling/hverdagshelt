//React
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

//MaterialUI
import { withStyles } from '@material-ui/core/styles';
import Hidden from '@material-ui/core/Hidden';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

//Own
import { reportButtonColor, logoColor, color4 } from './Colors';
import LoginPopup from './LoginPopup';

const styles = (theme) => ({
	navWrapper: {
		flexGrow: 1,
	},
	grow: {
		flexGrow: 1,
		display: 'inline-block'
	},
	floatRight: {
		display: 'inline-block'
	},
	appBar: {
		zIndex: '2000',
		backgroundColor: reportButtonColor,
	},
	logoPrim: {
		fontFamily: '"roboto", sans-serif',
		fontWeight: '900'
	},
	logoSec: {
		color: color4,
	},
	button:{
		color: 'white',
		textDecoration: 'none',
		'&:hover':{
			color: 'white',
			textDecoration: 'underline',
		}

	},
	activeButton: {
		color: color4,
        textDecoration: 'none',
        '&:hover':{
            color: color4,
            textDecoration: 'underline',
        }
	},
	linkHover: {
		display: 'inline-block',
	 	'&:hover': {
	 		textDecoration: 'underline',
	 		color: color4
		}
	}
});

class Navbar extends Component {
	constructor(props) {
		super(props);

		this.state = {
			anchorEl: null,
			top: false,
			navPos: 'static',
			drawerPadding: '0px',
			loggedOut: false,
			currentComponent: 'home'
		};
	}

	toggleDrawer() {
		let x = this.state.top;
		this.setState({
			top: !x
		});
	}

	handleClick = (event) => {
		this.setState({ anchorEl: event.currentTarget });
	};

	handleClose = () => {
		this.setState({ anchorEl: null });
	};
	render() {
		window.onscroll = function() {
			let n = this.refs.THEBAR;
			//Set navbar position to fixed if user has scrolled down 100vh and set drawer's padding to be displayed below navbar.
			if (n !== undefined) {
				if (window.pageYOffset >= n.offsetTop && this.state.navPos === 'static') {
					this.setState({ navPos: 'fixed', drawerPadding: '56px' });
				} else if (window.pageYOffset <= n.offsetTop && this.state.navPos === 'fixed') {
					this.setState({ navPos: 'static', drawerPadding: '0px' });
				}
			}
		}.bind(this);

		const { classes } = this.props;
		const { cookies } = this.props;

		//Login / logout button & welcome message
		let welcomeMessage = null;
		let welcomeMessageTwo = null;
		let adminButton = null;
		let adminTwo = null;
		let myIssuesButton = null;
		let loginbutton = (
			<LoginPopup
				buttonText="Logg inn"
				divStyle={classes.floatRight}
				buttonStyle={classes.loginButton}
				cookies={this.props.cookies}
			/>
		);
		if (cookies.get('userInfo') !== undefined) {
			welcomeMessage = (
				<div style={{ display: 'inline-block' }}>
					<i className="fas fa-user-circle fa-lg" style={{ paddingRight: '5px' }} />
					<Link onClick={() => this.setState({currentComponent: 'myissues'})}
						  style={{ color: logoColor}} to={'/brukerprofil'}>
						<h5 className={classes.linkHover}>{cookies.get('userInfo').email}</h5>
					</Link>
				</div>
			);
			welcomeMessageTwo = (
				<List>
					<Link to="../../brukerprofil">
						<ListItem button>
							<ListItemText>
								<div style={{ display: 'inline-block' }}>
									<i className="fas fa-user-circle " style={{ paddingRight: '5px' }} />
									Din Profil
								</div>
							</ListItemText>
						</ListItem>
					</Link>
				</List>
			);
			loginbutton = (
				<Button color="inherit" onClick={this.logout.bind(this)}>
					<Link to="" className={classes.button}>
						Logg ut
					</Link>
				</Button>
			);
			myIssuesButton = (
				<Button color="inherit" onClick={() => this.setState({currentComponent: 'myissues'})}>
					<Link to="../../brukerprofil" className={this.state.currentComponent === 'myissues' ? classes.activeButton : classes.button}>
						Mine saker
					</Link>
				</Button>
			);
			if (cookies.get('userInfo').category_id === 2){
				adminButton = (
					<Button color="inherit" onClick={() => this.setState({currentComponent: 'admin'})}>
						<Link to={'/administrator'} className={this.state.currentComponent === 'admin' ? classes.activeButton : classes.button}>
							Admin
						</Link>
					</Button>
				);
			}
			else if (cookies.get('userInfo').category_id === 3) {
				adminButton = (
					<Button color="inherit" onClick={() => this.setState({currentComponent: 'admin'})}>
						<Link to={'/kommunaladmin/saker'} className={this.state.currentComponent === 'admin' ? classes.activeButton : classes.button}>
							Admin
						</Link>
					</Button>
				);
				adminTwo = (
					<List>
						<Link to="../../../kommunaladmin/saker">
							<ListItem button>
								<ListItemText>
									<div style={{ display: 'inline-block' }}>
										<i className="fas fa-unlock-alt " style={{ paddingRight: '5px' }} />
										Admin
									</div>
								</ListItemText>
								</ListItem>
						</Link>
					</List>
				);
			} else if (cookies.get('userInfo').category_id === 4) {
				adminButton = (
					<Button color="inherit" onClick={() => this.setState({currentComponent: 'admin'})}>
						<Link to={'/bedrift/saker'} className={this.state.currentComponent === 'admin' ? classes.activeButton : classes.button}>
							Admin
						</Link>
					</Button>
				);
				adminTwo = (
					<List>
						<Link to="../../../bedrift/saker">
							<ListItem button>
								<ListItemText>
									<div style={{ display: 'inline-block' }}>
										<i className="fas fa-unlock-alt " style={{ paddingRight: '5px' }} />
										Admin
									</div>
								</ListItemText>
							</ListItem>
						</Link>
					</List>
				);
			}
		}
		const fullList = (
			<div className={classes.fullList}>
				<List>
					<Link to="">
						<ListItem button>
							<ListItemText>
								<div style={{ display: 'inline-block' }}>
									<i className="fas fa-home" style={{ paddingRight: '5px' }} />
									Hjem
								</div>
							</ListItemText>
						</ListItem>
					</Link>
					<Link to="../../nysak">
						<ListItem button>
							<ListItemText>
								<div style={{ display: 'inline-block' }}>
									<i className="fas fa-pen" style={{ paddingRight: '5px' }} />
									Registrer Sak
								</div>
							</ListItemText>
						</ListItem>
					</Link>
				</List>
				<Divider />
				{adminTwo}
				{welcomeMessageTwo}
			</div>
		);

		return (
			<div style={{minHeight:'100px'}}>
				<div ref="THEBAR" className={classes.navWrapper}>
					<Hidden mdDown>
						<AppBar className={classes.appBar} position={this.state.navPos} >
							<Toolbar>
								<div className={classes.grow}>
									<Typography
										component={Link}
										to=""
										onClick={() => this.setState({currentComponent: 'home'})}
										variant="h6"
										style={{
											paddingRight: '20px',
											color: logoColor,
											display: 'inline-block',
										}}
									>
										<div className={classes.logoPrim}>
											Hverdags<span className={classes.logoSec}>Helt</span>
										</div>
									</Typography>

									<Button color="inherit" onClick={() => {this.setState({currentComponent: 'home'})}}>
										<Link to="" className={this.state.currentComponent === 'home' ? classes.activeButton : classes.button}>
											Hjem
										</Link>
									</Button>
									<Button color="inherit" onClick={() => {this.setState({currentComponent: 'newissue'})}}>
										<Link to="../../nysak" className={this.state.currentComponent === 'newissue' ? classes.activeButton : classes.button}>
											Registrer Ny Sak
										</Link>
									</Button>
									{myIssuesButton}
									{adminButton}
								</div>
								<div style={{ display: 'inline-block' }}>
									{welcomeMessage}
									{loginbutton}
								</div>
							</Toolbar>
						</AppBar>
					</Hidden>
					<Hidden lgUp>
						<AppBar position={this.state.navPos} className={classes.appBar}>
							<Toolbar>
								<div className={classes.grow}>
									<Typography
										component={Link}
										to=""
										variant="h6"
										style={{ paddingRight: '20px', color: logoColor, display: 'inline-block' }}
									>
										<div className={classes.logoPrim}>
											Hverdags<span className={classes.logoSec}>Helt</span>
										</div>
									</Typography>
								</div>
								<div style={{ display: 'inline-block' }}>
									<div style={{ display: 'inline-block' }}>
										<IconButton
											color="inherit"
											aria-label="Menu"
											onClick={this.toggleDrawer.bind(this)}
										>
											<MenuIcon />
										</IconButton>
									</div>
									<Drawer anchor="top" open={this.state.top} onClose={this.toggleDrawer.bind(this)}>
										<div
											tabIndex={0}
											role="button"
											onClick={this.toggleDrawer.bind(this)}
											onKeyDown={this.toggleDrawer.bind(this)}
											style={{paddingTop: this.state.drawerPadding}}
										>
											{fullList}
										</div>
									</Drawer>
									{loginbutton}
								</div>
							</Toolbar>
						</AppBar>
					</Hidden>
				</div>
			</div>
		);
	}

	logout() {
		const { cookies } = this.props;
		cookies.remove('x-access-token');
		cookies.remove('userInfo');
		
		this.setState({loggedOut: true});
	}
}
export default withStyles(styles)(Navbar);
