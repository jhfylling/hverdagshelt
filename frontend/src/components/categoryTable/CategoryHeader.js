//React
import React, { Component } from 'react';

//Material-UI
import { withStyles } from '@material-ui/core/styles';


let styles = {
    rowWrapper: {
        display: 'grid',
        gridTemplateColumns: '1fr 1fr 1fr 1fr',
        gridTemplateAreas: "' checkBox id description national'",
        textAlign:'center',
    },
    checkBox: {
        gridArea: 'checkBox',
        height: '40px',
    },
    id: {
      gridArea: 'id'
    },
    description: {
        gridArea: 'description'
    },
    national: {
        gridArea: 'national'
    },

};

class CategoryHeader extends Component {
    render(){
        const {classes} = this.props;
        return(
            <div className={classes.rowWrapper}>
                <div className={classes.checkBox}><b><i className="fas fa-check fa-sm"/>&nbsp; Velg</b></div>
                <div className={classes.id}><b><i className="fas fa-hashtag fa-sm"/>&nbsp; Kategori id</b></div>
                <div className={classes.description}><b><i className="fas fa-align-justify fa-sm"/>&nbsp; Beskrivelse</b></div>
                <div className={classes.national}><b><i className="fas fa-globe fa-sm"/>&nbsp; Nasjonal</b></div>
            </div>
        );
    }

}
export default withStyles(styles)(CategoryHeader);