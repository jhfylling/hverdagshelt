//React
import React, { Component } from 'react';

//Material-UI
import { withStyles } from '@material-ui/core/styles';
import Checkbox from '@material-ui/core/Checkbox';
import { issueService } from "../../serviceObjects/IssueService";
import PositionedSnackbar, {handleSnackbarOpen} from '../Snackbar';

let styles = theme => ({
    rowWrapper: {
        height: '60px',
        display: 'grid',
        gridTemplateColumns: '1fr 1fr 1fr 1fr',
        gridTemplateAreas: "'checkBox id description national'",
        textAlign:'center',
    },
    checkbox: {
        gridArea: 'checkBox',
    },
    cell: {
      margin: 'auto'
    },
    id: {
        gridArea: 'id'
    },
    description: {
        gridArea: 'description'
    },
    national: {
        gridArea: 'national'
    }
});

class CategoryRow extends Component {
    state ={
        checked: false,
        national: false,
    };

    handleChange = event => {
        this.props.handleCheckboxChange(this.props.cat.id, event.target.checked);
        
        this.setState({checked: event.target.checked});

    };

    handleChangeNational = event => {
        //update issue_category to national / not national
        issueService.updateIssueCategoryNational(this.props.cat.id, event.target.checked)
        .then((response) => {
            handleSnackbarOpen({ message: 'Kategori ' + this.props.cat.description + ' oppdatert!', variant: 'success' });
        })
        .catch((error) => {
            handleSnackbarOpen({ message: 'Kunne ikke oppdatere kategori!', variant: 'error' });
            console.error(error);
        });

        //Set box checked state
        this.setState({national: event.target.checked});
    };


    render(){
        const { classes } = this.props;
        return(
            <div className={classes.rowWrapper}>
                <Checkbox
                    checked={this.state.checked}
                    onChange={this.handleChange}
                />

                <div className={classes.id + ' '+ classes.cell}><b>{this.props.cat.id}</b></div>
                <div className={classes.description + ' '+ classes.cell}><b>{this.props.cat.description}</b></div>

                <Checkbox
                    checked={this.state.national}
                    onChange={this.handleChangeNational}
                    value={this.state.national.value}/>
                <PositionedSnackbar />
            </div>
        );
    }
}

export default withStyles(styles)(CategoryRow);