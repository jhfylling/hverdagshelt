/*eslint no-useless-concat: "off"*/
//Material-UI
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { withStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import Select from "@material-ui/core/Select/Select";
import OutlinedInput from "@material-ui/core/OutlinedInput/OutlinedInput";
import Button from '@material-ui/core/Button';
import ListSubheader from "@material-ui/core/ListSubheader";

//Own
import React, { Component } from "react";
import { issueService } from "../../serviceObjects/IssueService";
import CategoryHeader from "./CategoryHeader";
import CategoryRow from "./CategoryRow";
import PositionedSnackbar, {handleSnackbarOpen} from '../Snackbar';

let styles = {
    categoryWrapper: {
        disaply: 'grid',
        gridTemplateAreas: "'list'" + "'button'",

    },
    button: {
        gridArea: 'button',
        marginTop: '10px',
    },
    list: {
        gridArea: 'list',
        height: '60vh',
        overflow: 'auto'
    },
    info: {
        margin:'20px',
    }
};

class CategoryTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: [''],
            checkedRow: [],
            newCategory: null,
        };
    }

    componentDidMount() {
        this.getIssueCategories();
    }

    getIssueCategories = () => {
        issueService.getIssueCategories()
            .then((categories) => {
                this.setState({
                    categories: categories
                });
            })
            .catch((error) => console.error(error));
    }

    render() {
        const { classes } = this.props;
        return (
            <div className={classes.categoryWrapper}>
                <List className={classes.list}>
                    <ListSubheader style={{ background: 'white', paddingTop: '-50px' }}>
                        <ListItemText>
                            <CategoryHeader />
                            <Divider />
                        </ListItemText>
                    </ListSubheader>

                    {this.state.categories.map((cat, i) => (
                        <ListItem className={classes.ListItem} key={i} role={undefined} button>
                            <ListItemText>
                                <CategoryRow cat={cat} handleCheckboxChange={this.handleCheckboxChange} />{' '}
                            </ListItemText>
                        </ListItem>
                    ))}

                    {this.state.categories[0] == null ?
                        <div style={{ textAlign: 'center' }}>Du har ingen registrete kategorier</div> : <div></div>}
                </List>
                <Divider/>
                <div className={classes.info}>
                    <h5>Refaktorer valgt(e) kategori(er) til ny kategori: </h5>
                    <Select
                        native
                        value={this.state.newCategory}
                        onChange={this.handleNewCategoryChange}
                        autoWidth={true}
                        input={
                            <OutlinedInput
                            name="category"
                            id="outlined-age-native-simple"/>
                        }
                    >
                        {this.state.categories.map(category =>
                            <option value={category.id} key={category.id}>
                                {category.description}
                            </option>
                        )}
                    </Select>
                    <br/>
                    <Button variant='outlined' className={classes.button} onClick={this.handleClick}>Refaktorer</Button>
                </div>
                <PositionedSnackbar />
            </div>
        );
    }

    handleNewCategoryChange = (event) => {
        this.setState({newCategory: event.target.value});
    }

    handleCheckboxChange = (id, value) => {
        let tempCheckedRow = this.state.checkedRow;

        let foundElement = tempCheckedRow.find((element) => (element === id));

        if(foundElement !== undefined){
            tempCheckedRow.splice(tempCheckedRow.indexOf(foundElement));
        }
        else{
            if(value === true){
                tempCheckedRow.push(id);
            }
        }

        this.setState({checkedRow: tempCheckedRow});
    };

    handleClick = () => {
        if(this.state.newCategory !== null && this.state.checkedRow.length !== 0){

            console.log('Changing ', this.state.checkedRow, ' categories to ', this.state.newCategory);
            issueService.updateIssueCategoryAndDelete(this.state.newCategory, this.state.checkedRow)
            .then((response) => {
                handleSnackbarOpen({ message: 'Kategorier oppdatert!', variant: 'success' });
            })
            .catch((error) => {
                handleSnackbarOpen({ message: 'Kunne ikke oppdatere kategorier!', variant: 'error' });
            });
            
            let tempCategories = this.state.categories;
            
            this.state.checkedRow.forEach((id) => {
                let element = tempCategories.find((element) => {
                    return element.id === id;
                });
                if(element !== undefined){
                    tempCategories.splice(tempCategories.indexOf(element));
                }
            });
            
            this.setState({categories: tempCategories, checkedRow: [], newCategory: null});
        }
        else{
            handleSnackbarOpen({ message: 'Du må velge kategorier!', variant: 'error' });
        }
    };


}

export default withStyles(styles)(CategoryTable);