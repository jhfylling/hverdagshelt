//React
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

//Material-UI
import List from '@material-ui/core/List';
import ListSubheader from '@material-ui/core/ListSubheader';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import { withStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import LocationOnIcon from '@material-ui/icons/LocationOn';

//Own
import { issueService } from '../../serviceObjects/IssueService';
import ListRow from './ListRow';
import ListHeader from './ListHeader';

let styles = {
	list: {
		height: '60vh',
		overflow: 'auto'
	}
};

class IssueTable extends Component {
	constructor(props) {
		super(props);
		this.state = {
			issues: [ '' ]
		};
	}

	componentDidMount() {
		if (this.props.municipality_id) {
			issueService
				.getIssues(this.props.municipality_id, [2,3])
				.then((issues) => {
					if (issues !== undefined) {
						this.setState({ issues: issues });
					}
				})
				.catch((error) => console.log(error));
		} else if (this.props.user_email) {
			issueService
				.getIssuesByUser(this.props.user_email)
				.then((issues) => {
					if (issues !== undefined) {
						this.setState({ issues: issues });
					}
				})
				.catch((error) => console.log(error));
		} else if (this.props.issues) {
			this.setState({ issues: this.props.issues });
		}
	}
	componentDidUpdate(prevProps) {
		if (prevProps !== this.props) {
			if (this.props.municipality_id) {
				issueService
					.getIssues(this.props.municipality_id, [2,3])
					.then((issues) => {
						if (issues !== undefined) {
							this.setState({ issues: issues });
						}
					})
					.catch((error) => console.log(error));
			} else if (this.props.user_email) {
				issueService
					.getIssuesByUser(this.props.user_email)
					.then((issues) => {
						if (issues !== undefined) {
							this.setState({ issues: issues });
						}
					})
					.catch((error) => console.log(error));
			} else if (this.props.issues) {
				this.setState({ issues: this.props.issues });
			}
		}
	}

	render() {
		const { classes } = this.props;
		return (
			<List className={classes.list}>
				<ListSubheader style={{ background: 'white'}}>
					<ListItemText>
						<ListHeader />
						<Divider />
					</ListItemText>
				</ListSubheader>
				{this.state.issues.map((issue, i) => (
					<ListItem key={i} role={undefined} dense button component={Link} to={'../saker/' + issue.id}>
						<ListItemText>
							<ListRow issue={issue} />{' '}
						</ListItemText>


						{this.props.loadInfowindow ?
                            <ListItemSecondaryAction>
                                {this.props.company_user ? null : (
                                    <Button key={issue.id} value={issue.id} onClick={() => this.handleButton(issue.id)}>
                                        {this.props.user_email !== undefined ? null : (
                                            <LocationOnIcon className={classes.icon} />
                                        )}
                                    </Button>
                                )}
                            </ListItemSecondaryAction>
							:
							null
						}

                        {this.props.deleteIssue ?
                            <ListItemSecondaryAction>
                                {issue.status === 'Ubehandlet' ? (
                                    <Button key={issue.id} value={issue.id} onClick={() => this.handleButton(issue.id)}>
										<DeleteIcon className={classes.icon}/>
                                    </Button>
									) :
									null
                                }
                            </ListItemSecondaryAction>
                            :
                            null
                        }
					</ListItem>
				))}
				{this.state.issues[0] == null ? (
					<div style={{ textAlign: 'center' }}>Du har ingen registrete saker</div>
				) : (
					<div />
				)}
			</List>
		);
	}

	handleButton(issueId) {
		if (this.props.deleteIssue) {
			this.props.deleteIssue(issueId);
		} else if (this.props.loadInfowindow) {
			this.props.loadInfowindow(issueId);
		}
	}
}

export default withStyles(styles)(IssueTable);
