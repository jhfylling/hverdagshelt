import React, {Component} from 'react';

class DropdownMenu extends Component {
    render(){
        return(

            <div className="dropdown">
                <button className="btn btn-secondary dropdown-toggle"
                        type="button"
                        id="dropdownMenuButton"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                        onClick={this.props.children}
                        value={this.props.id}>
                    {this.props.title}
                </button>
                <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <button className="dropdown-item"
                            value={this.props.id}
                            onClick={this.props.children}>
                        {this.props.name}</button>
                </div>
            </div>
        );
    }
}

export default DropdownMenu;
//<a className="dropdown-item">{this.props.items}</a>