//React
import { Component } from 'react';
import React from 'react';

import { logoColor } from "./Colors";

//MaterialUI
import { withStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';

const styles = {
    root: {
        width: '90%',
    },
    backButton: {
        marginRight: '10px',
    },
    instructions: {
        marginTop: '10px',
        marginBottom: '10px',
    },
    progressPoint: {
        backgroundColor: logoColor
    },
    bgColor: {
        backgroundColor: 'white'
    }
};

function getSteps() {
    return ['Ubehandlet', 'Registrert', 'Under behandling', 'Ferdig'];
}

class StatusStepper extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeStep: 0,
            statuses:[
                'Ubehandlet',
                'Registrert',
                'Under behandling',
                'Ferdig',
            ]
        };
    }

    componentDidMount() {
        if(this.props.status){
            this.setState({activeStep: this.props.status - 1});
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.status !== prevProps.status) {
            if(this.props.status){
                this.setState({activeStep: this.props.status - 1})
            }
        }
    }


    render() {
        const {classes} = this.props;
        const steps = getSteps();
        return (
            <div className={classes.root}>
                <Stepper activeStep={this.state.activeStep} className={classes.bgColor} alternativeLabel>
                    {steps.map(label => {
                        return (
                            <Step key={label}>
                                <StepLabel>{label}</StepLabel>
                            </Step>
                        );
                    })}
                </Stepper>
            </div>
        );
    }
}

export default withStyles(styles)(StatusStepper);