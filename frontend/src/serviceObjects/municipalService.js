import axios from 'axios';

class MunicipalService{
    getIssues(token){
        return axios.get('/api/municipaladmin/issues', {
            headers: {
                'x-access-token': token,
            }
        });
    }
}

export let municipalService = new MunicipalService();