import axios from 'axios';

class IssueService {

    getIssues(municipality_id, span){
        return axios.get('/api/issues',{params: {
            municipality_id: municipality_id,
            span: JSON.stringify(span),
        }} );
    }

    getIssue(id){
        return axios.get('/api/issues/' + id);
    }

    getStatus(id){
        return axios.get('/api/issues/' + id + '/status');
    }

    disableIssue(id){
        return axios.delete('/api/issues/' + id);
    }

    setStatus(issue_id, status_id){
        var data = {issue_id: issue_id, status_id: status_id};
        return axios.post('/api/issues/statuses', data);
    }

    getIssuesByUser(email) {
        return axios.get('/api/issues/user/' + email);
    }

    createIssueUpdate(id, updatemessage){
        return axios.post('/api/issues/progress/' + id, updatemessage);
    }

    getProgressUpdates(id){
        return axios.get('/api/issues/progress/' + id);
    }

    getStatuses(){
        return axios.get('/api/statustypes');
    }

    updateIssue(id, title, issue_category_id, estimated_time){
        var data = {title: title, issue_category_id: issue_category_id, estimated_time: estimated_time};
        return axios.put('/api/issues/' + id, data);
    }

    getIssueCategories(){

        return axios.get('/api/issuecategories');
    }

    addIssueCategory(category){
        let data = {description: category};
        return axios.post('/api/issuecategories', data);
    }

    updateIssueCategoryAndDelete(newCategory, categories){
        let data = {newCategory: newCategory, categories: categories};
        return axios.delete('/api/issuecategories', {data: data});
    }
    
    updateIssueCategoryNational(category_id, national){
        return axios.put('/api/issuecategories/' + category_id, {national: national});
    }
    
    getIssuesByCompany(company_id){
        return axios.get('/api/issues/business/' + company_id);
    }
}
export let issueService = new IssueService();