import axios from 'axios';
axios.interceptors.response.use(response => response.data);

class UserService {

    login(email, password){
        var data = {email: email, password: password};
        return axios.post('/api/login', data);
    }

    //added token to get authentication to create company_user
    createUser(email, password, municipality_id, new_user_category_id, token){
        var data = {email: email, password: password, municipality_id: municipality_id, category_id: new_user_category_id};
        return axios.post('/api/register', data,{
            headers: {
                'x-access-token': token
            }
        });
    }

    getUser(){
        return axios.get('/api/user');
    }

    updateUser(user){
        return axios.put('/api/user', user);
    }

    resetPasswordLink(email, newUser){
        let data = {email: email, newUser: newUser};
        return axios.put('/api/user/resetpassword', data);
    }

    resetPassword(token, newPassword){
        let data = {newPassword: newPassword};
        return axios.put('/api/user/resetpassword/' + token, data);
    }

    getPoints(id){
        return axios.get('/api/user/points/' + id);
    }

    addPoint(id){
        return axios.post('/api/user/points/' + id);
    }

}
export let userService = new UserService();