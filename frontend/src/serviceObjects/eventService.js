import axios from 'axios';

class EventService {
    getEvents(municipality_id){
        return axios.get('/api/events/municipality/' + municipality_id);
    }

    getEvent(id){
        return axios.get('/api/events/' + id);
    }
    getEventInCategory(event_category_id){
        return axios.get('/api/events/category/' + event_category_id);
    }

    createEvent(title,description,longitude,latitude,price,event_homepage,event_category_id,municipality_id){
        var data= {title: title, description: description, longitude: longitude, latitude: latitude, price: price, event_homepage: event_homepage, event_category_id: event_category_id, municipality_id: municipality_id};
        return axios.post('/api/events/',data);
    }
    updateEvent(id,event){
        return axios.put('/api/events/'+id,event);
    }
    getEventCategories(){
        return axios.get('/api/eventcategories');
    }
    deleteEvent(id){
        return axios.delete('/api/events/'+id);
    }
}
export let eventService = new EventService();
