import axios from 'axios';

class BusinessService{
    getBusinessByUserId(id){
        return axios.get('/api/user/' + id + '/business');
    }

    getIssues(id, token){
        return axios.get('/api/issues/business/' + id, {
            headers: {
                'x-access-token': token,
            }
        });
    }

    getCompaniesJoinCompanyUsers(municipality_id){
        return axios.get('/api/business/users/municipality/' + municipality_id);
    }

    getCompaniesByMunicipality(municipality_id){
        return axios.get('/api/business/municipality/' + municipality_id)
    }

    assignCompany(company_id, issue_id){
        var data = {issue_id: issue_id};
        return axios.post('/api/issues/' + issue_id + '/issue_company/company/' + company_id, data);
    }

    createCompany(name, description){
        var data = {name: name, description: description};
        return axios.post('/api/business',data)
    }

    createCompanyUserAssociation(email,company_name){
        var data = {email: email, company_name: company_name};
        return axios.post('/api/business_user', data)
    }

    disableCompanyUserAccount(companyid, userid) {
        let data = {companyid: companyid, userid: userid};
        return axios.delete('/api/business_user',{
            data: data,
        })
    }

}
export let businessService = new BusinessService();