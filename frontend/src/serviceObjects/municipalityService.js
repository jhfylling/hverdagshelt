import axios from 'axios';

class MunicipalityService{
    getMunicipalities(){
        return axios.get('/api/municipalities');
    }

    getMunicipality(id){
        return axios.get('/api/municipalities/'+id);
    }
    getMunicipalitiesWithZipCode(zipcode){
        return axios.get('api/municipalities/name/' + zipcode);
    }
}
export let municipalityService = new MunicipalityService();