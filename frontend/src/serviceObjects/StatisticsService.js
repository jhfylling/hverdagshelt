import axios from "axios";

class StatisticsService{

    getReport(municipality_id, start_date, end_date){
        return axios.get('/api/statistics', {
            params: {
                municipality_id: municipality_id,
                start_date: start_date,
                end_date: end_date
            }
        });
    }
}

export let statisticsService = new StatisticsService();