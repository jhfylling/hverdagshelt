//React
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {BrowserRouter, Route, Switch} from 'react-router-dom';


//React-cookie
import {CookiesProvider} from 'react-cookie';

//Own
import Print from './containers/statistics/Print'


ReactDOM.render(
    <CookiesProvider>
        <BrowserRouter>
        <Switch>
            <Route exact path="/statistics/print/:municipality_id/:start/:end" component={Print}/>
            <Route path="/" component={App}/> 
        </Switch>
            
        </BrowserRouter>
    </CookiesProvider>,
document.getElementById('root'));
