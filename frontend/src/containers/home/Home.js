/*eslint no-useless-concat: "off"*/
// React
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

// MaterialUI
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import OutlinedInput from "@material-ui/core/OutlinedInput";
import InputLabel from "@material-ui/core/InputLabel";

//Own
import { municipalityService } from '../../serviceObjects/municipalityService';
import IssueTable from '../../components/issueTable/IssueTable';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import EventTable from '../../components/eventTable/EventTable';
import {siteBackground, reportButtonColor, logoColor} from '../../components/Colors';
import MapWrapper from "../../components/map/MapWrapper";
import CardHeader from "@material-ui/core/CardHeader";

let styles = (theme) => ({
	tableWrapper: {
		textAlign: 'center',
		display: 'grid',
		gridTemplateColumns: '1fr 1fr',
		gridTemplateAreas: "' header header '" + "' tables maps '" + "' eventHeader eventHeader '" + "' event event '",
		backgroundColor: siteBackground,
		[theme.breakpoints.down('sm')]: {
			display: 'grid',
			gridTemplateColumns: '1fr',
			gridTemplateAreas: "' header'" + "' tables'" + "'  maps  '" + "' eventHeader '" + "' event '"
		}
	},
	headerWrapper: {
		gridArea: 'header',
		position: 'relative',
		textAlign: 'center',
		marginLeft: '4px',
		marginRight: '4px',
		top: '4px'
		//height: '20vh',
	},
	header: {
		textAlign: 'center',
		backgroundColor: reportButtonColor,
		padding: '10px',
	},
	headerText: {
		color: 'white',
		position: 'absolute',
		top: '80%',
		left: '50%',
		transform: 'translate(-50%, -50%)',
		textDecoration: 'underline'
	},
	choiceBox: {
		widht: '30%',
		textAlign: 'center',
		display: 'inline-block',
		position: 'absolute',
		padding: '20px'
	},
	dropMenues: {
		display: 'inline-block',
		position: 'absolute',
		top: '90%',
		left: '50%',
		transform: 'translate(-50%, -50%)'
	},
	tables: {
		gridArea: 'tables',
		[theme.breakpoints.down('sm')]: {
			marginBottom: '20px',
		}
	},
	maps: {
		gridArea: 'maps',
		width: '100%',
		height: '50vh'
	},
	eventHeaderWrapper: {
		gridArea: 'eventHeader',
		position: 'relative',
		textAlign: 'center',
		marginTop: '2%',
		marginLeft: '4px',
		marginRight: '4px',
		top: '4px',
		[theme.breakpoints.down('sm')]: {
			marginTop: '45%'
		}
	},
	eventGradient: {
		height: '20vh',
		backgroundColor: reportButtonColor,
	},
	event: {
		gridArea: 'event'
	},
	cardStyle: {
		margin: '4px'
	}
});

class Home extends Component{
    constructor(props) {
		super(props);
		this.state = {
			municipalities: [ '' ],
			municipality_id: 0,
			municipality_name: '',
			details: { type: '', user: 'Private' },
			positionMap: {
				lat: 0,
				lng: 0
			},
			infoWindowId: -1,
			labelWidth: 0
		};
		this.getCoordinatesParent = this.getCoordinatesParent.bind(this);
		this.handleChangeMunicipality = this.handleChangeMunicipality.bind(this);
		this.loadInfowindow = this.loadInfowindow.bind(this);
	}
	/**
     * Gets coordinates from map.
     *
     */
	getCoordinatesParent(lat, lng) {
		this.setState({
			positionMap: {
				lat: lat,
				lng: lng
			}
		});
	}
    

	updateDimensions() {
		let boxWidth = document.getElementById('mapGridBox').offsetWidth;
		let boxHeight = document.getElementById('mapGridBox').offsetHeight * 1.44;

		this.setState({ mapStyle: { width: boxWidth, height: boxHeight, position: 'absolute' } });
	}
	
	componentDidMount() {
		this.setState({ labelWidth: ReactDOM.findDOMNode(this.InputLabelRef).offsetWidth});
		municipalityService
			.getMunicipalities()
			.then((municipality) => {
				if (municipality !== undefined) {
					this.setState({ municipalities: municipality });
				}
			})
			.catch((error) => console.log(error));

		this.updateDimensions = this.updateDimensions.bind(this);
		window.addEventListener('resize', this.updateDimensions);

		this.updateDimensions();
	}

	componentWillUnmount(){
        window.removeEventListener('resize', this.updateDimensions);
    }

	handleChangeMunicipality = (e) => {
		let municipality_selected  = this.state.municipalities.find(municipality => municipality.municipality_name === e.target.value).id;
		this.setState({ municipality_id: municipality_selected, municipality_name: e.target.value});
	};

	render() {
		const { classes } = this.props;
		return (
			<div className={classes.tableWrapper}>

				<div className={classes.headerWrapper}>
					<Card>
						<div className={classes.header}>
							<h2 style={{ color: logoColor }}>Saker</h2>
						</div>
					</Card>
				</div>

				<div className={classes.tables}>
					<Card className={classes.cardStyle}>
						<IssueTable
							municipality_id={this.state.municipality_id !== 0 ? this.state.municipality_id : 1}
							cookies={this.props.cookies}
                            loadInfowindow={this.loadInfowindow}
						/>
						<div style={{ textAlign: 'left', margin: '30px' }}>
							<h5 style={{ display: 'inline-block', paddingTop: '15px', paddingRight: '15px'}}>Velg Kommune: </h5>
							<FormControl variant="outlined" style={{ minWidth: '150px' }}>
								<InputLabel
									ref={ref => {
										this.InputLabelRef = ref;
									}}
									htmlFor="outline-age-native-simple">
									Velg kommune
          								</InputLabel>
								<Select native value={this.state.municipality_name} onChange={this.handleChangeMunicipality} input={
									<OutlinedInput
										name="age"
										labelWidth={this.state.labelWidth}
										id="outlined-age-native-simple"
									/>
								}>
								<option> </option>
									{this.state.municipalities.map((e, i) => (
										<option value={e.municipality_name} key={i}>
											{e.municipality_name}{' '}
										</option>
									))}
								</Select>
							</FormControl>
						</div>
					</Card>
				</div>
				<Card className={classes.cardStyle}>
					<div className={classes.maps} id="mapGridBox">
						<div className={classes.mapCard}>
							<MapWrapper
								mapStyle={this.state.mapStyle}
								getCoordinates={this.getCoordinatesParent}
								cookies={this.props.cookies}
								infoWindowId={this.state.infoWindowId}
								municipality_name={this.state.municipality_name !== '' ? this.state.municipality_name : 'Trondheim'}
                                municipality_id={this.state.municipality_id !== 0 ? this.state.municipality_id : 1}
							/>
						</div>
					</div>
				</Card>
				<div className={classes.eventHeaderWrapper}>
					<Card>
						<div className={classes.header}>
							<h2 style={{ color: logoColor }}>Arrangementer</h2>
						</div>
					</Card>
				</div>
				<div className={classes.event}>
					<Card className={classes.cardStyle}>
						<div>
							<EventTable
								municipality_id={this.state.municipality_id !== 0 ? this.state.municipality_id : 1}
								cookies={this.props.cookies}
							/>
						</div>
						<div style={{ textAlign: 'left', margin: '30px' }}>
							<h5 style={{ display: 'inline-block', paddingTop: '15px', paddingRight: '15px' }}>Velg Kommune: </h5>
							<FormControl variant="outlined" style={{ minWidth: '150px' }}>
								<InputLabel
									ref={ref => {
										this.InputLabelRef = ref;
									}}
									htmlFor="outline-age-native-simple">
									Velg kommune
									</InputLabel>
								<Select native value={this.state.municipality_name} onChange={this.handleChangeMunicipality} input={
									<OutlinedInput
										name="age"
										labelWidth={this.state.labelWidth}
										id="outlined-age-native-simple"
									/>
								}>
									<option> </option>
									{this.state.municipalities.map((e, i) => (
										<option value={e.municipality_name } key={i}>
											{e.municipality_name}{' '}
										</option>
									))}
								</Select>
							</FormControl>
						</div>
					</Card>
				</div>
			</div>
		);
	}

    loadInfowindow(issueId){
		this.setState({infoWindowId: issueId});

	}

}
export default withStyles(styles)(Home);