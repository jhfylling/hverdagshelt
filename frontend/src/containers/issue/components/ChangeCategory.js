//React
import React, { Component } from 'react';

//Material-ui
import { withStyles } from '@material-ui/core/styles';
import Select from "@material-ui/core/Select/Select";
import OutlinedInput from "@material-ui/core/OutlinedInput/OutlinedInput";
import FormControl from "@material-ui/core/FormControl/FormControl";
import Button from "@material-ui/core/Button/Button";
import TextField from "@material-ui/core/TextField/TextField";

let styles = theme =>({
    header: {
        marginBottom: '15px',
        marginTop: '15px'
    },
    changeCategoryWrapper: {
        minWidth: '100%'
    },
    button: {
        marginTop: '10px',
        height: '60px',
        width: '30%',
        marginBottom: '20px',
         [theme.breakpoints.down('sm')]: {
            width: '60%'
        },
    }
});

class ChangeCategory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: [],
            category: ''
        }
    }

    componentDidMount() {
        this.setState({
            categories: this.props.categories,
        });
    }

    componentDidUpdate(prevProps){
        if(this.props !== prevProps){
            this.setState({
                categories: this.props.categories
            });
        }
    }

    render(){
        const {classes, cookies} = this.props;
        if(cookies.get('userInfo').category_id === 3) {
            return (
                <FormControl variant="outlined" className={classes.changeCategoryWrapper}>
                    <h5 className={classes.header}>Velg kategori </h5>
                    <Select
                        native
                        value={this.props.category_id}
                        onChange={this.handleChange}
                        input={
                            <OutlinedInput
                                name="category"
                                labelWidth={0}
                                id="outlined-age-native-simple"/>
                        }
                    >
                        {this.state.categories.map(category =>
                            <option value={category.id} key={category.id}>
                                {category.description}
                            </option>
                        )}
                    </Select>

                    <TextField
                        id="newCategory"
                        label="Ny kategori"
                        fullWidth
                        margin="normal"
                        variant="outlined"
                    />
                    <Button onClick={this.addCategory} variant="outlined" className={classes.button}>Legg til ny kategori</Button>
                </FormControl>
            );
        }else{
            return null;
        }
    }

    handleChange = (event) => {
        if(this.props.changeCategory){
            this.props.changeCategory(event.target.value);
        }
    };

    addCategory = () => {
        let newCategory = document.getElementById('newCategory').value;
        if(this.props.addCategory){
            this.props.addCategory(newCategory);
            document.getElementById('newCategory').value = '';
        }
    }
}

export default withStyles(styles)(ChangeCategory);