//React
import React, { Component } from 'react';

//MaterialUI
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import FormControl from "@material-ui/core/FormControl/FormControl";

let styles = {
    changeEstimatedTimeWrapper: {
        display: 'flex',
        flexWrap: 'wrap',
        minWidth: '100%'
    },
    header: {
        marginBottom: '15px',
        marginTop: '15px'
    },
};

class ChangeEstimatedTime extends Component{
    render(){
        const {classes, cookies} = this.props;
        if(cookies.get('userInfo').category_id === 3 || (cookies.get('userInfo').category_id === 4 && this.props.companyAssigned)){
            return(
                <FormControl className={classes.changeEstimatedTimeWrapper}>
                    <h5 className={classes.header}>Endre estimert tid til utbedring </h5>
                    <TextField
                        margin="dense"
                        variant="outlined"
                        label="Estimert tid"
                        type="date"
                        onChange={this.handleChange}
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                </FormControl>
            );
        }else{
            return null;
        }
    }

    handleChange = (event) =>{
        if(this.props.changeEstimatedTime){
            this.props.changeEstimatedTime(event.target.value);
        }
    }
}
export default withStyles(styles)(ChangeEstimatedTime);