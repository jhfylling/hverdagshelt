//React
import React, { Component } from 'react';

//MaterialUI
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import FormControl from "@material-ui/core/FormControl/FormControl";


let styles = {
    changeTitleWrapper: {
        minWidth: '100%'
    },
    header: {
        marginBottom: '15px',
        marginTop: '15px'
    },
};

class ChangeTitle extends Component{
    constructor(props){
        super(props);
        this.state = {
            title: ''
        }

    }
    componentDidMount() {
        this.setState({
            title: this.props.title
        });
    }

    componentDidUpdate(prevProps){
        if(this.props !== prevProps){
            this.setState({
                title: this.props.title
            });
        }
    }

    handleChange = (event) => {
        this.props.changeTitle(event.target.value);
    };

    render(){
        const {classes, cookies} = this.props;
        if(cookies.get('userInfo').category_id === 3){
            return(
                <FormControl variant="outlined" className={classes.changeTitleWrapper}>
                    <h5 className={classes.header}>Endre tittel </h5>
                    <TextField
                        label="Tittel"
                        variant="outlined"
                        value={this.state.title}
                        onChange={this.handleChange}
                        margin="dense"
                    />
                </FormControl>
            );
        }else{
            return null;
        }

    }
}
export default withStyles(styles)(ChangeTitle);