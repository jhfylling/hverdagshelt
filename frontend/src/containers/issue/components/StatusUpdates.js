//React
import React from 'react';
import { Component } from 'react';

//Material UI

import Button from "@material-ui/core/Button/Button";
import { withStyles } from '@material-ui/core/styles';
import ListItem from "@material-ui/core/ListItem/ListItem";
import ListItemText from "@material-ui/core/ListItemText/ListItemText";
import Icon from "@material-ui/core/Icon/Icon";
import TextField from "@material-ui/core/TextField";



let styles = theme => ({
    statusUpdatesWrapper: {
        marginTop: '20px'
    },

    statusUpdates: {
        width: '100%'
    },
    newStatusUpdate: {
        marginTop: '100px'
    },
    dense: {
        marginTop: 16
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    button: {
        marginTop: '10px',
        height: '60px',
        width: '30%',
        marginBottom: '20px'
    }
});

class StatusUpdates extends Component {
    constructor(props){
        super(props);
        this.state = {
            comments: null,
            labelWidth: 0,
            updates: null
        };
    }
    ws;

    componentDidMount(){

        this.onOpen = this.onOpen.bind(this);
        this.onMessage = this.onMessage.bind(this);
        this.initWebSocket();
    }

    render() {
        const {classes} = this.props;
        let statusList = "";
        if(this.state.updates && this.state.updates[0]){
            statusList =
            Object.values(this.state.updates).map((update) => {
                return(
                    <div className={classes.statusUpdates} key={update.id}>
                        <ListItem divider={true}>
                            <Icon className={'fa fa-info'}/>
                            <ListItemText primary={update.updatemessage} secondary={update.update_date} />
                        </ListItem>
                    </div>
                );
            });
        }else{
           statusList =
                <div className={classes.statusUpdates}>
                    <ListItem divider={true}>
                        <ListItemText primary={'Ingen statusoppdateringer er satt for denne saken..'}/>
                    </ListItem>
                </div>
        }

        let result =
        <div className={classes.statusUpdatesWrapper}>
            <h5 >Statusoppdateringer</h5>
            {statusList}
            <div className={classes.newStatusUpdate}>
                <h5>Skriv en statusoppdatering</h5>
                    <form className={classes.container}>
                            <TextField
                                id={'newCommentBody'}
                                fullWidth
                                multiline
                                label={'Statusoppdatering'}
                                margin="normal"
                                variant="outlined"
                                rows="3"
                            />
                            <Button fullWidth
                                variant="outlined"
                                onClick={this.newUpdate.bind(this)}
                                id="newCommentButton"
                                className={classes.button}
                            >Lagre</Button>
                    </form>
            </div>
        </div>
        if(this.props.cookies.get('userInfo') !== undefined){
            if(this.props.cookies.get('userInfo').category_id === 3 || (this.props.cookies.get('userInfo').category_id === 4 && this.props.companyAssigned)) {
                return result;
            }else{
                return (
                    <div className={classes.statusUpdatesWrapper}>
                        <h5 className="comment-container-title">Statusoppdateringer</h5>
                        {statusList}
                    </div>
                )
            }
        }else{
            return (
                <div className={classes.statusUpdatesWrapper}>
                    <h5 className="comment-container-title">Statusoppdateringer</h5>
                    {statusList}
                </div>
            )
        }
    }

    newUpdate(){
        let newCommentBody = document.getElementById('newCommentBody');
        //if(newCommentBody && !(newCommentBody instanceof HTMLInputElement)) throw new Error('Expected an HTMLInputElement');
        if(newCommentBody){
            if(newCommentBody.value !== ""){
                this.uploadUpdate(newCommentBody.value);
            }
        }
    }

    initWebSocket(){
        this.ws = new WebSocket("ws://localhost:3002");
        // this.ws = new WebSocket("ws://hverdagshelt.net:3002");

        this.ws.onopen = (event) => {
            this.onOpen(event);
        }

        this.ws.onmessage = (message) => {
            this.onMessage(message);
        }
    }

    onOpen(event){
        let message = {validator: 'safgn90aasdgfvvv!!z3c5xjy08', operation: 'load', id: this.props.issue_id};
        this.ws.send(JSON.stringify(message));
    }

    onMessage(message){
        this.setState({updates: JSON.parse(message.data)});
    }

    uploadUpdate = (updatemessage) =>{
        let message = {validator: "safgn90aasdgfvvv!!z3c5xjy08", operation: 'save',
            id: this.props.issue_id, data: {updatemessage: updatemessage,
                id: this.props.issue_id, user_id: this.props.cookies.get('userInfo').id}};
        this.ws.send(JSON.stringify(message));
    }
}

export default withStyles(styles)(StatusUpdates);