//React
import React, { Component } from 'react';

//MaterialUI
import { withStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import OutlinedInput from '@material-ui/core/OutlinedInput';

let styles = {
    header: {
        marginBottom: '15px',
        marginTop: '15px'
    },
    changeCompanyWrapper: {
        minWidth: '100%'
    },
};

class ChangeStatus extends Component {
    constructor(props) {
        super(props);
        this.state = {
            companies: [],
            company_selected: "",
        }
    }

    componentDidMount(){
        this.setState({
            companies: this.props.companies,
            company_selected: this.props.currentCompany
        });
    }

    componentDidUpdate(prevProps) {
        if (this.props !== prevProps) {
            this.setState({
                companies: this.props.companies,
                company_selected: this.props.currentCompany
            });
        }
    }
    render() {
        const {classes, cookies} = this.props;
        if(cookies.get('userInfo').category_id === 3){
            return (
                <FormControl variant="outlined" className={classes.changeCompanyWrapper}>
                    <h5 className={classes.header}>Velg bedrift </h5>
                    <Select
                        native
                        value={this.props.company_id}
                        onChange={this.handleChange}
                        input={
                            <OutlinedInput
                                name="company"
                                labelWidth={0}
                                />
                        }
                    >   <option >
                        Velg bedrift
                    </option>
                        {this.state.companies.map(companies =>
                            <option value={companies.id} key={companies.id}>
                                {companies.name}
                            </option>
                        )}
                    </Select>
                </FormControl>
            );
        }else {
            return null;
        }
    }
    handleChange = (event) => {
        if(this.props.changeCompany){
            this.props.changeCompany(event.target.value);
        }
    };
}
export default withStyles(styles)(ChangeStatus);
