//React
import React, { Component } from 'react';

//MaterialUI
import { withStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import OutlinedInput from '@material-ui/core/OutlinedInput';


let styles = {
    header: {
        marginBottom: '15px',
        marginTop: '15px'
    },
    changeStatusWrapper: {
        minWidth: '100%'
    }
};

class ChangeStatus extends Component {
    constructor(props) {
        super(props);
        this.state = {
            issueStatuses: [],
            currentStatus: 1,
            companyAssigned: false
        }
    }

    componentDidMount(){
        this.setState({
            issueStatuses: this.props.issueStatuses,
            currentStatus: this.props.currentStatus,
            companyAssigned: this.props.companyAssigned
        });
    }

    componentDidUpdate(prevProps) {
        if (this.props !== prevProps) {
            this.setState({
                issueStatuses: this.props.issueStatuses,
                currentStatus: this.props.currentStatus,
                companyAssigned: this.props.companyAssigned
            });
        }
    }
    render() {
        const {classes, cookies} = this.props;
        if(cookies.get('userInfo').category_id === 3 || (cookies.get('userInfo').category_id === 4 && this.state.companyAssigned)){
            return (
                <FormControl variant="outlined" className={classes.changeStatusWrapper}>
                    <h5 className={classes.header}>Oppdater status </h5>
                    <Select
                        native
                        value={this.state.currentStatus}
                        onChange={this.handleChange}
                        input={
                            <OutlinedInput
                                name="status"
                                labelWidth={0}
                                id="outlined-age-native-simple"
                            />
                        }
                    >
                        {this.state.issueStatuses.map(issueStatus =>
                            <option value={issueStatus.id} key={issueStatus.id}>
                                {issueStatus.type}
                            </option>
                        )}
                    </Select>
                </FormControl>
            );
        }else {
            return null;
        }
    }


    handleChange = (event) => {
        if(this.props.changeStatus){
            this.props.changeStatus(event.target.value);
        }
    };


}
export default withStyles(styles)(ChangeStatus);

