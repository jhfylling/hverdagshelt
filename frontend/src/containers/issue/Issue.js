//React
import React, { Component } from 'react';
import Geocode from 'react-geocode';
import { withRouter } from 'react-router-dom';

//MaterialUI
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card/Card';

//Own
import { issueService } from '../../serviceObjects/IssueService';
import { businessService } from '../../serviceObjects/businessService';
import StatusStepper from '../../components/StatusStepper.js';
import ChangeStatus from './components/ChangeStatus';
import PositionedSnackbar from '../../components/Snackbar';
import ChangeTitle from './components/ChangeTitle';
import { handleSnackbarOpen } from '../../components/Snackbar';
import ChangeCategory from './components/ChangeCategory';
import { logoColor, reportButtonColor } from '../../components/Colors';
import ChangeCompany from './components/ChangeCompany';
import StatusUpdates from './components/StatusUpdates';
import ChangeEstimatedTime from './components/ChangeEstimatedTime';
import { userService } from '../../serviceObjects/userService';

let styles = (theme) => ({
	mainWrapper: {
		minHeight: '500px',
		display: 'grid',
		gridTemplateColumns: '1fr 8fr 1fr',
		gridTemplateAreas: "'. issue .'",
		[theme.breakpoints.down('sm')]: {
			display: 'grid',
			gridTemplateColumns: '1fr 100fr 1fr',
			gridTemplateAreas: "'. issue .'"
		}
        
        
	},
	wrapper: {
		display: 'grid',
		gridArea: 'issue',
		gridTemplateColumns: '1fr 3fr 3fr 1fr',
		gridTemplateAreas:
			'' +
			"'title title title title'" +
			"'. step step .'" +
			"'. description imageWrapper .'" +
			"'. statusUpdates statusUpdates .'" +
			"'. selectStatus selectStatus .'" +
			"'. selectEstimatedTime selectEstimatedTime .'" +
			"'. selectCompany selectCompany .'" +
			"'. selectCategory selectCategory .'" +
			"'. changeTitle changeTitle .'" +
			"'. saveButton saveButton .'" +
			"'. deleteButton deleteButton .'",
		[theme.breakpoints.down('sm')]: {
			gridTemplateColumns: '1fr 30fr 1fr',
			gridTemplateAreas:
				'' +
				"'title title title'" +
				"'.step .'" +
				"'. imageWrapper .'" +
				"'. description .'" +
				"'. statusUpdates .'" +
				"'. selectStatus .'" +
				"'. selectEstimatedTime .'" +
				"'. selectCompany .'" +
				"'. selectCategory .'" +
				"'. changeTitle .'" +
				"'. saveButton .'" +
				"'. deleteButton .'",

		},
		marginTop: '20px'
	},
	 statusUpdates: {
        gridArea: 'statusUpdates'
    },
    setStatus: {
        gridArea: 'selectStatus',
        marginTop: '20px'
    },

    setEstimatedTime:{
        gridArea: 'selectEstimatedTime'
    },

    setCompany: {
        gridArea: 'selectCompany',
        marginTop: '20px'
    },

    setCategory: {
        gridArea: 'selectCategory',
        marginTop: '20px'
    },
    setTitle: {
        gridArea: 'changeTitle',
        marginTop: '20px'
    },
    issueImageWrapper: {
        gridArea: 'imageWrapper',
        width: '100%',
        height: 'auto',
        maxWidth: '100%',
        maxHeight: '100%',
        minWidth: '100%',
    },
    issueImage: {
        display: 'block',
        width: '100%',
        height: 'auto'
    },
    issueStep: {
        gridArea: 'step',
        width: '100%',
        marginLeft: '5%',
        marginRight: 'auto',
        [theme.breakpoints.down('sm')]: {
            marginLeft: '0%'
        },
    },
    issueTitle: {
        gridArea: 'title',
        textAlign: 'center',
        backgroundColor: reportButtonColor,
        padding: '10px'
    },
    issueDescription: {
        gridArea: 'description'
    },
    saveButton: {
        gridArea: 'saveButton',
        marginTop: '40px',
        height: '60px',
        width: '30%',
        marginBottom: '10px',
        [theme.breakpoints.down('sm')]: {
            width: '60%'
        },

    },
    saveButtonHidden:{
        display: 'none'
    },
    deleteButtonHidden: {
        display: 'none',
    },
    deleteButton: {
        gridArea: 'deleteButton',
        marginTop: '10px',
        height: '60px',
        width: '30%',
        marginBottom: '20px',
         [theme.breakpoints.down('sm')]: {
            width: '60%'
        },
    },
    dense: {
        fontStyle: 'italic',
        fontWeight: 'lighter',
        marginBottom: '0rem'
    },
    descBody: {
        marginTop: '1rem',
    },
    subheader: {
        margin: '0',
        fontWeight: 'bolder'
    },
    infoBody: {
        marginBottom: '1rem'
    },
    linkRedirectButton: {
        color: 'black',
        textDecoration: 'none',
        '&:hover': {
            color: 'black',
            textDecoration: 'none'
        }
    },
});

class Issue extends Component {

	constructor(props) {
		super(props);
		this.state = {
			issue: {},
			issueStatuses: [],
			address: '',
			companies: [],
			categories: [],
			selectedCompany: {},
			companyAssigned: false,
			companyChanged: false,
			statusChanged: false,
			company_id: -1,
			userPoints: 0
		};
	}

	componentDidMount() {
		let id = window.location.href.split('/');
		const { cookies } = this.props;

		issueService
			.getIssue(id[id.length - 1])
			.then((issue) => {
				this.setState({ issue: issue[0] });
				businessService
					.getCompaniesByMunicipality(issue[0].municipality_id)
					.then((companies) => {
						this.setState({ companies: companies });
					})
					.catch((error) => console.error('Error: ', error));
			})
			.then((response) => {
				let address = response.results[0].formatted_address;
				this.setState({ address: address });
			})
			.catch((error) => console.error('Error: ', error));

		issueService
			.getStatuses()
			.then((statuses) => {
				this.setState({ issueStatuses: statuses });
			})
			.then(() => {
				Geocode.setApiKey('AIzaSyATAz6K9aKSkbyQMLbivjIrp5KoPJ8_rX4');
				Geocode.fromLatLng(this.state.issue.latitude, this.state.issue.longitude)
					.then((response) => {
						let address = response.results[0].formatted_address;
						this.setState({ address: address });
					})
					.catch((error) => {
						console.error(error);
					});
			})
			.catch((error) => console.error('Error: ', error));

		issueService
			.getStatuses()
			.then((statuses) => {
				this.setState({ issueStatuses: statuses });
			})
			.catch((error) => console.log('Error: ', error));

		issueService
			.getIssueCategories()
			.then((categories) => {
				this.setState({ categories: categories });
			})
			.catch((error) => {
				console.error(error);
			});

		if (cookies.get('userInfo') !== undefined) {
			if (cookies.get('userInfo').category_id === 4) {
				businessService
					.getBusinessByUserId(cookies.get('userInfo').id)
					.then((company) => {
						issueService
							.getIssuesByCompany(company[0].id)
							.then((issues) => {
								let issue = issues.find((i) => i.id === this.state.issue.id);
								if (issue) {
									this.setState({ companyAssigned: true });
								}
							})
							.catch((error) => {
								console.log(error);
							});
					})
					.catch((error) => {
						console.log('AdministerIssues: Error finding business: ', error);
					});
			}
		}
	}

	render() {
		const { classes } = this.props;
		let noCategoryChosen = 'Ingen kategori er valgt for denne saken';
		if (this.props.cookies.get('userInfo') !== undefined) {
			return (
				<div className={classes.mainWrapper}>
					<Card className={classes.wrapper}>
						<div className={classes.issueTitle}>
							<h2 style={{ color: logoColor }}> Saksinformasjon </h2>
						</div>

						<div className={classes.issueStep}>
							<StatusStepper
								status={this.state.issue.status_id}
								issueStatuses={this.state.issueStatuses}
							/>
						</div>
						<div className={classes.issueDescription}>
							<h2>
								{this.state.issue.title === undefined ? 'Ingen tittel satt' : this.state.issue.title}
							</h2>
							<p className={classes.dense}>
								{this.state.address === '' ? 'Ingen adresse spesifisert' : this.state.address}
							</p>
							<p className={classes.dense}>{this.state.issue.date}</p>
							<p className={classes.descBody}>{this.state.issue.description}</p>
							<div>
								<h6 className={classes.subheader}>Estimert tid til utbedring</h6>
								<p className={classes.infoBody}>
									{this.state.issue.estimated_time === null ? (
										'Ikke satt'
									) : (
										this.state.issue.estimated_time
									)}
								</p>
								<h6 className={classes.subheader}>Kategori</h6>
								<p className={classes.infoBody}>
									{this.state.issue.issue_category_id === null ? (
										noCategoryChosen
									) : this.state.categories.find(
										(c) => this.state.issue.issue_category_id === c.id
									) === undefined ? (
										noCategoryChosen
									) : (
										this.state.categories.find((c) => this.state.issue.issue_category_id === c.id)
											.description
									)}
								</p>
								<h6 className={classes.subheader}>Oppdragstaker</h6>
								<p className={classes.infoBody}>
									{this.state.issue.name === null ? (
										'Ingen bedrift er tildelt denne saken'
									) : (
										this.state.issue.name
									)}
								</p>
								<Button variant="outlined">
									<a
										href={
											'https://www.google.com/maps/search/?api=1&query=' +
											this.state.issue.latitude +
											',' +
											this.state.issue.longitude
										}
										className={classes.linkRedirectButton}
										target="_blank"
										rel="noopener noreferrer"
									>
										Se sak på kart
									</a>
								</Button>
							</div>
						</div>
						<div className={classes.issueImageWrapper}>
							{this.state.issue.picture_filename === null ? null : (
								<img
									className={classes.issueImage}
									src={'/img/' + this.state.issue.id + '.jpg'}
									alt={'Saksbilde'}
								/>
							)}
						</div>

						<div className={classes.statusUpdates}>
							<StatusUpdates
								issue_id={this.props.match.params.id}
								cookies={this.props.cookies}
								companyAssigned={this.state.companyAssigned}
							/>
						</div>

						<div className={classes.setStatus}>
							<ChangeStatus
								changeStatus={this.changeStatus}
								issueStatuses={this.state.issueStatuses}
								currentStatus={this.state.issue.status_id}
								cookies={this.props.cookies}
								companyAssigned={this.state.companyAssigned}
							/>
						</div>
						<div className={classes.setEstimatedTime}>
							<ChangeEstimatedTime
								changeEstimatedTime={this.changeEstimatedTime}
								cookies={this.props.cookies}
								companyAssigned={this.state.companyAssigned}
							/>
						</div>
						<div className={classes.setCompany}>
							<ChangeCompany
								cookies={this.props.cookies}
								companies={this.state.companies}
								company_id={this.state.issue.company_id}
								changeCompany={this.changeCompany}
							/>
						</div>
						<div className={classes.setCategory}>
							<ChangeCategory
								categories={this.state.categories}
								cookies={this.props.cookies}
								category_id={this.state.issue.issue_category_id}
								changeCategory={this.changeCategory}
								addCategory={this.addCategory}
							/>
						</div>

						<div className={classes.setTitle}>
							<ChangeTitle
								cookies={this.props.cookies}
								title={this.state.issue.title}
								changeTitle={this.changeTitle}
							/>
						</div>

						<Button
							className={
								this.props.cookies.get('userInfo').category_id === 3 ||
								(this.props.cookies.get('userInfo').category_id === 4 && this.state.companyAssigned) ? (
									classes.saveButton
								) : (
									classes.saveButtonHidden
								)
							}
							fullWidth
							variant="outlined"
							onClick={this.saveChanges}
						>
							<i className="fas fa-save" style={{ marginRight: '10px' }} />Lagre endringer
						</Button>
						<Button
							className={
								this.props.cookies.get('userInfo').category_id === 3 ? (
									classes.deleteButton
								) : (
									classes.deleteButtonHidden
								)
							}
							fullWidth
							variant="outlined"
							onClick={this.deleteIssue}
						>
							<i className="fas fa-trash" style={{ marginRight: '10px' }} />Slett sak
						</Button>
						<PositionedSnackbar />
					</Card>
				</div>
			);
		} else {
			return (
				<div className={classes.mainWrapper}>
					<Card className={classes.wrapper}>
						<div className={classes.issueTitle}>
							<h2 style={{ color: logoColor }}> Saksinformasjon </h2>
						</div>
						<div className={classes.issueStep}>
							<StatusStepper
								status={this.state.issue.status_id}
								issueStatuses={this.state.issueStatuses}
							/>
						</div>
						<div className={classes.issueDescription}>
							<h2>
								{this.state.issue.title === undefined ? 'Ingen tittel satt' : this.state.issue.title}
							</h2>
							<p className={classes.dense}>
								{this.state.address === '' ? 'Ingen adresse spesifisert' : this.state.address}
							</p>
							<p className={classes.dense}>{this.state.issue.date}</p>
							<p className={classes.descBody}>{this.state.issue.description}</p>
							<div>
								<h6 className={classes.subheader}>Estimert tid til utbedring</h6>
								<p className={classes.infoBody}>
									{this.state.issue.estimated_time === null ? (
										'Ikke satt'
									) : (
										this.state.issue.estimated_time
									)}
								</p>
								<h6 className={classes.subheader}>Kategori</h6>
								<p className={classes.infoBody}>
									{this.state.issue.issue_category_id === null ? (
										noCategoryChosen
									) : this.state.categories.find(
										(c) => this.state.issue.issue_category_id === c.id
									) === undefined ? (
										noCategoryChosen
									) : (
										this.state.categories.find((c) => this.state.issue.issue_category_id === c.id)
											.description
									)}
								</p>
								<h6 className={classes.subheader}>Oppdragstaker</h6>
								<p className={classes.infoBody}>
									{this.state.issue.name === null ? (
										'Ingen bedrift er tildelt denne saken'
									) : (
										this.state.issue.name
									)}
								</p>
								<Button style={{ marginBottom: '1rem' }} variant="outlined">
									<a
										style={{ color: 'black' }}
										href={
											'https://www.google.com/maps/search/?api=1&query=' +
											this.state.issue.latitude +
											',' +
											this.state.issue.longitude
										}
										target="_blank"
										rel="noopener noreferrer"
									>
										Se sak på kart
									</a>{' '}
								</Button>
							</div>
						</div>
						<div className={classes.issueImageWrapper}>
							{this.state.issue.picture_filename === null ? null : (
								<img
									className={classes.issueImage}
									src={'/img/' + this.state.issue.id + '.jpg'}
									alt={'Saksbilde'}
								/>
							)}
						</div>
						<div className={classes.statusUpdates} style={{ marginBottom: '40px' }}>
							<StatusUpdates
								issue_id={this.props.match.params.id}
								cookies={this.props.cookies}
								companyAssigned={this.state.companyAssigned}
							/>
						</div>
					</Card>
				</div>
			);
		}
	}

	saveChanges = () => {
		const { cookies } = this.props;
		issueService
			.updateIssue(
				this.state.issue.id,
				this.state.issue.title,
				this.state.issue.issue_category_id,
				this.state.issue.estimated_time
			)
			.then((response) => {
				handleSnackbarOpen({ message: 'Oppdaterte sak', variant: 'success' });
			})
			.catch((error) => {
				handleSnackbarOpen({ message: 'Kunne ikke oppdatere sak', variant: 'error' });
			});

		if (this.state.statusChanged) {
			issueService
				.setStatus(this.state.issue.id, this.state.issue.status_id)
				.then((response) => {
					this.setState({ statusChanged: false });
					handleSnackbarOpen({ message: 'Oppdaterte sakens status', variant: 'success' });
					issueService
						.getIssue(this.state.issue.id)
						.then((issue) => {
							this.setState({ issue: issue[0] });
						})
						.catch((error) => console.log(error));
				})
				.catch((error) => {
					handleSnackbarOpen({ message: 'Kunne ikke oppdatere status', variant: 'error' });
				});
		}

		if (this.state.companyChanged) {
			businessService
				.assignCompany(this.state.company_id, this.state.issue.id)
				.then((response) => {
					this.setState({ companyChanged: false });
					handleSnackbarOpen({ message: 'Oppdaterte tildelt bedrift', variant: 'success' });
				})
				.catch((error) => {
					handleSnackbarOpen({ message: 'Kunne ikke oppdatere bedrift', variant: 'error' });
				});
		}
	};

	/**


     * Updates issue category in state which is used when saving changes made to issue.
     */
	changeCategory = (category) => {
		let issue = this.state.issue;
		issue.issue_category_id = category;
		this.setState({ issue: issue });
	};

	/**
     * Updates issue status in state which is used when saving changes made to issue.
     */
	changeStatus = (status_id) => {
		let issue = this.state.issue;
		issue.status_id = status_id;
		this.setState({ issue: issue, statusChanged: true });
	};

	/**
     * Updates issue title in state which is used when saving changes made to issue.
     */
	changeTitle = (newTitle) => {
		let issue = this.state.issue;
		issue.title = newTitle;
		this.setState({ issue: issue });
	};

	/**
     * Updates issue company in state which is used when saving changes made to issue.
     */
	changeCompany = (company_id) => {
		this.setState({ company_id: company_id, companyChanged: true });
	};

	/**
     * Updates issue estimated time in state which is used when saving changes made to issue.
     */
	changeEstimatedTime = (date) => {
		let issue = this.state.issue;
		issue.estimated_time = date;
		this.setState({ issue: issue });
	};

	/**
     * Disables issues in database
     */
	deleteIssue = () => {
		issueService
			.disableIssue(this.state.issue.id)
			.then((response) => {
				handleSnackbarOpen({ message: 'Sak slettet', variant: 'success' });
			})
			.then(this.props.history.push('/kommunaladmin/saker'))
			.catch((error) => {
				handleSnackbarOpen({ message: 'Kunne ikkke slette sak', variant: 'error' });
			});
	};

	/**
     * Adds a new issue category to database
     */
	addCategory = (newCategory) => {
		if (newCategory.length <= 2) {
			handleSnackbarOpen({ message: 'Ny kategori-feltet kan ikke være tomt', variant: 'warning' });
		} else {
			issueService
				.addIssueCategory(newCategory)
				.then((response) => handleSnackbarOpen({ message: 'Kategori lagt til', variant: 'success' }))
				.then(() => {
					issueService
						.getIssueCategories()
						.then((categories) => this.setState({ categories: categories }))
						.catch((error) => console.log(error));
				})
				.catch((error) => handleSnackbarOpen({ message: 'Kunne ikke legge til kategori', variant: 'error' }));
		}
	};
}

export default withRouter(withStyles(styles)(Issue));
