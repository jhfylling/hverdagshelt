import React, {Component} from 'react';
import { withStyles } from "@material-ui/core";
import FormData from 'form-data';

import axios from 'axios';

const style = theme => ({
    wrapper:{
        width: '100%'
    }
});
class ImageComponent extends Component{
    render(){
        const {classes} = this.props;
        return (
            <div className={classes.wrapper}>
                <input 
                    type="file" 
                    name="photo" 
                    accept="image/*" 
                    capture="camera" 
                    className="form-control-file btn btn-primary" 
                    id="formControlFile" 
                />
                <br /><br />
                <button className="btn btn-primary" onClick={this.setCanvas.bind(this)}>Send</button>
            </div>
        );
    }

    setCanvas(){
        let imagefile = document.querySelector("#formControlFile").files;

        var img = document.createElement("img");
        var reader = new FileReader();
        reader.onload = function(e) {
            img.src = e.target.result 
        }
        reader.readAsDataURL(imagefile[0]);

        img.onload = () => {
            
            let MAX_WIDTH = 700;
            let MAX_HEIGHT = 700;
            let width = img.width;
            let height = img.height;
            
            if (width > height) {
                if (width > MAX_WIDTH) {
                    height *= MAX_WIDTH / width;
                    width = MAX_WIDTH;
                }
            } else {
                if (height > MAX_HEIGHT) {
                    width *= MAX_HEIGHT / height;
                    height = MAX_HEIGHT;
                }
            }
            var canvas = document.createElement('canvas');

            canvas.width = width;
            canvas.height = height;
            
            console.log('sizes: ', canvas.width, canvas.height);

            let canvasContext = canvas.getContext('2d');
            canvasContext.drawImage(img, 0, 0, width, height);
            
            let getImageFromCanvasBound = this.getImageFromCanvas.bind(this);

            getImageFromCanvasBound(canvas);
        }
        //var dataurl = canvas.toDataURL("image/png");        
    }

    getImageFromCanvas(canvas){
        // var canvas = document.getElementById('canvas');
        let compressionRatio = 1.0;
        var image = canvas.toDataURL('image/jpeg', compressionRatio);
        
        let blob = this.dataURItoBlob(image);
        let originalSize = blob.size;
        let newSize = null;

        let MAX_PREF_SIZE = [
            300000,
            150000,
            100000
        ];

        if(blob.size > MAX_PREF_SIZE[0]){
            compressionRatio = 0.7;
        }
        else if(blob.size > MAX_PREF_SIZE[1]){
            compressionRatio = 0.8;
        }
        else if(blob.size > MAX_PREF_SIZE[2]){
            compressionRatio = 0.9;
        }
        
        if(compressionRatio !== 1.0){
            console.log("Compressing file with ratio: ", compressionRatio);
            image = canvas.toDataURL('image/jpeg', compressionRatio);
            blob = this.dataURItoBlob(image);
            newSize = blob.size;
            console.log("OriginalSize: ", originalSize, " newSize: ", newSize);
        }
        console.log('compressionratio: ', compressionRatio);
        this.sendImage(blob);
    }

    sendImage(image){
        let formData = new FormData();

        formData.append('photo', image, 'newImage.jpg');

        axios.post('/api/testFileUpload', formData, {
            headers: {
                'accept': 'application/json',
                'Accept-Language': 'en-US,en;q=0.8',
                'Content-Type': 'multipart/form-data',
            }
        })
        .then((response) => {
            //handle success
            //handleSnackbarOpen({message: 'Sak innsendt!', variant: 'success'});
            console.log("Successfully sent formdata to server");
        }).catch((error) => {
            //handle error
            //handleSnackbarOpen({message: 'Det oppstod en feil ved opplastning av sak.', variant: 'error'});
            console.log("Something went wrong while sending data to server");
        });
    }
    
    dataURItoBlob(dataURI) {
        // convert base64/URLEncoded data component to raw binary data held in a string
        var byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0)
            byteString = atob(dataURI.split(',')[1]);
        else
            byteString = unescape(dataURI.split(',')[1]);
    
        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
    
        // write the bytes of the string to a typed array
        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }
    
        return new Blob([ia], {type:mimeString});
    }

}

export default withStyles(style)(ImageComponent);