function getCompressedImageFromInput(imageFile) {

    return new Promise(function (resolve, reject) {

        //If imageFile is not supplied, allow issue upload without file
        if(imageFile.size === 0){
            resolve(null);
        }

        //Create HTML type image
        var img = document.createElement("img");
        var reader = new FileReader();
        reader.onload = function (e) {
            img.src = e.target.result
        }
        reader.readAsDataURL(imageFile);

        //Once image is loaded, create canvas with max width and height
        //Will keep aspect ratio
        img.onload = () => {
            let MAX_WIDTH = 700;
            let MAX_HEIGHT = 700;
            let width = img.width;
            let height = img.height;

            if (width > height) {
                if (width > MAX_WIDTH) {
                    height *= MAX_WIDTH / width;
                    width = MAX_WIDTH;
                }
            } else {
                if (height > MAX_HEIGHT) {
                    width *= MAX_HEIGHT / height;
                    height = MAX_HEIGHT;
                }
            }
            var canvas = document.createElement('canvas');

            canvas.width = width;
            canvas.height = height;

            //Draw image on hidden canvas
            let canvasContext = canvas.getContext('2d');
            canvasContext.drawImage(img, 0, 0, width, height);

            //Decide image quality based on converted filesize
            let compressionRatio = 1.0;
            var image = canvas.toDataURL('image/jpeg', compressionRatio);

            let blob = dataURItoBlob(image);


            let MAX_PREF_SIZE = [
                300000,
                150000,
                100000
            ];

            if (blob.size > MAX_PREF_SIZE[0]) {
                compressionRatio = 0.7;
            }
            else if (blob.size > MAX_PREF_SIZE[1]) {
                compressionRatio = 0.8;
            }
            else if (blob.size > MAX_PREF_SIZE[2]) {
                compressionRatio = 0.9;
            }

            //Log which compression was chosen
            if (compressionRatio !== 1.0) {
                image = canvas.toDataURL('image/jpeg', compressionRatio);
                blob = dataURItoBlob(image);
            }

            //return converted file object
            resolve(blob);
        }
    });
}

//This method is used to convert URIimageData to imageobject
var dataURItoBlob = (dataURI) => {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], { type: mimeString });
}

export { getCompressedImageFromInput };