/*eslint no-useless-concat: "off"*/
//React
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import FormData from 'form-data';
import axios from 'axios';
import Geocode from 'react-geocode';
import { withRouter } from 'react-router-dom';

//MaterialUI
import { withStyles } from '@material-ui/core/styles';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import withWidth from '@material-ui/core/withWidth';
import InputLabel from '@material-ui/core/InputLabel';

//Own
import { municipalityService } from '../../serviceObjects/municipalityService';
import PositionedSnackbar, { handleSnackbarOpen } from '../../components/Snackbar';
import { reportButtonColor, logoColor } from '../../components/Colors';
import { getCompressedImageFromInput } from './components/ImageCompression';
import MapWrapper from '../../components/map/MapWrapper';

const styles = (theme) => ({
	newIssueStyle: {
		minHeight: '500px',
		display: 'grid',
		gridTemplateColumns: '1fr 3fr 1fr',
		gridTemplateAreas: "'. form .'",
		[theme.breakpoints.down('sm')]: {
			display: 'grid',
			gridTemplateColumns: '1fr 10fr 1fr',
			gridTemplateAreas: "'. form .'"
		}
	},
	form: {
		gridArea: 'form'
	},
	innerWrapper: {
		display: 'grid',
		gridTemplateColumns: '1fr 22fr 1fr',
		gridTemplateAreas: "'formHeader formHeader formHeader'" + "'. startForm .'" + "'. map .'" + "'. endForm .'",
		backgroundColor: 'white',
		paddingBottom: '20px',
		[theme.breakpoints.down('sm')]: {
			width: '100vw'
		}
	},
	formHeader: {
		gridArea: 'formHeader',
		textAlign: 'center',
		backgroundColor: reportButtonColor,
		padding: '10px'
	},
	startForm: {
		gridArea: 'startForm',
		paddingTop: '30px'
	},
	map: {
		gridArea: 'map',
		width: '100%',
		height: '50vh',
		[theme.breakpoints.down('sm')]: {
			height: '50vh'
		}
	},
	endForm: {
		gridArea: 'endForm',
		paddingTop: '20px'
	},
	textField: {
		rows: '10'
	},
	sendButton: {
		gridArea: 'bot',
		[theme.breakpoints.down('sm')]: {
			height: '50px',
			width: '100%'
		}
	},
	inputfile: {
		width: '0.1px',
		height: '0.1px',
		opacity: '0',
		overflow: 'hidden',
		position: 'absolute',
		zIndex: '-1'
	},
	fileLabel: {
		fontSize: '0.875rem',
		fontWeight: '500',
		lineHeight: '1.75',
		fontFamiliy: '"Roboto", "Helvetica", "Arial", sans-serif',
		padding: '6px 16px',
		color: 'rgba(0, 0, 0, 0.87)',
		display: 'inline-block',
		border: '1px solid rgba(0, 0, 0, 0.23)',
		borderRadius: '4px',
		transition:
			'background-color 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,box-shadow 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,border 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms',
		'&:focus': {
			backgroundColor: 'red'
		},
		'&:hover': {
			backgroundColor: 'rgba(0, 0, 0, 0.08)'
		}
	},
	bottomBox: {
		display: 'grid',
		// gridTemplateColumns: '6fr 6fr',
		// gridTemplateAreas: "'top topRight'" + "'bot bot'",
		gridTemplateColumns: '12fr',
		gridTemplateAreas: "'top'" + "'topRight'" + "'bot'",
		[theme.breakpoints.down('sm')]: {
			gridTemplateColumns: '12fr',
			gridTemplateAreas: "'top'" + "'topRight'" + "'bot'"
		}
	},
	fileInput: {
		gridArea: 'top'
	},
	imagePreview: {
		gridArea: 'topRight',
		minHeight: '360px'
	}
});
/**
 * This class is used by users to submit new issues
 * Collects all info from a user and sends it to the server
 * 
 * Uses the browsers location, selects municipality based on map click
 * Picture uploads are resized and compressed client side
 */
class CreateIssue extends Component {
	constructor(props) {
		super(props);

		this.canvasWidth = 300;
		this.canvasHeight = 300;

		this.state = {
			description: '',
			municipality: '',
			municipalities: [ '' ],
			address: '',
			openDialog: false,
			positionMap: {
				lat: 0,
				lng: 0
			},
			labelWidth: 0,
			municipality_id: '',
		};
		this.getCoordinatesParent = this.getCoordinatesParent.bind(this);
	}

	updateDimensions() {
		let boxWidth = document.getElementById('mapGridBox').offsetWidth;
		let boxHeight = document.getElementById('mapGridBox').offsetHeight;

		this.setState({ mapStyle: { width: boxWidth, height: boxHeight, position: 'absolute' } });
	}

	componentDidMount() {
		this.updateDimensions = this.updateDimensions.bind(this);
		window.addEventListener('resize', this.updateDimensions);
		this.updateDimensions();

		this.setState({
			labelWidth: ReactDOM.findDOMNode(this.InputLabelRef).offsetWidth
		});

		municipalityService.getMunicipalities().then((municipalities) => this.setState({ municipalities }));

	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.updateDimensions);
	}

	handleChange = (event) => {
		this.setState({ description: event.target.value });
	};
	handleChangeMunicipality = (event) => {
		this.setState({ municipality: event.target.value });
	};

	handleClickOpenDialog = () => {
		this.setState({ openDialog: true });
	};

	handleCloseDialog() {
		this.setState({ openDialog: false });
	}
	handleSubmit() {
		this.setState({ openDialog: true });
	}
	render() {
		const { classes, width } = this.props;

		//Adjust number of rows in textfield on smaller devices
		let textfieldRows = 6;
		if (width === 'sm' || width === 'xs') {
			textfieldRows = 12;
		}

		return (
			<div className={classes.newIssueStyle}>
				<div className={classes.form}>
					<ValidatorForm
						id="issueForm"
						ref="form"
						onSubmit={this.handleSubmit.bind(this)}
						onError={(errors) => {
							console.log(errors);
                            handleSnackbarOpen({ message: 'Du må fylle ut informasjon', variant: 'warning' });

                        }}
					>
						<Card className={classes.innerWrapper}>
							<div className={classes.formHeader}>
								<h2 style={{ color: logoColor }}> Send inn feil </h2>
							</div>
							<div className={classes.startForm}>
								<h4 style={{ paddingBottom: '20px' }}>
									<u>S</u>kriv beskrivelse
								</h4>
								<TextValidator
									className={classes.textField}
									autoFocus
									multiline
									label="Skriv beskrivelse"
									onChange={this.handleChange}
									name="description"
									value={this.state.description}
									validators={[ 'required' ]}
									errorMessages={[ 'Fyll inn beskrivelse', 'Mailadresse ikke gyldig' ]}
									variant="outlined"
									rows={textfieldRows}
									rowsMax={textfieldRows * 2}
									fullWidth
								/>
								<h4 style={{ paddingBottom: '20px', paddingTop: '20px' }}>
									{' '}
									<u>M</u>arker på kartet hvor problemet befinner seg
								</h4>
							</div>

							<div className={classes.map} id="mapGridBox">
								<MapWrapper
									mapStyle={this.state.mapStyle}
									getCoordinates={this.getCoordinatesParent}
									cookies={this.props.cookies}
								/>
							</div>
							<div className={classes.endForm}>
								<h4>
									<u>P</u>lasseringen du har valgt{' '}
								</h4>
								<TextField
									id="outlined-name"
									label="Adresse"
									value={this.state.address}
									fullWidth
									margin="normal"
									variant="outlined"
									disabled
								/>
								<div style={{ paddingBottom: '30px' }}>
									<p>Velg kommune: </p>
									<FormControl variant="outlined" style={{ minWidth: '200px' }}>
										<InputLabel
											ref={(ref) => {
												this.InputLabelRef = ref;
											}}
											htmlFor="outline-age-native-simple"
										>
											Velg kommune
										</InputLabel>
										<Select
											native
											value={this.state.municipality}
											onChange={this.handleChangeMunicipality}
											input={
												<OutlinedInput
													name="age"
													labelWidth={this.state.labelWidth}
													id="outlined-age-native-simple"
												/>
											}
										>
											<option> </option>
											{this.state.municipalities.map((municipality) => (
												<option value={municipality.municipality_id}
													key={municipality.municipality_id}
												>
													{' '}
													{municipality.municipality_name}{' '}
												</option>
											))}
										</Select>
									</FormControl>
								</div>
								<input
									name="latitude"
									type={'hidden'}
									id="formLat"
									value={this.state.positionMap.lat}
								/>
								<input
									name="longitude"
									type={'hidden'}
									id="formLng"
									value={this.state.positionMap.lng}
								/>
								<input
									name="municipality_id"
									type={'hidden'}
									id="formMunicipality_id"
									value={this.state.municipality_id}
									/>
								<div className={classes.bottomBox}>
									<div className={classes.fileInput}>
										<input
											type="file"
											name="photo"
											accept="image/*"
											capture="camera"
											id="formControlFile"
											className={classes.inputfile}
											onChange={this.imagePreview.bind(this)}
										/>
										<label htmlFor="formControlFile" className={classes.fileLabel}>
											<i className="fas fa-camera"> </i>&nbsp; LAST OPP ET BILDE AV PROBLEMET
										</label>
									</div>
									<div className={classes.imagePreview}>
										<p>Valgt bilde:</p>
										<canvas
											alt="Bildet du har valgt eller tatt"
											id="imagePreviewCanvas"
											width="300px"
											height="300px"
										/>
									</div>
									<Button
										className={classes.sendButton}
										variant="outlined"
										type="submit"
										form="issueForm"
									>
										<i className="fas fa-save" style={{ marginRight: '4px' }} /> Send
									</Button>
									<Dialog
										open={this.state.openDialog}
										onClose={this.handleCloseDialog}
										aria-labelledby="alert-dialog-title"
										aria-describedby="alert-dialog-description"
									>
										<DialogTitle id="alert-dialog-title">Takk for at du bidrar!</DialogTitle>
										<DialogContent>
											<DialogContentText id="alert-dialog-description">
												Vennligst bekreft at du ønsker å laste opp denne saken
											</DialogContentText>
										</DialogContent>
										<DialogActions>
											<Button
												value="avbryt"
												onClick={this.handleCloseDialog.bind(this)}
												variant="contained"
											>
												Avbryt
											</Button>
											<Button
												value="bekreft"
												onClick={this.uploadIssue.bind(this)}
												variant="contained"
											>
												Bekreft
											</Button>
										</DialogActions>
									</Dialog>
								</div>
							</div>
						</Card>
					</ValidatorForm>
					<PositionedSnackbar />
				</div>
			</div>
		);
	}

	imagePreview() {
		let imageFiles = document.querySelector('#formControlFile').files;

		if (imageFiles.length === 1) {
			//Create HTML type image
			var img = document.createElement('img');
			var reader = new FileReader();
			reader.onload = function(e) {
				img.src = e.target.result;
			};
			reader.readAsDataURL(imageFiles[0]);

			var canvas = document.querySelector('#imagePreviewCanvas');
			//Once image is loaded, create canvas with max width and height
			//Will keep aspect ratio
			img.onload = () => {
				let MAX_WIDTH = this.canvasWidth;
				let MAX_HEIGHT = this.canvasHeight;
				let width = img.width;
				let height = img.height;

				if (width > height) {
					if (width > MAX_WIDTH) {
						height *= MAX_WIDTH / width;
						width = MAX_WIDTH;
					}
				} else {
					if (height > MAX_HEIGHT) {
						width *= MAX_HEIGHT / height;
						height = MAX_HEIGHT;
					}
				}

				canvas.width = width;
				canvas.height = height;

				//Draw image on hidden canvas
				let canvasContext = canvas.getContext('2d');
				canvasContext.drawImage(img, 0, 0, width, height);

			};
		}
	}

	/**
		 * Sends the new issue to server with formdata (image included)
		 * Called by Confirmer
		 */
	uploadIssue() {
		this.setState({ openDialog: false });
		let issueData = document.querySelector('#issueForm');
		let formdata = new FormData(issueData);
		formdata.append("municipality_id", this.state.municipality_id);

		//Send image from
		getCompressedImageFromInput(formdata.get('photo')).then((blob) => {
			if (blob !== null) {
				formdata.set('photo', blob, 'newPhoto.jpg');
			}

			let url = '/api/issues';

			const { cookies } = this.props;

			this.clearInput(); //Clear input form

			//Send form data including picture
			axios
				.post(url, formdata, {
					headers: {
						accept: 'application/json',
						'Accept-Language': 'en-US,en;q=0.8',
						'Content-Type': 'multipart/form-data'

					}
				})
				.then((response) => {
					//handle success
					handleSnackbarOpen({ message: 'Sak innsendt!', variant: 'success' });
				})
				.then(this.props.history.push('/brukerprofil'))
				.catch((error) => {
					//handle error
					handleSnackbarOpen({ message: 'Det oppstod en feil ved opplastning av sak.', variant: 'error' });
				});
		});
	}

	/**
     * Gets coordinates from map.
     *
     */
	getCoordinatesParent(lat, lng) {
		this.setState({
			positionMap: {
				lat: lat,
				lng: lng
			}
		});
		this.setAddress();
		
	}

	setAddress() {
		if (this.state.positionMap.lat!==0 && this.state.positionMap.lng!==0) {
			Geocode.setApiKey('AIzaSyATAz6K9aKSkbyQMLbivjIrp5KoPJ8_rX4');
			Geocode.fromLatLng(this.state.positionMap.lat, this.state.positionMap.lng).then(
				(response) => {
					let address = response.results[0].formatted_address;
					this.setState({ address: address });
					let zipCode = this.getZipCode(address);
					if (zipCode !== -1) {
						municipalityService.getMunicipalitiesWithZipCode(zipCode).then((municipality) => {
							this.setState({
								municipality: municipality[0].Kommunenavn,
								municipality_id: municipality[0].id
							});
						});
					} else {
						this.setState({
							municipality: '',
							municipality_id: ''
						});
					}
				},
				(error) => {
					this.setState({ address: '' });
				}
			);
		}
	}
	clearInput() {
		this.setState({
			description: '',
			address: '',
			positionMap: {
				lat: 0,
				lng: 0
			},
			municipality: '',
		});
		let canvas = document.querySelector('#imagePreviewCanvas');
		let canvasContext = canvas.getContext('2d');
		canvasContext.strokeStyle = "white";
		canvasContext.clearRect(0,0,300, 300);

	}
	getZipCode(address) {
		let count = 0;
		let start = 0;
		let char = address.charAt(start);
		while (char !== null) {
			if (!isNaN(char)) {
				count++;
				if (count === 4) break;
			} else {
				count = 0;
			}
			start++;
			char = address.charAt(start);
		}

		let zip = address.substring(start - 2, start + 2).trim();
		if (!isNaN(zip) && zip.length === 4) return zip;
		else return -1;
	}
}

export default withRouter(withWidth()(withStyles(styles)(CreateIssue)));