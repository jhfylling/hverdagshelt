/*eslint no-useless-concat: "off"*/
//React
import React, { Component } from 'react';
import classNames from 'classnames';

//MaterialUI
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Card from '@material-ui/core/Card/Card';
import Divider from "@material-ui/core/Divider/Divider";

//Own
import { issueService } from '../../serviceObjects/IssueService';
import { municipalityService } from '../../serviceObjects/municipalityService';
import { userService } from '../../serviceObjects/userService';
import { handleSnackbarOpen } from '../../components/Snackbar';
import PositionedSnackbar from '../../components/Snackbar';
import IssueTable from '../../components/issueTable/IssueTable';
import { reportButtonColor, logoColor } from '../../components/Colors';

let styles = (theme) => ({
	outerWrapper: {
		display: 'grid',
		gridTemplateColumns: '1fr 5fr 1fr',
		gridTemplateAreas: "'. content .'",
		[theme.breakpoints.down('sm')]: {
			
			gridTemplateAreas: "'content content content'"
		}
	},
	content: {
		gridArea: 'content'
	},
	userProfile: {
		display: 'grid',
		gridTemplateColumns: '1fr 8fr 8fr 1fr',
		gridTemplateAreas: "'header header header header'" + "'. userinfo issuetab .'",
		[theme.breakpoints.down('md')]: {
			gridTemplateAreas: '' + "'header'" + "'userinfo'" + "'issuetab'",
			gridTemplateColumns: 'auto'
		},
	},
	header: {
		gridArea: 'header',
		textAlign: 'center',
		padding: '10px',
		backgroundColor: reportButtonColor,
		color: logoColor
	},
	userInfo: {
		gridArea: 'userinfo',
		display: 'grid',
		gridTemplateAreas: '' + "'firstname'" + "'lastname'" + "'email'" + "'phonenumber'" + "'savebutton'",
		padding: '2%',
        
	},
	inputFirstname: {
		gridArea: 'firstname',
		padding: '2%'
	},
	inputLastname: {
		gridArea: 'lastname',
		padding: '2%'
	},
	inputEmail: {
		gridArea: 'email',
		padding: '2%'
	},
	inputPhonenumber: {
		gridArea: 'phonenumber',
		padding: '2%'
	},
	inputSavebutton: {
		gridArea: 'savebutton',
		padding: '2%'

	},
	issueTab: {
		gridArea: 'issuetab',
		paddingTop: '2%',
		paddingBottom: '2%',
	},
	list: {
		height: '50vh',
		overflow: 'auto'
	}
});

class Userprofile extends Component {
	constructor(props) {
		super(props);
		this.state = {
			issues: [],
			municipalities: [],
			firstname: '',
			lastname: '',
			email: '',
			municipality_id: -1,
			phone_number: '',
			myMunicipality: {
				municipality_id: -1,
				municipality_name: 'test'
			}
		};
		this.deleteIssue = this.deleteIssue.bind(this);
		this.editUser = this.editUser.bind(this);
		this.changeMunicipality = this.changeMunicipality.bind(this);
	}

	componentDidMount() {
		const { cookies } = this.props;
		issueService
			.getIssuesByUser(cookies.get('userInfo').email)
			.then((issues) => this.setState({ issues: issues }))
			.catch((error) => console.log(error));

		municipalityService
			.getMunicipalities()
			.then((municipalities) => this.setState({ municipalities: municipalities }))
			.catch((error) => console.log(error));

		this.setState({
			firstname: cookies.get('userInfo').firstname,
			lastname: cookies.get('userInfo').lastname,
			email: cookies.get('userInfo').email,
			municipality_id: cookies.get('userInfo').municipality_id,
			phone_number: cookies.get('userInfo').phone_number
		});

		let myMunicipality = {
			municipality_id: cookies.get('userInfo').municipality_id,
			municipality_name: cookies.get('userInfo').municipality_name
		};
		this.setState({ myMunicipality: myMunicipality });
	}

	render() {
		const { classes } = this.props;
		const { cookies } = this.props;

		if (cookies.get('userInfo') !== undefined) {
			return (
				<div className={classes.outerWrapper}>
					<div className={classes.content}>
						<Card className={classes.userProfile}>
							<div className={classes.header}>
								<h2>Min Profil</h2>
							</div>
							<div className={classes.userInfo}>
								<div className={classes.inputFirstname}>
                                <h4> Min informasjon </h4>
									<div className={classes.container}>
										<TextField
											id={'firstname'}
											label={'Fornavn'}
											className={classNames(classes.textField, classes.dense)}
											margin="dense"
											variant="outlined"
											onChange={(event) => this.inputChangedHandler(event)}
											defaultValue={
												cookies.get('userInfo').firstname ? (cookies.get('userInfo').firstname) : ('')
											}
										/>
									</div>
								</div>
								<div className={classes.inputLastname}>
									<div className={classes.container}>
										<TextField
											id={'lastname'}
											label={'Etternavn'}
											className={classNames(classes.textField, classes.dense)}
											margin="dense"
											variant="outlined"
											onChange={(event) => this.inputChangedHandler(event)}
											defaultValue={
												cookies.get('userInfo').lastname ? cookies.get('userInfo').lastname : ''
											}
										/>
									</div>
								</div>
								<div className={classes.inputEmail}>
									<div className={classes.container}>
										<TextField
											id={'email'}
											type={'email'}
											name={'email'}
											label={'Epost'}
                                             fullWidth
											variant={'outlined'}
											className={classNames(classes.textField, classes.dense)}
											defaultValue={
												cookies.get('userInfo').email ? cookies.get('userInfo').email : ''
											}
											onChange={(event) => this.inputChangedHandler(event)}
										/>
									</div>
								</div>
								<div className={classes.inputPhonenumber}>
									<div className={classes.container}>
										<TextField
											id={'phone_number'}
											type="number"
											name={'phonenumber'}
                                             fullWidth
											className={classNames(classes.textField, classes.dense)}
											label={'Telefonnummer'}
											variant={'outlined'}
											defaultValue={
												cookies.get('userInfo').phone_number ? (cookies.get('userInfo').phone_number) : ('')
											}
											onChange={(event) => this.inputChangedHandler(event)}
										/>
									</div>
								</div>
								<div className={classes.inputSavebutton}>
									<Button
										className={classes.button}
										variant="outlined"
										onClick={this.editUser}
									>
										Lagre
									</Button>
								</div>
							<Divider style={{marginTop: '20px'}}/>
							</div>
							<div className={classes.issueTab}>
								<IssueTable deleteIssue={this.deleteIssue} user_email={this.state.email} />
							</div>
							<PositionedSnackbar />
						</Card>
					</div>
				</div>
			);
		} else {
			return (
				<div>
					<h2>Du må logge inn først</h2>
				</div>
			);
		}
	}

	/**
     * Updates components state for each input.
     *
     */
	inputChangedHandler = (event) => {
		let updatedValue = event.target.value;
		let updatedItem = event.target.id;
		this.setState({ [updatedItem]: updatedValue });
	};

	/**
     * Deletes issue from DB.
     *
     */
	deleteIssue(issueId) {
		issueService
			.disableIssue(issueId)
			.then((response) => {
				handleSnackbarOpen({ message: 'Sak ble slettet.', variant: 'success' });
				let issues = this.state.issues;
				issues.filter((i) => i.issue_id !== issueId);
				this.setState({ issues: issues });
			})
			.catch((error) => {
				console.log('Error: ', error);
				handleSnackbarOpen({ message: 'Sak kunne ikke slettes.', variant: 'error' });
			});
	}

	/**
     * Updates user_details in DB.
     */
	editUser() {
		const { cookies } = this.props;
		let userDetails = {
			id: cookies.get('userInfo').id,
			email: this.state.email,
			municipality_id: this.state.myMunicipality.municipality_id,
			category_id: cookies.get('userInfo').category_id,
			category_name: cookies.get('userInfo').category_name,
			firstname: this.state.firstname,
			lastname: this.state.lastname,
			phone_number: this.state.phone_number,
			municipality_name: this.state.myMunicipality.municipality_name
		};

		if (JSON.stringify(cookies.get('userInfo')) === JSON.stringify(userDetails)) {
			handleSnackbarOpen({ message: 'Ingen endringer valgt.', variant: 'warning' });
		} else {
			userService
				.updateUser(userDetails)
				.then((response) => {
					handleSnackbarOpen({ message: 'Brukerinformasjon oppdatert.', variant: 'success' });
					cookies.set('userInfo', userDetails, { path: '/' });
				})
				.catch((error) => {
					console.log('Error: ', error);
					handleSnackbarOpen({ message: 'Kunne ikke oppdatere brukerinformasjon.', variant: 'error' });
				});
		}
	}

	/**
     * Updates components state with new information about current municipality.
     *
     */
	changeMunicipality(event) {
		this.setState({ myMunicipality: event.target.value });
	}
}
export default withStyles(styles)(Userprofile);
