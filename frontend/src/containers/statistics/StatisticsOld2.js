//React
import React, { Component } from "react";
import ReactDOMServer from 'react-dom/server';

//MaterialUI
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

//Charts
import { BarChart, Bar, YAxis, XAxis } from 'recharts';

//Own
import CalendarReport from "../../components/CalendarReport";

//pdf
import { PDFExport } from "@progress/kendo-react-pdf";
import html2canvas from "html2canvas";
import * as jsPDF from "jspdf";
import canvg from 'canvg';
import { exportImage } from '@progress/kendo-drawing';
import { saveAs } from '@progress/kendo-file-saver';


let styles = theme => ({
  pdfPage: {
    height: 792,
    width: 612,
    padding: "20px",
    backgroundColor: "white",
    boxShadow: "0px 0px 10px #999",
    margin: "auto",
    overflowX: "hidden",
    overflowY: "hidden",
    fontFamily: '"Times New Roman", Times, serif',
  },
});

class Statistics extends Component {
  constructor(props) {
    super(props);
    this.chart = null;
    this.state = {
      getReport: [],
      getNumberOfIssuesInCat: [],
      getNumberOfIssuesMunicipality: [],
      getNumberOfIssuesCompany: [],
      getAvgTimeInCat: [],
      getAvgTimeIssue: [],
      getActiveUsers: []
    };
  }

  /*componentDidMount(){
      this.setState({issues: this.props.issues})
  }*/

  convertSVGToImage = () => {
    console.log("Converting svg to image");

    let svgImage = document.querySelector("#numberOfIssuesInCatChart-clip").parentElement;
    //console.log(svgImage.outerHTML);
    //let htmlString = ReactDOMServer.renderToStaticMarkup(svgImage);

    let canvas = document.createElement("canvas");
    canvas.width = 500;
    canvas.height = 500;

    console.log(svgImage.outerHTML);

    canvg(canvas, svgImage.outerHTML);
    let convertedImage = canvas.toDataURL('image/png');

    let image = document.querySelector("#getNumberOfIssuesInCat_image");
    image.src = convertedImage;

    console.log(convertedImage);
  }

  componentDidMount() {
    
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
          <CalendarReport getStatistics={this.getStatistics} />

        <BarChart width={500} height={200} data={this.state.getNumberOfIssuesInCat} id="numberOfIssuesInCatChart">
          <XAxis dataKey='category' />
          <YAxis />
          <Bar type="monotone" dataKey="number_of_issues" barSize={20} fill="#8884d8" />
        </BarChart>

        <Button onClick={this.convertSVGToImage}>Convert Test</Button>

          <PDFExport
            paperSize={"Letter"}
            fileName="rapport.pdf"
            title="RAPPORT"
            subject=""
            keywords=""
            // forcePageBreak="hr"
            ref={r => (this.resume = r)}
            className={classes.pdfPage}
          >
            <div id="pdfCapture" className={classes.pdfPage}>
              <h4>Antall saker i kategorier</h4>
              {this.state.getNumberOfIssuesInCat.map((stat, index) => {
                return (
                  <div className={classes.statPrint} key={index}>
                    <div className={classes.title}>
                      <p>{stat.category}: {stat.number_of_issues}</p>
                    </div>
                  </div>
                );
              })}
              {/* <img id="getNumberOfIssuesInCat_image" src={this.chart} /> */}

            </div>
          </PDFExport>
          <Button onClick={this.generatePDF}>downloadTextPDF</Button>
      </div>
    );
  }

  exportPDF = () => {
    


    this.resume.save();
  };

  getStatistics = (statisticsRec) => {
    this.setState({ 
      getReport: statisticsRec.getReport,
      getNumberOfIssuesInCat: statisticsRec.getNumberOfIssuesInCat,
      getNumberOfIssuesMunicipality: statisticsRec.getNumberOfIssuesMunicipality,
      getNumberOfIssuesCompany: statisticsRec.getNumberOfIssuesCompany,
      getAvgTimeInCat: statisticsRec.getAvgTimeInCat,
      getAvgTimeIssue: statisticsRec.getAvgTimeIssue,
      getActiveUsers: statisticsRec.getActiveUsers
    });
  };

  generatePDF() {
    

    const input = document.getElementById("pdfCapture");
    html2canvas(input).then(canvas => {
      const imgData = canvas.toDataURL("image/png");

      const pdf = new jsPDF();
      pdf.addImage(imgData, "PNG", 0, 0);
      pdf.save("download.pdf");
    });
  }
}

export default withStyles(styles)(Statistics);