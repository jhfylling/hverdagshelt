//React
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

//MaterialUI
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

//Own
import VerticalBarChart from "./components/VerticalBarChart";
import {statisticsService} from "../../serviceObjects/StatisticsService";

let styles = theme => ({
	wrapper: {
		width: '1240px',
	},
	statcontainer: {
		height: '1754px',
		width: '100%',
		margin: "10px",
		pageBreakAfter: 'always',
	},
	headline: {
		width: '100%',
		height: '10%',
	},
	statbody: {
		width: '100%',
		height: '90%',
	},
	pb: {
		width: '100%',
		pageBreakAfter: 'always'
	}
});

class Print extends Component {
	constructor(props) {
		super(props);
		this.state = {
			national: {
				getReport: [],
				getNumberOfIssuesInCat: [],
				getNumberOfIssuesMunicipality: [],
				getNumberOfIssuesCompany: [],
				getAvgTimeInCat: [],
				getAvgTimeIssue: [],
				getActiveUsers: []
			},
			municipal: {
				getReport: [],
				getNumberOfIssuesInCat: [],
				getNumberOfIssuesMunicipality: [],
				getNumberOfIssuesCompany: [],
				getAvgTimeInCat: [],
				getAvgTimeIssue: [],
				getActiveUsers: []
			},
			municipality_id: null,
			range: [],
			municipalities: [],
			renderMunicipalStats: false,
			renderNationalStats: false,
		};
	}

	splitDataAndCreateJSX = (data, arraySize, header) => {
		console.log('length: ', data.length);
		const { classes } = this.props;
		let splitData = [];

		let x = 0;
		let temp = [];
		data.forEach((element, index) => {
			temp.push(element);
			x++;
			if(x === arraySize-1 || index === data.length-1){
				splitData.push(temp);
				temp = [];
				x = 0
			}
		});

		//console.log(issuesPerMunicipalityData);

		let splitJSX = [];

		splitData.forEach((element) => {
			let jsxTemp = <div className={classes.statcontainer}>
								<div className={classes.headline}>
									<h1 className="display-4">{header}</h1>
								</div>
								<div className={classes.statbody}>
									<VerticalBarChart data={element}/>
								</div>
							</div>;
			splitJSX.push(jsxTemp);
		});

		return splitJSX;
	}

	render() {
		let { national, municipal} = this.state;
		let {municipality_id} = this.props.match.params;

		document.body.style.width = '1240px';
		//document.body.style.height = '1754px';

		const { classes } = this.props;

		console.log('length: ', national.getNumberOfIssuesMunicipality.length);

		let issueMunicGraphs = this.splitDataAndCreateJSX(national.getNumberOfIssuesMunicipality, 50, "Nasjonalt: Saker fordelt på kommune");
		let nationalActiveUsers = this.splitDataAndCreateJSX(national.getActiveUsers, 50, "Nasjonalt: Aktive brukere");
		let nationalgetAvgTimeInCat = this.splitDataAndCreateJSX(national.getAvgTimeInCat, 50, "Nasjonalt: Gjennomsnittstid fordelt på kategori");
		let nationalIssuesPerCompany = this.splitDataAndCreateJSX(national.getNumberOfIssuesCompany, 50, "Nasjonalt: saker fordelt på bedrift");
		let nationalIssuesPerCat = this.splitDataAndCreateJSX(national.getNumberOfIssuesInCat, 50, "Nasjonalt: saker fordelt på kategori");

		let nationalJSX = (
			<div className={classes.wrapper}>
				{nationalActiveUsers}
				{issueMunicGraphs}
				{nationalgetAvgTimeInCat}
				{nationalIssuesPerCompany}
				{nationalIssuesPerCat}
				<div className={classes.statcontainer}>
					<div className={classes.headline}>
						Diverse statistikk
					</div>
					<div className={classes.statbody}>
						{national.getAvgTimeIssue.length === 0 ? null : "Gjennomsnittstid per sak: " + national.getAvgTimeIssue[0].value}
					</div>
				</div>;
			</div>
		);


		let municipalgetAvgTimeInCat = this.splitDataAndCreateJSX(municipal.getAvgTimeInCat, 50, "Kommunalt: Gjennomsnittstid fordelt på kategori");
		let municipalIssuesPerCompany = this.splitDataAndCreateJSX(municipal.getNumberOfIssuesCompany, 50, "Kommunalt: saker fordelt på bedrift");
		let municipalIssuesPerCat = this.splitDataAndCreateJSX(municipal.getNumberOfIssuesInCat, 50, "Kommunalt: saker fordelt på kategori");

		let municipalJSX = (
			<div className={classes.wrapper}>
				{municipalIssuesPerCat}<br />
				{municipalIssuesPerCompany}<br />
				{municipalgetAvgTimeInCat}<br />
				<div className={classes.statcontainer}>
					<div className={classes.headline}>
						Diverse statistikk
					</div>
					<div className={classes.statbody}>
						<p>Kommunalt</p>
						{municipal.getAvgTimeIssue.length === 0 ? null : "Gjennomsnittstid per sak: " + municipal.getAvgTimeIssue[0].value}
						{municipal.getNumberOfIssuesMunicipality.length === 0 ? null : "Antall saker i perioden: " + municipal.getNumberOfIssuesMunicipality[0].value}
						{municipal.getActiveUsers.length === 0 ? null : "Aktive brukere i perioden: " + municipal.getActiveUsers[0].value}
					</div>
				</div>;
			</div>
		);

		if(municipality_id === "null"){
			return nationalJSX;
		}
		else{
			return municipalJSX;
		}
	}

	componentDidMount(){
		let {municipality_id, start, end} = this.props.match.params;

		this.generateReport([start, end], municipality_id);
	}

	generateReport = (range, municipality_id) => {
		/* if (municipality_id !== null) {
			statisticsService.getReport(municipality_id, range[0], range[1])
			.then((res) => {
				this.setState({ municipal: res, renderMunicipalStats: true });
			});
		}
		statisticsService.getReport('', range[0], range[1])
			.then((res) => {
				this.setState({ national: res, renderNationalStats: true });
			}); */
		if (municipality_id === "null") {
			statisticsService.getReport('', range[0], range[1])
				.then((res) => {
					this.setState({ national: res, renderNationalStats: true }, () => {
						setTimeout(() => {
							window.print();
						}
						, 1000);
						
					});
				});
		}
		else{

			statisticsService.getReport(municipality_id, range[0], range[1])
			.then((res) => {
				this.setState({ municipal: res, renderMunicipalStats: true }, () => {
					setTimeout(() => {
						window.print();
					}
					, 1000);
				});
			});
		}
		
		
		
	}
}

export default withRouter(withStyles(styles)(Print));