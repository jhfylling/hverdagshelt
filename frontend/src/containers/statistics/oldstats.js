//React
import React, { Component } from "react";
import ReactDOMServer from 'react-dom/server';

//MaterialUI
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

//Charts
import { BarChart, Bar, YAxis, XAxis } from 'recharts';

//Own
import CalendarReport from "../../components/CalendarReport";

//pdf
import { PDFExport } from "@progress/kendo-react-pdf";
import html2canvas from "html2canvas";
import * as jsPDF from "jspdf";
import canvg from 'canvg';
import { exportImage } from '@progress/kendo-drawing';
import { saveAs } from '@progress/kendo-file-saver';



	/*componentDidMount(){
		this.setState({issues: this.props.issues})
	}*/

	/*  convertSVGToImage = () => {
	   console.log("Converting svg to image");
   
	   let svgImage = document.querySelector("#numberOfIssuesInCatChart-clip").parentElement;
	   //console.log(svgImage.outerHTML);
	   //let htmlString = ReactDOMServer.renderToStaticMarkup(svgImage);
   
	   let canvas = document.createElement("canvas");
	   canvas.width = 500;
	   canvas.height = 500;
   
	   console.log(svgImage.outerHTML);
   
	   canvg(canvas, svgImage.outerHTML);
	   let convertedImage = canvas.toDataURL('image/png');
   
	   let image = document.querySelector("#getNumberOfIssuesInCat_image");
	   image.src = convertedImage;
   
	   console.log(convertedImage);
	 } */

	/* <BarChart width={500} height={200} data={this.state.getNumberOfIssuesInCat} id="numberOfIssuesInCatChart">
			<XAxis dataKey='name' />
			<YAxis />
			<Bar type="monotone" dataKey="value" barSize={20} fill="#8884d8" />
		</BarChart> 
		<Button onClick={this.convertSVGToImage}>Convert Test</Button>
		<Button onClick={this.generatePDF}>downloadTextPDF</Button>
		<canvas id="canvastest" width="300" height="300"></canvas>
		*/


let styles = theme => ({
  wrapper: {
    display: "grid",
    gridArea: "card",
    gridTemplateColumns: "1fr 3fr 3fr 1fr",
    gridTemplateAreas:
      "" +
      "'. calendar calendar .'" +
      "'pdfPreviewer pdfPreviewer pdfPreviewer pdfPreviewer '",
    [theme.breakpoints.down("sm")]: {
      gridTemplateAreas:
        "" + "'step' 'title' 'imageWrapper' 'description' 'editIssue'",
      gridTemplateColumns: "1fr"
    },
    backgroundColor: "white",
    borderRadius: "15px",
    padding: "20px"
  },
  calendar: {
    gridArea: "calendar"
  },
  pdfPreviewer: {
    gridArea: "pdfPreviewer",
    borderRadius: "2px",
  },
  statPrint: {
    gridTemplateAreas:
      "" +
      "'. title title .'" +
      "' id date issueCategory municipalityname '" +
      "' description description description description '"
  },
  title: {
    gridArea: "title",
  },
  id: {
    gridArea: "id"
  },
  description: {
    gridArea: "description"
  },
  date: {
    gridArea: "date"
  },
  issueCategory: {
    gridArea: "issueCategory"
  },
  municipalityname: {
    gridArea: "municipalityname"
  }
});

class Statistics extends Component {
  constructor(props) {
    super(props);
    this.chart = null;
    this.state = {
      getReport: [],
      getNumberOfIssuesInCat: [],
      getNumberOfIssuesMunicipality: [],
      getNumberOfIssuesCompany: [],
      getAvgTimeInCat: [],
      getAvgTimeIssue: [],
      getActiveUsers: []
    };
  }

  /*componentDidMount(){
      this.setState({issues: this.props.issues})
  }*/

  convertSVGToImage = () => {
    let svgImage = document.querySelector("#numberOfIssuesInCatChart");
    let htmlString = ReactDOMServer.renderToStaticMarkup(svgImage);

    let canvas = document.createElement("canvas");
    canvas.width = 500;
    canvas.height = 500;

    canvg(canvas, htmlString);
    this.chart = canvas.toDataURL('image/png');
  }

  componentDidMount() {
    this.convertSVGToImage();
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.wrapper}>
        <div className={classes.calendar}>
          <CalendarReport getStatistics={this.getStatistics} />
        </div>
        <BarChart width={500} height={200} data={this.state.getNumberOfIssuesInCat} id="numberOfIssuesInCatChart">
          <XAxis dataKey='category' />
          <YAxis />
          <Bar type="monotone" dataKey="number_of_issues" barSize={20} fill="#8884d8" />
        </BarChart>
        <div className={classes.pdfPreviewer}>
          <PDFExport
            paperSize={"Letter"}
            fileName="rapport.pdf"
            title="RAPPORT"
            subject=""
            keywords=""
            forcePageBreak="hr"
            ref={r => (this.resume = r)}
          >
            <div
              style={{
                height: 792,
                width: 612,
                padding: "20px",
                backgroundColor: "white",
                boxShadow: "0px 0px 10px #999",
                margin: "auto",
                overflowX: "hidden",
                overflowY: "hidden"
              }}
            >
              <h4>Antall saker i kategorier</h4>
              {this.state.getNumberOfIssuesInCat.map((stat) => {
                return (
                  <div className={classes.statPrint} >
                    <div className={classes.title}>
                      <p>{stat.category}: {stat.number_of_issues}</p>
                    </div>
                  </div>
                );
              })}
              <img src={this.chart} />
              <hr></hr>

              <h4>Antall saker i hver kommune</h4>
              {this.state.getNumberOfIssuesMunicipality.map((stat) => {
                return (
                  <div className={classes.statPrint}>
                    <div className={classes.title}>
                      <p>{stat.municipality_name}: {stat.number_of_issues_for_municipality}</p>
                    </div>
                  </div>
                );
              })}
              <hr></hr>

              <h4>Antall saker i hvert firma</h4>
              {this.state.getNumberOfIssuesCompany.map((stat) => {
                return (
                  <div className={classes.statPrint}>
                    <div className={classes.title}>
                      <p>{stat.company_name}: {stat.number_of_issues_for_company}</p>
                    </div>
                  </div>
                );
              })}
              <hr></hr>

              <h4>Gjennomsnittstid for alle saker</h4>
              {this.state.getAvgTimeIssue.map((stat) => {
                return (
                  <div className={classes.statPrint}>
                    <div className={classes.title}>
                      <p>Gjennomsnittstid: {stat.avarage_time_on_a_issue}</p>
                    </div>
                  </div>
                );
              })}
              <hr></hr>

              <h4>Gjennomsnittstid for saker basert på kategori</h4>
              {this.state.getAvgTimeInCat.map((stat) => {
                return (
                  <div className={classes.statPrint}>
                    <div className={classes.title}>
                      <p>{stat.description}: {stat.avarage_time_for_cat}</p>
                    </div>
                  </div>
                );
              })}
              <hr></hr>
              <h4>Aktive brukere i hver kommune</h4>
              {this.state.getActiveUsers.map((stat) => {
                return (
                  <div className={classes.statPrint}>
                    <div className={classes.title}>
                      <p>{stat.municipality_name}: {stat.number_of_active_users}</p>
                    </div>
                  </div>
                );
              })}
              <hr></hr>

            </div>
          </PDFExport>
          <Button onClick={this.exportPDF}>downloadTextPDF</Button>
        </div>
      </div>
    );
  }

  /*onImageExportClick = () => {
    const chartVisual = exportVisual(this.BarChart);

    if (chartVisual) {
      this.chart = exportImage(chartVisual).then(dataURI => saveAs(dataURI, 'chart.png'));
    }
  }*/

  exportPDF = () => {
    this.resume.save();
  };

  getStatistics = (statisticsRec) => {
    this.setState({ 
      getReport: statisticsRec.getReport,
      getNumberOfIssuesInCat: statisticsRec.getNumberOfIssuesInCat,
      getNumberOfIssuesMunicipality: statisticsRec.getNumberOfIssuesMunicipality,
      getNumberOfIssuesCompany: statisticsRec.getNumberOfIssuesCompany,
      getAvgTimeInCat: statisticsRec.getAvgTimeInCat,
      getAvgTimeIssue: statisticsRec.getAvgTimeIssue,
      getActiveUsers: statisticsRec.getActiveUsers
    });
  };

  generatePDF() {
    const input = document.getElementById("capture");
    html2canvas(input).then(canvas => {
      const imgData = canvas.toDataURL("image/png");

      const pdf = new jsPDF();
      pdf.addImage(imgData, "PNG", 0, 0);
      pdf.save("download.pdf");
    });
  }
}

export default withStyles(styles)(Statistics);