//React
import React, { Component } from "react";
import ReactDOMServer from 'react-dom/server';
import Calendar from "react-calendar";
import {NavLink} from "react-router-dom";

//MaterialUI
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

//Charts
import { BarChart, Bar, YAxis, XAxis } from 'recharts';

//Own
import SimplePieChart from "./components/SimplePieChart";
import { statisticsService } from "../../serviceObjects/StatisticsService";
import { municipalityService } from "../../serviceObjects/municipalityService";
import VerticalBarChart from "./components/VerticalBarChart";
import StatDialog from "./components/StatDialog";

import DropDown from './components/DropDown';


//pdf
import html2canvas from "html2canvas";
import * as jsPDF from "jspdf";
import canvg from 'canvg';
// import { exportImage } from '@progress/kendo-drawing';
// import { saveAs } from '@progress/kendo-file-saver';


let styles = theme => ({
	pdfPage: {
		height: 792,
		width: 612,
		padding: "20px",
		backgroundColor: "white",
		boxShadow: "0px 0px 10px #999",
		margin: "auto",
		overflowX: "hidden",
		overflowY: "hidden",
		fontFamily: '"Times New Roman", Times, serif',
	},

	wrapper: {
		width: '100%',
		display: 'grid',
		gridTemplateColumns: '1fr 10fr 1fr',
		gridTemplateAreas:
			"'. top .'" +
			"'. mid .'" +
			"'. bot .'",
		[theme.breakpoints.down('sm')]: {
		}
	},
	actions: {
		gridArea: 'top',
		marginTop: '20px',
		height: '500px',
		display: 'inline-block',
		marginBottom: '100px',
	},
	municipalStats: {
		width: '100%',
		display: 'grid',
		gridTemplateColumns: '6fr 6fr',
		gridTemplateAreas:
			"'header header'" +
			"'one two'" +
			"'three four'",
		gridRowGap: '30px',
		gridColumnGap: '25px',
		[theme.breakpoints.down('sm')]: {
			width: '100%',
			gridTemplateColumns: '12fr',
			gridTemplateAreas:
				"'header'" +
				"'one'" +
				"'two'" +
				"'three'" +
				"'four'",
			gridRowGap: '10px',
		}
	},
	nationalStats: {
		width: '100%',
		display: 'grid',
		gridTemplateColumns: '6fr 6fr',
		gridTemplateAreas:
			"'header header'" +
			"'one two'" +
			"'three four'" +
			"'five six'",
		gridRowGap: '30px',
		gridColumnGap: '25px',
		[theme.breakpoints.down('sm')]: {
			width: '100%',
			gridTemplateColumns: '12fr',
			gridTemplateAreas:
				"'header'" +
				"'one'" +
				"'two'" +
				"'three'" +
				"'four'" +
				"'five'" +
				"'six'",
			gridRowGap: '10px',
		}
	},
	pieChartWrapper: {
		padding: '5px',
		width: '100%',
		height: '100%',
		margin: 'auto',
		boxShadow: '0px 0px 10px #999',
		[theme.breakpoints.down('sm')]: {
			width: '100%',
			boxShadow: '0px 0px 10px #999',
			//marginTop: '40px',
		}
	},
	chartTitle: {
		width: '100%',
		fontSize: '20px',
		//textAlign: 'center',
		padding: '4px',
		paddingLeft: '10px',
		borderBottom: '1px solid black',
	},
	graphGroupTitle: {
		gridArea: 'header',
		width: '100%',
		//fontSize: '40px',
		padding: '4px',
		paddingLeft: '10px',
		borderBottom: '1px solid black',
	},
	button:{
		height:'50px',
		marginTop:'20px',
	},
	button2:{
		height:'50px',
		marginTop:'20px',
		marginRight:'10px',
		[theme.breakpoints.down('sm')]: {
			marginRight:'0px',
			marginBottom: '200px',
			display: 'inline-block',
		}
	},
	link:{
		color:'black',
		'&:hover': {color: 'black'}
	}
});

class Statistics extends Component {
	constructor(props) {
		super(props);
		this.chart = null;
		this.state = {
			national: {
				getReport: [],
				getNumberOfIssuesInCat: [],
				getNumberOfIssuesMunicipality: [],
				getNumberOfIssuesCompany: [],
				getAvgTimeInCat: [],
				getAvgTimeIssue: [],
				getActiveUsers: []
			},
			municipal: {
				getReport: [],
				getNumberOfIssuesInCat: [],
				getNumberOfIssuesMunicipality: [],
				getNumberOfIssuesCompany: [],
				getAvgTimeInCat: [],
				getAvgTimeIssue: [],
				getActiveUsers: []
			},
			municipality_id: null,
			range: [],
			municipalities: [],
			renderMunicipalStats: false,
			renderNationalStats: false,
		};
	}

	generateReport = (range) => {
		statisticsService.getReport('', range[0], range[1])
			.then((res) => {
				this.setState({ national: res, renderNationalStats: true });
			});
		if (this.state.municipality_id !== null) {
			statisticsService.getReport(this.state.municipality_id, range[0], range[1])
				.then((res) => {
					this.setState({ municipal: res, renderMunicipalStats: true });
				});
		}
	}

	render() {
		const { classes } = this.props;
		let { national, municipal, municipalities, renderMunicipalStats, renderNationalStats, municipality_id, range } = this.state;

		return (
			<div className={classes.wrapper}>
				<div className={classes.actions}>
					<h5>Velg Periode</h5>
					<Calendar
						onChange={this.onCalenderChange}
						// value={this.state.date}
						selectRange={true}
						DateFormat={true}
						showWeekNumbers={true}
					/>
					<DropDown header="Velg kommune" name="municipality" actionHandler={this.changeMunicipality} items={municipalities} dataTag="municipality_name" />
					<Button variant="outlined" className={classes.button}> 
						<NavLink 
							to={"/statistics/print/" + municipality_id + "/" + range[0] + "/" + range[1]} 
							target="_blank"
							className={classes.link}
						>
							Print kommunal statistikk
						</NavLink> <br />
					</Button>
					
					<Button variant="outlined" className={classes.button2}> 
						<NavLink 
							to={"/statistics/print/" + "null" + "/" + range[0] + "/" + range[1]} 
							target="_blank"
							className={classes.link}
						>
							Print nasjonal statistikk
						</NavLink>
					</Button>
				</div>

				{/* Municipal statistics */}
				{(renderMunicipalStats === false || renderNationalStats === false) ? null :
					<div className={classes.municipalStats} style={{ gridArea: 'mid' }}>
						<div className={classes.graphGroupTitle}>
							<h1 className="display-4">Kommunal statistikk</h1>
						</div>
						<div style={{ gridArea: 'one' }} className={classes.pieChartWrapper}>
							<div className={classes.chartTitle}>
								Antall saker fordelt på kategori
							</div>
							<SimplePieChart data={municipal.getNumberOfIssuesInCat} />
							<StatDialog title="Antall saker fordelt på kategori">
								<VerticalBarChart data={municipal.getNumberOfIssuesInCat}/>
							</StatDialog>
						</div>
						<div style={{ gridArea: 'two' }} className={classes.pieChartWrapper}>
							<div className={classes.chartTitle}>
								Antall saker fordelt på bedrifter
							</div>
							<SimplePieChart data={municipal.getNumberOfIssuesCompany} />
							<StatDialog title="Antall saker fordelt på bedrifter">
								<VerticalBarChart data={municipal.getNumberOfIssuesCompany}/>
							</StatDialog>
						</div>
						<div style={{ gridArea: 'three' }} className={classes.pieChartWrapper}>
							<div className={classes.chartTitle}>
								Gjennomsnittstid per sak fordelt på kategori
							</div>
							<SimplePieChart data={municipal.getAvgTimeInCat} />
							<StatDialog title="Gjennomsnittstid per sak fordelt på kategori">
								<VerticalBarChart data={municipal.getAvgTimeInCat}/>
							</StatDialog>
						</div>
						<div style={{ gridArea: 'four' }} className={classes.pieChartWrapper}>
							<div className={classes.chartTitle}>
								Diverse statistikk
							</div>
							<p>
								{municipal.getActiveUsers.length === 0 ? null : "Antall aktive brukere: " + municipal.getActiveUsers[0].value}
							</p>
							<p>
								{municipal.getActiveUsers.length === 0 ? null : "Antall saker: " + municipal.getNumberOfIssuesMunicipality[0].value}
							</p>
							<p>
								{municipal.getActiveUsers.length === 0 ? null : "Gjennomsnittstid per sak: " + parseFloat(Math.round((municipal.getAvgTimeIssue[0].value / 3600) * 100) / 100).toFixed(2) + "timer"}
							</p>
						</div>
					</div>
				}


				{/* National statistics */}
				{renderNationalStats === false ? null :
					<div className={classes.nationalStats} style={{ gridArea: 'bot' }}>
						<div className={classes.graphGroupTitle}>
							<h1 className="display-4">Nasjonal statistikk</h1>
						</div>
						<div style={{ gridArea: 'one' }} className={classes.pieChartWrapper}>
							<div className={classes.chartTitle}>
								Antall saker fordelt på kategori
							</div>
							<SimplePieChart data={this.groupData(6, national.getNumberOfIssuesInCat)} />
							<StatDialog title="Antall saker fordelt på kategori">
								<VerticalBarChart data={national.getNumberOfIssuesInCat}/>
							</StatDialog>
						</div>
						<div style={{ gridArea: 'two' }} className={classes.pieChartWrapper}>
							<div className={classes.chartTitle}>
								Antall saker fordelt på bedrifter
							</div>
							<SimplePieChart data={this.groupData(6, national.getNumberOfIssuesCompany)} />
							<StatDialog title="Antall saker fordelt på bedrifter">
								<VerticalBarChart data={national.getNumberOfIssuesCompany}/>
							</StatDialog>
						</div>
						<div style={{ gridArea: 'three' }} className={classes.pieChartWrapper}>
							<div className={classes.chartTitle}>
								Antall saker fordelt på kommune
							</div>
							<SimplePieChart data={this.groupData(6, national.getNumberOfIssuesMunicipality)} />
							<StatDialog title="Antall saker fordelt på kommune">
								<VerticalBarChart data={national.getNumberOfIssuesMunicipality}/>
							</StatDialog>
						</div>
						<div style={{ gridArea: 'four' }} className={classes.pieChartWrapper}>
							<div className={classes.chartTitle}>
								Antall brukere fordelt på kommune
							</div>
							<SimplePieChart data={national.getActiveUsers} />
							<StatDialog title="Antall brukere fordelt på kommune">
								<VerticalBarChart data={national.getActiveUsers}/>
							</StatDialog>
						</div>
						<div style={{ gridArea: 'five' }} className={classes.pieChartWrapper}>
							<div className={classes.chartTitle}>
								Gjennomsnittstid fordelt på kategori
							</div>
							{national.getAvgTimeInCat.length === 0 ? null : <SimplePieChart data={national.getAvgTimeInCat} />}
						</div>
						<div style={{ gridArea: 'six' }} className={classes.pieChartWrapper}>
							<div className={classes.chartTitle}>
								Diverse statistikk
							</div>
							{national.getAvgTimeIssue.length === 0 ? null : "Gjennomsnittstid per sak: " + parseFloat(Math.round((national.getAvgTimeIssue[0].value / 3600) * 100) / 100).toFixed(2) + "timer"}
						</div>
					</div>
				}
			</div>
		);
	}

	groupData = (max_items, data) => {
		let shortenedData = [];
		let group = {name: "resten", value: 0};
		data.forEach((element, index) => {
			if(index < max_items-1){
				shortenedData.push(data[index]);
			}else{
				group.value += data[index].value;
			}
		});

		if(group.value !== 0){
			shortenedData.push(group);
		} 

		return shortenedData;
	}

	componentDidMount() {
		this.getAllMunicipalities();
	}

	changeMunicipality = (municipality_id) => {
		this.setState({ municipality_id: municipality_id, renderMunicipalStats: true }, () => {
			this.generateReport(this.state.range);
		});
	}

	getAllMunicipalities = () => {
		municipalityService.getMunicipalities()
			.then((response) => {
				this.setState({ municipalities: response });
			})
			.catch((error) => {
				console.error("Could not retrieve municipalities");
			});
	}

	onCalenderChange = (range) => {
		if (range[0] !== undefined && range[0] !== null && range[1] !== undefined && range[1] !== null) {
			range = [Math.round(range[0].valueOf()/1000), Math.round(range[1].valueOf()/1000)];
			this.generateReport(range);
			this.setState({ range: range});
		}
	}

	// generatePDF() {


	// 	let doc = new jsPDF({
	// 		orientation: 'portrait',
	// 		unit: 'pt',
	// 		format: [595.28, 841.89]
	// 	});


	// 	doc.setFontSize(20);
	// 	doc.text(50, 35, 'Hverdagshelt Rapport 21.01.2019-27.01.2019');



	// 	let svgImage = document.querySelector("#numberOfIssuesInCatChart-clip").parentElement;



	// 	// let canvas = document.createElement("canvas");
	// 	let canvas = document.querySelector("#canvastest");;
	// 	var pixelRatio = window.devicePixelRatio || 1;
	// 	console.log('pixelratio: ', pixelRatio);
	// 	/* canvas.style.width = canvas.width +'px';
	// 	canvas.style.height = canvas.height +'px';
	// 	canvas.width *= pixelRatio;
	// 	canvas.height *= pixelRatio;

	// 	let ctx = canvas.getContext('2d');
	// 	ctx.setTransform(pixelRatio,0,0,pixelRatio,0,0); */

	// 	canvg(canvas, svgImage.outerHTML);

	// 	let image = canvas.toDataURL('image/png', 1.0);

	// 	doc.addImage(image, 'JPEG', 50, 50);

	// 	doc.setFontSize(12)
	// 	doc.text(50, 230, 'Antall saker fordelt på kategorier nasjonalt');
	// 	//doc.output('dataurlnewwindow');
	// 	doc.save("download.pdf");

	// }
}

export default withStyles(styles)(Statistics);