//React
import React, { Component } from 'react';

//MaterialUI
import { withStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import OutlinedInput from '@material-ui/core/OutlinedInput';

let styles = {
    header: {
        marginBottom: '15px',
        marginTop: '15px'
    },
    dropDownWrapper: {
        minWidth: '100%'
    },
};

class ChangeStatus extends Component {
    constructor(props) {
        super(props);
        this.state = {
            companies: [],
            company_selected: "",
        }
    }

    componentDidMount() {
        // this.setState({
        //     company_selected: this.props.currentCompany
        // });
    }

    componentDidUpdate(prevProps) {
        // if (this.props !== prevProps) {
        //     this.setState({
        //         selectedItem: this.props.currentCompany
        //     });
        // }
    }
    render() {
        const { classes, cookies, items } = this.props;
        if (items !== undefined) {
            return (
                <FormControl variant="outlined" className={classes.dropDownWrapper}>
                    <h5 className={classes.header}>{this.props.header}</h5>
                    <Select
                        native
                        // value={this.props.company_id}
                        onChange={this.handleChange}
                        input={
                            <OutlinedInput
                                name={this.props.name}
                                labelWidth={this.state.labelWidth}
                            />
                        }
                    >   <option >
                            {this.props.header}
                        </option>
                        {items.map(items =>
                            <option value={items.id} key={items.id}>
                                {items[this.props.dataTag]}
                            </option>
                        )}
                    </Select>
                </FormControl>
            );
        }
        else{
            return null;
        }
    }
    handleChange = (event) => {
        if (this.props.actionHandler) {
            this.props.actionHandler(event.target.value);
        }
    };
}
export default withStyles(styles)(ChangeStatus);
