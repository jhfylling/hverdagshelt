//React
import React, { Component } from 'react';

//Material UI
import { withStyles } from '@material-ui/core';
import withWidth from '@material-ui/core/withWidth';

//Charts
import {
	PieChart,
	Pie,
	Sector,
	Cell,
	Tooltip,
	BarChart,
	Bar,
	YAxis,
	XAxis,
	LabelList,
	ResponsiveContainer,
	Legend
} from 'recharts';

let styles = (theme) => ({
	pdfPage: {
		height: 792,
		width: 612,
		padding: '20px',
		backgroundColor: 'white',
		boxShadow: '0px 0px 10px #999',
		margin: 'auto',
		overflowX: 'hidden',
		overflowY: 'hidden',
		fontFamily: '"Times New Roman", Times, serif'
	},

	chartWrapper: {
		width: '100%',
		height: '500px'
	}
});

class SimplePieChart extends Component {
	constructor(props) {
		super(props);
		this.RADIAN = Math.PI / 180;
		const data = [
			{ name: 'Group A', value: 400 },
			{ name: 'Group B', value: 300 },
			{ name: 'Group C', value: 300 },
			{ name: 'Group D', value: 200 }
		];

		this.state = {
			data: data
		};
	}

	renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
		const radius = innerRadius + (outerRadius - innerRadius) * 1.1;
		const x = cx + radius * Math.cos(-midAngle * this.RADIAN);
		const y = cy + radius * Math.sin(-midAngle * this.RADIAN);

		let { data } = this.state;
		if (this.props.data !== undefined && this.props.data.length > 0) {
			data = this.props.data;
		}

		return (
			<text x={x} y={y} fill="black" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
				{/* {`${(percent * 100).toFixed(0)}%`} */}
				{data[index].name + ': ' + data[index].value}
			</text>
		);
	};

	render() {
		const { classes } = this.props;

		//const COLORS = [ '#0088FE', '#00C49F', '#FFBB28', '#FF8042' ];
		const COLORS = [
			"rgb(103, 58, 183)",
			"rgb(0, 150, 136)",
			"rgb(255, 193, 7)",
			"rgb(233, 30, 99)",
			"rgb(76, 175, 80)",
			"rgb(96, 125, 139)",
			"rgb(121, 85, 72)",
			"rgb(255, 87, 34)",
			"rgb(156, 39, 176)",
			"rgb(0, 188, 212)",
			"rgb(139, 195, 74)"
		];

		let { data } = this.state;
		if (this.props.data !== undefined && this.props.data.length > 0) {
			data = this.props.data;
		}

		let deviceWidth = this.props.width;
		let chartRadius = '75%';
		if (deviceWidth === 'sm') {
			chartRadius = '75%';
		} else if (deviceWidth === 'xs') {
			chartRadius = '75%';
		}

		return (
			<div className={classes.chartWrapper}>
				<ResponsiveContainer width="100%" height="100%">
					<PieChart onMouseEnter={this.onPieEnter}>
						<Pie
							data={data}
							// cx={160}
							// cy={100}
							margin={{ top: 20, right: 30, left: 0, bottom: 0 }}
							labelLine={false}
							//label={this.renderCustomizedLabel}
							outerRadius={chartRadius}
							fill="#8884d8"
							isAnimationActive={false}
						>
							{data.map((entry, index) => <Cell fill={COLORS[index % COLORS.length]} />)}
							
                            {/*<LabelList dataKey="value" position="top"/>*/}

						</Pie>
						<Tooltip />
						<Legend />
					</PieChart>
				</ResponsiveContainer>
			</div>
		);
	}
}

export default withWidth()(withStyles(styles)(SimplePieChart));
