
//React
import React, { Component } from "react";

//Material UI
import { withStyles } from "@material-ui/core";
import withWidth from '@material-ui/core/withWidth';

//Charts
import { PieChart, Pie, Sector, Cell, Tooltip, BarChart, Bar, YAxis, XAxis, LabelList, ResponsiveContainer } from 'recharts';




let styles = theme => ({
    chartWrapper: {
        width: '100%',
        height: '100%',
    }
});

class SimplePieChart extends Component {

    constructor(props) {
        super(props);
        const data =
            [
                { name: 'Group A', value: 400 }, { name: 'Group B', value: 300 },
                { name: 'Group C', value: 300 }, { name: 'Group D', value: 200 }
            ];

        this.state = {
            data: data,
        };
    }

    render() {
        const { classes } = this.props;

        let { data } = this.state;
        if (this.props.data !== undefined && this.props.data.length > 0) {
            data = this.props.data;
        }


        return (
            <div className={classes.chartWrapper}>
                <ResponsiveContainer
                    width='100%'
                    height='100%'
                >
                    <BarChart 
                        width={'100%'} 
                        // height={500} 
                        data={data} 
                        id="numberOfIssuesInCatChart"
                        layout="vertical"
                    >
                        <YAxis dataKey='name' type='category' width={130}/>
                        <XAxis type='number' />
                        <Bar type="monotone" dataKey="value" barSize={10} fill="#8884d8" />
                    </BarChart>
                </ResponsiveContainer>
            </div>
        );
    }
}

export default withWidth()(withStyles(styles)(SimplePieChart));
