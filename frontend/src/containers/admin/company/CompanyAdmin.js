//React
import React, { Component } from 'react';

//MaterialUI
import { withStyles } from '@material-ui/core/styles';
import Card from "@material-ui/core/Card/Card";
import CardHeader from "@material-ui/core/CardHeader/CardHeader";
import {logoColor, reportButtonColor} from "../../../components/Colors";

//Own
import {businessService} from '../../../serviceObjects/businessService';
import IssueTable from "../../../components/issueTable/IssueTable";
import {issueService} from "../../../serviceObjects/IssueService";


const styles = {
    header: {
        backgroundColor: reportButtonColor,
        color: logoColor,
        textAlign: 'center',
        padding: '10px',
        color: 'white',
    }
};

/**
 * Table with issues assigned to a company
 */
class CompanyAdmin extends Component {
    constructor(props){
        super(props);
        this.state = {
            issues: []
        }
    }

    componentDidMount(){
        const {cookies} = this.props;
        businessService.getBusinessByUserId(cookies.get('userInfo').id)
            .then(company => {
                if(company[0] !== undefined){
                    issueService.getIssuesByCompany(company[0].id)
                        .then(issues => {
                            this.setState({issues: issues});
                        })
                        .catch(error => {
                            console.log(error);
                        });
                }

            })
            .catch((error) => {
                console.log("AdministerIssues: Error finding business: ", error);
            });
    }

    render(){
        const {classes} = this.props;
        return (
            <Card>
                <div className={classes.header}> 
                    <h2>Saker din bedrift er tildelt</h2>
                </div>
                <IssueTable issues={this.state.issues} company_user={true}/>
            </Card>
        );
    }
}

export default withStyles(styles)(CompanyAdmin);