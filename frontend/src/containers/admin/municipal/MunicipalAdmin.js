/*eslint no-useless-concat: "off"*/
//React
import React, { Component } from 'react';

//MaterialUI
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';

//Own
import IssueTable from './components/IssueTable';
import { municipalityService } from '../../../serviceObjects/municipalityService';
import CompanyTable from '../../../components/companyTable/CompanyTable';
import RegisterNewCompany from './components/RegisterNewCompany';
import { reportButtonColor, logoColor } from '../../../components/Colors';
import PositionedSnackbar from '../../../components/Snackbar';
import CreateEvent from './components/CreateEvent';
import EventTable from '../../../components/eventTable/EventTable';
import CreateMunicipalUser from './components/CreateMunicipalUser';
import AdministerCategories from './components/AdministerCategories';
import Statistics from '../../statistics/Statistics';

const styles = (theme) => ({
	cardStyle: {
		margin: '4px'
	},
	root: {
		flexGrow: 1
	},
	tableHeader: {
		backgroundColor: reportButtonColor,
		color: logoColor,
		textAlign: 'center',
		padding: '10px'
	},
	createWrapper: {
		display: 'grid',
		gridTemplateColumns: '1fr 3fr 1fr',
		gridTemplateAreas: '" . createCard . "',
		[theme.breakpoints.down('sm')]: {
			display: 'grid',
			gridTemplateColumns: '1fr',
			gridTemplateAreas: '"createCard"'
		}
	},
	createCard: {
		gridArea: 'createCard',
		margin: '20px',
		[theme.breakpoints.down('sm')]: {
			marginTop: '20px',
			marginBottm: '2px',
			marginRight: '2px',
			marginLeft: '2px',
		}
	}
});

function TabContainer(props) {
	return <Typography component="div">{props.children}</Typography>;
}

/**
 * Issues municipal users can administer 
 */
class MunicipalAdmin extends Component {
	constructor(props) {
		super(props);
		this.state = {
			municipality_name: null,
			municipality_id: 0,
			value: 0,
			eventListUpdated: false,
			companyAdded: false,
		};
	}
	handleChange = (event, value) => {
		this.setState({ value });
	};
	updateList() {
		this.setState({ eventListUpdated: !this.state.eventListUpdated });
	}
	updatedCompany() {
		this.setState({ companyAdded: !this.state.companyAdded });
	}

	render() {
		let title = <h2>Kommunale Saker</h2>;
		if (this.state.municipality_name !== null) {
			title = <h2>Kommunale Saker {this.state.municipality_name}</h2>;
		}
		let company = <h2>Bedriftsbrukere</h2>;
		if (this.state.municipality_name !== null) {
			company = <h2>Bedriftsbrukere {this.state.municipality_name}</h2>;
		}
		const { classes } = this.props;

		const {cookies} = this.props;
		
		let statIndex = 3;
		let categoryIndex = 4;
		let categoryTab = null;
		let statisticTab = (<Tab label={'Statistikk '}/>);
		if(cookies.get('userInfo').category_id === 2){
			categoryTab = ( <Tab label={'Kategori Administrasjon'} />);
			categoryIndex = 3;
			statIndex = 4;
		} 
		
		
		
		return (
			<div>
				<Paper className={classes.root}>
					<Tabs
						value={this.state.value}
						onChange={this.handleChange}
						variant="scrollable"
						scrollButtons="auto"
					>
						<Tab label={'Administrering av saker' } />
						<Tab label={'Administrering av bedrifter' } />
						<Tab label={'Administrering av events '} />
						{categoryTab}
						{statisticTab}
					</Tabs>
				</Paper>

				{this.state.value === 0 && (
					<TabContainer>
						<div className={classes.tables}>
							<Card className={classes.cardStyle}>
								<div className={classes.tableHeader}>{title}</div>

								<IssueTable
									municipality_id={this.state.municipality_id !== 0 ? this.state.municipality_id : 1}
									cookies={this.props.cookies}
									loadInfowindow={this.loadInfowindow}
								/>
							</Card>
						</div>
						<div className={classes.createWrapper}>
							<Card className={classes.createCard}>
								<div className={classes.tableHeader}>
									<h2>Opprett Kommunal Bruker</h2>
								</div>
								<CreateMunicipalUser cookies={this.props.cookies} />
							</Card>
						</div>
					</TabContainer>
				)}
				{this.state.value === 1 && (
					<TabContainer>
						<div className={classes.tables}>
							<Card className={classes.cardStyle}>
								<div className={classes.tableHeader}>{company}</div>
								<CompanyTable
									municipality_id={this.state.municipality_id !== 0 ? this.state.municipality_id : 1}
									cookies={this.props.cookies}
									loadInfowindow={this.loadInfowindow}
									updated={this.state.companyAdded}
								/>
							</Card>
							<div className={classes.createWrapper}>
								<Card className={classes.createCard}>
									<RegisterNewCompany
										cookies={this.props.cookies}
										updatedCompany={this.updatedCompany.bind(this)}
									/>
								</Card>
							</div>
						</div>
					</TabContainer>
				)}
				{this.state.value === 2 && (
					<TabContainer>
						<Card className={classes.cardStyle}>
							<div className={classes.tableHeader}>
								<h2>
									Kommunale Events{' '}
									{this.state.municipality_name !== undefined ? (
										this.state.municipality_name
									) : (
										''
									)}{' '}
								</h2>
							</div>
							<EventTable
								municipality_id={this.state.municipality_id !== 0 ? this.state.municipality_id : 1}
								cookies={this.props.cookies}
								loadInfowindow={this.loadInfowindow}
								eventListUpdated={this.state.eventListUpdated}
							/>
						</Card>
						<div className={classes.createWrapper}>
							<Paper className={classes.createCard}>
								<CreateEvent cookies={this.props.cookies} updateList={this.updateList.bind(this)} />
							</Paper>
						</div>
					</TabContainer>
				)}
				{this.state.value === categoryIndex && (
					<TabContainer>
						<Card className={classes.cardStyle}>
							<div className={classes.tableHeader}>
								<h2>
									Administrer Kategorier
								</h2>
							</div>
							<AdministerCategories />
						</Card>
					</TabContainer>
				)}
				{this.state.value === statIndex && (
					<TabContainer>
						{/* <Card className={classes.cardStyle}>
							<div>
								<h2 className={classes.tableHeader}>
									Statistikk
								</h2>
							</div>
						</Card> */}
						<Statistics/>

					</TabContainer>
				)}
				<PositionedSnackbar />
			</div>
		);
	}

	componentDidMount() {
		let { cookies } = this.props;
		municipalityService
			.getMunicipality(cookies.get('userInfo').municipality_id)
			.then((response) => {
				if (response[0].municipality_name !== undefined && response[0].municipality_name !== '') {
					this.setState({ municipality_name: response[0].municipality_name });
				}
			})
			.catch((error) => {
				console.log('MunicipalAdmin: Error finding muncipality: ', error);
			});
		
	}
	loadInfowindow(issueId) {
		this.setState({ infoWindowId: issueId });
	}
}

export default withStyles(styles)(MunicipalAdmin);
