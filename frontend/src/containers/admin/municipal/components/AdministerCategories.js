//React
import React, { Component } from 'react';

//MaterialUI
import { withStyles } from '@material-ui/core/styles';

//HverdagsHelt
import CategoryTable from '../../../../components/categoryTable/CategoryTable';

const styles = {
	cardStyle: {
		margin: '4px'
	}
};

/**
 * Categories admin can administer
 */
class AdministerCategories extends Component {
	render() {
		const { classes } = this.props;
		return (
			<div className={classes.tables}>
				<CategoryTable loadInfowindow={this.loadInfowindow} />
			</div>
		);
	}

	loadInfowindow(issueId) {
		this.setState({ infoWindowId: issueId });
	}
}
export default withStyles(styles)(AdministerCategories);
