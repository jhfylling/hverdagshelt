//React
import React from 'react';

//Material UI
import { withStyles } from '@material-ui/core/styles';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import Button from '@material-ui/core/Button';

//Own
import { userService } from '../../../../serviceObjects/userService';
import { businessService } from '../../../../serviceObjects/businessService';
import { handleSnackbarOpen } from '../../../../components/Snackbar';
import PositionedSnackbar from '../../../../components/Snackbar';
import { reportButtonColor, logoColor } from '../../../../components/Colors';

const styles = (theme) => ({

	header:{
        backgroundColor: reportButtonColor,
        color: logoColor,
        padding: '10px',
        textAlign:'center',
    },
	content:{
		padding: '20px',
	},
	input:{
		marginTop: '10px',
		marginBottom: '20px',
	},
	button:{
		marginTop: '30px',
		height: '50px',
	}
	
});

class RegisterNewCompany extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			company_name: '',
			description: '',
			email: ''
		};
	}

	render() {
		const { classes } = this.props;
		const { company_name, description, email } = this.state;
		return (
			<div className={classes.wrapper}>
				<div className={classes.header}><h2>Opprett bedrift og bedriftsbruker</h2></div>
				<div className={classes.content}>
					<div>
						<ValidatorForm
							id="registerForm"
							ref="form"
							onSubmit={this.handleSubmit}
							onError={(errors) => console.log(errors)}
							
						>	
							<h5>Bedriftsnavn</h5>
							<TextValidator
								fullWidth
								name={'company_name'}
								value={company_name}
								variant={'outlined'}
								label={'Navn på bedrift'}
								onChange={this.handleChange}
								validators={[ 'required' ]}
								errorMessages={[ 'Fyll inn navn på bedrift' ]}
								className={classes.input}
							/>
							<h5>Beskrivelse</h5>
							<TextValidator
								fullWidth
								name={'description'}
								value={description}
								variant={'outlined'}
								label={'Beskrivelse'}
								onChange={this.handleChange}
								validators={[ 'required' ]}
								errorMessages={[ 'Fyll inn beskrivelse' ]}
								className={classes.input}

							/>
							<h5>Email</h5>
							<TextValidator
								fullWidth
								name={'email'}
								value={email}
								variant={'outlined'}
								label={'Epost'}
								onChange={this.handleChange}
								validators={[ 'required', 'isEmail' ]}
								errorMessages={[ 'Fyll inn E-mail', 'Mail adresse må være ekte' ]}
								className={classes.input}

							/>
						</ValidatorForm>
					</div>
					<div >
						<Button className={classes.button} variant="outlined" onClick={this.handleSubmit}>
							Opprett bedrift
						</Button>
					</div>
				</div>
				<PositionedSnackbar/>
			</div>
		);
	}

	handleChange = (event) => {
		if (event.target.name === 'company_name') {
			const company_name = event.target.value;
			this.setState({ company_name });
		}
		if (event.target.name === 'description') {
			const description = event.target.value;
			this.setState({ description });
		}
		if (event.target.name === 'email') {
			const email = event.target.value;
			this.setState({ email });
		}
	};

	clearInput(){
		this.setState({
			company_name: '',
			description: '',
			email: ''
		});
	}

	handleSubmit = () => {
		const { cookies } = this.props;
		const company_name = this.state.company_name;
		const description = this.state.description;
		const email = this.state.email;
		const municipality_id = cookies.get('userInfo').municipality_id;
		const new_user_category_id = 4;
		let password = Math.random().toString(36).substring(2, 10);

		userService
			.createUser(email, password, municipality_id, new_user_category_id)
			.then(() => {
				businessService
					.createCompany(company_name, description)
					.then(() => {
						businessService
							.createCompanyUserAssociation(email, company_name)
							.then(() => {
								handleSnackbarOpen({ message: 'Bedriftsbruker opprettet! ', variant: 'success' });
								this.props.updatedCompany();
								this.clearInput();
							})
							.then(() => {
								//Send mail with link to set password
								userService.resetPasswordLink(email, true).catch(error => console.log(error));
							})
							.catch((error) => {
								console.log(error);
								handleSnackbarOpen({ message: 'Kunne ikke opprette bruker', variant: 'error'})
							});
					})
					.catch((error) => {
						console.log(error);
						handleSnackbarOpen({ message: 'En feil har oppstått', variant: 'error' });
					});
			})
			.catch((error) => {
				console.log(error);
				handleSnackbarOpen({ message: 'En feil har oppstått', variant: 'error' });
			});
	};
}

export default withStyles(styles)(RegisterNewCompany);