/*eslint no-useless-concat: "off"*/
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

//Material-UI
import { withStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select/Select';
import InputLabel from '@material-ui/core/InputLabel/InputLabel';
import FormControl from '@material-ui/core/FormControl/FormControl';
import OutlinedInput from '@material-ui/core/OutlinedInput/OutlinedInput';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import Button from '@material-ui/core/Button/Button';

//Own
import { municipalityService } from '../../../../serviceObjects/municipalityService';
import { userService } from '../../../../serviceObjects/userService';
import PositionedSnackbar, { handleSnackbarOpen } from '../../../../components/Snackbar';

const styles = (theme) => ({
	containerWrapper: {
		display: 'grid',
		gridTemplateColumns: '1fr 10fr 1fr',
		gridTemplateAreas: "'. email .'" + "'. municipality . '" + "'. buttonArea .'"
	},
	email: {
		gridArea: 'email',
		marginTop: '20px',
		marginBottom: '10px'
	},
	municipality: {
		gridArea: 'municipality',
		marginTop: '20px',
		marginBottom: '10px'
	},
	buttonArea: {
		gridArea: 'buttonArea',
		marginTop: '20px',
		marginBottom: '20px',
	},
	button:{
		height: '50px',
	}
});

class CreateMunicipalUser extends Component {
	constructor(props) {
		super(props);
		this.state = {
			municipalities: [],
			email: '',
			password: '',
			municipality_id: 0,
			token: '',
			labelWidth: 0,
		};
	}
	clearInput() {
		this.setState({
			email: '',
			password: '',
			municipality_id: 0
		});
	}

	componentDidMount() {
		this.setState({
			labelWidth: ReactDOM.findDOMNode(this.InputLabelRef).offsetWidth
    });
		municipalityService
			.getMunicipalities()
			.then((municipalities) => this.setState({ municipalities: municipalities }))
			.catch((error) => console.log(error));
	}

	render() {
		const { classes } = this.props;
		return (
			<div className={classes.containerWrapper}>
				<div className={classes.email}>
					<ValidatorForm
						id="registerForm"
						ref="form"
						onSubmit={this.handleSubmit}
						onError={(errors) => {
							console.log(errors);
							handleSnackbarOpen({ message: 'En feil oppstod.', variant: 'error' });
						}}
					>
						<h5>Skriv Email</h5>
						<TextValidator
							id={'email'}
							value={this.state.email}
							variant={'outlined'}
							label={'Epost'}
							name={'email'}
							onChange={(event) => this.inputChangedHandler(event)}
							validators={[ 'required', 'isEmail' ]}
							errorMessages={[ 'Dette feltet er påkrevd!', 'E-post-adressen er ugyldig!' ]}
							validatorListener={this.validatorListener}
						/>
					</ValidatorForm>
				</div>
				
				<div className={classes.municipality}>
					<h5>Velg Kommune </h5>
					<FormControl variant="outlined">
						<InputLabel
							ref={ref => {
								this.InputLabelRef = ref;
							}}
							htmlFor="outline-age-native-simple">
							Velg kommune
						</InputLabel>
						<Select native id={'municipality_id'} value={this.state.municipality_name}
							onChange={(event) => this.inputChangedHandler(event)} input={
								<OutlinedInput
									name="age"
									labelWidth={this.state.labelWidth}
									//id="municipality_id"
									id="outlined-age-native-simple"
								/>
							}>
							<option> </option>
								{this.state.municipalities.map((municipality, i) => (
									<option key={i} value={municipality.id}>
										{municipality.municipality_name}
									</option>
								))}
						</Select>
					</FormControl>
				</div>
				<div className={classes.buttonArea}>
					<Button type={'submit'} form={'registerForm'} className={classes.button} variant="outlined">
						Opprett bruker
					</Button>
				</div>

				<PositionedSnackbar />
			</div>
		);
	}

	inputChangedHandler = (event) => {
		let updatedValue = event.target.value;
		let updatedItem = event.target.id;
		this.setState({ [updatedItem]: updatedValue });
	};

	/**
     * Handles submission of new users
     */
	handleSubmit = () => {
		const email = this.state.email;
		const municipality_id = this.state.municipality_id;
		const new_user_category_id = 3;
		let password = Math.random().toString(36).substring(2, 10);

		userService
			.createUser(email, password, municipality_id, new_user_category_id)
			.then((response) => {
                handleSnackbarOpen({ message: 'Bruker opprettet!', variant: 'success' });
                this.clearInput();
                userService
					.resetPasswordLink(email)
					.catch((error) => console.error(error));
			})
			.catch((error) => {
				console.log('Error: ', error);
				handleSnackbarOpen({ message: 'En feil oppstod.', variant: 'error' });
			});
	};
}

export default withStyles(styles)(CreateMunicipalUser);
