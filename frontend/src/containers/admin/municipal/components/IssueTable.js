//React
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

//Material-UI
import List from '@material-ui/core/List';
import ListSubheader from '@material-ui/core/ListSubheader';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { withStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';

//Own
import { issueService } from '../../../../serviceObjects/IssueService';
import ListRow from './ListRow';
import ListHeader from './ListHeader';

let styles = {
	list: {
		height: '50vh',
		overflow: 'auto'
	}
};


class IssueTable extends Component {
	constructor(props) {
		super(props);
		this.state = {
			issues: [''],
		};
	}

	componentDidMount() {
		if (this.props.municipality_id) {
            issueService
                .getIssues(this.props.municipality_id, [1,2,3,4])
                .then((issues) => {
                    if (issues !== undefined) {
                        this.setState({ issues: issues });
                    }
                })
                .catch((error) => console.log(error));
        } else if (this.props.user_email) {
            issueService
                .getIssuesByUser(this.props.user_email)
                .then((issues) => {
                    if (issues !== undefined) {
                        this.setState({ issues: issues });
                    }
                })
                .catch((error) => console.log(error));

        }

	}
	componentDidUpdate(prevProps) {
		if (prevProps !== this.props) {
            if (this.props.municipality_id) {
                issueService
                    .getIssues(this.props.municipality_id, [1,2,3,4])
                    .then((issues) => {
                        if (issues !==undefined) {
                            this.setState({ issues: issues });
                        }
                    })
                    .catch((error) => console.log(error));
            } else if (this.props.user_email) {
                issueService
                    .getIssuesByUser(this.props.user_email)
                    .then((issues) => {
                        if (issues !==undefined) {
                            this.setState({ issues: issues });
                        }
                    })
                    .catch((error) => console.log(error));
            }
		}
	}

	render() {
		const { classes } = this.props;
		return (
			<List className={classes.list}>
				<ListSubheader style={{background:'white', paddingTop: '-50px'}}>
					<ListItemText>
                        <ListHeader/> 
                        <Divider/>
                    </ListItemText>
				</ListSubheader>
                
				{this.state.issues.map((issue, i) => (
					<ListItem key={i} role={undefined} dense button component={Link} to={'../saker/' + issue.id}>
						<ListItemText>
							<ListRow issue={issue} />{' '}
						</ListItemText>
					</ListItem>

				))}
                {this.state.issues[0] == null ? <div style={{textAlign: 'center'}}>Du har ingen registrete saker</div> : <div></div>}
			</List>
		);
	}
}

export default withStyles(styles)(IssueTable);
