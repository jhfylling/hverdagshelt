//React
import React, { Component } from 'react';

//Material-UI
import { withStyles } from '@material-ui/core/styles';


let styles = {
	rowWrapper: {
		display: 'grid',
		gridTemplateColumns: '1fr 1fr 1fr 1fr',
		gridTemplateAreas: "' title status position date'",
        textAlign:'center',
	},
	title: {
		gridArea: 'title',
        height: '40px',
	},
	status: {
		gridArea: 'status'
	},
	positon: {
		gridArea: 'position'
	},
	date: {
		gridArea: 'date'
	},
	
};

class ListHeader extends Component {
	
	render() {
		const { classes } = this.props;

		return (
			<div className={classes.rowWrapper}>
				<div className={classes.title}><b><i className="fas fa-file fa-sm"/>&nbsp; Tittel</b></div>
				<div className={classes.status}><b><i className="fas fa-spinner fa-sm"/>&nbsp; Status</b></div>
				<div className={classes.position}><b><i className="fas fa-map-marker-alt fa-sm"/> &nbsp; Adresse</b></div>
				<div className={classes.date}><b><i className="fas fa-calendar-week fa-sm"/> &nbsp; Dato</b></div>
				
			</div>
		);
	}
}

export default withStyles(styles)(ListHeader);
