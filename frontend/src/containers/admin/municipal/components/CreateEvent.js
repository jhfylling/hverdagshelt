//React
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

//MaterialUI
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Button from '@material-ui/core/Button';

//Own
import { eventService } from '../../../../serviceObjects/eventService';
import { reportButtonColor, logoColor } from '../../../../components/Colors';
import { handleSnackbarOpen } from '../../../../components/Snackbar';

let styles = {
	content: {
		padding: '20px'
	},
	button: {
		marginTop: '30px',
		height: '50px',
	},
	input: {
		marginTop: '10px',
		marginBottom: '20px'
	}
};

class CreateEvent extends Component {
	constructor(props) {
		super(props);
		this.state = {
			category: '',
			categories: [ '' ],
			title: '',
			adress: '',
			description: '',
			price: '',
			url: '',
			labelWidth: 0

		};
	}
	componentDidMount() {
		eventService.getEventCategories().then((categories) => this.setState({ categories })).catch((error) => {
			console.log('AdministerIssues: Error finding muncipality categories: ', error);
		});
		this.setState({
      labelWidth: ReactDOM.findDOMNode(this.InputLabelRef).offsetWidth
    });
	}
	handleChange = (name) => (event) => {
		this.setState({
			[name]: event.target.value
		});
	};
	handleSubmit() {
		let category_selected = this.state.categories.find((category) => category.description === this.state.category)
			.id;

		eventService
			.createEvent(
				this.state.title,
				this.state.description,
				0,
				0,
				this.state.price,
				this.state.url,
				category_selected,
				this.props.cookies.get('userInfo').municipality_id
			)
			.then((response) => {
				handleSnackbarOpen({ message: 'Opprettet event', variant: 'success' });
				this.props.updateList();
			})
			.then(() => this.clearInput())
			.catch((error) => console.log(error));
	}
	clearInput(){
		this.setState({
			category: '',
			title: '',
			adress: '',
			description: '',
			price: '',
			url: ''
		})
	}

	render() {
		const { classes } = this.props;

		return (
			<div>
				<div
					style={{
						backgroundColor: reportButtonColor,
						color: logoColor,
						padding: '10px',
						textAlign: 'center'
					}}
				>
					<h2>Lag et nytt event</h2>
				</div>
				<div className={classes.content}>
					<div>
						<h5>Tittel</h5>
						<TextField
							label={'Tittel'}
							onChange={this.handleChange('title')}
							value={this.state.title}
							margin="dense"
							variant="outlined"
							fullWidth
							className={classes.input}
						/>
					</div>
					<div>
						<h5>Addresse</h5>
						<TextField
							label={'Addresse'}
							onChange={this.handleChange('adress')}
							value={this.state.adress}
							margin="dense"
							variant="outlined"
							fullWidth
							className={classes.input}
						/>
					</div>
					<div>
						<h5> Beskrivelse</h5>
						<TextField
							label={'Beskrivelse'}
							onChange={this.handleChange('description')}
							value={this.state.description}
							margin="dense"
							variant="outlined"
							fullWidth
							className={classes.input}
						/>
					</div>
					<div>
						<h5> Pris</h5>
						<TextField
							type="number"
							label={'Pris'}
							onChange={this.handleChange('price')}
							value={this.state.price}
							margin="dense"
							variant="outlined"
							fullWidth
							className={classes.input}
						/>
					</div>
					<div>
						<h5> Hjemmeside til Event</h5>
						<TextField
							label={'Url'}
							onChange={this.handleChange('url')}
							value={this.state.url}
							margin="dense"
							variant="outlined"
							fullWidth
							className={classes.input}
						/>
					</div>
					<div>
						<h5> Velg Kategori</h5>
						<FormControl variant="outlined" className={classes.input}>
							<InputLabel 
							ref={ref => {
								this.InputLabelRef = ref;
							}}
							htmlFor="outlined-category-native-simple" >Velg kategori</InputLabel>
							<Select
								style={{ width: '200px' }}
								native
								value={this.state.category}
								onChange={this.handleChange('category')}
								input={
									<OutlinedInput
										name="status"
										labelWidth={this.state.labelWidth}
										id="outlined-category-native-simple"
									/>
								}
							>	
							<option/>
								{this.state.categories.map((category,i) => (
									<option key={i} value={category.description}>{category.description}</option>
								))}
							</Select>
						</FormControl>
					</div>
					<div>
						<Button className={classes.button} variant="outlined" onClick={this.handleSubmit.bind(this)}>
							Opprett Event
						</Button>
					</div>
				</div>
			</div>
		);
	}
}

export default withStyles(styles)(CreateEvent);
