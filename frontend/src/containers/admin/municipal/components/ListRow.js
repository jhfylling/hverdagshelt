//React
import React, { Component } from 'react';
import Geocode from 'react-geocode';

//Material-UI
import { withStyles } from '@material-ui/core/styles';

let styles = {
  rowWrapper: {
    display: "grid",
    gridTemplateColumns: "1fr 1fr 1fr 1fr",
    gridTemplateAreas: "' title status position date button'",
    textAlign: "center"
  },
  title: {
    gridArea: "title",
    height: "60px"
  },
  status: {
    gridArea: "status"
  },
  positon: {
    gridArea: "position"
  },
  date: {
    gridArea: "date"
  },
  button: {
    gridArea: "button"
  },
  icon: {
    backgroundColor: "#fffd38"
  }
};

class ListRow extends Component {
	constructor(props) {
		super(props);
		this.state = {
			issues: '',
			address: ''
		};
	}
	
    componentDidMount(){
        if(this.props.issue.latitude !== 0 && this.props.issue.longitude!==0){
            Geocode.setApiKey('AIzaSyATAz6K9aKSkbyQMLbivjIrp5KoPJ8_rX4');
            Geocode.fromLatLng( this.props.issue.latitude,  this.props.issue.longitude).then(
                (response) => {
                    let address = response.results[0].formatted_address;
                    this.setState({ address: address });
                },
                (error) => {
                    this.setState({ address: '' });
                }
            );
        }
    }

	render() {
		const { classes } = this.props;
		return (
			<div className={classes.rowWrapper}>
				<div className={classes.title}><b>{this.props.issue.title === null ? 'Ikke satt' : this.props.issue.title}</b></div>
				<div className={classes.status}>
					<b>
					{this.props.issue.status_id === 1 ? <i style={{ color: '#ffa720'}} className="fas fa-exclamation-triangle"/>: "" }
					
						{this.props.issue.status}
					</b>
				</div>
				<div className={classes.position}>{this.state.address === '' ? 'Ikke satt' : this.state.address}</div>
				<div className={classes.date}><b>{this.props.issue.date}</b></div>
			</div>
		);
	}
}

export default withStyles(styles)(ListRow);
