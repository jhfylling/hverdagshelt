
const Db = require("./db.js");

module.exports = class ProgressUpdateDAO extends Db{
  getUpdates(issue_id, callback){
    super.query("SELECT id, issue_id, DATE_FORMAT(update_date, \"%m.%d.%Y %H:%i\") AS update_date, updatemessage FROM progress_update WHERE issue_id = ?;", [issue_id], callback);
  }

  saveUpdate(data, callback){
    super.query("INSERT INTO progress_update (id, issue_id, updatemessage, update_date, user_id) VALUES (DEFAULT, ?, ?, DEFAULT, ?);",
    [data.issue_id, data.updatemessage, data.user_id],
    callback);
  }
};
