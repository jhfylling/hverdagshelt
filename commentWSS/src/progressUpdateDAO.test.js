var mysql = require("mysql");
var fs = require("fs");

const ProgressUpdateDAO = require('./progressUpdateDAO.js');

// Johandev MySQL server
var pool = mysql.createPool({
  connectionLimit: 1,
  host: "johandev.no",
  user: "hverdagsheltsys",
  password: "sVPFWg5EOYNFWs4N",
  database: "hverdagshelttest",
  debug: false,
  multipleStatements: true
});

var progressUpdateDAO = new ProgressUpdateDAO(pool);

afterAll(() => {
  pool.end((err) => {
    if(err){
      console.log(err);
    }
    else{
      console.log("Disconnected from database\nTest completed");
    }
  });
});

beforeAll(done => {
    runSQLfile("../backend/sql/createTable.sql", pool, () => {
        runSQLfile("../backend/sql/createData.sql", pool, done);
    });
});

test("getUpdates", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );

    expect(data.length).toBe(2);
    expect(data[0].updatemessage).toBe("Kampen AS har registrert saken og er på vei for å tette hullet");

    done();
  }

    progressUpdateDAO.getUpdates(1, callback);
});




test("saveUpdate", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    
    if(status === 200){
      expect(data.affectedRows).toBe(1);
      expect(data.serverStatus).toBe(2);
      expect(data.warningCount).toBe(0);
    }
    else{
      expect(status).toBe(200);
      console.error(data);
    }

      progressUpdateDAO.getUpdates(3, (status, data) => {
      expect(data[1].updatemessage).toBe("12390asdklcfutcfut123bvdfhsfgh0990234sdfvb");
      done();
    });

  }
  
  let inputdata = {body: "12390asdklcfutcfut123bvdfhsfgh0990234sdfvb", news_id: 3};
    progressUpdateDAO.saveUpdate(inputdata, callback);
});

var runSQLfile = ((filename, pool, done) => {
  console.log("runsqlfile: reading file " + filename);
  let sql = fs.readFileSync(filename, "utf8");
  pool.getConnection((err, connection) => {
    if (err || connection === undefined) {
      console.log("runsqlfile: error connecting");
      done();
    } else {
      console.log("runsqlfile: connected");
      connection.query(sql, (err, rows) => {
        connection.release();
        if (err) {
          console.log(err);
          done();
        } else {
          console.log("runsqlfile: run ok");
          done();
        }
      });
    }
  });
});
