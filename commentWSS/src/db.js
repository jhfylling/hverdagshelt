
var mysql = require("mysql");

module.exports = class Db{
  constructor(pool){
    this.pool = pool;
  }

  query(sql, params, callback){
    this.pool.getConnection((err, connection) => {
      if(err){
        callback(500, {error: "Error with database connection"});
      }
      else{
        connection.query(sql, params, (err, rows) => {
          connection.release();
          if(err){
            callback(500, {error: err});
          }
          else{
            callback(200, rows);
          }
        });
      }
    });
  }
};
