const WebSocket = require('ws');
const mysql = require("mysql");
const ProgressUpdateDAO = require('./progressUpdateDAO.js');

const wss = new WebSocket.Server({ port: 3002 });

console.log("Started commentWS server");

let pool = mysql.createPool({
  connectionLimit: 2,
  host: "johandev.no",
  user: "hverdagsheltsys",
  password: "sVPFWg5EOYNFWs4N",
  database: "hverdagshelt",
  debug: false
});

let progressUpdateDAO = new ProgressUpdateDAO(pool);

let connections = [];

 
wss.on('connection', function connection(ws) {
    
    console.log("Got new connection");
    ws.on('close', function close(){
        console.log("Closed connection");
        let conTemp = [];
        connections.forEach((conArt) => {
            if(conArt.socket != ws){
                conTemp.push(conArt);
            }
        });

        connections = conTemp;
    });

    ws.on('message', function incoming(message) {
        console.log("Recieved message: ", message);
        let msgJson = {};
        let parsed = true;
        try{
            msgJson = JSON.parse(message);
        }
        catch(e){
            console.log("Incorrect data recieved: " + e);
            parsed = false;
        }

        if(parsed){
            if(msgJson.validator == 'safgn90aasdgfvvv!!z3c5xjy08'){
                if(msgJson.operation == 'load'){

                    let progressUpdateConnection = {id: msgJson.id, socket: ws};
                    connections.push(progressUpdateConnection);

                    progressUpdateDAO.getUpdates(msgJson.id, (status, data) => {
                        ws.send(JSON.stringify(data));
                    });
                }
                if(msgJson.operation == 'save'){
                    let data = {updatemessage: msgJson.data.updatemessage, issue_id: msgJson.id, user_id: msgJson.data.user_id};
                    progressUpdateDAO.saveUpdate(data, (status, data) => {
                        if(status == 200){
                            console.log("Saved update message");
                            sendUpdates(msgJson.id);
                        }
                        else{
                            console.log("Could not save update message: " + status, data);
                        }
                        //Send update to clients currently reading the same article
                    });
                }
            }
        }  
    });
});

// Broadcast to all clients reading the issue with given id
function sendUpdates(id){
    progressUpdateDAO.getUpdates(id, (status, data) => {
        connections.forEach((progressUpdateConnection) => {
            if(progressUpdateConnection.id == id && progressUpdateConnection.socket.readyState === WebSocket.OPEN){
                progressUpdateConnection.socket.send(JSON.stringify(data));
            }
        });
        
    });
}

// Broadcast to all.
wss.broadcast = function broadcast(data) {
    wss.clients.forEach(function each(client) {
        if (client.readyState === WebSocket.OPEN) {
            client.send(data);
        }
    });
};