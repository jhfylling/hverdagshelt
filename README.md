# HverdagsHelt

Scrum prosjekt dataingeniør vår 2019

Nettside for rapportering av feil og mangler for alle kommuner i Norge.

Frontend bygget med React og Material Ui med et REST-api skrevet i Node.js og MySQL som backend.

Teamet på 8 personer brukte 3-4 uker på prosjektet.

Resultatet ble karakter A.