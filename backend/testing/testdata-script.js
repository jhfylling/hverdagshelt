var fs = require("fs");
var bcrypt = require("bcryptjs");
var Keypair = require("keypair");

var keypairBitSize = 2048;
var saltRounds = 10;

console.log("Generating jwt keypair...");
var keypair = Keypair(keypairBitSize);

try{ 
    var path = "./config/testConfig.js";
    fs.accessSync(path, fs.constants.R_OK)
    var contents = fs.readFileSync(path, "utf8");
    var rspub = new RegExp("-----BEGIN RSA PUBLIC KEY-----.*-----END RSA PUBLIC KEY-----", "g");
    var rspri = new RegExp("-----BEGIN RSA PRIVATE KEY-----.*-----END RSA PRIVATE KEY-----", "g");

    keypair.private = keypair.private.replace(new RegExp("\n", "g"), "\\n");
    keypair.public = keypair.public.replace(new RegExp("\n", "g"), "\\n");

    contents = contents.replace(rspub, keypair.public);
    contents = contents.replace(rspri, keypair.private);

    fs.writeFileSync(path, contents);
} catch(exception){   
    console.log("Caught exception when accessing file. Exception: " + exception);                                            
}

function hashPassword(password){
    return bcrypt.hashSync(password, saltRounds);
}

console.log("Creating testdata...");
class lineMaker{
    constructor(){
        this.lines = "";
        this.tableCount = 0;

        this.lines += 
        "USE hverdagshelttest;\n";/*+
        "DELETE FROM user_category;\n"+
        "DELETE FROM municipality;\n"+
        "DELETE FROM user_account;\n"+
        "DELETE FROM user_details;\n"+
        "DELETE FROM status;\n"+
        "DELETE FROM issue_category;\n"+
        "DELETE FROM company;\n"+
        "DELETE FROM issue;\n"+
        "DELETE FROM status_issue;\n"+
        "DELETE FROM progress_update;\n"+
        "DELETE FROM issue_company;\n"+
        "DELETE FROM company_municipality;\n"+
        "DELETE FROM company_user_account;\n"+
        "DELETE FROM event_category;\n"+
        "DELETE FROM event;\n\n"*/
    }
}
lineMaker.prototype.newTable = function(table_name, columns){
    this.columns = columns;
    var table = "INSERT INTO %table_name (%columns) VALUES ";
    this.tableLineCount = 0;

    table = table.replace("%table_name", table_name);
    
    var columns_string = "";
    for(var i = 0; i < columns.length; i++){
        columns_string += columns[i] + ((i != columns.length - 1) ? ", " : "");
    }
    table = table.replace("%columns", columns_string);

    this.table = table;
    this.lines += ((this.tableCount == 0)?(""):("\n\n")) + "\/* --" + table_name + "-- \*/\n";
    this.lines += table;
    this.tableCount++;
}
lineMaker.prototype.newLine = function(values){
    var newLine;

    if(this.tableLineCount == 0){
        newLine = "\n(%values)";
    } else{
        newLine = ",\n(%values)";
    }

    var values_string = "";
    for(var i = 0; i < values.length; i++){
        values_string += values[i] + ((i != values.length - 1) ? ", " : "");
    }
    newLine = newLine.replace("%values", values_string);    

    this.lines += newLine;
    this.tableLineCount++;
}

lineMaker.prototype.newComment = function(comment){
    this.lines += "\n\t\/* " + comment + " */";
}

lineMaker.prototype.endTable = function(values){
    var newLine = ";";
    this.lines += newLine;
}

lineMaker.prototype.getLines = function(){
    return this.lines;
}

var lm = new lineMaker();

/* user_category */
lm.newTable("user_category", ["id", "category_name"]);
lm.newLine(["1", "'Privat bruker'"]);
lm.newLine(["2", "'Administrator'"]);
lm.newLine(["3", "'Kommunal bruker'"]);
lm.newLine(["4", "'Bedriftsbruker'"]);
lm.endTable();

/* municipality */
lm.newTable("municipality", ["id", "municipality_name", "longitude", "latitude"]);
lm.newLine(["1", "'Trondheim'", "10", "10"]);
lm.newLine(["2", "'Oslo'", "15", "10"]);
lm.newLine(["3", "'Bergen'", "20", "20"]);
lm.newLine(["4", "'Drammen'", "11", "11"]);
lm.newLine(["5", "'Bodø'", "0", "0"]);
lm.endTable();

/* user_account */
lm.newTable("user_account", ["id", "email", "hash", "municipality_id", "category_id"]);
lm.newComment("Hashes are made from the users email address.");
lm.newLine(["1", "'sigurd@mail.no'", "'" + hashPassword("sigurd@mail.no") + "'", "1", "1"]);                // Privat bruker
lm.newLine(["2", "'jonas@mail.no'", "'" + hashPassword("jonas@mail.no") + "'", "1", "1"]);                  // Privat bruker
lm.newLine(["3", "'magnus@mail.no'", "'" + hashPassword("magnus@mail.no") + "'", "1", "1"]);                // Privat bruker
lm.newLine(["4", "'johan-henrik@mail.no'", "'" + hashPassword("johan-fredrik@mail.no") + "'", "1", "1"]);   // Privat bruker
lm.newLine(["5", "'kevin@mail.no'", "'" + hashPassword("kevin@mail.no") + "'", "1", "1"]);                  // Privat bruker
lm.newLine(["6", "'kristian@mail.no'", "'" + hashPassword("kristian@mail.no") + "'", "1", "4"]);            // Bedriftsbruker
lm.newLine(["7", "'morten@mail.no'", "'" + hashPassword("morten@mail.no") + "'", "1", "4"]);                // Bedriftsbruker
lm.newLine(["8", "'fredrik@mail.no'", "'" + hashPassword("fredrik@mail.no") + "'", "1", "4"]);              // Bedriftsbruker

lm.newLine(["9", "'privat@test.no'", "'" + hashPassword("123") + "'", "1", "1"]);                           // Privat bruker
lm.newLine(["10", "'administrator@test.no'", "'" + hashPassword("123") + "'", "1", "2"]);                    // Administrator
lm.newLine(["11", "'kommunal@test.no'", "'" + hashPassword("123") + "'", "1", "3"]);                         // Kommunal bruker
lm.newLine(["12", "'bedrift@test.no'", "'" + hashPassword("123") + "'", "1", "4"]);                          // Bedriftsbruker
lm.newLine(["13", "'nodetails@test.no'", "'" + hashPassword("123") + "'", "1", "1"]);                          // Bedriftsbruker
lm.endTable();

/* user_details */
lm.newTable("user_details", ["id", "firstname", "lastname", "phone_number"]);
lm.newLine(["1", "'Sigurd'", "'Hynne'", "'91928361'"]);
lm.newLine(["2", "'Jonas'", "'Kristoffersen'", "'68573948'"]);
lm.newLine(["3", "'Magnus'", "'Dahl'", "'94837456'"]);
lm.newLine(["4", "'Johan-Henrik'", "'Fylling'", "'94837456'"]);
lm.newLine(["5", "'Kevin'", "'Mentzoni Halvarsson'", "'23448577'"]);
lm.newLine(["6", "'Kristian'", "'Kampenhøy'", "'19457362'"]);
lm.newLine(["7", "'Morten'", "'Nordseth'", "'94827239'"]);
lm.newLine(["8", "'Fredrik'", "'Monsen'", "'83467890'"]);
lm.newLine(["9", "'Privatersen'", "'Testoffersen'", "'11111111'"]);
lm.newLine(["10", "'Administratorsen'", "'Testsen'", "'22222222'"]);
lm.newLine(["11", "'Kommunalsen'", "'Testers'", "'33333333'"]);
lm.newLine(["12", "'Bedriftsen'", "'Testan'", "'44444444'"]);
lm.endTable();

/* status */
lm.newTable("status", ["id", "type"]);
lm.newLine(["1", "'Registrert'"]);
lm.newLine(["2", "'Under behandling'"]);
lm.newLine(["3", "'Påbegynt'"]);
lm.newLine(["4", "'Ferdig'"]);
lm.endTable();

/* issue_category */
lm.newTable("issue_category", ["id", "description"]);
lm.newLine(["1", "'Vei'"]);
lm.newLine(["2", "'Trafikk'"]);
lm.newLine(["3", "'Strøm'"]);
lm.newLine(["4", "'Vinterdrift'"]);
lm.newLine(["5", "'Grafitti/Tagging'"]);
lm.endTable();

/* company */
lm.newTable("company", ["id", "name", "description"]);
lm.newLine(["1", "'Asfalt Remix AS'", "'Fresing av asfalt og betong'"]);
lm.newLine(["2", "'Vegvesenet'", "'Trafikk og veisikkerhet'"]);
lm.newLine(["3", "'TronderEnergi AS'", "'Strøm og energi'"]);
lm.endTable();

/* issue */
lm.newTable("issue", ["id", "title", "description", "issue_date", "longitude", "latitude", "picture_filename", "issue_category_id", "municipality_id", "user_id", "estimated_time", "isEnabled"]);
lm.newLine(["1", "'Title 1'", "'Description 1'", "'2020-02-05 10:19:18'", "1.11", "1.22", "'filename1.img'", "1", "1", "1", "NULL", "true"]);
lm.newLine(["2", "'Title 2'", "'Description 2'", "'2020-02-05 10:19:18'", "2.11", "2.22", "'filename2.img'", "2", "2", "2", "NULL", "true"]);
lm.newLine(["3", "'Title 3'", "'Description 3'", "'2020-02-05 10:19:18'", "3.11", "3.22", "'filename3.img'", "3", "3", "3", "NULL", "false"]);
lm.newLine(["4", "'Title 4'", "'Description 4'", "'2020-02-05 10:19:18'", "4.11", "4.22", "'filename4.img'", "4", "4", "4", "NULL", "false"]);
lm.newLine(["5", "'Title 5'", "'Description 5'", "'2020-02-05 10:19:18'", "5.11", "5.22", "'filename5.img'", "5", "5", "5", "NULL", "true"]);
lm.newLine(["6", "'Title 6'", "'Description 6'", "'2020-02-05 10:19:18'", "6.11", "6.22", "'filename6.img'", "1", "1", "6", "NULL", "false"]);
lm.newLine(["7", "'Title 7'", "'Description 7'", "'2020-02-05 10:19:18'", "7.11", "7.22", "'filename7.img'", "2", "2", "7", "NULL", "true"]);
lm.endTable();

/* status_issue */
lm.newTable("status_issue", ["id", "status_id", "issue_id", "update_date"]);
lm.newLine(["1", "1", "1", "'2018-12-07 08:17:16'"]);
lm.newLine(["2", "2", "2", "'2019-01-04 09:18:17'"]);
lm.newLine(["3", "3", "3", "'2020-02-05 10:19:18'"]);
lm.endTable();

/* progress_update */
lm.newTable("progress_update", ["issue_id", "updatemessage", "update_date", "user_id"]);
lm.newLine(["1", "'Issue 1 Update 2'", "'2019-01-04 09:18:17'", "1"]);
lm.newLine(["1", "'Issue 1 Update 1'", "'2018-12-07 08:17:16'", "1"]);
lm.newLine(["2", "'Issue 2 Update 1'", "'2020-02-05 10:19:18'", "3"]);
lm.newLine(["3", "'Issue 3 Update 1'", "'2020-02-05 10:19:18'", "4"]);
lm.endTable();

/* issue_company */
lm.newTable("issue_company", ["issue_id", "company_id", "update_date"]);
lm.newLine(["1", "1", "DEFAULT"]);
lm.newLine(["2", "2", "DEFAULT"]);
lm.newLine(["3", "1", "DEFAULT"]);
lm.endTable();

/* company_municipality */
lm.newTable("company_municipality", ["company_id", "municipality_id"]);
lm.newLine(["1", "4"]);
lm.newLine(["2", "1"]);
lm.newLine(["3", "4"]);
lm.newLine(["1", "3"]);
lm.newLine(["1", "5"]);
lm.endTable();

/* company_user_account */
lm.newTable("company_user_account", ["company_id", "user_account_id"]);
lm.newLine(["1", "5"]);
lm.newLine(["2", "6"]);
lm.newLine(["3", "7"]);
lm.newLine(["3", "12"]);
lm.endTable();

/* event_category */
lm.newTable("event_category", ["id", "description"]);
lm.newLine(["1", "'Kultur'"]);
lm.newLine(["2", "'Konsert'"]);
lm.newLine(["3", "'Sport'"]);
lm.endTable();

/* event */
lm.newTable("event", ["id", "title", "description", "event_date", "longitude", "latitude", "price", "event_homepage", "event_category_id", "municipality_id", "isEnabled"]);
lm.newLine(["1", "'Title 1'", "'Description 1'", "'2020-02-05 10:19:18'", "1", "1", "100", "'website.com/1'", "1", "1", "true"]);
lm.newLine(["2", "'Title 2'", "'Description 2'", "'2020-02-05 10:19:18'", "2", "2", "200", "'website.com/2'", "2", "2", "true"]);
lm.newLine(["3", "'Title 3'", "'Description 3'", "'2020-02-05 10:19:18'", "3", "3", "300", "'website.com/3'", "3", "3", "true"]);
lm.newLine(["4", "'Title 4'", "'Description 4'", "'2020-02-05 10:19:18'", "4", "4", "400", "'website.com/4'", "1", "4", "false"]);
lm.newLine(["5", "'Title 5'", "'Description 5'", "'2020-02-05 10:19:18'", "5", "5", "500", "'website.com/5'", "2", "5", "false"]);
lm.newLine(["6", "'Title 6'", "'Description 6'", "'2020-02-05 10:19:18'", "6", "6", "600", "'website.com/6'", "3", "1", "false"]);
lm.newLine(["7", "'Title 7'", "'Description 7'", "'2020-02-05 10:19:18'", "7", "7", "700", "'website.com/7'", "1", "1", "true"]);
lm.endTable();

try{
    var path = "./testing/createTestData.sql";
fs.writeFileSync(path, lm.getLines());
} catch(exception){
    console.log("Caught exception when writing to file. Exception: " + exception);                                            
}
