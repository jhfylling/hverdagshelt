USE hverdagshelttest;
/* --user_category-- */
INSERT INTO user_category (id, category_name) VALUES 
(1, 'Privat bruker'),
(2, 'Administrator'),
(3, 'Kommunal bruker'),
(4, 'Bedriftsbruker');

/* --municipality-- */
INSERT INTO municipality (id, municipality_name, longitude, latitude) VALUES 
(1, 'Trondheim', 10, 10),
(2, 'Oslo', 15, 10),
(3, 'Bergen', 20, 20),
(4, 'Drammen', 11, 11),
(5, 'Bodø', 0, 0);

/* --user_account-- */
INSERT INTO user_account (id, email, hash, municipality_id, category_id) VALUES 
	/* Hashes are made from the users email address. */
(1, 'sigurd@mail.no', '$2a$10$y6.YPPVJfT8bOtr/Bhwcw..Cw9CgS9DcBY6XdvwZMjGbdWaMLAM1a', 1, 1),
(2, 'jonas@mail.no', '$2a$10$6BSLuK3hUNGCnEm8SMRrfee8FP3VFzu6WMmVYxMy9rfzk1i1Jm5OC', 1, 1),
(3, 'magnus@mail.no', '$2a$10$bDyjVvJ78YVbtqEqh9Azp.k85YOmy1zmOdw.S0Kv3m2xgrq.edPfW', 1, 1),
(4, 'johan-henrik@mail.no', '$2a$10$94jvVNItP8zDdlyLEi6IBuGg35c0E.hE8Lkc1uBpjq1cJBtiS/mbm', 1, 1),
(5, 'kevin@mail.no', '$2a$10$rHjnYRyJovYHOUgw5lwr5.LBuBi27pfVx137lLURZcn55oeIxPTpe', 1, 1),
(6, 'kristian@mail.no', '$2a$10$zE9YfNlMG0VDbONe7y1dJeK17WSSWBqnVsCen6VcS0iThsUkif7bi', 1, 4),
(7, 'morten@mail.no', '$2a$10$P.m763o4GUKGykVXAtLsteJqvLyxnBRZMEO50TUUWRA7q5fLlmo1W', 1, 4),
(8, 'fredrik@mail.no', '$2a$10$htHOlobsWwp9OA1GV/gikefk3Fng5rUx9TNd9ffnA4oHdESCGhmJy', 1, 4),
(9, 'privat@test.no', '$2a$10$smbCexfJRHxNvjy6RO4IWeRauCBDPCrJR0EgvCtrNBqeu.L5yJvQG', 1, 1),
(10, 'administrator@test.no', '$2a$10$2tQWS6tIiFtfMszEkh7di.z0hqh2GoNjwdwKUtJsQsEy9fca2Nx2W', 1, 2),
(11, 'kommunal@test.no', '$2a$10$cRga5dT.qaiXE7KOlKwape.QSkIw2dxrN5f2FpPvibK710t.jXsKW', 1, 3),
(12, 'bedrift@test.no', '$2a$10$jpZFiqydyo2Z5auil1cQSepSTVg1TC4DetUXrySRlWb9Qz79CqdQu', 1, 4),
(13, 'nodetails@test.no', '$2a$10$a2VDuRFX/hJBmNN8DITdyuW20fl4L1TVCBGsw1LuE9khO/9ATtF6q', 1, 1);

/* --user_details-- */
INSERT INTO user_details (id, firstname, lastname, phone_number) VALUES 
(1, 'Sigurd', 'Hynne', '91928361'),
(2, 'Jonas', 'Kristoffersen', '68573948'),
(3, 'Magnus', 'Dahl', '94837456'),
(4, 'Johan-Henrik', 'Fylling', '94837456'),
(5, 'Kevin', 'Mentzoni Halvarsson', '23448577'),
(6, 'Kristian', 'Kampenhøy', '19457362'),
(7, 'Morten', 'Nordseth', '94827239'),
(8, 'Fredrik', 'Monsen', '83467890'),
(9, 'Privatersen', 'Testoffersen', '11111111'),
(10, 'Administratorsen', 'Testsen', '22222222'),
(11, 'Kommunalsen', 'Testers', '33333333'),
(12, 'Bedriftsen', 'Testan', '44444444');

/* --status-- */
INSERT INTO status (id, type) VALUES 
(1, 'Registrert'),
(2, 'Under behandling'),
(3, 'Påbegynt'),
(4, 'Ferdig');

/* --issue_category-- */
INSERT INTO issue_category (id, description) VALUES 
(1, 'Vei'),
(2, 'Trafikk'),
(3, 'Strøm'),
(4, 'Vinterdrift'),
(5, 'Grafitti/Tagging');

/* --company-- */
INSERT INTO company (id, name, description) VALUES 
(1, 'Asfalt Remix AS', 'Fresing av asfalt og betong'),
(2, 'Vegvesenet', 'Trafikk og veisikkerhet'),
(3, 'TronderEnergi AS', 'Strøm og energi');

/* --issue-- */
INSERT INTO issue (id, title, description, issue_date, longitude, latitude, picture_filename, issue_category_id, municipality_id, user_id, estimated_time, isEnabled) VALUES 
(1, 'Title 1', 'Description 1', '2020-02-05 10:19:18', 1.11, 1.22, 'filename1.img', 1, 1, 1, NULL, true),
(2, 'Title 2', 'Description 2', '2020-02-05 10:19:18', 2.11, 2.22, 'filename2.img', 2, 2, 2, NULL, true),
(3, 'Title 3', 'Description 3', '2020-02-05 10:19:18', 3.11, 3.22, 'filename3.img', 3, 3, 3, NULL, false),
(4, 'Title 4', 'Description 4', '2020-02-05 10:19:18', 4.11, 4.22, 'filename4.img', 4, 4, 4, NULL, false),
(5, 'Title 5', 'Description 5', '2020-02-05 10:19:18', 5.11, 5.22, 'filename5.img', 5, 5, 5, NULL, true),
(6, 'Title 6', 'Description 6', '2020-02-05 10:19:18', 6.11, 6.22, 'filename6.img', 1, 1, 6, NULL, false),
(7, 'Title 7', 'Description 7', '2020-02-05 10:19:18', 7.11, 7.22, 'filename7.img', 2, 2, 7, NULL, true);

/* --status_issue-- */
INSERT INTO status_issue (id, status_id, issue_id, update_date) VALUES 
(1, 1, 1, '2018-12-07 08:17:16'),
(2, 2, 2, '2019-01-04 09:18:17'),
(3, 3, 3, '2020-02-05 10:19:18');

/* --progress_update-- */
INSERT INTO progress_update (issue_id, updatemessage, update_date, user_id) VALUES 
(1, 'Issue 1 Update 2', '2019-01-04 09:18:17', 1),
(1, 'Issue 1 Update 1', '2018-12-07 08:17:16', 1),
(2, 'Issue 2 Update 1', '2020-02-05 10:19:18', 3),
(3, 'Issue 3 Update 1', '2020-02-05 10:19:18', 4);

/* --issue_company-- */
INSERT INTO issue_company (issue_id, company_id, update_date) VALUES 
(1, 1, DEFAULT),
(2, 2, DEFAULT),
(3, 1, DEFAULT);

/* --company_municipality-- */
INSERT INTO company_municipality (company_id, municipality_id) VALUES 
(1, 4),
(2, 1),
(3, 4),
(1, 3),
(1, 5);

/* --company_user_account-- */
INSERT INTO company_user_account (company_id, user_account_id) VALUES 
(1, 5),
(2, 6),
(3, 7),
(3, 12);

/* --event_category-- */
INSERT INTO event_category (id, description) VALUES 
(1, 'Kultur'),
(2, 'Konsert'),
(3, 'Sport');

/* --event-- */
INSERT INTO event (id, title, description, event_date, longitude, latitude, price, event_homepage, event_category_id, municipality_id, isEnabled) VALUES 
(1, 'Title 1', 'Description 1', '2020-02-05 10:19:18', 1, 1, 100, 'website.com/1', 1, 1, true),
(2, 'Title 2', 'Description 2', '2020-02-05 10:19:18', 2, 2, 200, 'website.com/2', 2, 2, true),
(3, 'Title 3', 'Description 3', '2020-02-05 10:19:18', 3, 3, 300, 'website.com/3', 3, 3, true),
(4, 'Title 4', 'Description 4', '2020-02-05 10:19:18', 4, 4, 400, 'website.com/4', 1, 4, false),
(5, 'Title 5', 'Description 5', '2020-02-05 10:19:18', 5, 5, 500, 'website.com/5', 2, 5, false),
(6, 'Title 6', 'Description 6', '2020-02-05 10:19:18', 6, 6, 600, 'website.com/6', 3, 1, false),
(7, 'Title 7', 'Description 7', '2020-02-05 10:19:18', 7, 7, 700, 'website.com/7', 1, 1, true);