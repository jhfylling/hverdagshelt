var mysql = require("mysql");

const StatisticsDao = require("../../src/dao/statisticsDao");
const runsqlfile = require("../runsqlfile");
var config = require("../../config/index")

var pool = mysql.createPool(config.poolOptions);

let statisticsDao = new StatisticsDao(pool);

beforeAll((done => {
    runsqlfile(__dirname + "/../createTestTable.sql", pool, () => {
        runsqlfile(__dirname + "/../createTestData.sql", pool, done)
    })
}));

afterAll(() => {
    pool.end();
});

test("test getReport()", done => {
    function callback(status, data) {
        console.log("Test callback status: " + status + ", test data: " + data.length);
        expect(data.length).toBeGreaterThanOrEqual(2);
        done();
    }

    statisticsDao.getReport(
        {
            municipality_id: '',
            start_date: '1483275736',
            end_date: '1672578136'
        }
        , 
        callback);
});

test("test getActiveUsers()", done => {
    function callback(status, data) {
        console.log("Test callback status: " + status + ", test data: " + data.length);
        expect(data.length).toBeGreaterThanOrEqual(1);
        done();
    }

    statisticsDao.getActiveUsers(
        {
            municipality_id: '',
            start_date: '1483275736',
            end_date: '1672578136'
        }
        ,
        callback);
});

test("test getAvgTimeInCat()", done => {
    function callback(status, data) {
        console.log("Test callback status: " + status + ", test data: " + data.length);
        expect(data.length).toBeGreaterThanOrEqual(0);
        done();
    }

    statisticsDao.getAvgTimeInCat(
        {
            municipality_id: '',
            start_date: '1483275736',
            end_date: '1672578136'
        }
        ,
        callback);
});

test("test getAvgTimeIssue()", done => {
    function callback(status, data) {
        console.log("Test callback status: " + status + ", test data: " + data.length);
        expect(data.length).toBeGreaterThanOrEqual(1);
        done();
    }

    statisticsDao.getAvgTimeIssue(
        {
            municipality_id: '',
            start_date: '1483275736',
            end_date: '1672578136'
        }
        ,
        callback);
});

test("test getNumberOfIssuesCompany()", done => {
    function callback(status, data) {
        console.log("Test callback status: " + status + ", test data: " + data.length);
        expect(data.length).toBeGreaterThanOrEqual(2);
        done();
    }

    statisticsDao.getNumberOfIssuesCompany(
        {
            municipality_id: '',
            start_date: '1483275736',
            end_date: '1672578136'
        }
        ,
        callback);
});

test("test getNumberOfIssuesInCat()", done => {
    function callback(status, data) {
        console.log("Test callback status: " + status + ", test data: " + data.length);
        expect(data.length).toBeGreaterThanOrEqual(2);
        done();
    }

    statisticsDao.getNumberOfIssuesInCat(
        {
            municipality_id: '',
            start_date: '1483275736',
            end_date: '1672578136'
        }
        ,
        callback);
});

test("test getNumberOfIssuesMunicipality()", done => {
    function callback(status, data) {
        console.log("Test callback status: " + status + ", test data: " + data.length);
        expect(data.length).toBeGreaterThanOrEqual(2);
        done();
    }

    statisticsDao.getNumberOfIssuesMunicipality(
        {
            municipality_id: '',
            start_date: ('1483275736'),
            end_date: ('1672578136')
        }
        ,
        callback);
});