var server = require("../../src/server.js");
var app = server.app;
var request = require("supertest");
var chai = require("chai");
var expect = chai.expect;
var config = require("../../config/index")
var jwt = require("jsonwebtoken");

var runsqlfile = require("../runsqlfile");

function createToken(category_id, userinfo){
    if(category_id){
        userinfo.category_id = category_id;
    }
    return jwt.sign(userinfo,
        config.JWT_keypair.private,
        {
            algorithm: 'RS256',
            expiresIn: '5m'
        });
}

beforeAll((done => {
    runsqlfile(__dirname + "/../createTestTable.sql", server.pool, () => {
        runsqlfile(__dirname + "/../createTestData.sql", server.pool, done)
    })
}));


describe("IssueRoutes tests", function () {
    describe("GET /statustypes", function () {
        describe("Existing statustypes", function () {
            it("Should retrieve statustypes", function (done) {
                request(app)
                    .get("/statustypes")
                    .end(function (error, res) {
                        expect(res.statusCode).to.equal(200); // Ok
                        expect(error).to.be.null;
                        expect(JSON.stringify(res.body[0])).to.equal(
                            '{"id":1,"type":"Registrert"}'
                        );
                        done();
                    });
            });
        });
    });
    describe("GET /issues/", function () {
        describe("Existing issues in municipality", function () {
            it("Should retrieve issues in municipality", function (done) {
                request(app)
                    .get("/issues")
                    .query({
                        "municipality_id": 1,
                        "span": [1]
                    })
                    .end(function (error, res) {
                        expect(res.statusCode).to.equal(200); // Ok
                        expect(error).to.be.null;
                        expect(JSON.stringify(res.body[0])).to.equal(
                            '{"id":1,"title":"Title 1","description":"Vei","date":"05.02.20","longitude":1.11,"latitude":1.22,"picture_filename":"filename1.img","name":"Asfalt Remix AS","userId":1,"status":"Registrert","update_date":"2018-12-07T07:17:16.000Z","municipality_id":1,"status_id":1}'
                        );
                        done();
                    });
            });
        });

        describe("Non-existing municipality", function () {
            it("Should not retrieve issues", function (done) {
                request(app)
                    .get("/issues")
                    .query({
                        "municipality_id": 10000,
                        "span": [1]
                    })
                    .end(function (error, res) {
                        expect(res.statusCode).to.equal(200); // Ok
                        expect(error).to.be.null;
                        expect(res.body[0]).to.be.undefined;
                        done();
                    });
            });
        });
    });

    describe("GET /issues", function () {
        describe("Get issues by ID", function () {
            it("Should retrieve issue by ID", function (done) {
                request(app)
                    .get("/issues/" + 1)
                    .end(function (error, res) {
                        expect(res.statusCode).to.equal(200); // Ok
                        expect(error).to.be.null;
                        
                        var categorySpan = [1];

                        expect(JSON.stringify(res.body[0])).to.equal(
                            '{"id":1,"title":"Title 1","description":"Description 1","date":"05.02.20","longitude":1.11,"latitude":1.22,"picture_filename":"filename1.img","issue_category_id":1,"estimated_time":null,"name":"Asfalt Remix AS","issue_status_description":"Vei","userId":1,"status":"Registrert","status_id":1,"update_date":"2018-12-07T07:17:16.000Z","municipality_id":1}'
                        );
                        done();
                    });
            });
        });

        describe("Non-Existing issue", function () {
            it("Should retrieve no issues", function (done) {
                request(app)
                    .get("/issues/" + 99)
                    .end(function (error, res) {
                        expect(res.statusCode).to.equal(200); // Ok
                        expect(error).to.be.null;

                        expect(JSON.stringify(res.body)).to.be.equal("[]");
                        done();
                    });
            });
        });

        describe("Get status for issue by ID", function () {
            it("Should retrieve a status for issues by ID", function (done) {
                request(app)
                    .get("/issues/" + 1 + "/status")
                    .end(function (error, res) {
                        expect(res.statusCode).to.equal(200); // Ok
                        expect(error).to.be.null;

                        expect(JSON.stringify(res.body[0])).to.equal(
                            '{"status_id":1}'
                        );
                        done();
                    });
            });
        });

        describe("Get updatemessages for issue by ID", function () {
            it("Should retrieve updatemessages for issue by ID", function (done) {
                request(app)
                    .get("/issues/" + 1 + "/progress")
                    .end(function (error, res) {
                        expect(res.statusCode).to.equal(200); // Ok
                        expect(error).to.be.null;

                        expect(JSON.stringify(res.body[0])).to.equal(
                            '{"updatemessage":"Issue 1 Update 2","date":"04.01.19 kl.09:18"}'
                        );
                        done();
                    });
            });
        });

        describe("Get issues by user", function () {
            it("Should retrieve issues for user by ID", function (done) {
                request(app)
                    .get("/issues/user/" + 'sigurd@mail.no')
                    .set("x-access-token", createToken(1, {"email" : "sigurd@mail.no"}))
                    .end(function (error, res) {
                        expect(res.statusCode).to.equal(200); // Ok
                        expect(error).to.be.null;

                        expect(JSON.stringify(res.body[0])).to.equal(
                            '{"id":1,"title":"Title 1","description":"Vei","date":"05.02.20","longitude":1.11,"latitude":1.22,"picture_filename":"filename1.img","name":"Asfalt Remix AS","userId":1,"status":"Registrert","update_date":"2018-12-07T07:17:16.000Z","municipality_id":1}'
                        );
                        done();
                    });
            });
        });

        describe("Get issuecategories", function () {
            it("Should retrieve issuecategories", function (done) {
                request(app)
                    .get("/issuecategories")
                    .end(function (error, res) {
                        expect(res.statusCode).to.equal(200); // Ok
                        expect(error).to.be.null;

                        expect(JSON.stringify(res.body[0])).to.equal(
                            '{"id":1,"description":"Vei","national":0}'                        );
                        done();
                    });
            });
        });

        describe("Get issues by company", function () {
            it("Should retrieve issues by company", function (done) {
                request(app)
                    .get("/issues/business/" + 1)
                    .set("x-access-token", createToken(2, {}))
                    .end(function (error, res) {
                        expect(res.statusCode).to.equal(200); // Ok
                        expect(error).to.be.null;

                        expect(JSON.stringify(res.body[0])).to.equal(
                            '{"id":1,"title":"Title 1","description":"Vei","date":"05.02.20","longitude":1.11,"latitude":1.22,"picture_filename":"filename1.img","name":"Asfalt Remix AS","userId":1,"status":"Registrert","update_date":"2018-12-07T07:17:16.000Z","municipality_id":1}'
                        );
                        done();
                    });
            });
        });
    });




    describe("POST /issues/", function () {
        /*describe("Create new issue", function () {
            it("Should create a new issue 8", function (done) {
                request(app)
                    .post("/issues")
                    .send({
                        "description": "test description",
                        "longitude": 1.11,
                        "latitude": 2.22,
                        "picture_filename": "jpgbilde.png",
                        "user_id": 1,
                        "municipality_id": 2,
                        "issue_category_id": 1
                    })
                    .end(function (error, res) {
                        expect(res.statusCode).to.equal(201); // Created
                        expect(error).to.be.null;

                        expect(res.body.affectedRows).to.equal(1);
                        expect(res.body.insertId).to.equal(8);
                        done();
                    });
            });
        });

        describe("Edit issue", function () {
            it("Should edit issue 5", function (done) {
                // Edit event
                request(app)
                    .put("/issues/" + 5)
                    .send({
                        "title": "Title 8 new",
                        "issue_category_id": 1,
                        "estimated_time": "2018-12-07T07:17:16.000Z"
                    })
                    .end(function (error, res) {
                        expect(res.statusCode).to.equal(200); // Ok
                        expect(error).to.be.null;
                        expect(res.body.affectedRows).to.equal(1);
                        expect(res.body.changedRows).to.equal(1);

                        // Check that edit was successful
                        request(app)
                            .get("/issues/" + 5)
                            .end(function (error, res) {
                                expect(res.statusCode).to.equal(200); // Ok
                                expect(error).to.be.null;
                                var recievedBody = res.body;
                                recievedBody[0].date = "null"; // As date is updated on the server, it is ignored under testing by setting it to null
                                expect(JSON.stringify(recievedBody[0])).to.equal(
                                    '{"id":5,"title":"Title 8 new"}'
                                );
                                done();
                            });
                    });
            });
        });*/

        describe("Disable issue 8", function () {
            it("Should set issue.isEnabled to false", function (done) {
                // Disable issue
                request(app)
                    .delete("/issues/" + 1)
                    .set("x-access-token", createToken(2, {}))
                    .end(function (error, res) {
                        expect(res.statusCode).to.equal(200); // Ok
                        expect(error).to.be.null;
                        expect(res.body.affectedRows).to.equal(1);
                        expect(res.body.changedRows).to.equal(1);

                        //Check that edit was successful
                        request(app)
                            .get("/issues/" + 1)
                            .end(function (error, res) {
                                expect(res.statusCode).to.equal(200); // Ok
                                expect(error).to.be.null;
                                expect(res.body[0]).to.be.undefined;
                                done();
                            });
                    });
            });
        });

        describe("Create issue update", function () {
            it("Should create a new issue update", function (done) {
                request(app)
                    .post("/issues/progress/" + 1)
                    .set("x-access-token", createToken(2, {}))
                    .send({
                        "updatemessage":"New update message", 
                        "user_id":1
                    })
                    .end(function (error, res) {
                        expect(res.statusCode).to.equal(200); // Created
                        expect(error).to.be.null;

                        expect(res.body.affectedRows).to.equal(1);
                        done();
                    });
            });
        });

        describe("Set status", function () {
            it("Should set a status", function (done) {
                request(app)
                    .post("/issues/statuses")
                    .set("x-access-token", createToken(2, {}))
                    .send({
                        "status_id": 1,
                        "issue_id": 3
                    })
                    .end(function (error, res) {
                        expect(res.statusCode).to.equal(200); // Created
                        expect(error).to.be.null;

                        expect(res.body.affectedRows).to.equal(1);
                        done();
                    });
            });
        });

        describe("Add issue category", function () {
            it("Should add a category for issue", function (done) {
                request(app)
                    .post("/issuecategories")
                    .set("x-access-token", createToken(2, {}))
                    .send({
                        "description": "New category"
                    })
                    .end(function (error, res) {
                        expect(res.statusCode).to.equal(200);
                        expect(error).to.be.null;

                        expect(res.body.affectedRows).to.equal(1);
                        done();
                    });
            });
        });

        describe("update issue category", function () {
            it("Should add a category for issue", function (done) {
                request(app)
                    .put("/issuecategories/" + 1)
                    .set("x-access-token", createToken(2, {}))
                    .send({
                        "national":false
                    })
                    .end(function (error, res) {
                        expect(res.statusCode).to.equal(200); // Created
                        expect(error).to.be.null;

                        expect(res.body.affectedRows).to.equal(1);
                        done();
                    });
            });
        });

        /*describe("Disable issuecategories", function () {
            it("Should delete issuecategories", function (done) {
                // Disable issuecategory
                request(app)
                    .delete("/issuecategories")
                    .send({
                        "categories" : ["vgs"]
                    })
                    .end(function (error, res) {
                        expect(res.statusCode).to.equal(200); // Ok
                        expect(error).to.be.null;
                        //expect(res.body.affectedRows).to.equal(1);
                        //expect(res.body.changedRows).to.equal(1);                    
                    });
            });
        });*/

        describe("Assign company", function () {
            it("Should add a category for issue", function (done) {
                request(app)
                    .post("/issues/3/issue_company/company/"+3)
                    .set("x-access-token", createToken(2, {}))
                    .send({
                        "issue_id": 3
                    })
                    .end(function (error, res) {
                        expect(res.statusCode).to.equal(200);
                        expect(error).to.be.null;

                        expect(res.body.affectedRows).to.equal(1);
                        done();
                    });
            });
        });
    });
});