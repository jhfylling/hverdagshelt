var mysql = require("mysql");
const BusinessDao = require("../../src/dao/businessDao");
const runsqlfile = require("../runsqlfile");
var config = require("../../config/index")

var pool = mysql.createPool(config.poolOptions);

let businessDao = new BusinessDao(pool);

beforeAll((done => {
    runsqlfile(__dirname + "/../createTestTable.sql", pool, () => {
        runsqlfile(__dirname + "/../createTestData.sql", pool, done)
    })
}));

afterAll(() => {
    pool.end();
});

test("test getBusinessByUserId()", done => {
    function callback(status,data) {
        console.log("Test callback status: " + status + ", test data: " + data.length);
        expect(data.length).toBeGreaterThanOrEqual(1);
        done();
    }
    businessDao.getBusinessByUserId(5, callback);
});

test("test getBusinessIdByName()", done => {
    function callback(status, data) {
        console.log("Test callback status: " + status + ", test data: " + data.length);
        expect(data.length).toBeGreaterThanOrEqual(1);
        expect(data[0].id).toBe(2);
        done();
    }
    businessDao.getBusinessIdByName('Vegvesenet', callback);
});

test("test getCompaniesJoinCompanyUsers()", done => {
    function callback(status, data) {
        console.log("Test callback status: " + status + ", test data: " + data.length);
        expect(data.length).toBeGreaterThanOrEqual(1);
        expect(data[0].name).toBe('Vegvesenet');
        done();
    }
    businessDao.getCompaniesJoinCompanyUsers(1, callback);
});

test("test getCompaniesByMunicipality()", done => {
    function callback(status, data) {
        console.log("Test callback status: " + status + ", test data: " + data.length);
        expect(data.length).toBeGreaterThanOrEqual(1);
        expect(data[0].name).toBe('Vegvesenet');
        done();
    }
    businessDao.getCompaniesByMunicipality(1, callback);
});

test("test assignCompany()", done => {
    function callback(status, data) {
        console.log("Test callback status: " + status + ", test data: " + data.length);
        expect(data.affectedRows).toBeGreaterThanOrEqual(1);
        done();
    }
    businessDao.assignCompany(1, {
        issue_id: 2,
        company_id: 1
    }, callback);
});

test("test createCompany()", done => {
    function callback(status, data) {
        console.log("Test callback status: " + status + ", test data: " + data.length);
        expect(data.affectedRows).toBeGreaterThanOrEqual(1);
        done();
    }
    businessDao.createCompany({
        name: 'Dahly gaming AS',
        description: 'gamer'
    }, callback);
});

test("test createCompanyMunicipality()", done => {
    function callback(status, data) {
        console.log("Test callback status: " + status + ", test data: " + data.length);
        expect(data.affectedRows).toBeGreaterThanOrEqual(1);
        done();
    }
    businessDao.createCompanyMunicipality(2, 2, callback);
});

test("test createCompanyUserAccount()", done => {
    function callback(status, data) {
        console.log("Test callback status: " + status + ", test data: " + data.length);
        expect(data.affectedRows).toBeGreaterThanOrEqual(1);
        done();
    }
    businessDao.createCompanyUserAccount(3, 1, callback);
});