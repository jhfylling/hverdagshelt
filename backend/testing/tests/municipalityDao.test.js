var mysql = require("mysql");

const MunicipalityDao = require("../../src/dao/municipalityDao");
const runsqlfile = require("../runsqlfile");
var config = require("../../config/index")

var pool = mysql.createPool(config.poolOptions);

let municipalityDao = new MunicipalityDao(pool);

beforeAll((done => {
    runsqlfile(__dirname + "/../createTestTable.sql", pool, () => {
        runsqlfile(__dirname + "/../createTestData.sql", pool, done)
    })
}));

afterAll(() => {
    pool.end();
});

//import {municipalitySerice} from '../../frontend/src/serviceObjects/municipalityService';
/*
// async/await can be used.
it('works with async/await', async () => {
    expect.assertions(1);
    const data = await municipalityService.getMunicipality(1);
    expect(data).toEqual('Trondheim');
});
*/
/*
// async/await can also be used with `.resolves`.
it('works with async/await and resolves', async () => {
    expect.assertions(1);
    await expect(user.getUserName(5)).resolves.toEqual('Paul');
});
*/

/*
test("testing axios with promise", () => {
    expect.assertions(1);
    return  municipalityService.getMunicipalities()
        .then(data => {
            expect(data.length).toBe(5)
        });
});
*/

test("Test getMunicipalities()",done => {
    function callback(status,data){
        console.log("Test callback status: " + status + ", test data: " + data.length);
        expect(data.length).toBeGreaterThanOrEqual(5);
        expect(data[0].municipality_name).toBe('Bergen');
        done();
    }
    municipalityDao.getMunicipalities(callback)
});

test("Test getMunicipality(id)",done => {
    function callback(status,data){
        console.log("Test callback status: " + status + ", test data: " + JSON.stringify(data));
        expect(data.length).toBe(1);
        expect(data[0].id).toBe(1);
        expect(data[0].municipality_name).toBe('Trondheim');
        done();
    }
    municipalityDao.getMunicipality(1, callback)
});
