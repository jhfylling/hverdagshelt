var server = require("../../src/server.js");
var app = server.app;
var request = require("supertest");
var chai = require("chai");
var expect = chai.expect;
var fs = require("fs");

var runsqlfile = require("../runsqlfile");

beforeAll((done => {
    runsqlfile(__dirname + "/../createTestTable.sql", server.pool, () => {
        runsqlfile(__dirname + "/../createTestData.sql", server.pool, () => {
            runsqlfile(__dirname + "/../../sql/postnummer.sql", server.pool, done);
        })
    })
}));

describe("MunicipalityRoutes tests", function() { 
    describe("GET /municipalities", function() { 
        it("Should retrieve municipalities", function(done) { 
            request(app)
            .get("/municipalities")
            .end(function(error, res) { 
                expect(res.statusCode).to.equal(200); // Ok
                expect(error).to.be.null;
                expect(JSON.stringify(res.body[0])).to.equal(
                    '{"id":294,"municipality_name":"AGDENES"}'
                );
                expect(JSON.stringify(res.body[1])).to.equal(
                    '{"id":376,"municipality_name":"ALSTAHAUG"}'
                );
                expect(JSON.stringify(res.body[2])).to.equal(
                    '{"id":411,"municipality_name":"ALTA"}'
                );
                expect(JSON.stringify(res.body[3])).to.equal(
                    '{"id":63,"municipality_name":"ALVDAL"}'
                );
                expect(JSON.stringify(res.body[4])).to.equal(
                    '{"id":364,"municipality_name":"ANDØY"}'
                );
                done(); 
            }); 
        }); 
    });
    describe("GET /municipalities/:id", function() { 
        describe("Existing municipality", function() { 
            it("Should retrieve municipality", function(done) { 
                request(app)
                .get("/municipalities/" + 1)
                .end(function(error, res) { 
                    expect(res.statusCode).to.equal(200); // Ok
                    expect(error).to.be.null;
                    expect(JSON.stringify(res.body[0])).to.equal(
                        '{"id":1,"municipality_name":"TRONDHEIM"}'
                    );
                    done(); 
                }); 
            }); 
        });
        describe("Non-existing municipality", function() { 
            it("Should not retrieve any municipalities, and not throw error", function(done) { 
                request(app)
                .get("/municipalities/" + 999)
                .end(function(error, res) { 
                    expect(res.statusCode).to.equal(200); // Ok
                    expect(error).to.be.null;
                    expect(JSON.stringify(res.body[0])).to.be.undefined;
                    done(); 
                }); 
            }); 
        });
    });

    describe("GET /municipalities/name/:zipcode", function() { 
        describe("Real zipcode", function() { 
            it("Should retrieve municipality name", function(done) { 
                request(app)
                .get("/municipalities/name/" + 7031)
                .end(function(error, res) { 
                    expect(res.statusCode).to.equal(200); // Ok
                    expect(error).to.be.null;
                    //fs.writeFileSync("./print.json", JSON.stringify(res.body));
                    expect(JSON.stringify(res.body[0])).to.equal(
                        '{"Postnummer":7031,"Poststed":"TRONDHEIM","Kommunenummer":5001,"Kommunenavn":"TRONDHEIM","Kategori":"G","id":1,"municipality_name":"TRONDHEIM","longitude":0,"latitude":0}'
                    );
                    done(); 
                }); 
            }); 
        });
        describe("Non-existing zipcode", function() { 
            it("Should not retrieve municipality name, and not throw error", function(done) { 
                request(app)
                .get("/municipalities/name/" + 99999)
                .end(function(error, res) { 
                    expect(res.statusCode).to.equal(200); // Ok
                    expect(error).to.be.null;
                    expect(res.body[0]).to.be.undefined;
                    done(); 
                }); 
            }); 
        });
    });
});