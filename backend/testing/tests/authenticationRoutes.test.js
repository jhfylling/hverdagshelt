var server = require("../../src/server.js");
var app = server.app;
var config = server.config;
var jwt = require("jsonwebtoken");
var request = require("supertest");
var chai = require("chai");
var expect = chai.expect;

var runsqlfile = require("../runsqlfile");

var testData = {};
beforeAll((done => {
    testData.validToken = jwt.sign(
        {
            "id" : 1,
            "category_id" : 1,
            "email" : "test@email.com",
            "municipality_id" : 1
        },
        config.JWT_keypair.private, // Same keypair as used in the server test environment
        {
            algorithm: 'RS256',
            expiresIn: '1m'
        });

    testData.expiredToken = jwt.sign(
        {
            "id" : 1,
            "category_id" : 1,
            "email" : "test@email.com",
            "municipality_id" : 1
        },
        config.JWT_keypair.private, // Same keypair as used in the server test environment
        {
            algorithm: 'RS256',
            expiresIn: 0
        });

    testData.nullToken = null; 

    runsqlfile(__dirname + "/../createTestTable.sql", server.pool, () => {
        runsqlfile(__dirname + "/../createTestData.sql", server.pool, done)
    })
}));

describe("Authenticationroutes tests", function() { 
    describe("GET /session", function() { 
        describe("Valid token", function() { 
            it("Should retrieve a new session", function(done) { 
                request(app)
                .get("/session")
                .set("x-access-token", testData.validToken)
                .end(function(error, res) { 
                    expect(res.statusCode).to.equal(201); // Created 
                    expect(error).to.be.null;
                    expect(JSON.stringify(res.body.userInfo)).to.equal(
                        '{"id":1,"email":"sigurd@mail.no","municipality_id":1,"category_id":1,"category_name":"Privat bruker","firstname":"Sigurd","lastname":"Hynne","phone_number":"91928361","municipality_name":"Trondheim"}'
                    );
                    expect(res.body["x-access-token"]).to.not.be.undefined;
                    expect(res.body["x-access-token"]).to.not.be.null;
                    done(); 
                }); 
            }); 
        });
        describe("Expired token", function() { 
            it("Should not retrieve a new session", function(done) { 
                request(app)
                .get("/session")
                .set("x-access-token", testData.expiredToken)
                .end(function(error, res) { 
                    expect(error.statusCode).to.equal(400);
                    expect(error.rawResponse).to.equal("Expired session, please log in again.");
                    expect(res).to.be.undefined;
                    done(); 
                }); 
            }); 
        });
        describe("Null token", function() { 
            it("Should not respond with new token", function(done) { 
                request(app)
                .get("/session")
                .set("x-access-token", testData.nullToken)
                .end(function(error, res) {
                    expect(error.statusCode).to.equal(500); // Internal server error 
                    expect(error.rawResponse).to.be.equal("Internal server error");
                    expect(res).to.be.undefined;
                    done(); 
                }); 
            }); 
        });
    });
});