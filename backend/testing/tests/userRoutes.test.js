var server = require("../../src/server.js");
var app = server.app;
var request = require("supertest");
var chai = require("chai");
var expect = chai.expect;
var jwt = require("jsonwebtoken");
var config = require("../../config/index")

var fs = require("fs");

var runsqlfile = require("../runsqlfile");

function getCategoryName(category_id){
    var name;
    switch(category_id){
        case 0:
        name = "Gjest";
        break;
        case 1:
        name = "Privat bruker";
        break;
        case 2:
        name = "Administrator";
        break;
        case 3:
        name = "Kommunal bruker";
        break;
        case 4:
        name = "Bedriftsbruker";
        break;
    }
    return name;
}

function canRegister(category_a, category_b){
    if(category_b == 1){
        return true;
    }
    if(category_b == 2){
        return false;
    }
    if(category_b == 3 && category_a == 2){
        return true;
    }
    if(category_b == 4 && (category_a == 2 || category_a == 3) ){
        return true;
    }
}

function decodeToken(token, callback){
    jwt.verify(
        token,
        config.JWT_keypair.public,
        {
            algorithm: 'RS256'
        },
        (error, decoded) =>{
            callback(decoded);
        });
}

function createToken(category_id, userinfo){
    if(category_id){
        userinfo.category_id = category_id;
    }
    return jwt.sign(userinfo,
        config.JWT_keypair.private,
        {
            algorithm: 'RS256',
            expiresIn: '5m'
        });
}

beforeAll((done => {
    runsqlfile(__dirname + "/../createTestTable.sql", server.pool, () => {
        runsqlfile(__dirname + "/../createTestData.sql", server.pool, done)
    })
}));

describe("UserRoutes tests", function() { 
    describe("POST /register", function() { 
        describe("Create private user with guest", function(){
            it("Should create private user", function(done) {
                var newUserInformation = {
                    "email" : "email1@mail.no",
                    "password" : "password1",
                    "municipality_id" : 1,
                    "category_id" : 1,
                    "firstname" : "firstname1",
                    "lastname" : "lastname1",
                    "phone_number" : "11111111"
                };
                request(app)
                    .post("/register")
                    .send(
                        newUserInformation
                    )
                    .end(function(error, res) { 
                        expect(res.status).to.equal(201); // Created
                        expect(error).to.be.null;
                    
                        decodeToken(res.body["x-access-token"], (decoded) => {
                            expect(decoded.id).to.not.be.null;
                            expect(decoded.id).to.not.be.undefined;
                            expect(decoded.category_id).to.equal(1);
                            expect(decoded.municipality_id).to.equal(newUserInformation.municipality_id);
                            expect(decoded.email).to.equal(newUserInformation.email);
                            done(); 
                        });
                    });
            });
        });

        describe("Create administrator user with guest", function(){
            it("Should not create user", function(done) {
                var newUserInformation = {
                    "email" : "email1@mail.no",
                    "password" : "password1",
                    "municipality_id" : 1,
                    "category_id" : 2,
                    "firstname" : "firstname1",
                    "lastname" : "lastname1",
                    "phone_number" : "11111111"
                };
                request(app)
                    .post("/register")
                    .send(
                        newUserInformation
                    )
                    .end(function(error, res) { 
                        expect(error.statusCode).to.equal(401); // Unauthorized
                        expect(error.rawResponse).to.equal("You dont have the authorization to create this type of user!");
                        done();
                    });
            });
        });

        describe("Create user that already exists", function(){
            it("Should not create user", function(done) {
                var newUserInformation = {
                    "email" : "email1@mail.no",
                    "password" : "password1",
                    "municipality_id" : 1,
                    "category_id" : 1,
                    "firstname" : "firstname1",
                    "lastname" : "lastname1",
                    "phone_number" : "11111111"
                };
                request(app)
                    .post("/register")
                    .send(
                        newUserInformation
                    )
                    .end(function(error, res) { 
                        expect(error.statusCode).to.equal(409); // Conflict
                        expect(error.rawResponse).to.equal("A user with this email already exists!");
                        done();
                    });
            });
        });

        describe("Create user that without optional information", function(){
            it("Should create user", function(done) {
                var newUserInformation = {
                    "email" : "emailer@mail.no",
                    "password" : "password1",
                    "municipality_id" : 1,
                    "category_id" : 1,
                };
                request(app)
                    .post("/register")
                    .send(
                        newUserInformation
                    )
                    .end(function(error, res) { 
                        expect(res.status).to.equal(201); // Created
                        expect(error).to.be.null;
                    
                        decodeToken(res.body["x-access-token"], (decoded) => {
                            expect(decoded.id).to.not.be.null;
                            expect(decoded.id).to.not.be.undefined;
                            expect(decoded.category_id).to.equal(1);
                            expect(decoded.municipality_id).to.equal(newUserInformation.municipality_id);
                            expect(decoded.email).to.equal(newUserInformation.email);
                            done(); 
                        });
                    });
            });
        });

        describe("Create user without all mandatory information", function(){
            it("Should not create user", function(done) {
                var newUserInformation = {
                    "password" : "password1",
                    "municipality_id" : 1,
                    "category_id" : 1,
                };
                request(app)
                    .post("/register")
                    .send(
                        newUserInformation
                    )
                    .end(function(error, res) { 
                        expect(error.statusCode).to.equal(400); // Bad request
                        expect(error.rawResponse).to.equal("Email, password and/or municipality_id not specified!");
                        done();
                    });
            });
        });
    });

    describe("POST /login", function() { 
        describe("Login with valid userinformation and check that session has correct information", function(){
            it("Should create and respond with a new session", function(done) {
                var loginInformation = {
                    "email" : "jonas@mail.no",
                    "password" : "jonas@mail.no"
                };
                request(app)
                    .post("/login")
                    .send(
                        loginInformation
                    )
                    .end(function(error, res) { 
                        expect(res.status).to.equal(201); // Created
                        expect(error).to.be.null;

                        decodeToken(res.body["x-access-token"], (decoded) => {
                            expect(decoded.id).to.not.be.null;
                            expect(decoded.id).to.not.be.undefined;
                            expect(decoded.category_id).to.equal(1);
                            expect(decoded.municipality_id).to.equal(1);
                            expect(decoded.email).to.equal(loginInformation.email);

                            expect(res.body.userInfo.id).to.not.be.null;
                            expect(res.body.userInfo.id).to.not.be.undefined;
                            expect(res.body.userInfo.email).to.equal(loginInformation.email);
                            expect(res.body.userInfo.municipality_id).to.equal(1);
                            expect(res.body.userInfo.category_id).to.equal(1);
                            expect(res.body.userInfo.category_name).to.equal("Privat bruker");
                            expect(res.body.userInfo.firstname).to.equal("Jonas");
                            expect(res.body.userInfo.lastname).to.equal("Kristoffersen");
                            expect(res.body.userInfo.phone_number).to.equal("68573948");
                            expect(res.body.userInfo.municipality_name).to.equal("Trondheim");
                            done(); 
                        });                        
                    });
            });
        });
        describe("Login with invalid password", function(){
            it("Should not create and respond with a new session", function(done) {
                var loginInformation = {
                    "email" : "jonas@mail.no",
                    "password" : "jonass@mail.no"
                };
                request(app)
                    .post("/login")
                    .send(
                        loginInformation
                    )
                    .end(function(error, res) { 
                        expect(res).to.be.undefined;
                        expect(error.statusCode).to.equal(401); // Bad request
                        expect(error.rawResponse).to.equal("Wrong password or email!");
                        done();               
                    });
            });
        });
        describe("Login with non-existant user", function(){
            it("Should not create and respond with a new session", function(done) {
                var loginInformation = {
                    "email" : "jonasdws@mail.no",
                    "password" : "jonas@mail.no"
                };
                request(app)
                    .post("/login")
                    .send(
                        loginInformation
                    )
                    .end(function(error, res) { 
                        expect(res).to.be.undefined;
                        expect(error.statusCode).to.equal(401); // Bad request
                        expect(error.rawResponse).to.equal("Wrong password or email!");
                        done();               
                    });
            });
        });
        describe("Login with invalid password or email", function(){
            it("Should respond the same way to not give too much information", function(done) {
                var errorInvalidPassword;
                var responseInvalidPassword;
                var errorInvalidEmail;
                var responseInvalidEmail;

                var loginInformationWrongPassword = {
                    "email" : "jonas@mail.no",
                    "password" : "jonass@mail.no"
                };
                request(app)
                    .post("/login")
                    .send(
                        loginInformationWrongPassword
                    )
                    .end(function(error, res) { 
                        expect(error).to.not.be.undefined;
                        expect(res).to.be.undefined;
                        errorInvalidPassword = error;
                        responseInvalidPassword = res;
                    });

                    var loginInformationWrongEmail = {
                        "email" : "jonasrewe@mail.no",
                        "password" : "jonas@mail.no"
                    };
                request(app)
                    .post("/login")
                    .send(
                        loginInformationWrongEmail
                    )
                    .end(function(error, res) { 
                        expect(error).to.not.be.undefined;
                        expect(res).to.be.undefined;
                        errorInvalidEmail = error;
                        responseInvalidEmail = res;
                    });
                    expect(JSON.stringify(errorInvalidPassword)).to.equal(JSON.stringify(errorInvalidEmail));
                    expect(JSON.stringify(responseInvalidPassword)).to.equal(JSON.stringify(responseInvalidEmail));
                    done();
            });
        });
    });

    describe("GET /user", function() { 
        it("Should get correct user details", function(done) {
            request(app)
                .get("/user")
                .set(
                    "x-access-token", createToken(1, {
                    "id": 5, 
                    "email" : "doesentMatter@mail.no", 
                    "municipality_id" : 1
                }))
                .end(function(error, res) { 
                    expect(JSON.stringify(res.body)).to.equal(
                        '{"id":5,"email":"kevin@mail.no","municipality_id":1,"category_id":1,"category_name":"Privat bruker","firstname":"Kevin","lastname":"Mentzoni Halvarsson","phone_number":"23448577","municipality_name":"Trondheim"}'                    
                    );
                    done();
                });
        });
    });

    describe("PUT /user", function() { 
        it("Should update user details and email", function(done) {
            var newDetails = {
                "email" : "newmail@mail.no",
                "firstname" : "newFirstname",
                "lastname" : "newLastname",
                "phone_number" : "99999999"
            };
            // Update details
            request(app)
                .put("/user")
                .set("x-access-token", createToken(1, 
                    {"id": 5, 
                    "email" : "doesentMatter@mail.no", 
                    "municipality_id" : 1}))
                .send(
                    newDetails
                )
                .end(function(error, res) { 
                    expect(error).to.be.null;
                    // Get updated details
                    request(app)
                        .get("/user")
                        .set(
                            "x-access-token", createToken(1, {
                            "id": 5, 
                            "email" : "doesentMatter@mail.no", 
                            "municipality_id" : 1
                        }))
                        .end(function(error, res) { 
                            expect(error).to.be.null;
                            expect(res.body.email).to.equal(newDetails.email);
                            expect(res.body.firstname).to.equal(newDetails.firstname);
                            expect(res.body.lastname).to.equal(newDetails.lastname);
                            expect(res.body.phone_number).to.equal(newDetails.phone_number);
                            done();
                        });
                });                 
        });
    });

    describe("PUT /user/resetpassword/:token", function() { 
        describe("Valid token", function() {
            it("Should change password", function(done) {
                var userId = 9;

                var loginInformation = {
                    "email" : "privat@test.no",
                    "password" : "12341234"
                }
                // Try logging in with newPassword before change
                request(app)
                    .post("/login")
                    .send(
                        loginInformation
                    )
                    .end(function(error, res) { 
                        expect(error.statusCode).to.equal(401); // Unauthorized
                        expect(res).to.be.undefined;                        
                        // Change password
                        request(app)
                            .put("/user/resetpassword/" + createToken(null, { "id": userId, }))
                            .send({"newPassword" : loginInformation.password})
                            .end(function(error, res) { 
                                expect(error).to.be.null;
                                expect(res.status).to.equal(200); // Ok
                                
                                // Try logging in with new information
                                request(app)
                                    .post("/login")
                                    .send(
                                        loginInformation
                                    )
                                    .end(function(error, res) { 
                                        expect(error).to.be.null;
                                        expect(res.status).to.equal(201); // Created
                                        done();
                                    });
                            });          
                    });
            });
        });
    });
    
    describe("GET /user/:id/business", function() { 
        it("Should get business information from userid", function(done) {
            request(app)
                .get("/user/" + 12 + "/business")
                .set("x-access-token", createToken(2, {}))
                .end(function(error, res) { 
                    expect(error).to.be.null;
                    expect(res.status).to.equal(200); // Ok
                    expect(JSON.stringify(res.body)).to.equal(
                        '[{"id":3,"name":"TronderEnergi AS","description":"Strøm og energi","isEnabled":1}]'
                    );
                    done();
                });
        });
    });
});