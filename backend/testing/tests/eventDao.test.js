var mysql = require("mysql");

const EventDao = require("../../src/dao/eventDao");
const runsqlfile = require("../runsqlfile");
var config = require("../../config/index")

var pool = mysql.createPool(config.poolOptions);

let eventDao = new EventDao(pool);

beforeAll((done => {
    runsqlfile(__dirname + "/../createTestTable.sql", pool, () => {
        runsqlfile(__dirname + "/../createTestData.sql", pool, done)
    })
}));

afterAll(() => {
    pool.end();
});

test("test getEvents(municipality_id)", done => {
    function callback(status,data) {
        console.log("Test callback status: " + status + ", test data: " + JSON.stringify(data));
        expect(data[0].title).toBe("Title 1");
        expect(data[0].longitude).toBe(1);
        done();
    }
    eventDao.getEvents(1,callback);
});

test("test getEvent(id)", done => {
    function callback(status,data){
        console.log("Test callback status: " + status + ", test data: " + JSON.stringify(data));
        expect(data.length).toBe(1);
        expect(data[0].id).toBe(1);
        done();
    }
    eventDao.getEvent(1,callback)
});

test("test getEventInCategory(event_category_id)", done => {
    function callback(status,data){
        console.log("Test callback status: " + status + ", test data: " + JSON.stringify(data));
        expect(data[0].title).toBe("Title 1");
        done();
    }
    eventDao.getEventInCategory(1,callback)
});


test("test createEvent(json)",done => {
    function callback(status,data){
        console.log("Test callback status: " + status + ", test data: " + JSON.stringify(data));
        expect(data.affectedRows).toBe(1);
        done();
    }
    eventDao.createEvent({
        title: 'title',
        description:'description',
        longitude:10,
        latitude: 10,
        price:'price',
        event_homepage:'event_homepage',
        event_category_id:1,
        municipality_id:1
    },callback)
});


test("test updateEvent(id,json)", done=> {
    function callback(status,data){
        console.log("Test callback status: " + status + ", test data: " + JSON.stringify(data));
        expect(data[0].title).toBe('newTitle');
        done();
    }
    function callback2(){
        eventDao.getEvent(2,callback)
    }
    eventDao.updateEvent(2,{
        title:'newTitle',
        description: 'thisDescription',
        longitude: 10,
        latitude: 10,
        price: '1200',
        event_homepage: 'homepage.com',
        event_category_id: 1,
        municipality_id: 2
    },callback2)
});

test("test disableEvent(id)", done => {
    function callback(status,data){
        console.log("Test callback status: " + status + ", data: " + JSON.stringify(data));
        expect(data).toEqual([]);
        //expect(data.affectedRows).toBe(1);
        done();
    }
    function callback2(){
        eventDao.getEvent(1,callback)
    }
    eventDao.disableEvent(1,callback2);
});

test("test2 disableEvent(id)", done => {
    function callback(status,data){
        console.log("Test callback status: " + status + ", data: " + JSON.stringify(data));
        expect(data.affectedRows).toBe(1);
        done();
    }
    eventDao.disableEvent(1,callback);
});

test("test2 getEventCategories()", done => {
    function callback(status, data) {
        console.log("Test callback status: " + status + ", data: " + JSON.stringify(data));
        expect(data.length).toBeGreaterThanOrEqual(1);
        done();
    }
    eventDao.getEventCategories(callback);
});