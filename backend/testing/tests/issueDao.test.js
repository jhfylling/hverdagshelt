var mysql = require("mysql");

const IssueDao = require("../../src/dao/issueDao");
const runsqlfile = require("../runsqlfile");
var config = require("../../config/index")

var pool = mysql.createPool(config.poolOptions);

let issueDao = new IssueDao(pool);

beforeAll((done => {
    runsqlfile(__dirname + "/../createTestTable.sql", pool, () => {
        runsqlfile(__dirname + "/../createTestData.sql", pool, done)
    })
}));

afterAll(() => {
    pool.end();
});

/*
describe('#getUser() using async/await', () => {
    it('should load user data', async () => {
        const data = await issueDao.getIssue(1);
        expect(data).toBeDefined();
        expect(data.entity.name).toEqual('Hull i asfalt')
    })
});*/

test("test getIssues()", done => {
    function callback(status, data) {
        console.log("Test callback status: " + status + ", test data: " + data.length);
        expect(data.length).toBeGreaterThanOrEqual(1);
        //expect(data[0].municipality_id).toBe('1');
        done();
    }
    var span = [1]; // Span of categories
    issueDao.getIssues(1, span, callback);
});

test("test getIssue()", done => {
    function callback(status,data){
        console.log("Test callback status: " + status + ", test data: " + JSON.stringify(data));
        expect(data.length).toBe(1);
        //expect(data[0].id).toBe(1);
        done();
    }
    issueDao.getIssue(1, callback);
});

test("test createIssue()", done => {
    function callback(status,data){
        console.log("Test status: " + status + ", test data: " + JSON.stringify(data));
        expect(data.affectedRows).toBe(1);
        done();
    }
    issueDao.createIssue(
        {
            id : 4,
            title : 'test issue',
            description : 'create issue testing',
            issue_date : '14.01.20',
            longitude : 10.41523432,
            latitude : 41.98724356,
            picture_filename : 'filename.img',
            issue_category_id : 1,
            municipality_id : 1,
            user_id : 1,
            isEnabled: true},
        callback
    );
});

test("test getStatuses()", done => {
    function callback(status, data) {
        console.log(
            "Test callback: status=" + status + ", data=" + JSON.stringify(data)
        );
        expect(data.length).toBeGreaterThanOrEqual(1);
        done();
    }

    issueDao.getStatuses(callback);
});

test("test getStatus()", done => {
    function callback(status, data) {
        console.log(
            "Test callback: status=" + status + ", data=" + JSON.stringify(data)
        );
        expect(data.length).toBe(1);
        done();
    }

    issueDao.getStatus(1, callback);
});

test("test disableIssue()", done => {
    function callback(status, data) {
        console.log(
            "Test callback: status=" + status + ", data=" + JSON.stringify(data)
        );
        expect(data.affectedRows).toBe(1);
        done();
    }

    issueDao.disableIssue(2, callback);
});

test("test setStatus()", done => {
    function callback(status, data) {
        console.log(
            "Test callback: status=" + status + ", data=" + JSON.stringify(data)
        );
        expect(data.affectedRows).toBe(1);
        done();
    }

    issueDao.setStatus(
        {
            status_id : 1,
            issue_id : 2
        }
        ,
        callback);
});

test("test createIssueUpdate()", done => {
    function callback(status, data) {
        console.log(
            "Test callback: status=" + status + ", data=" + JSON.stringify(data)
        );
        expect(data.affectedRows).toBe(1);
        //expect(data[0].updatemessage).toBe("oppdateringsmelding fra tester");
        done();
    }

    issueDao.createIssueUpdate(
        1,
        {
            issue_id : 1,
            updatemessage : 'oppdateringsmelding fra tester',
            user_id: 1,
        },
        callback);
});

test("test getProgressUpdates()", done => {
    function callback(status, data) {
        console.log(
            "Test callback: status=" + status + ", data=" + JSON.stringify(data)
        );
        expect(data.length).toBeGreaterThanOrEqual(1);
        done();
    }

    issueDao.getProgressUpdates(1, callback);
});

test("test updateIssue()", done => {
    function callback(status, data) {
        console.log(
            "Test callback: status=" + status + ", data=" + JSON.stringify(data)
        );
        expect(data.affectedRows).toBeGreaterThanOrEqual(1);
        done();
    }

    issueDao.updateIssue(1,
        {
            title : 'untitestTITLEE',
            description : 'beskrivelse for unittestingE',
            longitude : 10.43567875,
            latitude : 11.23456542,
            picture_filename : 'unittestt.jpg',
            issue_category_id : 2,
            company_id : 1,
            id : 1
        },
        callback);
});

test("test getIssuesByUser()", done => {
    function callback(status, data) {
        console.log(
            "Test callback: status=" + status + ", data=" + JSON.stringify(data)
        );
        
        expect(data.length).toBeGreaterThanOrEqual(1);
        done();
    }

    issueDao.getIssuesByUser('morten@mail.no', callback);
});

/**
 * Test to see if updateIssueCategories will update category_id's of issues
 * Test to see if delete/issuecategoires endpoint deletes specified categories
 */
test("test updateIssueCategories()", done => {
    function callback(status, data) {
        //console.error("Test callback: status=" + status + ", data=" + JSON.stringify(data));
        
        expect(status).toBe(200);
        expect(data.affectedRows).toBe(4);
        
        function callback(status, data) {
            console.log(
                "Test callback: status=" + status + ", data=" + JSON.stringify(data)
            );
    
            expect(status).toBe(200);
            expect(data.affectedRows).toBe(2);
            
            done();
        }
        let categories = ['2', '4'];
        issueDao.deleteIssueCategories(categories, callback);
        
        //done();
    }
    let categories = ['2', '4'];
    issueDao.updateIssueCategories(1, categories, callback);
});

test("test getIssuesByCompany()", done => {
    function callback(status, data) {
        console.log(
            "Test callback: status=" + status + ", data=" + JSON.stringify(data)
        );
        expect(data.length).toBeGreaterThanOrEqual(1);
        expect(data[0].id).toBe(1);
        done();
    }

    issueDao.getIssuesByCompany(1, callback);
});

test("test getIssueCategories()", done => {
    function callback(status, data) {
        console.log(
            "Test callback: status=" + status + ", data=" + JSON.stringify(data)
        );
        expect(data.length).toBeGreaterThanOrEqual(1);
        expect(data[0].id).toBe(1);
        done();
    }

    issueDao.getIssueCategories(callback);
});

test("test addIssueCategory()", done => {
    function callback(status, data) {
        console.log(
            "Test callback: status=" + status + ", data=" + JSON.stringify(data)
        );
        expect(data.affectedRows).toBeGreaterThanOrEqual(1);
        done();
    }

    issueDao.addIssueCategory('testcategory',callback);
});

test("test updateIssueCategoryNational()", done => {
    function callback(status, data) {
        console.log(
            "Test callback: status=" + status + ", data=" + JSON.stringify(data)
        );
        expect(data.affectedRows).toBeGreaterThanOrEqual(1);
        done();
    }

    issueDao.updateIssueCategoryNational(3,true, callback);
});