import Error from '../../src/error';    
var errorHandler = require("../../src/routes/errorHandlerRoute").method;

class fakeRes{};
fakeRes.prototype.status = function(statusCode){
    this.statusCode = statusCode;
    return this;
}
fakeRes.prototype.send = function(errorMessage){
    this.errorMessage = errorMessage;
}

var testErrors = {};
beforeAll((done => {
    testErrors.hiddenError = new Error("Secret", 200, null, null);
    testErrors.notHiddenError = new Error(null, null, "Wrong password!", 401);
    testErrors.hiddenAndNotHidden = new Error("Secret errormessage", 400, "Something went wrong!", 401);
    done();
}));

describe("Hidden error", function() { 
    it("Should not expose error in response", function(done) { 
        function evaluate(res) {
            expect(res.statusCode).toEqual(500);
            expect(res.errorMessage).toEqual("Internal server error");
            done();
        }
        var error = testErrors.hiddenError;
        var res = new fakeRes();
        var req = null;
        var next = null;
        errorHandler(error, req, res, next);
        evaluate(res);
    }); 
});

describe("Not hidden error", function() { 
    it("Should expose error in response", function(done) { 
        function evaluate(res) {
            expect(res.statusCode).toEqual(401);
            expect(res.errorMessage).toEqual("Wrong password!");
            done();
        }
        var error = testErrors.notHiddenError;
        var res = new fakeRes();
        var req = null;
        var next = null;
        errorHandler(error, req, res, next);
        evaluate(res);
    }); 
});

describe("Hidden and not hidden error", function() { 
    it("Should expose not hidden error, but not expose hidden error", function(done) { 
        function evaluate(res) {
            expect(res.statusCode).toEqual(401);
            expect(res.errorMessage).toEqual("Something went wrong!");
            done();
        }
        var error = testErrors.hiddenAndNotHidden;
        var res = new fakeRes();
        var req = null;
        var next = null;
        errorHandler(error, req, res, next);
        evaluate(res);
    }); 
});