var server = require("../../src/server.js");
var app = server.app;
var request = require("supertest");
var chai = require("chai");
var expect = chai.expect;
var jwt = require("jsonwebtoken");
var config = require("../../config/index")

var runsqlfile = require("../runsqlfile");

function createToken(category_id, userinfo){
    if(category_id){
        userinfo.category_id = category_id;
    }
    return jwt.sign(userinfo,
        config.JWT_keypair.private,
        {
            algorithm: 'RS256',
            expiresIn: '5m'
        });
}

beforeAll((done => {
    runsqlfile(__dirname + "/../createTestTable.sql", server.pool, () => {
        runsqlfile(__dirname + "/../createTestData.sql", server.pool, done)
    })
}));

describe("EventRoutes tests", function() { 
    describe("GET /events/municipality/:municipality_id", function() { 
        describe("Existing municipality with events", function() { 
            it("Should retrieve events from municipality", function(done) { 
                request(app)
                .get("/events/municipality/" + 1)
                .end(function(error, res) { 
                    expect(res.statusCode).to.equal(200); // Ok
                    expect(error).to.be.null;
                    expect(JSON.stringify(res.body[0])).to.equal(
                        '{"id":1,"title":"Title 1","description":"Description 1","date":"05.02.20 10:19","latitude":1,"longitude":1,"price":"100","event_homepage":"website.com/1","category":"Kultur"}'                    );
                    done(); 
                }); 
            }); 
        });

        describe("Non-existing municipality", function() { 
            it("Should not retrieve events from municipality", function(done) { 
                request(app)
                .get("/events/municipality/" + 99)
                .end(function(error, res) { 
                    expect(res.statusCode).to.equal(200); // Ok
                    expect(error).to.be.null;
                    expect(res.body[0]).to.be.undefined;
                    done(); 
                }); 
            }); 
        });
    });
    describe("GET /events/:id", function() { 
        describe("Existing event", function() { 
            it("Should retrieve event", function(done) { 
                request(app)
                .get("/events/" + 1)
                .end(function(error, res) { 
                    expect(res.statusCode).to.equal(200); // Ok
                    expect(error).to.be.null;
                    expect(JSON.stringify(res.body[0])).to.equal(
                        '{"id":1,"title":"Title 1","description":"Description 1","date":"05.02.20 10:19","latitude":1,"longitude":1,"price":"100","event_homepage":"website.com/1","category":"Kultur"}'                    );
                    done(); 
                }); 
            }); 
        });

        describe("Non-existing event", function() { 
            it("Should not retrieve event", function(done) { 
                request(app)
                .get("/events/" + 99)
                .end(function(error, res) { 
                    expect(res.statusCode).to.equal(200); // Ok
                    expect(error).to.be.null;
                    expect(res.body[0]).to.be.undefined;
                    done(); 
                }); 
            }); 
        });
    });

    describe("GET /events/category/:id", function() { 
        describe("Existing category", function() { 
            it("Should retrieve both events from category 1", function(done) { 
                request(app)
                .get("/events/category/" + 1)
                .end(function(error, res) { 
                    expect(res.statusCode).to.equal(200); // Ok
                    expect(error).to.be.null;

                    expect(JSON.stringify(res.body[0])).to.equal(
                        '{"id":1,"title":"Title 1","description":"Kultur","date":"05.02.20 10:19","latitude":1,"longitude":1,"price":"100","event_homepage":"website.com/1"}'
                    );
                    expect(JSON.stringify(res.body[1])).to.equal(
                        '{"id":7,"title":"Title 7","description":"Kultur","date":"05.02.20 10:19","latitude":7,"longitude":7,"price":"700","event_homepage":"website.com/7"}'
                    );
                    
                    done(); 
                }); 
            }); 
        });
        describe("Non-Existing category", function() { 
            it("Should retrieve no events from non-existing category", function(done) { 
                request(app)
                .get("/events/category/" + 99)
                .end(function(error, res) { 
                    expect(res.statusCode).to.equal(200); // Ok
                    expect(error).to.be.null;

                    expect(JSON.stringify(res.body)).to.be.equal("[]");
                    done(); 
                }); 
            }); 
        });
    });

/* 
            let inputData = {
                title: req.fields.title,
                description: req.fields.description,
                longitude: req.fields.longitude,
                latitude: req.fields.latitude,
                price: req.fields.price,
                event_homepage: req.fields.event_homepage,
                event_category_id: req.fields.event_category_id,
                municipality_id: req.fields.municipality_id,
            };
*/

    describe("POST /events/", function() { 
        describe("Create new event", function() { 
            it("Should create a new event 8", function(done) { 
                request(app)
                .post("/events")
                .set("x-access-token", createToken(2, {}))
                .send({
                    "title" : "Title 8",
                    "description" : "Description 8",
                    "longitude" : "80",
                    "latitude" : "80",
                    "price" : "800",
                    "event_homepage" : "website.com/8",
                    "event_category_id" : "3",
                    "municipality_id" : "2"
                })
                .end(function(error, res) { 
                    expect(res.statusCode).to.equal(201); // Created
                    expect(error).to.be.null;

                    expect(res.body.affectedRows).to.equal(1);
                    expect(res.body.insertId).to.equal(8);
                    done(); 
                }); 
            }); 
        });

        describe("Edit event", function() { 
            it("Should edit event 8", function(done) { 
                // Edit event
                request(app)
                .put("/events/" + 8)
                .set("x-access-token", createToken(2, {}))
                .send({
                    "title" : "Title 8new",
                    "description" : "Description 8new",
                    "longitude" : "81",
                    "latitude" : "81",
                    "price" : "801",
                    "event_homepage" : "website.com/8new",
                    "event_category_id" : "2",
                    "municipality_id" : "2"
                })
                .end(function(error, res) { 
                    expect(res.statusCode).to.equal(200); // Ok
                    expect(error).to.be.null;
                    expect(res.body.affectedRows).to.equal(1);
                    expect(res.body.changedRows).to.equal(1);

                    // Check that edit was successful
                    request(app)
                    .get("/events/" + 8)
                    .end(function(error, res) { 
                        expect(res.statusCode).to.equal(200); // Ok
                        expect(error).to.be.null;
                        var recievedBody = res.body;
                        recievedBody[0].date = "null"; // As date is updated on the server, it is ignored under testing by setting it to null
                        expect(JSON.stringify(recievedBody[0])).to.equal(
                            '{"id":8,"title":"Title 8new","description":"Description 8new","date":"null","latitude":81,"longitude":81,"price":"801","event_homepage":"website.com/8new","category":"Konsert"}'                        );
                        done(); 
                    }); 
                }); 
            }); 
        });
        describe("Disable event", function() { 
            it("Should set event.isEnabled to false", function(done) { 
                // Disable event
                request(app)
                .delete("/events/" + 8)
                .set("x-access-token", createToken(2, {}))
                .end(function(error, res) { 
                    expect(res.statusCode).to.equal(200); // Ok
                    expect(error).to.be.null;
                    expect(res.body.affectedRows).to.equal(1);
                    expect(res.body.changedRows).to.equal(1);

                    //Check that edit was successful
                    request(app)
                    .get("/events/" + 8)
                    .end(function(error, res) { 
                        expect(res.statusCode).to.equal(200); // Ok
                        expect(error).to.be.null;
                        expect(res.body[0]).to.be.undefined;
                        done(); 
                    }); 
                }); 
            }); 
        });
    });
});