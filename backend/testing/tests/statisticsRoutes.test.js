var server = require("../../src/server.js");
var app = server.app;
var request = require("supertest");
var chai = require("chai");
var expect = chai.expect;
var config = require("../../config/index")
var jwt = require("jsonwebtoken");

var runsqlfile = require("../runsqlfile");

function createToken(category_id, userinfo) {
    if (category_id) {
        userinfo.category_id = category_id;
    }
    return jwt.sign(userinfo,
        config.JWT_keypair.private,
        {
            algorithm: 'RS256',
            expiresIn: '5m'
        });
}

beforeAll((done => {
    runsqlfile(__dirname + "/../createTestTable.sql", server.pool, () => {
        runsqlfile(__dirname + "/../createTestData.sql", server.pool, done)
    })
}));

describe("StatisticsRoutes tests", function () {
    describe("GET /statistics", function () {
        describe("Get all statistics", function () {
            it("Should retrieve statistics", function (done) {
                request(app)
                    .get("/statistics/")
                    .set("x-access-token", createToken(2,{}))
                    .query({
                        "municipality_id": "",
                        "start_date": "1483275736",
                        "end_date": "1672578136"
                    })
                    .end(function (error, res) {
                        expect(res.statusCode).to.equal(200); // Ok
                        expect(error).to.be.null;
                        expect(JSON.stringify(res.body.getNumberOfIssuesInCat[0])).to.equal(
                            '{"name":"Grafitti/Tagging","value":1}'
                        );
                        expect(JSON.stringify(res.body.getNumberOfIssuesCompany[0])).to.equal(
                            '{"name":"Asfalt Remix AS","value":2}'
                        );
                        expect(JSON.stringify(res.body.getAvgTimeInCat[0])).to.equal(
                            undefined
                        );
                        expect(JSON.stringify(res.body.getAvgTimeIssue[0])).to.equal(
                            '{"value":null}'
                        );
                        expect(JSON.stringify(res.body.getActiveUsers[0])).to.equal(
                            '{"name":"Trondheim","value":7}'
                        );
                        done();
                    });
            });
        });
    });
});