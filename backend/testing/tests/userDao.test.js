var mysql = require("mysql");

const UserDao = require("../../src/dao/userDao");
const runsqlfile = require("../runsqlfile");
var config = require("../../config/index")
var fs = require("fs");

var pool = mysql.createPool(config.poolOptions);

let userDao = new UserDao(pool);

beforeAll((done => {
    runsqlfile(__dirname + "/../createTestTable.sql", pool, () => {
        runsqlfile(__dirname + "/../createTestData.sql", pool, done)
    })
}));

afterAll(() => {
    pool.end();
});

test("test getId(email)",done => {
    function callback(status,data){
        console.log("Test callback status: " + status + ", data: " + JSON.stringify(data));
        expect(data.length).toBe(1);
        expect(data[0].id).toBe(1);
        done();
    }
    userDao.getId('sigurd@mail.no',callback)
});

test("test getHash(id)",done => {
    function callback(status,data){
        console.log("Test callback status: " + status + ", data: " + JSON.stringify(data));
        expect(data.length).toBe(1);
        expect(data[0].hash.length).toBe(60);
        done();
    }
    userDao.getHash(1,callback);
});

test("test createUser(json)",done => {
    function callback(status,data){
        console.log("Test callback status: " + status + ", data: " + JSON.stringify(data));
        expect(data.affectedRows).toBe(1);
        done();
    }
    userDao.createUser({
            email: 'mail@mail.com',
            hash: '9asd8fasmAnEjksad21aid0',
            municipality_id: 1,
            category_id: 1
    },callback);
});


test("test createUserDetails(id,json)", done => {
    function callback(status,data){
        console.log("Test callback status:" + status + ", data: " + JSON.stringify(data));
        expect(data.affectedRows).toBe(1);
        done();
    }
    userDao.createUserDetails(13,{
        firstname:'Bruker',
        lastname: 'Detaljer',
        phone_number: 90807060
    },callback)
});

test("test createUserDetails(id,json)", done => {
    function callback(status,data){
        console.log("Test callback status:" + status + ", data: " + JSON.stringify(data));
        expect(status).toEqual(500); // Internal server error
        expect(data.error.code).toEqual("ER_DUP_ENTRY");
        done();
    }
    userDao.createUserDetails(13,{
        firstname:'Bruker',
        lastname: 'Detaljer',
        phone_number: 90807060
    },callback)
});

test("test getUser(id)", done => {
    function callback(status,data){
        console.log("Test callback status: " + status + ", data: " + JSON.stringify(data));
        expect(data.length).toBe(1);
        expect(data[0].firstname).toBe('Sigurd');
        done();
    }
    userDao.getUser(1,callback)
});

test("test updateUserEmail(id,email)", done => {
    function callbackUpdate(status,data){
        console.log("Test callbackUpdate status: ", status + ", data: " + JSON.stringify(data));
        //expect(data.affectedRows).toBe(1);
        expect(data[0].email).toBe("ny@mail.no");
        done();
    }
    function callback(status,data){
        userDao.getUser(1,callbackUpdate);
    }
    userDao.updateUserEmail(1,'ny@mail.no',callback)
});

test("test updateUserDetails(id,json)", done => {
    function callback(status, data){
        console.log("Test callback: " + status + ", data: " + JSON.stringify(data));
        //expect(data[0].affectedRows).toBe(1);
        expect(data[0].firstname).toBe("Morten");
        done();
    }
    function callback2(){
        userDao.getUser(2,callback)
    }
    userDao.updateUserDetails(2,{
        firstname: 'Morten',
        lastname: 'Monsen',
        phone_number: 12345678
    },callback2)
});

test("test updatePassword()", done => {
    function callback(status, data) {
        console.log("Test callback: " + status + ", data: " + JSON.stringify(data));
        expect(data.affectedRows).toBe(1);
        done();
    }
    userDao.updatePassword(1, 'asdfghjkl', callback)
});

test("test disableUserAccount()", done => {
    function callback(status, data) {
        console.log("Test callback: " + status + ", data: " + JSON.stringify(data));
        expect(data.affectedRows).toBe(1);
        done();
    }
    userDao.disableUserAccount(1, callback)
});