USE hverdagshelt_dev;

UPDATE issue SET issue_category_id 1 WHERE issue_category_id IN (2,3);

-- Query to get business user is connected to
SELECT c.* FROM company c
LEFT JOIN company_user_account cua ON c.id = cua.company_id
WHERE cua.user_account_id = 5;



SELECT * FROM issue_category;

-- Query used to get issuelist with latest status
SELECT i.id, i.title, i.description, DATE_FORMAT(i.issue_date, '%d.%m.%y') as date, 
 i.longitude, i.latitude, i.picture_filename, c.name, issue_category.description, 
 i.user_id as userId, s.type as status, si.update_date, i.municipality_id 
 FROM issue i
 LEFT JOIN issue_company ic ON i.id = ic.issue_id
 LEFT JOIN company c ON ic.company_id = c.id
 LEFT JOIN issue_category ON i.issue_category_id = issue_category.id
 LEFT JOIN status_issue si ON i.id = si.issue_id
 LEFT JOIN status s ON si.status_id = s.id
 WHERE CONCAT(i.id, si.update_date) IN (SELECT CONCAT(issue_id, MAX(update_date)) as update_date FROM status_issue GROUP BY issue_id)
 AND c.id = 1;

USE hverdagshelt;
  SELECT i.id, i.title, i.description, DATE_FORMAT(i.issue_date, '%d.%m.%y') as date,
            i.longitude, i.latitude, i.picture_filename, c.name, issue_category.description,
            i.user_id as userId, s.type as status, si.update_date, i.municipality_id
            FROM issue i
            LEFT JOIN issue_company ic ON i.id = ic.issue_id
            LEFT JOIN company c ON ic.company_id = c.id
            LEFT JOIN issue_category ON i.issue_category_id = issue_category.id
            LEFT JOIN status_issue si ON i.id = si.issue_id
            LEFT JOIN status s ON si.status_id = s.id
            WHERE (CONCAT(i.id, si.update_date) IN (SELECT CONCAT(issue_id, MAX(update_date)) as update_date FROM status_issue GROUP BY issue_id) OR si.update_date IS NULL)
            AND (CONCAT(i.id, ic.update_date) IN (SELECT CONCAT(issue_id, MAX(update_date)) as update_date FROM issue_company GROUP BY issue_id) OR ic.update_date IS NULL)
            AND i.municipality_id = 1 AND i.isEnabled = 1;


-- ###### STATISTICS #####

-- Query to generate reports between intevals nationally or in a municipality.
SELECT i.id, i.title, i.description, DATE_FORMAT(issue_date, '%d.%m.%y') as date, ic.description as categoryname, m.municipality_name FROM issue i
JOIN issue_category ic ON i.issue_category_id = ic.id
JOIN municipality m ON i.municipality_id = m.id
where i.municipality_id like '%'
AND 
cast(i.issue_date as date) between '2019-01-16' AND '2019-01-18' ORDER BY i.issue_date ASC;


-- Query for:
-- Antall saker av hver kategorier
SELECT ic.description as 'category', count(i.id) as 'number_of_issues' FROM issue i 
JOIN issue_category ic ON i.issue_category_id = ic.id
WHERE i.municipality_id like '%'
AND
cast(i.issue_date as date) BETWEEN '2019-01-18' AND '2019-01-22' 
GROUP BY ic.description;

-- Antall saker registret / ferdig (Må se litt på den, ligger duplikater)
-- SELECT count() as 'Saker som er registrert', count() as 'Saker som er ferdig',
-- SELECT i.id FROM issue i JOIN status_issue si ON i.id = si.issue_id WHERE si.status_id = 1 AND si.status_id != 4;

-- Gjennomsnittstid per sak (i dager)
SELECT avg(cast(si.update_date as date) - cast(i.issue_date as date)) as 'avarage_time_on_a_issue' FROM issue i 
JOIN status_issue si ON i.id = si.issue_id 
WHERE si.status_id = 4
AND
i.municipality_id like '%'
AND 
cast(i.issue_date as date) 
AND 
cast(si.update_date as date) BETWEEN '2019-01-18' AND '2019-01-30';

--hjelpemetode til gjennomsnittstid for så se hvilke saker som er ferdig
SELECT i.issue_date,si.update_date FROM issue i JOIN status_issue si ON i.id = si.issue_id WHERE si.status_id = 4;

-- Gjennomsnittstid per kategori (i dager)
USE hverdagshelt;

SELECT ic.description, avg(cast(si.update_date as date) - cast(i.issue_date as date)) as 'avarage_time_for_cat' FROM issue i 
JOIN status_issue si ON i.id = si.issue_id 
JOIN issue_category ic ON i.issue_category_id = ic.id 
WHERE si.status_id = 4 
AND i.municipality_id like '%'
AND cast(i.issue_date as date) 
AND cast(si.update_date as date) BETWEEN '2019-01-18' AND '2019-01-30' GROUP BY ic.description;

SELECT * FROM status_issue;

-- Antall saker per bedrift
SELECT c.name as 'company_name', count(i.id) as 'number_of_issues_for_company' FROM issue i 
JOIN issue_company ic ON i.id = ic.issue_id
JOIN company c ON ic.company_id = c.id 
WHERE i.municipality_id like '%'
AND
cast(i.issue_date as date) BETWEEN '2019-01-18' AND '2019-01-25' GROUP BY c.name;

SELECT * FROM issue_company;

-- Antall saker per kommune/eller en bestemt
SELECT m.municipality_name, count(i.id) as 'number_of_issues_for_municipality' FROM issue i 
JOIN municipality m ON i.municipality_id = m.id 
WHERE i.municipality_id like '%'
AND
cast(i.issue_date as date) BETWEEN '2019-01-18' AND '2019-01-23' GROUP BY m.municipality_name;

-- Hvor mange aktive brukere er det i en kommune
SELECT m.municipality_name, count(DISTINCT ua.id) as 'number_of_active_users' FROM user_account ua 
JOIN municipality m ON ua.municipality_id = m.id 
JOIN issue i ON ua.id = i.user_id 
WHERE m.id like '%'
AND
cast(i.issue_date as date) BETWEEN '2019-01-18' AND '2019-01-21' GROUP BY m.municipality_name;

SELECT i.id, i.title, i.description, DATE_FORMAT(i.issue_date, '%d.%m.%y') as date,
            i.longitude, i.latitude, i.picture_filename, c.name, issue_category.description,
            i.user_id as userId, s.type as status, si.update_date, i.municipality_id
            FROM issue i
             LEFT JOIN issue_company ic ON i.id = ic.issue_id
             LEFT JOIN company c ON ic.company_id = c.id
             LEFT JOIN issue_category ON i.issue_category_id = issue_category.id
             LEFT JOIN status_issue si ON i.id = si.issue_id
             LEFT JOIN status s ON si.status_id = s.id
             WHERE (CONCAT(i.id, si.update_date) IN (SELECT CONCAT(issue_id, MAX(update_date)) as update_date FROM status_issue GROUP BY issue_id) OR si.update_date IS NULL)
             AND (CONCAT(i.id, ic.update_date) IN (SELECT CONCAT(issue_id, MAX(update_date)) as update_date FROM issue_company GROUP BY issue_id) OR ic.update_date IS NULL)
             AND i.municipality_id = 1 AND i.isEnabled = 1 ORDER BY i.issue_date DESC;

             INSERT INTO issue (id,title,description,issue_date,longitude,latitude,picture_filename,issue_category_id,municipality_id,user_id, estimated_time, isEnabled) VALUES (DEFAULT,'Strøm','Strømmen har gått på risvollan',DEFAULT,63.37579,10.34734,'grafitti.img',5,1,1,null,DEFAULT);


SELECT m.municipality_name as 'name', count(i.id) as 'value' FROM issue i JOIN municipality m ON i.municipality_id = m.id WHERE i.municipality_id like '%' AND cast(i.issue_date as date) BETWEEN FROM_UNIXTIME('2019-01-27') AND FROM_UNIXTIME('2019-03-01') GROUP BY m.municipality_name ORDER BY value DESC