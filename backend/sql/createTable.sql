-- use hverdagshelt_dev to test script
-- use hverdagshelt to change demo DB
-- remove use when done editing

 USE hverdagshelt;
-- USE hverdagshelttest;

DROP TABLE IF EXISTS company_user_account;
DROP TABLE IF EXISTS user_details;
DROP TABLE IF EXISTS status_issue;
DROP TABLE IF EXISTS status;
DROP TABLE IF EXISTS event;
DROP TABLE IF EXISTS event_category;
DROP TABLE IF EXISTS issue_company;
DROP TABLE IF EXISTS progress_update;
DROP TABLE IF EXISTS issue;
DROP TABLE IF EXISTS company_municipality;
DROP TABLE IF EXISTS company;
DROP TABLE IF EXISTS issue_category;
DROP TABLE IF EXISTS user_point;
DROP TABLE IF EXISTS user_account;
DROP TABLE IF EXISTS municipality;
DROP TABLE IF EXISTS user_category;

CREATE TABLE user_account (
  id INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
  email VARCHAR(256) NOT NULL UNIQUE,
  hash VARCHAR(256) NOT NULL,
  municipality_id INTEGER,
  category_id INTEGER NOT NULL,
  isEnabled BOOLEAN DEFAULT TRUE NOT NULL
);

CREATE TABLE user_details (
  id INTEGER PRIMARY KEY NOT NULL,
  firstname VARCHAR(256),
  lastname VARCHAR (256),
  phone_number VARCHAR (256)
);

CREATE TABLE user_category (
  id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  category_name VARCHAR (256) NOT NULL
);

CREATE TABLE municipality (
  id INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
  municipality_name VARCHAR(256) NOT NULL,
  longitude DECIMAL(11, 8) NOT NULL,
  latitude DECIMAL(10, 8) NOT NULL
);

CREATE TABLE company (
  id INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name VARCHAR(256) NOT NULL,
  description VARCHAR(256) NOT NULL,
  isEnabled BOOLEAN DEFAULT TRUE NOT NULL
);

CREATE TABLE event (
  id INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
  title VARCHAR(256) NOT NULL,
  description VARCHAR(256),
  event_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  longitude DECIMAL(11, 8) NOT NULL,
  latitude DECIMAL(10, 8) NOT NULL,
  price VARCHAR(256),
  event_homepage VARCHAR(256),
  event_category_id INTEGER NOT NULL,
  municipality_id INTEGER NOT NULL,
  isEnabled BOOLEAN DEFAULT TRUE
);

CREATE TABLE status (
  id INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
  type VARCHAR(256) NOT NULL
);

CREATE TABLE issue (
  id INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
  title VARCHAR(256),
  description VARCHAR(256) NOT NULL,
  issue_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  longitude DECIMAL(11, 8) NOT NULL,
  latitude DECIMAL(10, 8) NOT NULL,
  picture_filename VARCHAR(256),
  issue_category_id INTEGER NOT NULL DEFAULT 1,
  municipality_id INTEGER NOT NULL,
  user_id INTEGER NOT NULL,
  estimated_time DATETIME,
  isEnabled BOOLEAN DEFAULT TRUE
);

CREATE TABLE progress_update(
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  issue_id INTEGER NOT NULL,
  updatemessage VARCHAR(256) NOT NULL,
  update_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  user_id INTEGER NOT NULL,
  CONSTRAINT update_user FOREIGN KEY (user_id) REFERENCES user_account (id)
);

CREATE TABLE status_issue (
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  status_id INTEGER NOT NULL,
  issue_id INTEGER NOT NULL,
  update_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);

CREATE TABLE issue_category (
  id INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
  description VARCHAR(256) NOT NULL,
  national BOOLEAN DEFAULT FALSE NOT NULL
);

CREATE TABLE event_category (
  id INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
  description VARCHAR(256) NOT NULL
);

CREATE TABLE issue_company (
  issue_id INTEGER NOT NULL,
  company_id INTEGER NOT NULL,
  update_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT pk PRIMARY KEY (issue_id, company_id)
);

CREATE TABLE company_municipality (
  company_id INTEGER NOT NULL,
  municipality_id INTEGER NOT NULL,
  CONSTRAINT pk PRIMARY KEY (company_id, municipality_id)
);

CREATE TABLE company_user_account(
  company_id INTEGER NOT NULL,
  user_account_id INTEGER NOT NULL,
  CONSTRAINT pk PRIMARY KEY (company_id, user_account_id),
  CONSTRAINT cua_user_account FOREIGN KEY (user_account_id) REFERENCES user_account (id),
  CONSTRAINT cua_company FOREIGN KEY (company_id) REFERENCES company (id)
);

CREATE TABLE user_point(
  point_id INTEGER AUTO_INCREMENT NOT NULL,
  date DATE NOT NULL,
  user_id INTEGER NOT NULL, 
  CONSTRAINT pk PRIMARY KEY(point_id),
  CONSTRAINT fk FOREIGN KEY (user_id) REFERENCES user_account(id)
);


ALTER TABLE user_account ADD FOREIGN KEY (category_id) REFERENCES user_category (id);
ALTER TABLE user_account ADD FOREIGN KEY (municipality_id) REFERENCES municipality (id);
ALTER TABLE user_details ADD FOREIGN KEY (id) REFERENCES user_account (id);
ALTER TABLE event ADD FOREIGN KEY (event_category_id) REFERENCES event_category (id);
ALTER TABLE event ADD FOREIGN KEY (municipality_id) REFERENCES municipality (id);
ALTER TABLE issue ADD FOREIGN KEY (issue_category_id) REFERENCES issue_category (id);
ALTER TABLE issue ADD FOREIGN KEY (municipality_id) REFERENCES municipality (id);
ALTER TABLE issue ADD FOREIGN KEY (user_id) REFERENCES user_account (id);
ALTER TABLE status_issue ADD FOREIGN KEY (issue_id) REFERENCES issue (id);
ALTER TABLE status_issue ADD FOREIGN KEY (status_id) REFERENCES status (id);
ALTER TABLE progress_update ADD FOREIGN KEY (issue_id) REFERENCES issue (id);
ALTER TABLE issue_company ADD FOREIGN KEY (company_id) REFERENCES company (id);
ALTER TABLE issue_company ADD FOREIGN KEY (issue_id) REFERENCES issue (id);
ALTER TABLE company_municipality ADD FOREIGN KEY (company_id) REFERENCES company (id);
ALTER TABLE company_municipality ADD FOREIGN KEY (municipality_id) REFERENCES municipality (id);

ALTER TABLE user_account AUTO_INCREMENT = 1;
ALTER TABLE user_details AUTO_INCREMENT = 1;
ALTER TABLE status_issue AUTO_INCREMENT = 1;
ALTER TABLE status AUTO_INCREMENT = 1;
ALTER TABLE event AUTO_INCREMENT = 1;
ALTER TABLE event_category AUTO_INCREMENT = 1;
ALTER TABLE progress_update AUTO_INCREMENT = 1;
ALTER TABLE issue_company AUTO_INCREMENT = 1;
ALTER TABLE issue AUTO_INCREMENT = 1;
ALTER TABLE company_municipality AUTO_INCREMENT = 1;
ALTER TABLE company AUTO_INCREMENT = 1;
ALTER TABLE issue_category AUTO_INCREMENT = 1;
ALTER TABLE municipality AUTO_INCREMENT = 1;
ALTER TABLE user_category AUTO_INCREMENT = 1;
ALTER TABLE user_account AUTO_INCREMENT = 1;