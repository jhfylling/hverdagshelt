/**
 * The test config makes the json web token keypair available through the environment
 * so that tokens that are created by tests are accepted by the server.
 * It also changes request limiters so tests can run without being stopped by the
 * limiters of other environments, which are lot smaller.
 */

'use strict';

module.exports = {
  env: 'test',
  imagePath: '/../../../frontend/public/img/',
  poolOptions: {
    connectionLimit: 2,
    host: "johandev.no",
    user: "hverdagsheltsys",
    password: "sVPFWg5EOYNFWs4N",
    database: "hverdagshelttest",
    debug: false,
    multipleStatements: true
  },
  port: process.env.PORT || 5000,
  JWT_keypair: {"public":"-----BEGIN RSA PUBLIC KEY-----\nMIIBCgKCAQEAyuoSQemzuhGWyozkYrq1kYDRmMJFiJ7chLAetkqCDdBj8+BKepM7OI+DbtKB\n3YVgm3XLORC/BWq4M3pk76a6YDG/I9xOeHgJmcPw5uHnVw68uPjwwrnxo3f4oU65CIU7Lhzy\nHJmUKyUYncHGZNFTa1cUrqYgymrkg9HV1W5UT5WQ/NXiWTZig9XAfMOr2S4QgyhB9XjbAO+O\npKRxbAccf3qnoxhaiPnvC+7FCdTrIB8wy9LIQQ/mFBanCmqVZ00vYXLGR1Ay57VBofOZMUdF\n3SIyjHQVHOoJZGdhoZedRtDwM/HB8NxJwZOPOJ3ZD9F2PYmSzh32KKGOtBI9ic5p+QIDAQAB\n-----END RSA PUBLIC KEY-----\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n","private":"-----BEGIN RSA PRIVATE KEY-----\nMIIEpAIBAAKCAQEAyuoSQemzuhGWyozkYrq1kYDRmMJFiJ7chLAetkqCDdBj8+BKepM7OI+D\nbtKB3YVgm3XLORC/BWq4M3pk76a6YDG/I9xOeHgJmcPw5uHnVw68uPjwwrnxo3f4oU65CIU7\nLhzyHJmUKyUYncHGZNFTa1cUrqYgymrkg9HV1W5UT5WQ/NXiWTZig9XAfMOr2S4QgyhB9Xjb\nAO+OpKRxbAccf3qnoxhaiPnvC+7FCdTrIB8wy9LIQQ/mFBanCmqVZ00vYXLGR1Ay57VBofOZ\nMUdF3SIyjHQVHOoJZGdhoZedRtDwM/HB8NxJwZOPOJ3ZD9F2PYmSzh32KKGOtBI9ic5p+QID\nAQABAoIBAQDGi659rCzd/ePsIVw/dq2+x6tOq5jaC4d+lc4XVGRZG/URZf+6XGHwAAz83SGR\nHbbfpqzJE6e1pyzBwhsNm7h183cxoZluqxXJzT3kzFpOhu8Glx8Nil/0MamoPJ00xxiXEF+e\nuG+Qn+WsRzTW5TOYrkZgddFT/q/GhAWVpzT8AH5NzhorpvWteKNdDIdEZk/K7eXa8UNkh9Mz\nyi7UVdm56PXP9HuHoqGhbhyiaG2JMbTJzZNsJ2oB7VuM5np34CXUSJB4X7TXrSzGGuSDxgYc\nEpQVULlmmr1ouwQQBaAx2q1WfGjX4IgFuMdb7oWSKwFS9MqlIxW34F1JFn2rmplhAoGBAP9K\nQUP8rMjnDWYBTEMgiMgkw+ALSoegJ/8wf8ms3XBBuxUe8o1mZHxveqh4ZobPZcwLBQSkfB2/\nzjRbQH8ZY/7x9Ug4lS6h6TN5bMbgIFn0c2KcFjUwVpIrHU4aryercZRY9FcSGJrf0HJ5zqxE\nwJxTjr2FQZR5M85muDfe0XcdAoGBAMt6h34XCa23EtfssSRjWesVWmgv4yLqDnAZ1+p2GI3k\n1zLsu4pi0xz8yv0BEC1086/W2tp6zOjP2asNXu37eIIa4RPMikaZvFs9zlu1NXoyR3qkrjOi\nTkIG0auPPKURS/3+3UJrsH9PfrLa399BT4u7aDQTBBWA3z47f+ulktuNAoGAR9aTaxhNXvN1\n18CBv1oGY5vE6uUCggvVdYAmVZApeA9nH9hnlNa9bC7Kq2HGKCVwlex76wSGULDKy/zpt1pj\nwzg/N6WEhWzWfL+sloI3WFjrp1IZBbqmgddUFch4hYc/HOmzSFU1S0+0DqKsP9nRAayZvEcl\nXyyEIY7SRBaVhxkCgYAWsi4WxF8R/0+cwgvoMSTAcftJj73YER9XJarNvipy+Ul/pqQshWVi\n4FeL7OhvCZJ81IxIvlXIyhZPXAhwpxIEAReF69Sol1FJJBW2aPn0DVOnvGKp47qy5qvQZQOw\nrcM6K4lmdTiUmrtZG8zIzDIhNeAjntNfyqYb/OsBz9AXqQKBgQD4m2v8dHyZ4OL19vN3IU/m\nKS9FFQFAPgkxgBsdmbAvG+DSV5mLgx0aOiE2SuH32OAsUqRJfTMLjwNZI5OuH5/YgWCSeFyV\nKs2gRRGRCll5pt0Kg3bC+qzTRP03pDK6Vh4VVrnUM82t8mt6MFNLVJpUwZ1aqeKrT8xCaDbZ\ngiAOUw==\n-----END RSA PRIVATE KEY-----\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"},
  log: false,
  sessionLimiter: 500,
  passwordResetLimiter: 500,
  generalLimiter: 500
};