/**
 * Configs are used to describe different run environments.
 * They among other things describe the databases used in each environment
 * and other variables.
 * Which config is used is defined by the startup scripts in package.json by
 * setting the environment variable "NODE_ENV".
 */
'use strict';

var env = process.env.NODE_ENV || "test";
var config = require(`./${env}` + "Config");

module.exports = config;