import Error from '../error';
import '../log'

module.exports = function (app, authentication, businessDao) {
    var config = require("../../config/index");
    /* 
        Main authenticator, must always be the first route! 
                                                            */
    /**
     * Runs before every other route to decode the access token specified,
     * the decoded information is then easily accessible in every route afterwards.
     * The decoded information can be access in the request object under authentication.
     */
    app.use((req, res, next) => {
        try{    
            res.set({
                "Content-Type": "application/json",
                "X-Content-Type-Options" : "nosniff"
              });
    
            req.authentication = {
                "token_specified" : false,
                "category_id" : 0, 
                "email" : null, 
                "id" : null,
                "business_id" : null,
                "municipality_id" : null
            };
    
            //var headerToken = req.headers["x-access-token"];
            //var cookietoken = (req.cookies['x-access-token']);
            var token;
            if(config.env == "test"){
                token = req.headers["x-access-token"];
            } else{
                token = req.cookies["x-access-token"];
            }

            req.authentication.token_specified = (token != "undefined" && token != "null" && token)?(true):(false);

            if(req.authentication.token_specified){    
                authentication.verifyToken(token, (error, decoded) => {
                    if(!error){
                        if(decoded.id != null){
                            req.authentication.id = decoded.id;
                        }
                        if(decoded.category_id != null){
                            req.authentication.category_id = decoded.category_id;
                        }
                        if(decoded.email != null){
                            req.authentication.email = decoded.email;
                        }
                        if(decoded.municipality_id != null){
                            req.authentication.municipality_id = decoded.municipality_id;
                        }
                        if(decoded.category_id == 4){
                            businessDao.getBusinessByUserId(decoded.id, (status, rows) => {
                                if(status == 200 && rows[0] != null){
                                    req.authentication.business_id = rows[0].id;
                                } else if(status == 200 && rows[0] == null){
                                    console.log("Tried getting business_id of a business user, but the user isn't assigned to a business," 
                                    + "continuing request as a private user...");
                                    req.authentication.category_id = 1;
                                } else{
                                    var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
                                    next(newError);
                                }
                            });
                        }
                        next();
                    } else{
                        next(error);
                    }
                });
            } else{
                next();
            }
        } catch (exception) {
            var newError = new Error("Caught exception when verifying user, exception: " + exception, 500, null, null);
            next(newError); 
        }
    });
    
    /**
     * Creates a new session (An access token and user information) from specified token.
     * Used to update a pre-existing session with new information, or to refresh it whenever
     * actions are taken on the website.
     */
    app.get("/session", (req, res, next) => {
        try{
            if(req.authentication.token_specified){
                authentication.newSession(req.authentication.id, (error, session) => {
                    if(!error){
                        res.status(201); // Created
                        res.json(session);
                    } else{
                        next(error);
                    }
                });
            } else{
                var newError = new Error("Token not specified when requesting new session.", 400 /* Bad request */, null, null);
                next(newError);  
            }
        } catch(exception){
            var newError = new Error("Caught exception when updating session, exception: " + exception, 500, null, null);
            next(newError);         
        }
    });
};