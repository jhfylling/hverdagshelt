import Error from '../error';
import '../log'

module.exports = function (app, eventDao) {
    /**
     * Gets information about every enabled event in a municipality.
     */
    app.get("/events/municipality/:id",(req, res, next) => {
        try{
            eventDao.getEvents(req.params.id,(status,data) => {
                if(status == 200){
                    res.status(status);
                    res.json(data)
                } else{
                    var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
                    next(newError);
                }
            });
        } catch(exception){
            var newError = new Error("Caught exception when getting events from municipality. Exception: " + exception, 500, null, null);
            next(newError);
        }
    });

    /**
     * Gets information about specified event.
     */
    app.get("/events/:id", (req, res, next) => {
        try{
            eventDao.getEvent(req.params.id,(status, data) => {
                if(status == 200) {
                    res.status(status);
                    res.json(data)
                } else{
                    var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
                    next(newError);
                }
            });
        } catch(exception){
            var newError = new Error("Caught exception when getting event. Exception: " + exception, 500, null, null);
            next(newError);
        }
    });

    /**
     * Gets information about every event in specified category.
     */
    app.get("/events/category/:id", (req, res, next) => {
        try{
            eventDao.getEventInCategory(req.params.id,(status, data) => {
                if(status == 200) {
                    res.status(status);
                    res.json(data)
                } else{
                    var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
                    next(newError);
                }
            });
        } catch(exception){
            var newError = new Error("Caught exception when getting event from category. Exception: " + exception, 500, null, null);
            next(newError);
        }
    });

    /**
     * Creates a new event.
     * Authentication required is to be either a municipality user or an admin user.
     */
    app.post("/events", (req,res,next) => {
        try{
            /* Authentication begin */
            if(req.authentication.category_id == 3 || req.authentication.category_id == 2){
                // Do nothing
            } else{
                var newError = new Error(null, null, "You dont have the authorization to create an event!", 401 /* Unauthorized */);
                next(newError);
            }
            /* Authentication end */

            let inputData = {
                title: req.fields.title,
                description: req.fields.description,
                longitude: req.fields.longitude,
                latitude: req.fields.latitude,
                price: req.fields.price,
                event_homepage: req.fields.event_homepage,
                event_category_id: req.fields.event_category_id,
                municipality_id: req.fields.municipality_id,
            };
            eventDao.createEvent(inputData,(status,data) => {
                if(status == 200) {
                    res.status(201); // Created
                    res.json(data)
                } else{
                    var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
                    next(newError);
                }
            });
        }catch (exception) {
            var newError = new Error("Caught exception when creating new event. Exception: " + exception, 500, null, null);
            next(newError);
        }
    });

    /**
     * Edits the specified event.
     * Authentication required is to be either a municipality user or an admin user.
     */
    app.put("/events/:id",(req,res,next) => {
        try{
            /* Authentication begin */
            if(req.authentication.category_id == 3 || req.authentication.category_id == 2){
                // Do nothing
            } else{
                var newError = new Error(null, null, "You dont have the authorization to edit an event!", 401 /* Unauthorized */);
                next(newError);
            }
            /* Authentication end */

            let inputData = {
                title: req.fields.title,
                description: req.fields.description,
                longitude: req.fields.longitude,
                latitude: req.fields.latitude,
                price: req.fields.price,
                event_homepage: req.fields.event_homepage,
                event_category_id: req.fields.event_category_id,
                municipality_id: req.fields.municipality_id,
            };

            eventDao.updateEvent(req.params.id,inputData,(status,data) => {
                if(status == 200) {
                    res.status(status);
                    res.json(data)
                } else{
                    var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
                    next(newError);
                }
            })
        }catch(exception){
            var newError = new Error("Caught exception when updating event. Exception: " + exception, 500, null, null);
            next(newError);
        }
    });

    /**
     * Disables the specified event.
     * Authentication required is to be either a municipality user or an admin user.
     */
    app.delete("/events/:id",(req,res,next) => {
        try{
            /* Authentication begin */
            if(req.authentication.category_id == 3 || req.authentication.category_id == 2){
                // Do nothing
            } else{
                var newError = new Error(null, null, "You dont have the authorization to create an event!", 401 /* Unauthorized */);
                next(newError);
            }
            /* Authentication end */

            eventDao.disableEvent(req.params.id,(status,data) => {
                if(status == 200) {
                    res.status(200); // Ok
                    res.json(data)
                } else{
                    var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
                    next(newError);
                }
            })
        }catch(exception){
            var newError = new Error("Caught exception when deleting/disabling event. Exception: " + exception, 500, null, null);
            next(newError);
        }
    });

    /**
     * Gets event categories.
     */
    app.get("/eventcategories", (req,res,next) =>{
        try{
            eventDao.getEventCategories((status,data) => {
                if(status == 200) {
                    res.status(status);
                    res.json(data);
                }else{
                    var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
                    next(newError);
                }
            })
        }catch(exception){
            var newError = new Error("Caught exception when deleting/disabling event. Exception: " + exception, 500, null, null);
            next(newError);
        }
    });
};