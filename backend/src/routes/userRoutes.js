import Error from '../error';
import sendEmail from "../emailService";
import '../log'
import { EDESTADDRREQ } from 'constants';
import { fstat } from 'fs';

module.exports = function (app, userDao, authentication, businessDao) {
    const rateLimit = require("express-rate-limit");
    var config = require("../../config/index");
    var fs = require("fs");

    /**
     * Limits the amount of login and register attempts for each ip in a time window.
     */
    const sessionLimiter = rateLimit({
        windowMs: 3 * 60 * 1000,    // 3 minutes
        max: (config.sessionLimiter) || 5,                     // Max amount of login/register requests within 3 minutes 
        message: "For mange innlogginger/registreringer på et lite tidsvindu, vennligst vent 3 minutter før du prøver igjen!"
      });

    app.post("/register", sessionLimiter);
    app.post("/login", sessionLimiter);

    /**
     * Authenticates registration of a new user.
     * Authentication required to create a 'Privat bruker' is none.
     * Authentication required to create a 'Kommunal bruker' is to be an admin user.
     * Authentication required to create a 'Bedriftsbruker' is to be a 'Kommunal bruker' an 'Administrator'.
     * Admin users can not be created by this endpoint. Admin users must be created by the system administrator manually.
     */
    app.post("/register", (req, res, next) => {
        try{
            /* Authentication begin */
            if(!req.fields.category_id){
                req.fields.category_id = 1;
            }

            var category_id = req.authentication.category_id;        
            var newCategoryId = req.fields.category_id;
            if(typeof newCategoryId == "string"){
                newCategoryId = parseInt(newCategoryId);
            }

            var hasAuthentication;
            switch(newCategoryId){
                case 1: // Anyone can create a 'Privat bruker'
                    hasAuthentication = true;
                break;
                case 2: // Nobody can create an 'Administrator' bruker
                    hasAuthentication = false;
                break;
                case 3: // Only 'Administrator' can create 'Kommunal bruker'
                    if(category_id == 2 || category_id == 3){
                        hasAuthentication = true;
                    } else{
                        hasAuthentication = false;
                    }
                break;
                case 4: // Only 'Kommunal bruker' and 'Administrator' can create 'Bedriftsbruker'
                    if(category_id == 2 || category_id == 3){
                        hasAuthentication = true;
                    } else{
                        hasAuthentication = false;
                    }
                    break;
                default: // Defaults to not having authentication
                    hasAuthentication = false;
                break;
            }

            if(hasAuthentication){
                next();
            } else{
                var newError = new Error(null, null,"You dont have the authorization to create this type of user!", 401 /* Unauthorized */);
                next(newError);
            }
            /* Authentication end */

        } catch(exception){
            var newError = new Error("Caught exception when registering user. Exception: " + exception, 500 /* Internal server error */, null, null);
            next(newError);
        }
    });

    /**
     * Registers a user and responds with a session for the new user.
     * Authentications required are described above.
     */
    app.post('/register', (req, res, next) => {
        try {
            var email = req.fields.email;
            var password = req.fields.password;
            var municipality_id = req.fields.municipality_id;
            var category_id = req.fields.category_id;

            // Optional information
            var firstname = req.fields.firstname;
            var lastname = req.fields.lastname;
            var phone_number = req.fields.phone_number;

            if(email && password && municipality_id && category_id){
                userDao.getId(email, (status, rows) => {
                    if(status == 200 && rows[0] == null){ // Check if user with this email already exists
                        authentication.hashPassword(password, (error, hash) => {
                            if(!error){
                                userDao.createUser({email, hash, municipality_id, category_id}, (status, rows) =>{
                                    if(status == 200){
                                        if(config.env!="test"){
                                            sendEmail(email, 'newUserEmail', null).catch(error => console.log(error));
                                        }
                                        userDao.getId(email, (status, rows) => {
                                            if(status == 200 && rows[0] != null){
                                                var id = rows[0].id;
                                                if(firstname || lastname || phone_number){
                                                    userDao.createUserDetails(id, {"firstname" : firstname, "lastname" : lastname, "phone_number" : phone_number}, (status, rows) => {
                                                        if(status == 200){
                                                            authentication.newSession(id, (error, session) => {
                                                                if(!error){
                                                                    res.status(201); // Created
                                                                    res.json(session);
                                                                } else{
                                                                    next(error);
                                                                }
                                                            });
                                                        } else{
                                                            var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
                                                            next(newError);
                                                        }
                                                    });
                                                } else{
                                                    authentication.newSession(id, (error, session) => {
                                                        if(!error){
                                                            res.status(201); // Created
                                                            res.json(session);
                                                        } else{
                                                            next(error);
                                                        }
                                                    });
                                                }
                                            } else{
                                                var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
                                                next(newError);
                                            }
                                        });
                                    } else{
                                        var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
                                        next(newError);
                                    }
                                });
                            } else{
                                next(error);
                            }
                        });
                    } else if (status == 200 && rows[0] != null){
                        var newError = new Error(null, null, "A user with this email already exists!", 409 /* Conflict */);
                        next(newError);
                    } else if (status != 200){
                        var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
                        next(newError);
                    }
                });
            } else{
                var newError = new Error(null, null, "Email, password and/or municipality_id not specified!", 400 /* Bad request */);
                next(newError);
            }
        } catch (exception) {
            var newError = new Error("Caught exception when registering user. Exception: " + exception, 500 /* Internal server error */, null, null);
            next(newError);
        }
    });

    
    /**
     * Logs in the user and responds with a new session if the details are correct.
     * Authentication required is to enter the correct login details as described by
     * the email and hashed password in the database.
     */
    app.post("/login", (req, res, next) => {
        try {
            /* Authentication begin */
            // No authentication necessary
            /* Authentication end */
            var email = req.fields.email;
            var password = req.fields.password;

            if (email && password){
                userDao.getId(email, (status, rows) => {
                    if(status == 200 && rows[0] != null){
                        var id = rows[0].id;
                        userDao.getHash(id, (status, rows) => {
                            if(status == 200){
                                var hash = rows[0].hash;
                                authentication.comparePasswords(password, hash, (error, success) => {
                                    if(!error){
                                        if(success){
                                            authentication.newSession(id, (error, session) => {
                                                if(!error){
                                                    res.status(201); // Created
                                                    res.json(session);
                                                } else{
                                                    next(error);
                                                }
                                            });
                                        } else{
                                            var newError = new Error(null, null, "Wrong password or email!", 401 /* Bad request */);
                                            next(newError);
                                        }
                                    } else{
                                        next(error);
                                    }
                                });
                            } else{
                                var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
                                next(newError);
                            }
                        });
                    } else{
                        var newError = new Error(null, null, "Wrong password or email!", 401 /* Bad request */);
                        next(newError);
                    }
                });
            } else{
                var newError = new Error(null, null, "Email and/or password not specified!", 400 /* Bad request */);
                next(newError);
            }       
        } catch(exception){
            var newError = new Error("Caught exception when logging in. Exception: " + exception, 500 /* Internal server error */, null, null);
            next(newError);
        }
    });

    /**
     * Edits information about the user.
     * Authentication required is to have the access token of the user.
     */
    app.put("/user", (req, res, next) => {
        try{
            /* Authentication begin */
            if(req.authentication.id){
            /* Authentication end */
                userDao.getUserDetails(req.authentication.id, (status, rows) => {
                    // If user already has userdetails in the database
                    if(status == 200 && rows[0] != null){
                        // Update userdetails
                        userDao.updateUserDetails(req.authentication.id, req.fields, (status, rows) => {
                            if(status == 200){
                                if(req.fields.email){
                                    userDao.updateUserEmail(req.authentication.id, req.fields.email, (status, rows) => {
                                        if (status == 200) {
                                            res.status(200);
                                            res.json(rows);
                                        } else{
                                            var newError = new Error("SQL query failed, SQL error: " + status, 500 /* Internal server error */, null, null);
                                            next(newError);
                                        }
                                    });
                                } else{
                                    res.status(200); // Ok
                                    next();
                                }
                            } else{
                                var newError = new Error("SQL query failed, SQL error: " + status, 500 /* Internal server error */, null, null);
                                next(newError);
                            }
                        });
                    // If user doesen't already have user_details in the database
                    } else if(status == 200 && rows[0] == null){
                        // Create userdetails
                        userDao.createUserDetails(req.authentication.id, req.fields, (status, rows) => {
                            if(status == 200){
                                if(req.fields.email){
                                    userDao.updateUserEmail(req.authentication.id, req.fields.email, (status, rows) => {
                                        if (status == 200) {
                                            res.status(200);
                                            res.json(rows);
                                        } else{
                                            var newError = new Error("SQL query failed, SQL error: " + status, 500 /* Internal server error */, null, null);
                                            next(newError);
                                        }
                                    });
                                } else{
                                    res.status(200); // Ok
                                    next();
                                }
                            } else{
                                var newError = new Error("SQL query failed, SQL error: " + status, 500 /* Internal server error */, null, null);
                                next(newError);
                            }
                        });

                    } else{
                        var newError = new Error("SQL query failed, SQL error: " + status, 500 /* Internal server error */, null, null);
                        next(newError);
                    }
                });
            } else{
                var newError = new Error(null, null, "You need to be logged in to edit your user details! Please log in again.", 400 /* Bad request */);
                next(newError);
            }
        }catch(exception){
            var newError = new Error("Caught exception when updating user. Exception: " + exception, 500 /* Internal server error */, null, null);
            next(newError);
        }
    });

    /**
     * Gets information about the user.
     * Authentication required is to have the access token of the user.
     */
    app.get("/user", (req, res, next) => {
        try{
            /* Authentication begin */
            if(req.authentication.id){
            /* Authentication end */
                userDao.getUser(req.authentication.id, (status, rows) => {
                    if(status == 200 && rows[0] != null){
                        res.status(status);
                        res.json(rows[0]);
                    } else if(status = 200 && rows[0] == null){
                        var newError = new Error(null, null, "Coulden't find requested user.", 404 /* Not found */);
                        next(newError);
                    } else {
                        var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
                        next(newError);
                    }
                });
            } else{
                var newError = new Error(null, null, "You need to be logged in to get your user details! Please log in again.", 400 /* Bad request */);
                next(newError);
            }
        } catch(exception){
            var newError = new Error("Caught exception when getting user. Exception: " + exception, 500, null, null);
            next(newError);
        }
    });

    const passwordResetLimiter = rateLimit({
        windowMs: 3 * 60 * 1000,    // 3 minutes
        max: ((config.passwordResetLimiter) || 5) * 2,         // Max amount of password reset requests within 3 minutes 
        message: "For mange passord reset forespørsler på et lite tidsvindu, vennligst vent 3 minutter før du prøver igjen!"
      });

    app.put("/user/resetpassword", passwordResetLimiter);

    /**
     * Sends a reset password email to the specified user.
     */
    app.put("/user/resetpassword", (req, res, next) => {
        try{
            if(req.fields.email){
                authentication.newTokenPasswordReset(req.fields.email, (error, token) => {
                    if(!error){
                        let url = 'https://hverdagshelt.net/nyttpassord/' + token;
                        if(req.fields.newUser){
                            sendEmail(req.fields.email, 'newCompanyUserEmail', url).catch(error => console.log(error))
                            res.status(200);
                            //res.json(res);
                        } else {
                            sendEmail(req.fields.email, 'passwordChangeEmail', url).catch(error => console.log(error))
                            res.status(200);
                            //res.json(res);
                        }
                    }else{
                        next(error);
                    }
                })
            }else{
                var newError = new Error(null, null, "Email not specified!", 400);
                next(newError);
            }
        }catch(exception){
            var newError = new Error("Caught exception when resetting password. Exception: " + exception, 500, null, null);
            next(newError);
        }
    });

    /**
     * Changes the password of the user.
     * Authentication required is to have the reset password token of the user.
     */
    app.put("/user/resetpassword/:token", (req, res, next) => {
        try{
            authentication.verifyToken(req.params.token, (error, decoded) => {
                if(!error){
                    authentication.hashPassword(req.fields.newPassword, (error, hashed) => {
                        if(!error){
                            userDao.updatePassword(decoded.id, hashed, (status, rows) => {
                                if(status == 200){
                                    res.status(200);
                                    res.json(rows);
                                }else{
                                    var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
                                    next(newError);
                                }
                            })
                        }else{
                            next(error);
                        }
                    })
                }else{
                    next(error);
                }
            })
        }catch(exception){
            var newError = new Error("Caught exception when resetting password. Exception: " + exception, 500, null, null);
            next(newError);
        }
    });

    /**
     * Gets information about the user and the company the user is assigned to.
     * Authentication required is to be either a municipality user or an admin user.
     */
    app.get("/user/:id/business", (req, res, next) => {
        try{
            if(req.authentication.category_id == 3 || req.authentication.category_id == 2 ||req.authentication.category_id == 4 ){
                // Do nothing
            } else{
                var newError = new Error(null, null, "You dont have the authorization to get business by user id!", 401 /* Unauthorized */);
                next(newError);
            }
            /* Authentication end */
            
            businessDao.getBusinessByUserId(req.params.id, (status, data) => {
                if(status == 200) {
                    res.status(status);
                    res.json(data)
                } else{
                    var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
                    next(newError);
                }
            });
        }catch(exception){
            var newError = new Error("Caught exception when getting business by user id, exception: " + exception, 500, null, null);
            next(newError);      
        }
    });

    /**
     * Gets the points of the specified user.
     * Authentication required is to be either a municipality user or an admin user.
     */
    app.get("/user/points/:id", (req, res, next) => {
        try{
            /* Authentication begin */
            if(req.authentication.category_id == 3 || req.authentication.category_id == 2){
                // Do nothing
            } else{
                var newError = new Error(null, null, "You dont have the authorization to access user points!", 401 /* Unauthorized */);
                next(newError);
            }
            /* Authentication end */
            userDao.getPoints(req.params.id, (status, data) => {
                if (status == 200) {
                    res.status(status);
                    res.json(data)
                } else {
                    var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
                    next(newError);
                }
            });
        }catch (exception){
            var newError = new Error("Caught exception when getting a users points. Exception: " + exception, 500, null, null);
            next(newError);
        }
    });

    /**
     * Penalizes the specified user with a point.
     * Authentication required is to be either a municipality user or an admin user.
     */
    app.post("/user/points/:id", (req, res, next) => {
        try{
            /* Authentication begin */
            if(req.authentication.category_id == 3 || req.authentication.category_id == 2){
                // Do nothing
            } else{
                var newError = new Error(null, null, "You dont have the authorization to add a point to a user!", 401 /* Unauthorized */);
                next(newError);
            }
            /* Authentication end */
            userDao.addPoint(req.params.id, (status, data) => {
                if (status == 200) {
                    res.status(status);
                    res.json(data)
                } else {
                    var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
                    next(newError);
                }
            })
        } catch (exception){
            var newError = new Error("Caught exception when adding a point. Exception: " + exception, 500, null, null);
            next(newError);
        }
    })  
};