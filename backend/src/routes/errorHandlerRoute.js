import '../log'

/**
 * A last middleware that handles errors.
 * Prints error to server console and responds to user based on error supplied
 * by routes before it.
 */

var errorHandler =  function (error, req, res, next) {
    var errorMessage = error.getErrorMessage();
    var statusCode = error.getStatusCode();
    var errorMessageForResponse = error.getErrorMessageForResponse() || "Internal server error";
    var statusCodeForResponse = error.getStatusCodeForResponse() || 500;

    if(statusCode != null && errorMessage != null){
        console.log("Got error... \n\tStatuscode: " + statusCode + "\n\tError message: " + errorMessage);
    }
    res.status(statusCodeForResponse).send(errorMessageForResponse);
}

module.exports = {
    "route" : function (app) { app.use(errorHandler); },
    "method" : errorHandler
};