import Error from '../error';
import '../log';

module.exports = function(app, businessDao, userDao) {

	/**
	 * Gets information about companies and their associated users from specified municipality.
	 * Authentication required is to be either a municipality user from the specified municipality
	 * or an admin user.
	 */
	app.get('/business/users/municipality/:id', (req, res) => {
		try{
			/* Authentication begin */
			if ((req.authentication.category_id == 3 && req.authentication.municipality_id == req.params.id) || 
				req.authentication.category_id == 2) {
				// Do nothing
			} else {
				var newError = new Error(null, null, 'You dont have the authorization to access get companies and their users!', 401 /* Unauthorized */);
				next(newError);
			}
			/* Authentication end */

			businessDao.getCompaniesJoinCompanyUsers(req.params.id, (status, data) => {
				res.status(status);
				res.json(data);
			});
		} catch(exception){
			var newError = new Error("Caught exception when accessing companies and their users in a municipality, exception: " + exception, 500, null, null);
			next(newError);    
		} 
	});

	/**
	 * Gets information about companies from the specified municipality.
	 */
	app.get('/business/municipality/:id', (req, res) => {
		try{
			/* Authentication begin */
			if ((req.authentication.category_id == 3 && req.authentication.municipality_id == req.params.id) || 
				req.authentication.category_id == 2) {
				// Do nothing
			} else {
				var newError = new Error(null, null, 'You dont have the authorization to get companies by municipality!', 401 /* Unauthorized */);
				next(newError);
			}
			/* Authentication end */
			businessDao.getCompaniesByMunicipality(req.params.id, (status, data) => {
				res.status(status);
				res.json(data);
			});
		} catch(exception){
			var newError = new Error("Caught exception when accessing companies by municipality, exception: " + exception, 500, null, null);
			next(newError);    
		}
	});

	/**
	 * Associates a company user with a company.
	 * Authentication required is to be either a municipality user or an admin user.
	 */
	app.post('/business_user', (req, res, next) => {
		try{
			/* Authentication begin */
			if ((req.authentication.category_id == 3) || req.authentication.category_id == 2) {
				// Do nothing
			} else {
				var newError = new Error(null, null, 'You dont have the authorization to associate a user with a company!', 401 /* Unauthorized */);
				next(newError);
			}
			/* Authentication end */
			var email = req.fields.email;
			userDao.getId(email, (status, rows) => {
				if (status === 200 && rows[0] !== null) {
					var userId = rows[0].id;
					businessDao.getBusinessIdByName(req.fields.company_name, (status, rows) => {
						if (status === 200 && rows[0] !== null) {
							var businessId = rows[0].id;
							businessDao.createCompanyUserAccount(businessId, userId, (status, rows) => {
								if (status === 200) {
									res.status(201);
									res.json(rows);
								} else {
									var newError = new Error('SQL query failed, SQL error: ' + status, 500, null, null);
									next(newError);
								}
							});
						} else if (status === 200 && rows[0] == null) {
							var newError = new Error(null, null, "Coulden't find company!", 401 /* Bad request */);
							next(newError);
						} else {
							var newError = new Error('SQL query failed, SQL error: ' + status, 500, null, null);
							next(newError);
						}
					});
				} else if (status === 200 && rows[0] == null) {
					var newError = new Error(null, null, "Coulden't find user!", 401 /* Bad request */);
					next(newError);
				} else {
					var newError = new Error('SQL query failed, SQL error: ' + status, 500, null, null);
					next(newError);
				}
			});
		} catch(exception){
			var newError = new Error("Caught exception when associating user with company, exception: " + exception, 500, null, null);
			next(newError);    
		}
	});

	/**
     * Disables a business user by updating the company_user_account tables-
     * and the user_account tables isEnabled column.
	 * Authentication required is to be either a municipality user or an admin user.
     */
	app.delete('/business_user', (req, res, next) => {
		try {
			/* Authentication begin */
			if ((req.authentication.category_id == 3) || req.authentication.category_id == 2) {
				// Do nothing
			} else {
				var newError = new Error(null, null, 'You dont have the authorization to remove association a user with a company!', 401 /* Unauthorized */);
				next(newError);
			}
			/* Authentication end */
			businessDao.disableCompany(req.fields.companyid, (status, rows) => {
				if (status === 200) {
					try {
						userDao.disableUserAccount(req.fields.userid, (status, rows) => {
							if (status === 200) {
								res.status(200);
								res.json(rows);
							} else {
								var newError = new Error('SQL query failed, SQL error: ' + status, 500, null, null);
								next(newError);
							}
						});
					} catch (exception) {
						var newError = new Error('SQL query failed, SQL error: ' + status, 500, null, null);
						next(newError);
					}
				} else {
					var newError = new Error('SQL query failed, SQL error: ' + status, 500, null, null);
					next(newError);
				}
			});
		} catch (exception) {
			var newError = new Error('Caught exception when removing association between a user and a company, exception: ' + exception, 500, null, null);
			next(newError);
		}
	});

	/**
	 * Creates a business.
	 * Authentication required is to be either a municipality user or an admin user.
	 */
	app.post('/business', (req, res, next) => {
		try{
			/* Authentication begin */
			if ((req.authentication.category_id == 3) || 
				req.authentication.category_id == 2) {
				// Do nothing
			} else {
				var newError = new Error(null, null, 'You dont have the authorization to create a business!', 401 /* Unauthorized */);
				next(newError);
			}
			/* Authentication end */
			let companyData = {
				name: req.fields.name,
				description: req.fields.description
			};
			businessDao.createCompany(companyData, (status, rows) => {
				if (status === 200 && rows[0] !== null) {
					businessDao.getBusinessIdByName(req.fields.name, (status, rows) => {
						if (status === 200 && rows[0] !== null) {
							var id = rows[0].id;
							businessDao.createCompanyMunicipality(
								id,
								req.authentication.municipality_id,
								(status, rows) => {
									if (status === 200) {
										res.status(status);
										res.json(rows);
									} else {
										var newError = new Error('SQL query failed, SQL error: ' + status, 500, null, null);
										next(newError);
									}
								}
							);
						} else {
							var newError = new Error('SQL query failed, SQL error: ' + status, 500, null, null);
							next(newError);
						}
					});
				} else {
					var newError = new Error('SQL query failed, SQL error: ' + status, 500, null, null);
					next(newError);
				}
			});
		} catch(exception){
			var newError = new Error("Caught exception when creating company, exception: " + exception, 500, null, null);
			next(newError);    
		}
	});
};
