import Error from '../error';
import '../log'

module.exports = function (app, municipalityDao) {
    /**
     * Get information about every municipality.
     */
    app.get("/municipalities", (req, res) => {
        try{
            municipalityDao.getMunicipalities((status, data) => {
                if(status == 200) {
                    res.status(status);
                    res.json(data)
                } else{
                    var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
                    next(newError);
                }
            })
        } catch(exception){
            var newError = new Error("Caught exception when getting municipalities. Exception: " + exception, 500, null, null);
            next(newError);
        }
    });

    /**
     * Gets information about specified municipality.
     */
    app.get("/municipalities/:id", (req, res) => {
        try{
            municipalityDao.getMunicipality(req.params.id, (status, data) => {
                if(status == 200) {
                    res.status(status);
                    res.json(data)
                } else{
                    var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
                    next(newError);
                }
            })
        } catch(exception){
            var newError = new Error("Caught exception when getting municipality. Exception: " + exception, 500, null, null);
            next(newError);
        }
    });

    /**
     * Gets information about specified municipality with zipcode.
     */
    app.get("/municipalities/name/:zipcode", (req,res) =>{
        try{
            municipalityDao.getMunicipalitiesWithZipCode(req.params.zipcode, (status,data) =>{
                res.status(status);
                res.json(data);
            })
        } catch(exception){
            var newError = new Error("Caught exception when getting municipality name with zipcode. Exception: " + exception, 500, null, null);
            next(newError);
        }  
    });
};