module.exports = function (app, pool, statisticsDao) {
	/**
	 * Gets communal and national statistics.
	 * Authentication required is to be either a municipality user or an admin user.
	 */
	app.get("/statistics/", (req, res, next) => {
		try{
			/* Authentication begin */
			if ((req.authentication.category_id == 3) || 
				req.authentication.category_id == 2) {
				// Do nothing
			} else {
				var newError = new Error(null, null, 'You dont have the authorization to get statistics!', 401 /* Unauthorized */);
				next(newError);
			}
			/* Authentication end */

			console.log("Got statistics request");
			let promise1 = new Promise((resolve, reject) => {
				statisticsDao.getReport(req.query, (status, rows) => {
					let result = {status: status, rows: rows};
					resolve(result);
				});
			});

			let promise2 = new Promise((resolve, reject) => {
				statisticsDao.getNumberOfIssuesInCat(req.query, (status, rows) => {
					let result = {status: status, rows: rows};
					resolve(result);
				});
			});

			let promise3 = new Promise((resolve, reject) => {
				statisticsDao.getNumberOfIssuesMunicipality(req.query, (status, rows) => {
					let result = {status: status, rows: rows};
					resolve(result);
				});
			});	

			let promise4 = new Promise((resolve, reject) => {
				statisticsDao.getNumberOfIssuesCompany(req.query, (status, rows) => {
					let result = {status: status, rows: rows};
					resolve(result);
				});
			});

			let promise5 = new Promise((resolve, reject) => {
				statisticsDao.getAvgTimeInCat(req.query, (status, rows) => {
					let result = {status: status, rows: rows};
					resolve(result);
				});
			});

			let promise6 = new Promise((resolve, reject) => {
				statisticsDao.getAvgTimeIssue(req.query, (status, rows) => {
					let result = {status: status, rows: rows};
					resolve(result);
				});
			});

			let promise7 = new Promise((resolve, reject) => {
				statisticsDao.getActiveUsers(req.query, (status, rows) => {
					let result = {status: status, rows: rows};
					resolve(result);
				});
			});

			Promise.all([promise1, promise2, promise3, promise4, promise5, promise6, promise7])
			.then((result) => {
				//console.log(result);

				//check if any of the queries failed and send error to errorhandler if any of them failed
				result.forEach((queryResult) => {
					if(queryResult.status !== 200){
						var newError = new Error("One or more statistics SQL queries failed: " + queryResult.rows, queryResult.status, null, null);
						next(newError);
					}
				});

				let data = {
					getReport: result[0].rows,
					getNumberOfIssuesInCat: result[1].rows,
					getNumberOfIssuesMunicipality: result[2].rows,
					getNumberOfIssuesCompany: result[3].rows,
					getAvgTimeInCat: result[4].rows,
					getAvgTimeIssue: result[5].rows,
					getActiveUsers: result[6].rows,
				}

				res.status(200);
				res.json(data);
			});
		}catch(exception){
			var newError = new Error("Caught exception when getting statistics, exception: " + exception, 500, null, null);
			next(newError);      
		}
	});
}