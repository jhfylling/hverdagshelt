import Error from '../error';
import sendEmail from '../emailService';
import config from '../../config/index'
import '../log'

module.exports = function(app, issueDao, userDao, businessDao) {
	const fs = require('fs-extra');
	const path = require('path');

	/**
	 * This endpoint recieves all formdata from an issue and does three things:
	 * 1. Insert formdata as new issue in issue table
	 * 2. Recieve uploaded file and place it correctly
	 * 3. Send email to user about posted issue
	 */
	app.post('/issues', (req, res, next) => {
		/* Authentication begin */
		// Ingen autentisering nødvendig
		/* Authentication end */
		try{
			
			let inputData = {
				description: req.fields.description,
				longitude: req.fields.longitude,
				latitude: req.fields.latitude,
				picture_filename: null,
				user_id: req.authentication.id,
				municipality_id: req.fields.municipality_id,
				issue_category_id: '1'
			};

			if (req.files.photo.size !== 0) {
				inputData.picture_filename = "notnull.jpg";
			}
			
			issueDao.createIssue(inputData, (status, data) => {
				if (status === 200) {
					if (req.files.photo.size !== 0) {
						inputData.picture_filename = data.insertId + ".jpg";
					}
					
					issueDao.setStatus({issue_id: data.insertId, status_id: 1}, (status, data) => {
						userDao.getUser(req.authentication.id, (status, data) => {
							try{
								//File upload
								if (req.files.photo.size === 0) {
									//No file uploaded
										//console.log('no file uploaded');
									inputData.picture_filename = null;
								} else {
									let tempPath = req.files.photo.path;		//Path of temporary file
									let fileName = inputData.picture_filename;	//picture_filname. Currently set to be primary key of issue
									let imagePath = "";

									try{
										imagePath = path.join(__dirname, config.imagePath);	//Create crossplatform imagepath. See backend/config/ for paths.
									}catch(exception){
										var newError = new Error('Caught exception joining filepathnames while uploading new issue, exception' + exception, 500, null, null);
										next(newError);
									}
									
									//Copy file from temp folder to final destination.
									fs.copy(tempPath, imagePath + fileName, (error) => {
										if (error) {
											console.error('Error moving file on server ', error);
										} else {
											console.log('Successfully moved file');
										}
									});
								}
								
								//Send mail to user
								console.log("Sending mail to user");
								sendEmail(req.authentication.email, 'issueCreationEmail', null).catch(console.error);

								//If we got to this point, respond to user with success!
								res.status(status);
								res.json(data);

							}catch(exception){
								var newError = new Error("Caught exception when creating issue, exception: " + exception, 500, null, null);
								next(newError);
							}
						});
					});
				}
			});
		}catch(exception){
			var newError = new Error("Caught exception when creating issue, exception: " + exception, 500, null, null);
            next(newError);      
		}
	});

	/**
	 * Disables specified issue.
     * Authentication required is to be either a municipality user or an admin user.
	 */
    app.delete("/issues/:id", (req,res, next) => {
		try{
			/* Authentication begin */
			if(req.authentication.category_id == 3 || req.authentication.category_id == 2){
				// Do nothing
			} else{
				var newError = new Error(null, null, "You dont have the authorization to disable an issue!", 401 /* Unauthorized */);
				next(newError);
			}
			/* Authentication end */
        	let email = null;
    		issueDao.getIssue(req.params.id, (status, data) => {
    			if(status == 200){
    				let user_id = data.user_id;
    				userDao.getUser(user_id, (status, data) => {
    					if(status == 200){
							email = data.email;
        	                issueDao.disableIssue(req.params.id,(status,data) => {
								if(status == 200){
									if(email != null){
        	                            sendEmail(email, 'issueDeletedEmail', null).catch(error => console.log(error));
        	                        }
        	                        res.status(status);
        	                        res.json(data);
        	                    } else{
        	                        var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
        	                        next(newError);
        	                    }
        	                });
						}
					})
				}
			});
		}catch(exception){
			var newError = new Error("Caught exception when deleting issue, exception: " + exception, 500, null, null);
			next(newError);      
		}
    });

	/**
	 * Updates progress on an issue.
     * Authentication required is to be either a municipality user, an admin user or a company user.
	 */
    app.post("/issues/progress/:id", (req,res, next) => {
		try{
			/* Authentication begin */
			if(req.authentication.category_id == 3 || req.authentication.category_id == 2 || req.authentication.category_id == 4){
				// Do nothing
			} else{
				var newError = new Error(null, null, "You dont have the authorization to delete an issue!", 401 /* Unauthorized */);
				next(newError);
			}
			/* Authentication end */
        	issueDao.createIssueUpdate(req.params.id, req.fields,(status,data) => {
        	    if(status == 200){
        	    	res.status(status);
					res.json(data);
				} else{
					var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
        	        next(newError);
				}
			});
		}catch(exception){
			var newError = new Error("Caught exception when creating issue progess, exception: " + exception, 500, null, null);
            next(newError);      
		}
    });

	/**
	 * Creates an issue status.
     * Authentication required is to be either a municipality user or an admin user.
	 */
    app.post("/issues/statuses", (req,res, next) => {
		try{
			/* Authentication begin */
			if(req.authentication.category_id == 3 || req.authentication.category_id == 2){
				// Do nothing
			} else{
				var newError = new Error(null, null, "You dont have the authorization to create an issue status!", 401 /* Unauthorized */);
				next(newError);
			}
			/* Authentication end */
        	issueDao.setStatus(req.fields, (status,data) => {
        	    if(status == 200){
        	    	//Send mail to user that issue is closed
					if(req.fields.status_id == 4){

        	            let issue_id = req.fields.issue_id;
						issueDao.getIssue(issue_id, (status, data) => {
							if(status == 200){
        	                    let user_id = data.userId;
								userDao.getUser(user_id, (status, data) => {
									if(status == 200){
        	                            let email = data.email;
										sendEmail(email, 'issueFinshedEmail', null).catch(error => console.log(error));
									}else{
        	                        	var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
        	                        	next(newError);
        	                    	}
								});
							}else{
        	                    var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
        	                    next(newError);
							}
						})
					}

        	        res.status(status);
        	        res.json(data);
				} else{
					var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
        	        next(newError);
				}
			});
		}catch(exception){
			var newError = new Error("Caught exception when creating setting issue status, exception: " + exception, 500, null, null);
			next(newError);      
		}
    });

	/**
	 * Gets every status type.
	 */
    app.get('/statustypes', (req, res, next) => {
		try{
        	issueDao.getStatuses((status, data) => {
				if (status == 200) {
					res.status(status);
					res.json(data);
				} else {
					var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
					next(newError);
				}
			});
		}catch(exception){
			var newError = new Error("Caught exception when getting statustypes, exception: " + exception, 500, null, null);
			next(newError);      
		}
    });

	// Span is a span of categories, or an array of issue_category id's.
	/**
	* Gets every issue in a municipality in the categories specified in span (An array of issue categories).
	*/
	app.get('/issues', (req, res) => {
		try{
			issueDao.getIssues(req.query.municipality_id, req.query.span, (status, data) => {
				if(status == 200){
        	    	res.status(status);
					res.json(data);
				} else{
					console.log("NO");
					var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
        	        next(newError);
				}
			});
		}catch(exception){
			var newError = new Error("Caught exception when getting issues, exception: " + exception, 500, null, null);
			next(newError);      
		}
	});

	/**
	 * Gets information about specified issue.
	 */
	app.get('/issues/:id', (req, res, next) => {
		try{
			issueDao.getIssue(req.params.id, (status, data) => {
				if(status == 200){
        	    	res.status(status);
					res.json(data);
				} else{
					var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
        	        next(newError);
				}
			});
		}catch(exception){
			var newError = new Error("Caught exception when getting issue, exception: " + exception, 500, null, null);
            next(newError);      
		}
	});

	/**
	 * Gets the status of specified issue.
	 */
	app.get('/issues/:id/status', (req, res, next) => {
		try{
			issueDao.getStatus(req.params.id, (status, data) => {
				if(status == 200){
        	    	res.status(status);
					res.json(data);
				} else{
					var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
        	        next(newError);
				}
			});
		}catch(exception){
			var newError = new Error("Caught exception when getting issue status, exception: " + exception, 500, null, null);
			next(newError);      
		}
	});

	/**
	 * Gets the progress of specified issue.
	 */
    app.get('/issues/:id/progress', (req, res, next) => {
		try{
        	issueDao.getProgressUpdates(req.params.id, (status, data) => {
        	    if(status == 200){
        	    	res.status(status);
					res.json(data);
				} else{
					var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
        	        next(newError);
				}
			});
		}catch(exception){
			var newError = new Error("Caught exception when getting issue progress, exception: " + exception, 500, null, null);
			next(newError);      
		}
    });

	/**
	 * Updates/edits a specified issue.
	 * Authentication required is to be either a municipality user or an admin user.
	 */
    app.put('/issues/:id', (req, res, next) => {
		try{
			/* Authentication begin */
			if(req.authentication.category_id == 3 || req.authentication.category_id == 2){
				// Do nothing
			} else{
				var newError = new Error(null, null, "You dont have the authorization to update an issue!", 401 /* Unauthorized */);
				next(newError);
			}
			/* Authentication end */
    		issueDao.updateIssue(req.params.id, req.fields, (status, data) => {
    			if(status == 200){
        	    	res.status(status);
					res.json(data);
				} else{
					var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
        	        next(newError);
				}
			});
		}catch(exception){
			var newError = new Error("Caught exception when editing issue, exception: " + exception, 500, null, null);
			next(newError);      
		}
	});

	/**
	 * Gets information about every issue made by the specified user.
	 * Authentication required is to be either a municipality user, admin user, 
	 * or to be the user that created the issue.
	 */
    app.get('/issues/user/:email', (req, res, next) => {
		try{
			/* Authentication begin */
			if(req.authentication.category_id == 3 || 
				req.authentication.category_id == 2 || 
				req.authentication.category_id == 4 ||
				req.authentication.email == req.params.email){
				// Do nothing
			} else{
				var newError = new Error(null, null, "You dont have the authorization to get this users issues!", 401 /* Unauthorized */);
				next(newError);
			}
			/* Authentication end */

    		issueDao.getIssuesByUser(req.params.email, (status, data) => {
    			if(status == 200){
        	    	res.status(status);
					res.json(data);
				} else{
					var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
        	        next(newError);
				}
			});
		}catch(exception){
			var newError = new Error("Caught exception when getting issues of user, exception: " + exception, 500, null, null);
            next(newError);      
		}
	});

	/**
	 * Get all issue_categories from database
	 */
    app.get('/issuecategories', (req, res, next) => {
    	try{
    		issueDao.getIssueCategories((status, data) => {
        	    if(status == 200){
        	        res.status(status);
        	        res.json(data);
        	    }else{
        	        var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
        	        next(newError);
        	    }
			});
		}catch(exception){
			var newError = new Error("Caught exception when getting issue categories, exception: " + exception, 500, null, null);
			next(newError);      
		}
	});

	/**
	 * Insert new category in database
	 * Authentication required is to be either a municipality user or an admin user.
	 */
    app.post('/issuecategories', (req, res, next) => {
    	try{
			/* Authentication begin */
			if(req.authentication.category_id == 3 || req.authentication.category_id == 2){
				// Do nothing
			} else{
				var newError = new Error(null, null, "You dont have the authorization to create a user category!", 401 /* Unauthorized */);
				next(newError);
			}
			/* Authentication end */

    		issueDao.addIssueCategory(req.fields.description, (status, data) => {
    			if(status == 200){
    				res.status(status);
    				res.json(data);
				}else{
        	        var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
        	        next(newError);
				}
			});
		}catch(exception){
			var newError = new Error("Caught exception when creating issue category, exception: " + exception, 500, null, null);
			next(newError);      
		}
	});

	/**
	 * Set issue_category to national or communal.
	 * Authentication required is to be either a municipality user or an admin user.
	 */
	app.put('/issuecategories/:id', (req, res, next) => {
		try{
			/* Authentication begin */
			if(req.authentication.category_id == 3 || req.authentication.category_id == 2){
				// Do nothing
			} else{
				var newError = new Error(null, null, "You dont have the authorization to update issue category!", 401 /* Unauthorized */);
				next(newError);
			}
			/* Authentication end */

    		issueDao.updateIssueCategoryNational(req.params.id, req.fields.national, (status, data) => {
    			if(status == 200){
    				res.status(status);
    				res.json(data);
				}else{
        	        var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
        	        next(newError);
				}
			});
		}catch(exception){
			var newError = new Error("Caught exception when updating issue category, exception: " + exception, 500, null, null);
            next(newError);      
		}
	});


	/**
	 * Delete specified category.
	 * Updates all issues with these categories to new specified category.
	 * Authentication required is to be an admin user.
	 */
	app.delete('/issuecategories', (req, res, next) => {
		try{
			/* Authentication begin */
			if(req.authentication.category_id == 2){
				// Do nothing
			} else{
				var newError = new Error(null, null, "You dont have the authorization to delete an issue category!", 401 /* Unauthorized */);
				next(newError);
			}
			/* Authentication end */
			issueDao.updateIssueCategories(req.fields.newCategory, req.fields.categories, (status, data) => {
				if(status === 200){
					issueDao.deleteIssueCategories(req.fields.categories, (status, data) => {
						if(status === 200){
							res.status(status);
							res.json(data);
						}
						else{
							var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
							next(newError);
						}
					});
				}
				else{
					var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
        	        next(newError);
				}
			});
		}catch(exception){
			var newError = new Error("Caught exception when deleting issue category, exception: " + exception, 500, null, null);
			next(newError);      
		}
	});

	/**
	 * Gets the issues assigned to the specified business.
	 * Authentication required is to be either a municipality user, an admin user or
	 * a business user assigned to the specified business.
	 */
	app.get('/issues/business/:id', (req, res, next) => {
		try{
			/* Authentication begin */
			if(req.authentication.category_id == 3 || 
				req.authentication.category_id == 2 ||
				req.authentication.business_id == req.params.id || req.authentication.category_id == 4){
				// Do nothing
			} else{
				var newError = new Error(null, null, "You dont have the authorization to update an issue!", 401 /* Unauthorized */);
				next(newError);
			}
			/* Authentication end */
        	issueDao.getIssuesByCompany(req.params.id, (status, rows) => {
        	    if(status == 200){
        	        res.status(status);
        	        res.json(rows);
        	    } else{
					var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
					next(newError);
        	    }
			});
		}catch(exception){
			var newError = new Error("Caught exception when getting issues of a business, exception: " + exception, 500, null, null);
			next(newError);      
		}
	});

	/**
	 * Assigns specified issue to specified company.
	 * Authentication required is to be either a municipality user or an admin user.
	 */
	app.post('/issues/:issue_id/issue_company/company/:company_id', (req, res) => {
		try{
			/* Authentication begin */
			if(req.authentication.category_id == 3 || req.authentication.category_id == 2){
				// Do nothing
			} else{
				var newError = new Error(null, null, "You dont have the authorization to associate an issue and a company!", 401 /* Unauthorized */);
				next(newError);
			}
			/* Authentication end */
        	businessDao.assignCompany(req.params.company_id, req.fields, (status, data) => {
        		if(status == 200){
        			//Mail to company user
        			userDao.getUserByCompanyId(req.params.company_id, (status, data) => {
        				if(status == 200){
        					let user_id = data.userId;
        					userDao.getUser(user_id, (status, data) => {
        						if(status == 200){
									if(config.env != "test"){
        								let email = data.email;
										sendEmail(email, 'companyAssignedEmail', null).catch(error => console.log(error));
									}
								}
							})
        	            }else{
        	                var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
        	                next(newError);
        	            }
        	        });
                    res.status(status);
                    res.json(data);
				}else{
        	        var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
        	        next(newError);
				}
			});

		}catch(exception){
			var newError = new Error("Caught exception when associating issue with company, exception: " + exception, 500, null, null);
			next(newError);      
		}
    });
};