/* External modules */
    var express = require("express");
    var mysql = require("mysql");
    var bodyParser = require("body-parser");
    var https = require("https");
    var fs = require("fs");
    var rateLimit = require("express-rate-limit");
    var keypair = require("keypair");
    var fileUpload = require("express");
    var formidable = require("express-formidable");
    var cookieParser = require("cookie-parser");

/* Interal modules */
    var config = require("../config/index");
    var Authentication = require("./authentication");
    var EventDao = require("./dao/eventDao");
    var IssueDao = require("./dao/issueDao");
    var MunicipalityDao = require("./dao/municipalityDao");
    var UserDao = require("./dao/userDao");
    var BusinessDao = require("./dao/businessDao");
    var StatisticsDao = require("./dao/statisticsDao");

/* Internal classes */
    import Error from './error';    
    import './log'

/* Initialization */
    /* Server and database */
    var app = express();
    var pool = mysql.createPool(config.poolOptions);

    /* DAO */
    var eventDao = new EventDao(pool);
    var issueDao = new IssueDao(pool);
    var municipalityDao = new MunicipalityDao(pool);
    var userDao = new UserDao(pool);
    var businessDao = new BusinessDao(pool);
    var statisticsDao = new StatisticsDao(pool);

    /* Authentication */
    var keypairBitSize = 2048;
    var keypair;
    if(config.JWT_keypair == null){
        try{
            fs.accessSync("./JWT_keypair/keypair.pem", fs.constants.R_OK)
            console.log("Read JWT_keypair from file.");
            keypair = JSON.parse(fs.readFileSync("./JWT_keypair/keypair.pem", "utf8"));
        } catch(exception){                                               
            console.log("JWT_keypair dident exist. Generating new keypair...");
            keypair = keypair(keypairBitSize);
            fs.writeFileSync("./JWT_keypair/keypair.pem", JSON.stringify(keypair));
        }
    } else{
        console.log("Using JWT_keypair from " + config.env + ".js as it was defined there.");
        keypair = config.JWT_keypair;
    }
    var authentication = new Authentication(userDao, keypair, businessDao);

/* Pre-route middlewares */
    /* Removes "X-Powered-By express" */
    app.use(function (req, res, next) {
        res.removeHeader("X-Powered-By");
        next();
      });

    const generalLimiter = rateLimit({
        windowMs: 1 * 60 * 1000,   // 1 minute
        max: (config.generalLimiter) || 100,                  // limit each IP to 100 requests per 1 minutes
        message: "Request limit exceeded, try again later."
    });
    app.use(generalLimiter); // Limits request rate to prevent spam
    app.use(formidable());
    app.use(cookieParser());

/* Routes */
    require("./routes/authenticationRoutes")(app, authentication, businessDao);      // Must be first of all routes!
    require("./routes/userRoutes.js")(app, userDao, authentication, businessDao);
    require("./routes/issueRoutes.js")(app, issueDao, userDao, businessDao);
    require("./routes/eventRoutes")(app, eventDao);
    require("./routes/municipalityRoutes")(app, municipalityDao);
    require("./routes/otherRoutes")(app, businessDao, userDao);
    require("./routes/statisticsRoutes")(app, pool, statisticsDao);
    require("./routes/errorHandlerRoute").route(app);                         // Must be the last of all routes!

/* Mail service */
    import sendEmail from './emailService';
    app.post("/email", (req, res, next) => {
        try{
            console.log("recived email call");
            sendEmail(req.fields.toEmail, req.fields.emailType).catch(console.error);
            res.status(201); // Created
            next();
        } catch(exception){
            var newError = new Error("Caught exception when sending email. Exception: " + exception, 500, null, null);
            next(newError);
        }
    });

/* Server */
if(config.env != "test"){
    var server = app.listen(config.port, 'localhost', function(){
        if(server != null){
          var host = server.address().address;
          var port = server.address().port;
          console.log("Server listening on http://%s:%s in %s environment.", host, port, config.env);
        }
        else{
          console.log("Server could not start");
        }
      });
}

/**
 * Used for testing purposes.
 */
module.exports = {
    "app" : app,
    "config" : config,
    "pool": pool
};