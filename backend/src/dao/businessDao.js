const Dao = require("./dao.js");
module.exports = class businessDao extends Dao {

    /**
     * Gets information about the specified company user,
     * and the company it's assigned to.
     */
    getBusinessByUserId(user_id, callback){
        super.query(
            "SELECT c.* FROM company c"
            + " LEFT JOIN company_user_account cua ON c.id = cua.company_id"
            + " WHERE cua.user_account_id = ?",
            [user_id],
            callback
        );
    }


    /**
     * Gets information about the specified company.
     */
    getBusinessIdByName(name,callback){
        super.query(
            "SELECT id FROM company c WHERE c.name = ?",
            [name],
            callback
        );
    }

    /**
     * Gets information about the company specified,
     * and the company users assigned to it.
     */
    getCompaniesJoinCompanyUsers(id, callback){
        super.query(
            "SELECT c.id as companyid, c.name, c.description, ua.email, ua.id as userid FROM company c JOIN company_municipality cm ON c.id = cm.company_id JOIN company_user_account uac ON uac.company_id = c.id JOIN user_account ua ON ua.id = uac.user_account_id WHERE ua.isEnabled = true and c.isEnabled = true and cm.municipality_id = ?",
                [id],
            callback
        );
    }

    /**
     * Gets information about every company assigned
     * to the specified municipality.
     */
    getCompaniesByMunicipality(id, callback){
        super.query(
            "SELECT DISTINCT company.id, company_municipality.municipality_id, company.name, company.description FROM company JOIN company_municipality ON company.id = company_municipality.company_id WHERE company_municipality.municipality_id = ?",
            [id],
            callback
        );
    }

    /**
     * Assigns an issue to a company.
     */
    assignCompany(company_id, json, callback){
        var val = [json.issue_id, company_id];
        super.query(
            "INSERT INTO issue_company(issue_id, company_id, update_date)  VALUES (?,?, DEFAULT)",
            val,
            callback
        );
    }

    /**
     * Creates a company.
     */
    createCompany(json,callback){
        var values = [json.name,json.description];
        super.query(
            "INSERT INTO company (name,description) VALUES (?,?)",
            values,
            callback
        );
    }

    /**
     * Assigns a company to a municipality.
     */
    createCompanyMunicipality(company_id,municipality_id,callback){
        var values = [company_id, municipality_id];
        super.query(
            "INSERT INTO company_municipality (company_id,municipality_id) VALUES (?,?)",
            values,
            callback
        );
    }

    /**
     * Assigns a user to a company. 
     */
    createCompanyUserAccount(company_id,user_account_id, callback){
        super.query(
            "INSERT INTO company_user_account (company_id,user_account_id) VALUES (?,?)",
            [company_id, user_account_id],
            callback
        );
    }

    /**
     * Disables a company.
     */
    disableCompany(companyid, callback){
        super.query(
            "UPDATE company SET isEnabled = false WHERE id = ?",
            [companyid],
            callback
        );
    }
};