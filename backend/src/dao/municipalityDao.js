const Dao = require("./dao");
module.exports = class municipalityDao extends Dao {

    /**
     * Gets information about every municipality.
     */
    getMunicipalities(callback){
        super.query(
            "SELECT id,municipality_name FROM municipality ORDER BY municipality_name ASC",
            [],
            callback
        );
    }

    /**
     * Gets information about specified municipality.
     */
    getMunicipality(id,callback){
        super.query(
            "SELECT id,municipality_name FROM municipality WHERE municipality.id = ?",
            [id],
            callback
        );
    }

    /**
     * Gets information about specified municipality with zipcode.
     */
    getMunicipalitiesWithZipCode(zipcode, callback){
        super.query(
            "SELECT * from postnummer join municipality on Kommunenavn = municipality_name WHERE Postnummer = ? LIMIT 1;",
            [zipcode],
            callback
        );
    }
};