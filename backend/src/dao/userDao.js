const Dao = require("./dao.js");

module.exports = class userDao extends Dao {

    /**
     * Gets id of specified user.
     */
    getId(email, callback){
        super.query("SELECT id FROM user_account WHERE email = ?",
            [email],
            callback);
    }

    /**
     * Gets hashed password of specified user.
     */
    getHash(id, callback) {
        super.query("SELECT hash FROM user_account WHERE id = ?",
            [id],
            callback);
    }

    /**
     * Creates a new user.
     */
    createUser(json, callback) {
        var val = [json.email, json.hash, json.municipality_id, json.category_id];
        super.query("INSERT INTO user_account (email, hash, municipality_id, category_id) VALUES (?, ?, ?, ?)",
            val,
            callback
        );
    }

    /**
     * Creates userdetails of specified user (Optional information).
     */
    createUserDetails(id, json, callback) {
        var val = [id, json.firstname, json.lastname, json.phone_number];
        super.query(
            "INSERT INTO user_details (id, firstname, lastname, phone_number) VALUES(?, ?, ?, ?)",
            val,
            callback
        );
    }

    /**
     * Updates/changes specified users email address.
     */
    updateUserEmail(id, email, callback){
        super.query(
            "UPDATE user_account SET email = ? WHERE id = ?",
            [email, id],
            callback
        );
    }

    /**
     * Updates/changes a users details.
     */
    updateUserDetails(id, json, callback) {
        var val = [json.firstname, json.lastname, json.phone_number, id];
        console.log(val);
        super.query(
            "UPDATE user_details SET firstname = ?, lastname = ?, phone_number = ? WHERE id = ?",
            val,
            callback
        );
    }

    /**
     * Gets information about specified user.
     */
    getUser(id, callback){
        super.query(
            "SELECT user_account.id, user_account.email, user_account.municipality_id, user_account.category_id, user_category.category_name, user_details.firstname, user_details.lastname, user_details.phone_number, " +
            "municipality.municipality_name FROM user_category JOIN user_account ON user_category.id = user_account.category_id " +
            "JOIN municipality ON user_account.municipality_id = municipality.id LEFT JOIN user_details " +
            "ON user_account.id = user_details.id WHERE user_account.id = ?",
            [id],
            callback
        );
    };

    /**
     * Gets userdetails of specified user.
     */
    getUserDetails(id, callback){
        super.query(
            "SELECT * FROM user_details WHERE id = ?",
            [id],
            callback
        );
    };

    /**
     * Changes the hashed password of specified user.
     */
    updatePassword(id, hashed, callback){
        super.query(
            "UPDATE user_account SET hash = ? WHERE id = ?",
            [hashed, id],
            callback
        );
    };


    /**
     * Disables the specified user.
     */
    disableUserAccount(user_account_id, callback){
        super.query(
            "UPDATE user_account SET isEnabled = false WHERE id = ?",
            [user_account_id],
            callback
        );
    }

    /**
     * Gets the points of a specified user (A type of reputation).
     */
    getPoints(id, callback){
        super.query(
            "SELECT COUNT(user_id) AS points FROM user_point WHERE (CURDATE() - date) <= 31 "+
            "AND user_id = ?",
            [id],
            callback
        );
    };

    /**
     * Adds a point to the specified user.
     */
    addPoint(id, callback){
        super.query(
            "INSERT INTO user_point (id, date, user_id) VALUES(DEFAULT, NOW(), ?)",
            [id],
            callback
        )
    }

    /**
     * Gets the association between a user and the company it's assigned to.
     */
    getUserByCompanyId(id, callback){
        super.query(
            "SELECT * FROM company_user_account WHERE company_id = ?",
            [id],
            callback
        );
    };
};