const Dao = require("./dao");
module.exports = class eventDao extends Dao {

    /**
     * Gets events from municipality.
     */
    getEvents(municipality_id,callback){
        super.query(
            "SELECT event.id, title, event.description, DATE_FORMAT(event_date, '%d.%m.%y %H:%i') as date," +
            " latitude, longitude, price, " +
            "event_homepage, event_category.description as category FROM event JOIN event_category " +
            "ON event_category.id = event_category_id WHERE event.municipality_id = ? AND isEnabled = TRUE",
            [municipality_id],
            callback
        );
    }

    /**
     * Gets a specified event.
     */
    getEvent(id,callback){
        super.query(
            "SELECT event.id, title, event.description, DATE_FORMAT(event_date, '%d.%m.%y %H:%i') as date," +
            "latitude, longitude, price, event_homepage, event_category.description as category FROM event JOIN event_category " +
            "ON event_category.id = event_category_id WHERE event.id = ? AND isEnabled = TRUE",
            [id],
            callback
        );
    }

    /**
     * Gets every enabled event from specified category.
     */
    getEventInCategory(event_category_id,callback){
        super.query(
            "SELECT event.id, title, event.description, DATE_FORMAT(event_date, '%d.%m.%y %H:%i') as date, latitude, " +
            "longitude, price, event_homepage, event_category.description FROM event JOIN event_category " +
            "ON event_category.id = event_category_id " +
            "WHERE event.event_category_id = ? AND isEnabled = TRUE",
            [event_category_id],
            callback
        );
    }

    /**
     * Creates an event.
     */
    createEvent(json,callback){
        var values = [json.title,json.description,json.longitude,json.latitude,json.price,json.event_homepage,json.event_category_id,json.municipality_id];
        super.query(
            "INSERT INTO event (title,description,longitude,latitude,price,event_homepage,event_category_id,municipality_id) VALUES (?,?,?,?,?,?,?,?)",
            values,
            callback
        );
    }

    /**
     * Updates/edits details of an event.
     */
    updateEvent(id,json,callback){
        var values = [json.title,json.description,json.longitude,json.latitude,json.price,json.event_homepage,json.event_category_id,json.municipality_id,id];
        super.query(
            "UPDATE event SET title = ?, description = ?, longitude = ?, latitude = ?, price = ?, event_homepage = ?, event_category_id = ?, municipality_id = ? WHERE id = ?",
            values,
            callback
        );
    }

    /**
     * Disables the specified event.
     */
    disableEvent(id,callback){
        super.query(
            "UPDATE event SET isEnabled = FALSE WHERE id = ?",
            [id],
            callback
        );
    }

    /**
     * Gets every event category.
     */
    getEventCategories(callback){
        super.query(
            "SELECT * FROM event_category",
            [],
            callback
        );
    }
};