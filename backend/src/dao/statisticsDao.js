/**
 * A dao for statistical data
 */
const Dao = require("./dao");
module.exports = class statisticsDao extends Dao {
/**
 * getReport() method calls for a list of issue within a specified range of date. 
 * its optional to specifie municipality_id.
 * @param {*} json a param for the req.body in statisticsRoutes
 * @param {*} callback a callback with status and data
 */
getReport(json, callback) {
  var val = [json.municipality_id, json.start_date, json.end_date];
  super.query(
    "SELECT i.id, i.title, i.description, DATE_FORMAT(issue_date, '%d.%m.%y') as date, ic.description as categoryname, m.municipality_name, i.latitude, i.longitude FROM issue i JOIN issue_category ic ON i.issue_category_id = ic.id JOIN municipality m ON i.municipality_id = m.id where i.municipality_id like '%' ? AND cast(i.issue_date as date) between FROM_UNIXTIME(?) AND FROM_UNIXTIME(?) ORDER BY i.issue_date ASC", 
    val, 
    callback
    );
  }
  
getNumberOfIssuesInCat(json, callback){
  var val = [json.municipality_id, json.start_date, json.end_date];
  super.query("SELECT ic.description as 'name', count(i.id) as 'value' FROM issue i JOIN issue_category ic ON i.issue_category_id = ic.id WHERE i.municipality_id like '%' ? AND cast(i.issue_date as date) BETWEEN cast(FROM_UNIXTIME(?) as date) AND cast(FROM_UNIXTIME(?) as date) GROUP BY ic.description", val, callback);
}

getNumberOfIssuesMunicipality(json, callback) {
  var val = [json.municipality_id, json.start_date, json.end_date];
  super.query("SELECT m.municipality_name as 'name', count(i.id) as 'value' FROM issue i JOIN municipality m ON i.municipality_id = m.id WHERE i.municipality_id like '%' ? AND cast(i.issue_date as date) BETWEEN FROM_UNIXTIME(?) AND FROM_UNIXTIME(?) GROUP BY m.municipality_name ORDER BY value DESC", val, callback);
  
}

getNumberOfIssuesCompany(json, callback) {
  var val = [json.municipality_id, json.start_date, json.end_date];
  super.query("SELECT c.name as 'name', count(i.id) as 'value' FROM issue i JOIN issue_company ic ON i.id = ic.issue_id JOIN company c ON ic.company_id = c.id WHERE i.municipality_id like '%' ? AND cast(i.issue_date as date) BETWEEN FROM_UNIXTIME(?) AND FROM_UNIXTIME(?) GROUP BY c.name ORDER BY value DESC", val, callback);

}

getAvgTimeInCat(json, callback){
  var val = [json.municipality_id, json.start_date, json.end_date];
  super.query("SELECT ic.description as 'name', avg(cast(si.update_date as datetime) - cast(i.issue_date as datetime)) as 'value' FROM issue i JOIN status_issue si ON i.id = si.issue_id JOIN issue_category ic ON i.issue_category_id = ic.id WHERE si.status_id = 4 AND i.municipality_id like '%' ? AND cast(i.issue_date as date) AND cast(si.update_date as date) BETWEEN FROM_UNIXTIME(?) AND FROM_UNIXTIME(?) GROUP BY ic.description", val, callback);

}

getAvgTimeIssue(json, callback) {
  var val = [json.municipality_id, json.start_date, json.end_date];
  super.query("SELECT avg(cast(si.update_date as datetime) - cast(i.issue_date as datetime)) as 'value' FROM issue i JOIN status_issue si ON i.id = si.issue_id WHERE si.status_id = 4 AND i.municipality_id like '%' ? AND cast(i.issue_date as date) AND cast(si.update_date as date) BETWEEN FROM_UNIXTIME(?) AND FROM_UNIXTIME(?)", val, callback);
}

getActiveUsers(json, callback){
  var val = [json.municipality_id, json.start_date, json.end_date];
  super.query("SELECT m.municipality_name as 'name', count(DISTINCT ua.id) as 'value' FROM user_account ua JOIN municipality m ON ua.municipality_id = m.id JOIN issue i ON ua.id = i.user_id WHERE m.id like '%' ? AND cast(i.issue_date as date) BETWEEN FROM_UNIXTIME(?) AND FROM_UNIXTIME(?) GROUP BY m.municipality_name", val, callback);
}
};