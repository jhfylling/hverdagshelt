const Dao = require("./dao");
module.exports = class issueDao extends Dao {

  /*  getIssues(municipality_id, callback){
        super.query(
            "SELECT issue.id, title, issue.description, DATE_FORMAT(issue_date, '%d.%m.%y') as date, longitude, latitude, " +
            "picture_filename, company.name, issue_category.description, user_account.id as userId, status.type as status, status.id as status_id, status_issue.update_date, " +
            "issue.municipality_id FROM issue LEFT JOIN issue_company ON issue.id = issue_company.issue_id LEFT JOIN company " +
            "ON issue_company.company_id = company.id LEFT JOIN ((  Select status_issue.issue_id, status_id, status_issue.update_date from status_issue join" +
            "((Select issue_id, Max(update_date) as update_date from status_issue group by issue_id) as ii) on status_issue.issue_id = ii.issue_id and "+
            "status_issue.update_date = ii.update_date) as status_issue) ON issue.id = status_issue.issue_id LEFT JOIN status ON " +
            "status_issue.status_id = status.id LEFT JOIN issue_category ON issue.issue_category_id = issue_category.id LEFT JOIN " +
            "user_account ON issue.user_id = user_account.id WHERE issue.municipality_id = ?", 
            [municipality_id],
            callback
        );
    }*/

    /**
     * Gets every issue in a municipality in the categories specified in span (An array of issue categories).
     */
    getIssues(municipality_id, span, callback){
        let questionMarks = [];
        let values = [];
        for(let i=0; i<span.length; i++){
            questionMarks.push('?');
            values.push(span[i]);
        }
        values.push(municipality_id);
        super.query(
            /*"SELECT i.id, i.title, i.description, DATE_FORMAT(i.issue_date, '%d.%m.%y') as date," +
            " i.longitude, i.latitude, i.picture_filename, c.name, issue_category.description," +
            " i.user_id as userId, s.type as status, si.update_date, i.municipality_id" +
            " FROM issue i" +
            " LEFT JOIN issue_company ic ON i.id = ic.issue_id" +
            " LEFT JOIN company c ON ic.company_id = c.id" +
            " LEFT JOIN issue_category ON i.issue_category_id = issue_category.id" +
            " LEFT JOIN status_issue si ON i.id = si.issue_id" +
            " LEFT JOIN status s ON si.status_id = s.id" +
            " WHERE (CONCAT(i.id, si.update_date) IN (SELECT CONCAT(issue_id, MAX(update_date)) as update_date FROM status_issue GROUP BY issue_id) OR si.update_date IS NULL)" +
            " AND (CONCAT(i.id, ic.update_date) IN (SELECT CONCAT(issue_id, MAX(update_date)) as update_date FROM issue_company GROUP BY issue_id) OR ic.update_date IS NULL)" +
            " AND i.municipality_id = ? AND i.isEnabled = 1;",*/
            "SELECT i.id, i.title, i.description, DATE_FORMAT(i.issue_date, '%d.%m.%y') as date,"+
            "i.longitude, i.latitude, i.picture_filename, c.name, issue_category.description,"+
            "i.user_id as userId, s.type as status, si.update_date, i.municipality_id, si.status_id as status_id FROM issue i " +
            "LEFT JOIN issue_company ic ON i.id = ic.issue_id " +
            "LEFT JOIN company c ON ic.company_id = c.id " +
            "LEFT JOIN issue_category ON i.issue_category_id = issue_category.id " +
            "JOIN (SELECT * FROM status_issue WHERE status_id in(" + questionMarks.join() +  ")) as si ON i.id = si.issue_id " +
            "LEFT JOIN status s ON si.status_id = s.id " +
            "WHERE (CONCAT(i.id, si.update_date) IN (SELECT CONCAT(issue_id, MAX(update_date)) as update_date FROM status_issue GROUP BY issue_id) OR si.update_date IS NULL) " +
            "AND (CONCAT(i.id, ic.update_date) IN (SELECT CONCAT(issue_id, MAX(update_date)) as update_date FROM issue_company GROUP BY issue_id) OR ic.update_date IS NULL) " +
            "AND i.municipality_id = ? AND i.isEnabled = 1 ORDER BY i.issue_date DESC;",
            values,
            callback
        );
    }

    //WHERE status_issue.update_date = (Select Max(update_date) from status_issue order by issue_id)

    /**
     * Gets information about specified issue.
     */
    getIssue(id,callback){
        super.query(
            /* "SELECT issue.id, title, issue.description, DATE_FORMAT(issue_date, '%d.%m.%y %H:%i') as date, longitude, latitude, " +
            "picture_filename,issue.issue_category_id, company.name, issue_category.description as category, user_account.id as user_id, status.type, status.id AS status_id, status_issue.update_date," +
            "issue.municipality_id FROM issue LEFT JOIN issue_company ON issue.id = issue_company.issue_id LEFT JOIN company " +
            "ON issue_company.company_id = company.id LEFT JOIN " +
            "(( Select status_issue.issue_id, status_id, status_issue.update_date from status_issue join" +
            "((Select issue_id, Max(update_date) as update_date from status_issue group by issue_id) as ii) on status_issue.issue_id = ii.issue_id and " +
            "status_issue.update_date = ii.update_date) as status_issue)" +
            "ON issue.id = status_issue.issue_id LEFT JOIN status ON " +
            "status_issue.status_id = status.id LEFT JOIN issue_category ON issue.issue_category_id = issue_category.id LEFT JOIN " +
            "user_account ON issue.user_id = user_account.id " +
            "WHERE issue.id = ? AND issue.isEnabled = 1", */
            "SELECT i.id, i.title, i.description, DATE_FORMAT(i.issue_date, '%d.%m.%y') as date," +
            " i.longitude, i.latitude, i.picture_filename, i.issue_category_id, DATE_FORMAT(i.estimated_time, '%d.%m.%Y') as estimated_time, c.name, issue_category.description as issue_status_description," +
            " i.user_id as userId, s.type as status, si.status_id, si.update_date, i.municipality_id" +
            " FROM issue i" +
            " LEFT JOIN issue_company ic ON i.id = ic.issue_id" +
            " LEFT JOIN company c ON ic.company_id = c.id" +
            " LEFT JOIN issue_category ON i.issue_category_id = issue_category.id" +
            " LEFT JOIN status_issue si ON i.id = si.issue_id" +
            " LEFT JOIN status s ON si.status_id = s.id" +
            " WHERE (CONCAT(i.id, si.update_date) IN (SELECT CONCAT(issue_id, MAX(update_date)) as update_date FROM status_issue GROUP BY issue_id) OR si.update_date IS NULL)" +
            " AND (CONCAT(i.id, ic.update_date) IN (SELECT CONCAT(issue_id, MAX(update_date)) as update_date FROM issue_company GROUP BY issue_id) OR ic.update_date IS NULL)" +
            " AND i.id = ? AND i.isEnabled = 1;",
            id,
            callback
        );
    }

    /**
     * Creates an issue.
     */
    createIssue(json, callback){
        var values = [json.title, json.description, json.longitude, json.latitude, json.picture_filename, json.issue_category_id, json.municipality_id, json.user_id];
        super.query(
            "INSERT INTO issue (id, title, description, issue_date, longitude, latitude, picture_filename, issue_category_id, municipality_id, user_id, isEnabled) VALUES(DEFAULT , ?, ?, NOW(), ?, ?, ?, ?, ?, ?, default)",
            values,
            callback
        );
    }

    /**
     * Gets every status.
     */
    getStatuses(callback){
        super.query("SELECT * FROM status",
            [],
            callback
        );
    }

    /**
     * Gets status of specified issue.
     */
    getStatus(id, callback){
        super.query(
            "SELECT status_id FROM status_issue JOIN issue ON status_issue.issue_id = issue.id WHERE issue.id = ?",
            [id],
            callback
        );
    }

    /**
     * Disables specified issue.
     */
    disableIssue(id, callback){
        super.query(
            "UPDATE issue SET isEnabled = false WHERE id = ?",
            [id],
            callback
        );
    }

    /**
     * Sets the status of specified issue.
     */
    setStatus(json, callback){
        let {status_id, issue_id} = json;
        super.query(
            "INSERT INTO status_issue (status_id, issue_id, update_date) VALUES (?, ?, NOW())",
            [status_id, issue_id],
            callback
        );
    }

    /**
     * Creates an issue update for the specified issue.
     */
    createIssueUpdate(id, json, callback){
        var val = [id, json.updatemessage, json.user_id];
        super.query(
            "INSERT INTO progress_update (issue_id, updatemessage, update_date, user_id) VALUES(?, ?, DEFAULT, ?)",
            val,
            callback
        );
    }

    /**
     * Gets information about every progress update for specified issue.
     */
    getProgressUpdates(id, callback){
        super.query(
            "SELECT updatemessage, DATE_FORMAT(update_date, '%d.%m.%y kl.%H:%i') as date FROM progress_update WHERE issue_id = ?",
            [id],
            callback
        );
    }

    /**
     * Updates information of an issue.
     */
    updateIssue(id, json, callback){
        var val = [json.title, json.issue_category_id, json.estimated_time, id];
        super.query("UPDATE issue SET title = ?, issue_category_id = ?, estimated_time = ? WHERE id = ?",
            val,
            callback
        );
    }

    /**
     * Selects issues by user. Currently uses email
     * TODO: use id as parameter instead of email
     */
    getIssuesByUser(email, callback){
        super.query(
            /* "SELECT issue.id, title, issue.description, DATE_FORMAT(issue_date, '%d.%m.%y') as date, longitude, latitude, " +
            "picture_filename, company.name, issue_category.description, user_account.id as userId, status.type as status, status_issue.update_date, " +
            "issue.municipality_id FROM issue LEFT JOIN issue_company ON issue.id = issue_company.issue_id LEFT JOIN company " +
            "ON issue_company.company_id = company.id LEFT JOIN ((  Select status_issue.issue_id, status_id, status_issue.update_date from status_issue join" +
            "((Select issue_id, Max(update_date) as update_date from status_issue group by issue_id) as ii) on status_issue.issue_id = ii.issue_id and "+
            "status_issue.update_date = ii.update_date) as status_issue) ON issue.id = status_issue.issue_id LEFT JOIN status ON " +
            "status_issue.status_id = status.id LEFT JOIN issue_category ON issue.issue_category_id = issue_category.id LEFT JOIN " +
            "user_account ON issue.user_id = user_account.id WHERE issue.user_id IN (SELECT id FROM user_account WHERE email = ?) AND issue.isEnabled = 1", 
            */
           "SELECT i.id, i.title, i.description, DATE_FORMAT(i.issue_date, '%d.%m.%y') as date," +
           " i.longitude, i.latitude, i.picture_filename, c.name, issue_category.description," +
           " i.user_id as userId, s.type as status, si.update_date, i.municipality_id" +
           " FROM issue i" +
           " LEFT JOIN issue_company ic ON i.id = ic.issue_id" +
           " LEFT JOIN company c ON ic.company_id = c.id" +
           " LEFT JOIN issue_category ON i.issue_category_id = issue_category.id" +
           " LEFT JOIN status_issue si ON i.id = si.issue_id" +
           " LEFT JOIN status s ON si.status_id = s.id" +
           " WHERE (CONCAT(i.id, si.update_date) IN (SELECT CONCAT(issue_id, MAX(update_date)) as update_date FROM status_issue GROUP BY issue_id) OR si.update_date IS NULL)" +
           " AND (CONCAT(i.id, ic.update_date) IN (SELECT CONCAT(issue_id, MAX(update_date)) as update_date FROM issue_company GROUP BY issue_id) OR ic.update_date IS NULL)" +
           " AND i.user_id IN (SELECT id FROM user_account WHERE email = ?) AND i.isEnabled = 1;",
            [email],
            callback
        );
    }

    /**
    * Gets information about every issue assigned to a company that are enabled (Issues that are ongoing).
     */
    getIssuesByCompany(company_id, callback){
        super.query(
              "SELECT i.id, i.title, i.description, DATE_FORMAT(i.issue_date, '%d.%m.%y') as date," 
            + " i.longitude, i.latitude, i.picture_filename, c.name, issue_category.description," 
            + " i.user_id as userId, s.type as status, si.update_date, i.municipality_id" 
            + " FROM issue i" 
            + " LEFT JOIN issue_company ic ON i.id = ic.issue_id" 
            + " LEFT JOIN company c ON ic.company_id = c.id"
            + " LEFT JOIN issue_category ON i.issue_category_id = issue_category.id"
            + " LEFT JOIN status_issue si ON i.id = si.issue_id"
            + " LEFT JOIN status s ON si.status_id = s.id"
            + " WHERE (CONCAT(i.id, si.update_date) IN (SELECT CONCAT(issue_id, MAX(update_date)) as update_date FROM status_issue GROUP BY issue_id) OR si.update_date IS NULL)"
            + " AND (CONCAT(i.id, ic.update_date) IN (SELECT CONCAT(issue_id, MAX(update_date)) as update_date FROM issue_company GROUP BY issue_id) OR ic.update_date IS NULL)"
            + " AND c.id = ? AND i.isEnabled = 1",
            [company_id],
            callback
        );
    }

    /**
     * Gets every issue category.
     */
    getIssueCategories(callback){
        super.query(
            "SELECT * FROM issue_category",
            [],
            callback
        );
    };

    /**
     * Adds an issue category.
     */
    addIssueCategory(category, callback){
        super.query(
            "INSERT INTO issue_category (id, description) VALUES(DEFAULT, ?)",
            [category],
            callback
        );
    };
    
    /**
     * This will update all issues with categories in categories[]
     * and set issue_category_id = newCategory.
     * 
     * This query needs to be built special because of sql's prepared statments lacking support for arrays.
     * This is still secure against SQL injections since nothing but ? is inserted in the query, and no data.
     * @author Johan
     */
    updateIssueCategories(newCategory, categories, callback){

        let questionMarks = [];
        let values = [];
        values.push(newCategory);
        for(let i=0; i<categories.length; i++){
            questionMarks.push('?');
            values.push(categories[i]);
        }

        super.query(
            "UPDATE issue SET issue_category_id = ? WHERE issue_category_id IN ("+ questionMarks.join() +");",
            values,
            callback
        );
    }

    /**
     * Deletes all categories with id in categories[]
     * 
     * This query needs to be built special because of sql's prepared statments lacking support for arrays.
     * This is still secure against SQL injections since nothing but a number of ? is inserted in the query, and no data.
     * @author Johan
     */
    deleteIssueCategories(categories, callback){

        let questionMarks = [];
        let values = [];
        for(let i=0; i<categories.length; i++){
            questionMarks.push('?');
            values.push(categories[i]);
        }

        super.query(
            "DELETE FROM issue_category WHERE id IN ("+ questionMarks.join() +");",
            values,
            callback
        );
    }

    /**
     * Sets issue to either national or municipal.
     */
    updateIssueCategoryNational(id, national, callback){
        super.query(
            "UPDATE issue_category SET national = ? WHERE id = ?",
            [national, id],
            callback
        );
    }
};

