var config = require("../config/index")

/**
 * Overwrites the console.log function to do 
 * nothing if log = false in the environment config.
 */
if(!config.log){
    console.log = function (){};
}