var config = require("../config/index")

/**
 * Error object.
 */
export default class Error{
    constructor(message, statusCode, messageForResponse, statusCodeForResponse){
        this.message = message;
        this.statusCode = statusCode;
        this.messageForResponse = messageForResponse;
        this.statusCodeForResponse = statusCodeForResponse;  
    }
}

Error.prototype.getErrorMessage = function(){
    return this.message;
}

Error.prototype.getStatusCode = function(){
    return this.statusCode;
}

Error.prototype.getErrorMessageForResponse = function(){
    return this.messageForResponse;
}

Error.prototype.getStatusCodeForResponse = function(){
    return this.statusCodeForResponse;
}