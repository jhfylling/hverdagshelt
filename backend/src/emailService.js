'use strict';
import './log'
const nodemailer = require('nodemailer');

let transporter = nodemailer.createTransport({
	host: 'smtp.gmail.com',
	port: 465,
	secure: true,
	auth: {
		user: 'noreply.hverdagshelt@gmail.com',
		pass: 'team1scrum2018'
	}
});

// Email variables
let subjectText;
let textBody;
let htmlBody;

/**
 * Sends an email
 */
async function sendEmail(userData, emailType, link) {
    let account = await nodemailer.createTestAccount(); // slette?
    switch (emailType) {
        case 'passwordChangeEmail':
            generateResetPasswordEmail(userData, link);
            break;
        case 'newCompanyUserEmail':
            generateNewCompanyUserEmail(userData, link);
            break;
        case 'issueCreationEmail':
            generateIssueCreatedEmail(userData);
            break;
        case 'issueFinshedEmail':
            generateIssueFinishedEmail(userData);
            break;
        case 'companyAssignedEmail':
            generateCompanyAssignedEmail(userData);
            break;
        case 'newUserEmail':
            generateNewUserEmail(userData);
            break;
        case 'issueDeletedEmail':
            generateIssueDeletedEmail(userData);
            break;
        default:
            break;
    }

    function generateResetPasswordEmail(userData, link){
        subjectText = 'Nytt passord';
        textBody = 'Hei! Du sendte en forespørsel om å endre passordet ditt. Trykk på linken under for å endre passordet ditt:\n' + link + '\nHvis dette ikke var deg kan du ignorere denne meldingen.';
        htmlBody = '<h3>Hei! </h3><br/><p> Du sendte en forespørsel om å endre passordet ditt. Trykk på linken under for å endre passordet ditt:</p><b>' + link + '</b><p>Hvis dette ikke var deg kan du ignorere denne meldingen.</p>';
    }

    function generateNewCompanyUserEmail(userData, link){
        subjectText = 'Ny bruker på HverdagsHelt.net';
        textBody = 'Hei! Din bedrift er registrert med en ny brukerkonto på hverdagshelt.net. Trykk på linken under for å velge passord:\n' + link;
        htmlBody = '<h3>Hei! </h3><br/><p> Din bedrift er registrert med en ny brukerkonto på hverdagshelt.net. Trykk på linken under for å velge passord:</p><b>' + link + '</b>';
    }

    function generateIssueCreatedEmail(userData) {
        subjectText = 'Sak opprettet';
        textBody = 'Takk for at du hjelper til! Du er med på å gjøre lokalmiljøet ditt bedre,' +
            'og vi synes at du på grunn av dette er en HverdagsHelt! Du vil motta en mail når saken har blitt ordnet.';
        htmlBody = '<b>Takk for at du hjelper til!</b>' +
            '<p>Du er med på å gjøre lokalmiljøet ditt bedre, og vi synes du på grunn av dette er en HverdagsHelt!</p><br/>'+
            '<p>Du vil motta en mail når saken har blitt ordnet.</p>';
    }

    function generateIssueFinishedEmail(userData) {
        subjectText = 'Sak avsluttet';
        textBody = 'Hei! En sak opprettet av deg har blitt avsluttet. Takk for innmeldingen!';
        htmlBody = '<b>Hei,</b><br/>' + '<p>En sak opprettet av deg har blitt avsluttet. Takk for innmeldingen!</p>';
    }

    function generateCompanyAssignedEmail(userData) {
        subjectText = 'Sak tildelt';
        textBody = 'Hei! En sak er blitt tildelt din bedrift. Denne kan sees på www.hverdagshelt.net/bedrift/saker !';
        htmlBody = '<b>Hei!</b><br/>' + '<p>En sak er blitt tildelt din bedrift. Denne kan sees på www.hverdagshelt.net/bedrift/saker!</p>';
    }

    function generateNewUserEmail(userData){
        subjectText = 'Ny bruker på HverdagsHelt.net';
        textBody = 'Hei! Det er blitt opprettet en bruker med din epost hos hverdagshelt.net!\n' +
            'Vi er glade for å ha deg med i gjengen, vær en hverdagshelt du også!\n\n\n\n' +
        'Hvis du ikke har registrert bruker, vennligst ta kontakt med hverdagshelt på hverdagshelt@hverdagshelt.net.';
        htmlBody = '<b>Hei!</b><br/>' + '<p>Det er blitt opprettet en bruker med din epost hos hverdagshelt.net!</p>'+
            '<p>Vi er glade for å ha deg med i gjengen, vær en hverdagshelt du også!</p><br/><br/><br/><br/>'+
            '<p>Hvis du ikke har registrert bruker, vennligst ta kontakt med hverdagshelt på hverdagshelt@hverdagshelt.net.</p>';
    }

    function generateIssueDeletedEmail(userData){
        subjectText = 'Sak du har opprettet er blitt slettet';
        textBody = 'Hei! En sak du har opprettet er blitt slettet. Ved spørsmål ta kontakt med hverdagshelt teamet!';
        htmlBody = '<b>Hei!</b><br/><p>En sak du har opprettet er blitt slettet. Ved spørsmål ta kontakt med hverdagshelt teamet!</p>';
    }

    let mailOptions = {
        from: '"HverdagsHelt-teamet" <noreply.hverdagshelt@gmail.com>',
        to: "<" + userData + ">",
        subject: subjectText,
        text: textBody,
        html: htmlBody
    };

    console.log(userData);
    console.log(mailOptions.to);
    let info = await transporter.sendMail(mailOptions);
	console.log('Message sent: %s', info.messageId);
	return true;
}

export default sendEmail;
