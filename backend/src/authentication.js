import Error from './error';
import './log'

var bcrypt = require("bcryptjs");
var jwt = require("jsonwebtoken");

module.exports = class Authentication {
    constructor(userDao, otherKeyPair) {
        // Dependency Injection
        this.userDao = userDao;
        this.keyPair = otherKeyPair;
        this.lifeTime = '4h';
        this.lifeTimePasswordReset = '1h';
        this.saltRounds = 10;
    }

    /**
     * Creates an access token for the specified user.
     */
    newToken(id, callback) {
        try{
            if(id){
                this.userDao.getUser(id, (status, rows) => {
                    if(status == 200){
                        var category_id = rows[0].category_id;
                        var email = rows[0].email;
                        var id = rows[0].id;
                        var municipality_id = rows[0].municipality_id;

                        var token = jwt.sign(
                            {
                                "id" : id,
                                "category_id" : category_id,
                                "email" : email,
                                "municipality_id" : municipality_id
                            },
                            this.keyPair.private,
                            {
                                algorithm: 'RS256',
                                expiresIn: this.lifeTime
                            });

                        callback(null, token);
                    } else{
                        var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
                        callback(newError, null);
                    }
                });
            } else{
                var newError = new Error("Id not specified when creating new user", 400 /* Bad request */, null, null);
                callback(newError, null);
            }
        } catch(exception){
            var newError = new Error("Caught exception when creating new token. Exception: " + exception, 500 /* Internal server error */, null, null);
            callback(newError, null);
        }
    }

    /**
     * Verifies and decodes the token specified.
     */
    verifyToken(token, callback) {
        try{
            jwt.verify(
                token,
                this.keyPair.public,
                {
                    algorithm: 'RS256'
                },
                (error, decoded) =>{
                    if(!error){
                        callback(null, decoded);
                    } else{
                        if(error.name == "TokenExpiredError"){
                            var newError = new Error(null, null, "Expired session, please log in again.", 400 /* Bad request */);
                            callback(newError, null);
                        } else if (error.name = "JsonWebTokenError"){
                            var newError = new Error("Got JsonWebTokenError when verifying token, error: " + error.message, 500 /* Internal server error */, null, null);
                            callback(newError, null);
                        } else if (error.name = "NotBeforeError"){
                            var newError = new Error("Got NotBeforeError when verifying token, error: " + error.message, 500, null, null);
                            callback(newError, null);
                        } else{
                            var newError = new Error("Got error when verifying token, error: " + error, 500 /* Internal server error */, null, null);
                            callback(newError, null);
                        }
                    }
                });
        } catch(exception){
            var newError = new Error("Caught exception when verifying token. Exception: " + exception, 500 /* Internal server error */, null, null);
            callback(newError, null);
        }
    }

    /**
     * Hashes the specified password.
     */
    hashPassword(password, callback){
        try{
            bcrypt.hash(password, this.saltRounds, (error, hash) => {
                if(!error)
                {
                    callback(null, hash);
                } else{
                    var newError = new Error("Got error while hashing password. Error: " + error, 500 /* Internal server error */, null, null);
                    callback(newError, null);
                }
            });
        } catch(exception){
            var newError = new Error("Caught exception when hashing password. Exception: " + exception, 500 /* Internal server error */, null, null);
            callback(newError, null);
        }
    }

    /**
     * Checks if the hash is the hashed counterpart of the specified password.
     */
    comparePasswords(password, hashedPassword, callback){
        try{
            bcrypt.compare(password, hashedPassword, (error, success) =>{
                if(!error){
                    callback(null, success);
                } else{
                    var newError = new Error("Got error when comparing passwords. Error: " + error, 500 /* Internal server error */, null, null);
                    callback(newError, null);
                }
            });
        } catch(exception){
            var newError = new Error("Caught exception when comparing passwords. Exception: " + exception, 500 /* Internal server error */, null, null);
            callback(newError, null);
        }
    }

    /**
     * Creates a new session for the specified user.
     * Contains the access token and information about the user.
     */
    newSession(id, callback){
        try{
            this.userDao.getUser(id, (status, rows) => {
                if(status == 200){
                    this.newToken(id, (error, token) => {
                        if(!error){
                            var session = { "x-access-token" : token, userInfo : rows[0] };
                            callback(null, session);
                        } else{
                            callback(error, null);
                        }
                    });
                } else{
                    var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
                    callback(newError, null);
                }
            });
        } catch(exception){
            var newError = new Error("Caught exception when creating new session. Exception: " + exception, 500 /* Internal server error */, null, null);
            callback(newError, null);
        }
    }

    /**
     * Creates a password reset token for the specified user.
     * The reason a normal access token is not used, is to 
     * make the token as small as possible.
     */
    newTokenPasswordReset(email, callback) {
        try{
            if(email){
                this.userDao.getId(email, (status, rows) => {
                    if(status == 200){
                        this.userDao.getUser(rows[0].id, (status, rows) => {
                            //Check if user exists
                            if(status == 200 && rows[0] !== null){
                                let token = jwt.sign(
                                    {
                                        "id" : rows[0].id
                                    },
                                    this.keyPair.private,
                                    {
                                        algorithm: 'RS256',
                                        expiresIn: this.lifeTimePasswordReset
                                    });

                                callback(null, token);
                            }else{
                                var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
                                callback(newError, null);
                            }
                        })
                    } else{
                        var newError = new Error("SQL query failed, SQL error: " + status, 500, null, null);
                        callback(newError, null);
                    }
                });
            } else{
                var newError = new Error("Email not specified!", 400, null, null);
                callback(newError, null);
            }
        } catch(exception){
            var newError = new Error("Caught exception when creating password reset token!", 500, null, null);
            callback(newError, null);
        }
    }
}